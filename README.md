# **Tiger Will Shake The World** #

Tiger is an enterprise application suite based on modern Java EE Technology Stack. It will consist of :

1. Human Resource Management app,
2. Sales & Customer Relationship Management app,
3. Manufacturing Resource Planning app,and
4. Accounting app.

Importantly, It has developed with flexibility in mind and prepared to face the Internet of Things Era.

Tiger can also be easily transformed in such a way to be a completely different app.