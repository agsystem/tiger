/* 
 * Copyright 2016 mbahbejo.
 * All rights reserved
 
 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo
 
 */

PrimeFaces.locales['in'] = {
    closeText: 'tutup',
    prevText: 'sebelum',
    nextText: 'sesudah',
    currentText: 'sekarang',
    monthNames: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun', 'Jul','Agu','Sep','Okt','Nov','Des'],
    dayNames: ['Minggu', 'Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu'],
    dayNamesShort: ['Min','Sen','Sel','Rab','Kam','Jum','Sab'],
    dayNamesMin: ['Mi','Sn','Sl','Rb','Km','Jm','Sb'],
    weekHeader: 'Hf',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Hanya Waktu',
    timeText: 'Waktu',
    hourText: 'Jam',
    minuteText: 'Menit',
    secondText: 'Detik',
    ampm: false,
    month: 'Bulan',
    week: 'Minggu',
    day: 'Hati',
    allDayText : 'Semua Hari'
};
