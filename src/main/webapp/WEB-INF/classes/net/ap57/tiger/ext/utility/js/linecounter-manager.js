/* 
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved
 
 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>
 
 */

var managerSocket;
var managerUri = 'ws://' + document.location.host
        + document.location.pathname.substr(0, document.location.pathname.indexOf("pages"))
        + 'websocket/linecounter/manager/';

var onManagerOpen = function (evt) {
    notifyLocking([
        {name: 'locked', value: 'true'}
    ]);
    lock = true;
};

var onManagerMessage = function (evt) {
    invalidateDisplay([
        {name: 'positionNumber', value: evt.data}
    ]);
};

var onManagerError = function (evt) {
    alert("ERROR");
};

var onManagerClose = function (evt) {
    notifyLocking([
        {name: 'locked', value: 'false'}
    ]);
    lock = false;
};

var lock = isLocked();

function onLocking(unit) {
    if (!lock) {
        managerSocket = new WebSocket(managerUri + unit);
        managerSocket.onopen = onManagerOpen;
        managerSocket.onmessage = onManagerMessage;
        managerSocket.onerror = onManagerError;
        managerSocket.onclose = onManagerClose;
    } else {
        managerSocket.close();
    }
}

function onManagerUnitChange() {
    if (managerSocket !== undefined)
        managerSocket.close();
}

