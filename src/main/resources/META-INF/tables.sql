-- ID Generator Table

CREATE TABLE IF NOT EXISTS id_gen (gen_name VARCHAR(80), gen_val BIGINT, PRIMARY KEY (gen_name));

--

-- Party

CREATE TABLE IF NOT EXISTS party (id BIGINT NOT NULL, PRIMARY KEY (id));

--


-- Person extends Party

CREATE TABLE IF NOT EXISTS person (id BIGINT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255), birthdate DATE, birthplace VARCHAR(255), gender INTEGER, idnumber VARCHAR(255), idtype INTEGER, maritalstatus INTEGER, mothername VARCHAR(255), religion INTEGER, photo OID, comment VARCHAR(255), PRIMARY KEY (id), FOREIGN KEY (id) REFERENCES party(id));

--


-- Organization extends Party

CREATE TABLE IF NOT EXISTS organization ( id BIGINT NOT NULL, name VARCHAR(255), description VARCHAR(255), PRIMARY KEY (id), FOREIGN KEY (id) REFERENCES party(id));

--


-- RoleType

CREATE TABLE IF NOT EXISTS roletype (id BIGINT NOT NULL, name VARCHAR(255), PRIMARY KEY (id));

--


-- PartyRoleType extends RoleType

CREATE TABLE IF NOT EXISTS partyroletype (id BIGINT NOT NULL,  PRIMARY KEY (id), FOREIGN KEY (id) REFERENCES roletype(id));

--


-- PartyRole

CREATE TABLE IF NOT EXISTS partyrole (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, thrudate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id) REFERENCES party(id), FOREIGN KEY (partyroletype_id) REFERENCES partyroletype(id));

--


-- OrganizationRole extends PartyRole

CREATE TABLE IF NOT EXISTS organizationrole (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES partyrole(party_id, partyroletype_id, fromdate));

--


-- PersonRole extends PartyRole

CREATE TABLE IF NOT EXISTS personrole (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES partyrole(party_id, partyroletype_id, fromdate));

--


-- PartyType

CREATE TABLE IF NOT EXISTS partytype (id BIGINT NOT NULL, description VARCHAR(255), PRIMARY KEY (id));

--


-- PartyClassification

CREATE TABLE IF NOT EXISTS partyclassification (party_id BIGINT NOT NULL, partytype_id BIGINT NOT NULL, fromdate DATE NOT NULL, thrudate DATE, PRIMARY KEY (party_id, partytype_id, fromdate), FOREIGN KEY (party_id) REFERENCES party(id), FOREIGN KEY (partytype_id) REFERENCES partytype(id));

--


-- PartyRelationshipType

CREATE TABLE IF NOT EXISTS partyrelationshiptype (id BIGINT NOT NULL, description VARCHAR(255), name VARCHAR(255) NOT NULL, establishertype_id BIGINT, participanttype_id BIGINT, PRIMARY KEY (id), FOREIGN KEY (establishertype_id) REFERENCES partyroletype(id), FOREIGN KEY (participanttype_id) REFERENCES partyroletype(id));

--


-- PartyRelationship

CREATE TABLE IF NOT EXISTS partyrelationship (establisher_party_id BIGINT NOT NULL, establisher_partyroletype_id BIGINT NOT NULL, establisher_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, participant_party_id BIGINT NOT NULL, participant_partyroletype_id BIGINT NOT NULL, participant_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, relationshiptype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, thrudate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (establisher_party_id, establisher_partyroletype_id, establisher_fromdate, participant_party_id, participant_partyroletype_id, participant_fromdate, relationshiptype_id, fromdate), FOREIGN KEY (establisher_party_id, establisher_partyroletype_id, establisher_fromdate) REFERENCES partyrole(party_id, partyroletype_id, fromdate), FOREIGN KEY (participant_party_id, participant_partyroletype_id, participant_fromdate) REFERENCES partyrole(party_id, partyroletype_id, fromdate), FOREIGN KEY (relationshiptype_id) REFERENCES partyrelationshiptype(id));

--


-- ContactMechanismType Entity.

CREATE TABLE IF NOT EXISTS contactmechanismtype (id BIGINT NOT NULL, name VARCHAR(255), PRIMARY KEY (id), UNIQUE (name));

--


-- ContactMechanism Entity.

CREATE TABLE IF NOT EXISTS contactmechanism (id BIGINT NOT NULL, contactvalue VARCHAR(255), contactmechanismtype_id BIGINT, PRIMARY KEY (id), FOREIGN KEY (contactmechanismtype_id) REFERENCES contactmechanismtype(id));

--


-- PartyContactMechanism Entity.

CREATE TABLE IF NOT EXISTS partycontactmechanism (party_id BIGINT NOT NULL, contactmechanism_id BIGINT NOT NULL, fromdate DATE NOT NULL, thrudate DATE, PRIMARY KEY (party_id, contactmechanism_id,fromdate), FOREIGN KEY (party_id) REFERENCES party(id), FOREIGN KEY (contactmechanism_id) REFERENCES contactmechanism(id));

--


-- PostalAddress Entity.

CREATE TABLE IF NOT EXISTS postaladdress (id BIGINT NOT NULL, address1 VARCHAR(255) NOT NULL, address2 VARCHAR(255), PRIMARY KEY (id));

--


-- PartyPostalAddress Entity.

CREATE TABLE IF NOT EXISTS partypostaladdress (party_id BIGINT NOT NULL, postaladdress_id BIGINT NOT NULL, fromdate DATE NOT NULL, PRIMARY KEY (party_id, postaladdress_id,fromdate), FOREIGN KEY (party_id) REFERENCES party(id), FOREIGN KEY (postaladdress_id) REFERENCES postaladdress(id));

--

-- UnitOfMeasure

CREATE TABLE IF NOT EXISTS unitofmeasure (id BIGINT NOT NULL, name VARCHAR(255) NOT NULL, abbreviation VARCHAR(255), PRIMARY KEY (id));

--

-- Config Entity

CREATE TABLE IF NOT EXISTS config (config_key VARCHAR(255), config_value VARCHAR(255), name VARCHAR(255), notes VARCHAR(255), PRIMARY KEY (config_key));

--

-- SecurityUser extends PersonRole

CREATE TABLE IF NOT EXISTS securityuser (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, username VARCHAR(255), password VARCHAR(255), PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES personrole(party_id, partyroletype_id, fromdate));

--

-- SecurityGroup extends OrganizationRole

CREATE TABLE IF NOT EXISTS securitygroup (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES organizationrole(party_id, partyroletype_id, fromdate));

--

-- SecurityMap

CREATE TABLE IF NOT EXISTS securitymap (user_party_id BIGINT, user_partyroletype_id BIGINT, user_fromdate TIMESTAMP WITHOUT TIME ZONE, group_party_id BIGINT, group_partyroletype_id BIGINT, group_fromdate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (user_party_id, user_partyroletype_id, user_fromdate, group_party_id, group_partyroletype_id, group_fromdate), FOREIGN KEY (user_party_id, user_partyroletype_id, user_fromdate) REFERENCES securityuser(party_id, partyroletype_id, fromdate), FOREIGN KEY (group_party_id, group_partyroletype_id, group_fromdate) REFERENCES securitygroup(party_id, partyroletype_id, fromdate));

--

--------------------------------------------------------------------------------
---                                                                          ---
---  HRD MODULE                                                              ---
---                                                                          ---
--------------------------------------------------------------------------------

-- Employee extends PersonRole

CREATE TABLE IF NOT EXISTS employee (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES personrole(party_id, partyroletype_id, fromdate));

--

-- InternalOrganization extends OrganizationRole

CREATE TABLE IF NOT EXISTS internalorganization (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, parent_party_id BIGINT, parent_partyroletype_id BIGINT, parent_fromdate TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES organizationrole(party_id, partyroletype_id, fromdate), FOREIGN KEY (parent_party_id, parent_partyroletype_id, parent_fromdate) REFERENCES internalorganization(party_id, partyroletype_id, fromdate));

--

-- WorkUnit extends OrganizationRole

CREATE TABLE IF NOT EXISTS workunit (party_id BIGINT NOT NULL, partyroletype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE, description VARCHAR(255), PRIMARY KEY (party_id, partyroletype_id, fromdate), FOREIGN KEY (party_id, partyroletype_id, fromdate) REFERENCES organizationrole(party_id, partyroletype_id, fromdate));

--

-- EmployeeAgreementType

CREATE TABLE IF NOT EXISTS employeeagreementtype (id BIGINT NOT NULL, PRIMARY KEY (id));

--

-- Employment extends PartyRelationship

CREATE TABLE IF NOT EXISTS employment (establisher_party_id BIGINT NOT NULL, establisher_partyroletype_id BIGINT NOT NULL, establisher_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, participant_party_id BIGINT NOT NULL, participant_partyroletype_id BIGINT NOT NULL, participant_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, relationshiptype_id BIGINT NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, thrudate TIMESTAMP WITHOUT TIME ZONE, agreementtype_id BIGINT, PRIMARY KEY (establisher_party_id, establisher_partyroletype_id, establisher_fromdate, participant_party_id, participant_partyroletype_id, participant_fromdate, relationshiptype_id, fromdate), FOREIGN KEY (establisher_party_id, establisher_partyroletype_id, establisher_fromdate, participant_party_id, participant_partyroletype_id, participant_fromdate, relationshiptype_id, fromdate) REFERENCES partyrelationship(establisher_party_id, establisher_partyroletype_id, establisher_fromdate, participant_party_id, participant_partyroletype_id, participant_fromdate, relationshiptype_id, fromdate), FOREIGN KEY (agreementtype_id) REFERENCES employeeagreementtype(id));

--

-- PositionType

CREATE TABLE IF NOT EXISTS positiontype (id BIGINT NOT NULL, title VARCHAR(255), description VARCHAR(255), PRIMARY KEY (id));

--

-- Position

CREATE TABLE IF NOT EXISTS position (id BIGINT NOT NULL, name VARCHAR(255) NOT NULL, hiringorganization_party_id BIGINT NOT NULL, hiringorganization_partyroletype_id BIGINT NOT NULL, hiringorganization_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, workunit_party_id BIGINT NOT NULL, workunit_partyroletype_id BIGINT NOT NULL, workunit_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, type_id int, status VARCHAR(255), estimatedfromdate DATE, estimatedthrudate DATE, actualfromdate DATE, actualthrudate DATE, fulltime BOOLEAN, permanent BOOLEAN, salary BOOLEAN, exempt BOOLEAN, description VARCHAR(255), required int default 1 NOT NULL, parent_id BIGINT, PRIMARY KEY (id), FOREIGN KEY (hiringorganization_party_id, hiringorganization_partyroletype_id, hiringorganization_fromdate) REFERENCES internalorganization (party_id, partyroletype_id, fromdate), FOREIGN KEY (workunit_party_id, workunit_partyroletype_id, workunit_fromdate) REFERENCES workunit (party_id, partyroletype_id, fromdate), FOREIGN KEY (type_id) REFERENCES positiontype (id), FOREIGN KEY (parent_id) REFERENCES position (id));

--

-- Position *<------->* WorkUnit

CREATE TABLE IF NOT EXISTS position_workunit (position_id BIGINT NOT NULL, workunit_party_id BIGINT NOT NULL, workunit_partyroletype_id BIGINT NOT NULL, workunit_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, PRIMARY KEY (position_id, workunit_party_id, workunit_partyroletype_id, workunit_fromdate), FOREIGN KEY (position_id) REFERENCES position (id), FOREIGN KEY (workunit_party_id, workunit_partyroletype_id, workunit_fromdate) REFERENCES workunit (party_id, partyroletype_id, fromdate));

--

-- PositionFulfillment

CREATE TABLE IF NOT EXISTS positionfulfillment (position_id BIGINT NOT NULL, employee_party_id BIGINT NOT NULL, employee_partyroletype_id BIGINT NOT NULL, employee_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, fromdate DATE NOT NULL, thrudate DATE, remarks VARCHAR(255), PRIMARY KEY (position_id, employee_party_id, employee_partyroletype_id, employee_fromdate, fromdate), FOREIGN KEY (position_id) REFERENCES position (id), FOREIGN KEY (employee_party_id, employee_partyroletype_id, employee_fromdate) REFERENCES employee(party_id, partyroletype_id, fromdate));

--

-- PositionWorkSchedule

CREATE TABLE IF NOT EXISTS positionworkschedule (position_id BIGINT NOT NULL, workdate DATE NOT NULL, starttime TIMESTAMP WITHOUT TIME ZONE, finishtime TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (position_id, workdate), FOREIGN KEY (position_id) REFERENCES position(id));

--

-- EmployeeWorkSchedule

CREATE TABLE IF NOT EXISTS employeeworkschedule (employee_party_id BIGINT NOT NULL, employee_partyroletype_id BIGINT NOT NULL, employee_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, workdate DATE NOT NULL, starttime TIMESTAMP WITHOUT TIME ZONE, finishtime TIMESTAMP WITHOUT TIME ZONE, PRIMARY KEY (employee_party_id, employee_partyroletype_id, employee_fromdate, workdate), FOREIGN KEY (employee_party_id, employee_partyroletype_id, employee_fromdate) REFERENCES employee(party_id, partyroletype_id, fromdate));

--

-- PositionReportingStructure

CREATE TABLE IF NOT EXISTS positionreportingstructure(manager_id BIGINT NOT NULL, managed_id BIGINT NOT NULL, fromdate DATE NOT NULL, thrudate DATE, PRIMARY KEY (manager_id, managed_id, fromdate), FOREIGN KEY (manager_id) REFERENCES position (id), FOREIGN KEY (managed_id) REFERENCES position (id));

--

--------------------------------------------------------------------------------
---                                                                          ---
---  MEKANIK MODULE                                                          ---
---                                                                          ---
--------------------------------------------------------------------------------

-- EquipmentType

CREATE TABLE IF NOT EXISTS equipmenttype (id BIGINT NOT NULL, name VARCHAR(255) NOT NULL, remarks VARCHAR(255), PRIMARY KEY (id));

--

-- EquipmentClass

CREATE TABLE IF NOT EXISTS equipmentclass (id BIGINT NOT NULL, type_id BIGINT, name VARCHAR(255), uom_id BIGINT, remarks VARCHAR(255), PRIMARY KEY (id), FOREIGN KEY (type_id) REFERENCES equipmenttype (id), FOREIGN KEY (uom_id) REFERENCES unitofmeasure(id));

--

-- Equipment

CREATE TABLE IF NOT EXISTS equipment (id VARCHAR(255) NOT NULL, equipmentclass_id BIGINT NOT NULL, serialnumber VARCHAR(255), lotnumber VARCHAR(255), productnumber VARCHAR(255), uom_id BIGINT, measuredvalue FLOAT8, remarks VARCHAR(255), PRIMARY KEY (id), FOREIGN KEY (equipmentclass_id) REFERENCES equipmentclass (id), FOREIGN KEY (uom_id) REFERENCES unitofmeasure(id));

--

-- EquipmentPlacementType

CREATE TABLE IF NOT EXISTS equipmentplacementtype (id BIGINT NOT NULL, name VARCHAR(255) NOT NULL UNIQUE, PRIMARY KEY (id));

--

-- EquipmentPlacement

CREATE TABLE IF NOT EXISTS equipmentplacement (equipment_id VARCHAR(255) NOT NULL, workunit_party_id BIGINT NOT NULL, workunit_partyroletype_id BIGINT NOT NULL, workunit_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, thrudate TIMESTAMP WITHOUT TIME ZONE, employee_party_id BIGINT NOT NULL, employee_partyroletype_id BIGINT NOT NULL, employee_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, placementtype_id BIGINT NOT NULL, equipmentcondition VARCHAR(255), reference_equipment_id VARCHAR(255) NOT NULL, reference_workunit_party_id BIGINT NOT NULL, reference_workunit_partyroletype_id BIGINT NOT NULL, reference_workunit_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, reference_fromdate TIMESTAMP WITHOUT TIME ZONE NOT NULL, uom_id BIGINT, measuredvalue FLOAT8, PRIMARY KEY (equipment_id, workunit_party_id, workunit_partyroletype_id, workunit_fromdate, fromdate), FOREIGN KEY (equipment_id) REFERENCES equipment (id), FOREIGN KEY (workunit_party_id, workunit_partyroletype_id, workunit_fromdate) REFERENCES workunit (party_id, partyroletype_id, fromdate), FOREIGN KEY (reference_equipment_id, reference_workunit_party_id, reference_workunit_partyroletype_id, reference_workunit_fromdate, reference_fromdate) REFERENCES equipmentplacement (equipment_id, workunit_party_id, workunit_partyroletype_id, workunit_fromdate, fromdate), FOREIGN KEY (uom_id) REFERENCES unitofmeasure (id));

--