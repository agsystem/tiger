/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.utility.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.base.entity.StringKeyedEntity;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.system.entity.SecurityUser;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(HelpDeskCaseId.class)
public class HelpDeskCase extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    private PartyRole srcWorkUnit;

    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date submitDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date solvedDate;

    @ManyToOne
    private PartyRole destWorkUnit;

    @Lob
    private String content;

    private boolean solved;

    @ManyToOne
    private SecurityUser creator;

    public WorkUnit getSrcWorkUnit() {
        return (WorkUnit) srcWorkUnit;
    }

    public void setSrcWorkUnit(WorkUnit srcWorkUnit) {
        this.srcWorkUnit = srcWorkUnit;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Date getSolvedDate() {
        return solvedDate;
    }

    public void setSolvedDate(Date solvedDate) {
        this.solvedDate = solvedDate;
    }

    public WorkUnit getDestWorkUnit() {
        return (WorkUnit) destWorkUnit;
    }

    public void setDestWorkUnit(WorkUnit destWorkUnit) {
        this.destWorkUnit = destWorkUnit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    public SecurityUser getCreator() {
        return creator;
    }

    public void setCreator(SecurityUser creator) {
        this.creator = creator;
    }

    @Override
    public void prePersist() {
        super.prePersist();
        submitDate = Calendar.getInstance().getTime();
    }

    public String getStrSubmitDate() {
        DateFormat sdf = new SimpleDateFormat("MMMMM d, yyyy HH:mm:ss");
        if (submitDate != null) {
            return sdf.format(submitDate);
        } else {
            return null;
        }
    }
}
