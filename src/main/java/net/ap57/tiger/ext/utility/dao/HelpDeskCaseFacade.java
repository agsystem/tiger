/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.utility.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseDeleteEvent;
import net.ap57.tiger.ext.utility.entity.HelpDeskCase;
import net.ap57.tiger.ext.utility.entity.HelpDeskCaseId;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseNewEvent;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseSolveEvent;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseUpdateEvent;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.system.dao.SecurityUserFacade;
import net.ap57.tiger.std.system.entity.SecurityUser;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class HelpDeskCaseFacade extends AbstractFacade<HelpDeskCase> {

    @Inject
    private EntityManager em;

    @Inject
    @HelpDeskCaseNewEvent
    private Event<HelpDeskCase> helpDeskCaseNewEvent;

    @Inject
    @HelpDeskCaseSolveEvent
    private Event<HelpDeskCase> helpDeskCaseSolveEvent;

    @Inject
    @HelpDeskCaseUpdateEvent
    private Event<HelpDeskCase> helpDeskCaseUpdateEvent;

    @Inject
    @HelpDeskCaseDeleteEvent
    private Event<HelpDeskCase> helpDeskCaseDeleteEvent;

    @Resource
    private SessionContext ctx;

    @Inject
    private SecurityUserFacade userFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HelpDeskCaseFacade() {
        super(HelpDeskCase.class);
    }

    @Override
    protected void addCriteria(Root<HelpDeskCase> root, CriteriaQuery<HelpDeskCase> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("submitDate")));
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<HelpDeskCase> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();
        switch (filterName) {
            case "unsolved":
                predicates.add(cb.isFalse(root.get("solved")));
                break;
            default:
                break;
        }
        return predicates;
    }

    @Override
    public HelpDeskCase createTransient(Map<String, Object> params) {
        HelpDeskCase entity = new HelpDeskCase();
        entity.setSolved(false);
        String username = ctx.getCallerPrincipal().getName();
        SecurityUser sUser = userFacade.findSingleByAttribute("username", username);
        entity.setCreator(sUser);
        return entity;
    }

    @Override
    public Object getId(HelpDeskCase entity) {
        HelpDeskCaseId id = new HelpDeskCaseId();
        WorkUnit src = (WorkUnit) entity.getSrcWorkUnit();
        if (src != null) {
            id.setSrcWorkUnit(src.getId());
        }
        id.setSubmitDate(entity.getSubmitDate());
        return id;
    }

    @Override
    public void create(HelpDeskCase entity) throws EJBException {
        entity.setSubmitDate(Calendar.getInstance().getTime());
        super.create(entity);
        getEntityManager().flush();
        helpDeskCaseNewEvent.fire(entity);
    }

    @Override
    protected void preProcessFilter(Map<String, Object> filters) {
        filters.put("unsolved", Boolean.TRUE);
    }

    public void caseSolved(HelpDeskCase aCase) {
        String creatorName = aCase.getCreator().getUsername();
        String editorName = ctx.getCallerPrincipal().getName();
        System.out.println("EDITOR = " + editorName);
        if (creatorName.equals(editorName) || editorName.equals("ANONYMOUS")) {
            aCase.setSolvedDate(Calendar.getInstance().getTime());
            aCase.setSolved(true);
            edit(aCase);
            getEntityManager().flush();
            helpDeskCaseSolveEvent.fire(aCase);
        } else {
            throw new EJBException("Illegal Operation");
        }
    }

    @Override
    public void remove(HelpDeskCase entity) throws EJBException {
        String creatorName = entity.getCreator().getUsername();
        String username = ctx.getCallerPrincipal().getName();
        if (creatorName.equals(username)) {
            super.remove(entity);
            getEntityManager().flush();
            helpDeskCaseDeleteEvent.fire(entity);
        } else {
            throw new EJBException("Illegal Operation");
        }
    }

    @Override
    public void edit(HelpDeskCase entity) throws EJBException {
        String creatorName = entity.getCreator().getUsername();
        String username = ctx.getCallerPrincipal().getName();
        if (creatorName.equals(username) || username.equals("ANONYMOUS")) {
            super.edit(entity);
            getEntityManager().flush();
            helpDeskCaseUpdateEvent.fire(entity);
        } else {
            throw new EJBException("Illegal Operation");
        }
    }

}
