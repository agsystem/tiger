/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.utility.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.utility.entity.WorkUnitDisplayer;
import net.ap57.tiger.ext.utility.entity.WorkUnitDisplayerId;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.iot.entity.Thing;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.std.iot.event.ThingRemoveStartEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class WorkUnitDisplayerFacade extends AbstractFacade<WorkUnitDisplayer> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WorkUnitDisplayerFacade() {
        super(WorkUnitDisplayer.class);
    }

    @Override
    public WorkUnitDisplayer createTransient(Map<String, Object> params) throws EJBException {
        WorkUnitDisplayer wud = new WorkUnitDisplayer();
        wud.setDisplayer((VersatileDisplayer) params.get("displayer"));
        return wud;
    }

    @Override
    public Object getId(WorkUnitDisplayer entity) {
        WorkUnitDisplayerId id = new WorkUnitDisplayerId();
        WorkUnit wu = entity.getWorkUnit();
        if (wu == null) {
            return null;
        }
        id.setWorkUnit(wu.getId());
//        id.setDisplayer(entity.getDisplayer().getId());
        return id;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<WorkUnitDisplayer> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        List<Predicate> predicates = new ArrayList<>();

        switch (filterName) {
            case "workUnit":
                WorkUnit wu = (WorkUnit) filterValue;
                predicates.add(cb.equal(root.get("workUnit").get("id"), wu.getId()));
                break;
            case "displayer":
                VersatileDisplayer vd = (VersatileDisplayer) filterValue;
                predicates.add(cb.equal(root.get("displayer").get("id"), vd.getId()));
                break;
        }

        return predicates;
    }

    @Override
    protected void addCriteria(Root<WorkUnitDisplayer> root, CriteriaQuery<WorkUnitDisplayer> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.asc(root.get("seq")));
    }

    public List<WorkUnitDisplayer> findUsages(VersatileDisplayer displayer) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkUnitDisplayer> cq = cb.createQuery(WorkUnitDisplayer.class);
        Root<WorkUnitDisplayer> root = cq.from(WorkUnitDisplayer.class);
        cq.select(root).where(cb.equal(root.get("displayer").get("id"), displayer.getId()));

        TypedQuery<WorkUnitDisplayer> q = getEntityManager().createQuery(cq);

        return q.getResultList();
    }

    public void onThingRemoveStartEvent(@Observes @ThingRemoveStartEvent Thing thing) {
        System.out.println("ON THING REMOVE START EVENT");
        if (thing instanceof VersatileDisplayer) {
            for (WorkUnitDisplayer wud : findUsages((VersatileDisplayer) thing)) {
                remove(wud);
            }
        }
    }

}
