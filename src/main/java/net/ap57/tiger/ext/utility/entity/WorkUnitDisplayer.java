/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.utility.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import net.ap57.tiger.base.entity.StringKeyedEntity;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
@IdClass(WorkUnitDisplayerId.class)
public class WorkUnitDisplayer extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    private WorkUnit workUnit;

    @Id
    @ManyToOne
    private VersatileDisplayer displayer;

    @Id
    @TableGenerator(name = "WorkUnitDisplayer", table = "KEYGEN")
    @GeneratedValue(generator = "WorkUnitDisplayer", strategy = GenerationType.TABLE)
    private Long seq;

    public WorkUnit getWorkUnit() {
        return workUnit;
    }

    public void setWorkUnit(WorkUnit workUnit) {
        this.workUnit = workUnit;
    }

    public VersatileDisplayer getDisplayer() {
        return displayer;
    }

    public void setDisplayer(VersatileDisplayer displayer) {
        this.displayer = displayer;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

}
