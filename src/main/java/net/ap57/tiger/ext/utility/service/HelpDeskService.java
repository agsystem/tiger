/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.utility.service;

import java.util.Date;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.ext.utility.dao.HelpDeskCaseFacade;
import net.ap57.tiger.ext.utility.entity.HelpDeskCase;
import net.ap57.tiger.ext.utility.entity.HelpDeskCaseId;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.system.dao.SecurityUserFacade;
import net.ap57.tiger.std.system.entity.SecurityUser;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class HelpDeskService {

    @Inject
    private EntityManager em;

    @Inject
    private HelpDeskCaseFacade helpDeskCaseFacade;

    @Inject
    private SecurityUserFacade userFacade;

    public HelpDeskCase submitCase(WorkUnit source, WorkUnit destination, String submiteeUsername, String content) {
        HelpDeskCase submittedCase = helpDeskCaseFacade.createTransient(null);
        submittedCase.setSrcWorkUnit(source);
        submittedCase.setDestWorkUnit(destination);
        submittedCase.setContent(content);
        SecurityUser submitee = userFacade.findSingleByAttribute("username", submiteeUsername);
        if (submitee != null) {
            submittedCase.setCreator(submitee);
            synchronized (this) {
                helpDeskCaseFacade.create(submittedCase);
                em.flush();
            }
            return submittedCase;
        } else {
            return null;
        }
    }

    public HelpDeskCase closingCase(WorkUnit source, Date submitDate) {

        HelpDeskCaseId helhDeskCaseId = new HelpDeskCaseId();
        helhDeskCaseId.setSrcWorkUnit(source.getId());
        helhDeskCaseId.setSubmitDate(submitDate);

        HelpDeskCase helpDeskCase = helpDeskCaseFacade.find(helhDeskCaseId);
        if (helpDeskCase != null) {
            helpDeskCaseFacade.caseSolved(helpDeskCase);
            em.flush();
            return helpDeskCase;
        } else {
            return null;
        }
    }

}
