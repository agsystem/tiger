/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.utility.ui.bean;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.SessionContext;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.ext.utility.dao.AllHelpDeskCaseFacade;
import net.ap57.tiger.ext.utility.dao.HelpDeskCaseFacade;
import net.ap57.tiger.ext.utility.entity.HelpDeskCase;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "helpDeskPage")
@RequestScoped
@PageView(rel = "HelpDesk")
public class HelpDeskPage extends Page {

    @Inject
    private HelpDeskCaseFacade helpDeskCaseOp;

    @View
    private DataTable helpDeskCaseTable;

    @Inject
    private AllHelpDeskCaseFacade allHelpDeskCaseOp;

    @View
    private DataTable alldataTable;

    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private DataList workUnitList;

    @Inject
    private SessionContext ctx;

    @Override
    protected void initLayout() {
        setEditor("Helpdesk Case", "100%");
    }

    @Override
    protected void initComponents() {
        
        helpDeskCaseTable = new HelpDeskCaseTable() {

            @Override
            protected HelpDeskCaseFacade getOp() {
                return helpDeskCaseOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                filters.put("unsolved", Boolean.TRUE);
                return filters;
            }

        };

        alldataTable = new PFLazyDataTable<AllHelpDeskCaseFacade, HelpDeskCase>() {

            @Override
            protected AllHelpDeskCaseFacade getOp() {
                return allHelpDeskCaseOp;
            }

        };

        workUnitList = new DataList<WorkUnitFacade, WorkUnit>() {

            @Override
            public WorkUnitFacade getOp() {
                return workUnitOp;
            }

            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("name", value);
            }

        };
    }

}
