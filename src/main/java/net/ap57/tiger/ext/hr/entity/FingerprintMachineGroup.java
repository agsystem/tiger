/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
public class FingerprintMachineGroup extends StringKeyedEntity implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  @Id
  @TableGenerator(name = "FingerprintMachineGroup", table = "KEYGEN")
  @GeneratedValue(generator = "FingerprintMachineGroup", strategy = GenerationType.TABLE)
  private Long id;
  
  @Column(unique=true)
  private String name;
  
  private String remarks;
  
  @OneToMany(mappedBy = "machineGroup", cascade=CascadeType.ALL)
  private List<FingerprintMachine> machines;
  
  @OneToOne(mappedBy = "id", cascade=CascadeType.ALL)
  private FingerprintMachineGroupNum fingerprintMachineGroupNum;
  
  @OneToMany(mappedBy = "fingerprintMachineGroup", fetch=FetchType.EAGER)
  private List<FingerprintMachineGroupMap> fingerprintMachineGroupMaps;  

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public List<FingerprintMachine> getMachines() {
    return machines;
  }

  public void setMachines(List<FingerprintMachine> machines) {
    this.machines = machines;
  }

//  public List<WorkUnit> getWorkUnits() {
//    return workUnits;
//  }
//
//  public void setWorkUnits(List<WorkUnit> workUnits) {
//    this.workUnits = workUnits;
//  }

  public FingerprintMachineGroupNum getFingerprintMachineGroupNum() {
    return fingerprintMachineGroupNum;
  }

  public void setFingerprintMachineGroupNum(FingerprintMachineGroupNum fingerprintMachineGroupNum) {
    this.fingerprintMachineGroupNum = fingerprintMachineGroupNum;
  }

  public List<FingerprintMachineGroupMap> getFingerprintMachineGroupMaps() {
    return fingerprintMachineGroupMaps;
  }

  public void setFingerprintMachineGroupMaps(List<FingerprintMachineGroupMap> fingerprintMachineGroupMaps) {
    this.fingerprintMachineGroupMaps = fingerprintMachineGroupMaps;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof FingerprintMachineGroup)) {
      return false;
    }
    FingerprintMachineGroup other = (FingerprintMachineGroup) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return name;
  }
  
}
