/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.ext.hr.ui.bean;

import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.ext.hr.dao.AttendanceLogFacade;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineGroupFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroup;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author mbahbejo
 */
@Named(value = "attendanceLogPage")
@RequestScoped
@PageView(rel = "attendanceLog")
public class AttendanceLogPage extends Page {

    @Inject
    private FingerprintMachineGroupFacade machineGroupOp;
    
    @View
    private FingerprintMachineGroupTable machineGroupTable;

    @Inject
    private AttendanceLogFacade logFacade;

    @View
    private AttendanceLogTable logTable;
    
    @Override
    protected void initLayout() {
        setExplorer("Machine Group", "25%");
        setEditor("Log", "75%");
    }
    
    @Override
    protected void initComponents() {
        machineGroupTable = new FingerprintMachineGroupTable() {

            @Override
            protected FingerprintMachineGroupFacade getOp() {
                return machineGroupOp;
            }

        };

        logTable = new AttendanceLogTable() {

            @Override
            protected AttendanceLogFacade getOp() {
                return logFacade;
            }

            @Override
            protected void addStaticFilter(Map<String, Object> filters) {
                FingerprintMachineGroup group = machineGroupTable.getSelection();
                if (group != null) {
                    filters.put("FingerprintMachineGroup", group);
                }
            }

        };
    }

}
