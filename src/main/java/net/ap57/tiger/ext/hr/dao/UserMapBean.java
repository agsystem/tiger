/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.ext.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUser;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserMap;
import net.ap57.tiger.ext.hr.entity.UserMap;
import net.ap57.tiger.std.hr.dao.EmployeeFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionWorkUnit;

/**
 *
 * @author mbahbejo
 *
 */
@Stateless
public class UserMapBean extends AbstractFacade<UserMap> {

    @Inject
    private EntityManager em;

    @Inject
    private EmployeeFacade employeeFacade;

    @Inject
    private FingerprintMachineUserFacade machineUserFacade;

    @Inject
    private FingerprintMachineUserMapFacade machineUserMapFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserMapBean() {
        super(UserMap.class);
    }

    @Override
    public UserMap createTransient(Map<String, Object> params) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getId(UserMap entity) {
        return entity.getEmployee().getId().toString();
    }

    @Override
    public UserMap getObj(String strKey) {
//        UUID id = UUID.fromString(strKey);
        FingerprintMachineUserMap mUserMap = machineUserMapFacade.find(strKey);
        Employee e = null;
        if (mUserMap == null) {
            e = employeeFacade.find(strKey);
        } else {
            e = mUserMap.getEmployee();
        }
        UserMap userMap = new UserMap(e, null, mUserMap);
        return userMap;
    }

    @Override
    protected CriteriaQuery constructFindQuery(Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<UserMap> cq = cb.createQuery(UserMap.class);

        Root<Employee> e1 = cq.from(Employee.class);
        Join<Employee, FingerprintMachineUserMap> machineUserMap
                = e1.join("machineUserMap", JoinType.LEFT);

        Subquery<UUID> sq1 = cq.subquery(UUID.class);
        Root<Employee> e2 = sq1.from(Employee.class);
        Join<Employee, PositionFulfillment> pf1 = e2.join("positionFulfillments");

        Subquery<Date> sq2 = sq1.subquery(Date.class);
        Root<PositionFulfillment> pf2 = sq2.from(PositionFulfillment.class);
        sq2.select(cb.greatest(pf2.<Date>get("fromDate"))).where(new Predicate[]{
            cb.equal(pf2.get("employee").get("id"), e2.get("id")),
            cb.lessThanOrEqualTo(pf2.<Date>get("fromDate"), Calendar.getInstance().getTime())
        });

        Subquery<UUID> sq3 = sq1.subquery(UUID.class);
        Root<Position> p = sq2.from(Position.class);
        Join<Position, PositionWorkUnit> pwu = p.join("workUnits");
        sq3.select(p.get("id")).where(cb.equal(pwu.get("workUnit").get("name"), filters.get("WorkUnit")));

        sq1.select(e2.get("id")).where(new Predicate[]{
            cb.equal(pf1.get("fromDate"), sq2),
            pf1.get("position").get("id").in(sq3)
        });

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(e1.get("id").in(sq1));
        for (String filterName : filters.keySet()) {
            switch (filterName) {
                case "employee.person.name":
                    Expression<String> literal = cb.upper(cb.literal("%" + filters.get(filterName) + "%"));
                    predicates.add(cb.or(cb.like(cb.upper(e1.get("person").<String>get("firstName")), literal),
                            cb.like(cb.upper(e1.get("person").<String>get("lastName")), literal)));
                    break;
                default:
                    break;
            }
        }

        cq.multiselect(e1, pf1, machineUserMap).where(predicates.toArray(new Predicate[predicates.size()]));

        return cq;
    }

    @Override
    protected CriteriaQuery constructCountQuery(Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<Employee> e1 = cq.from(Employee.class);
        Join<Employee, FingerprintMachineUserMap> machineUserMap
                = e1.join("machineUserMap", JoinType.LEFT);

        Subquery<UUID> sq1 = cq.subquery(UUID.class);
        Root<Employee> e2 = sq1.from(Employee.class);
        Join<Employee, PositionFulfillment> pf1 = e2.join("positionFulfillments");

        Subquery<Date> sq2 = sq1.subquery(Date.class);
        Root<PositionFulfillment> pf2 = sq2.from(PositionFulfillment.class);
        sq2.select(cb.greatest(pf2.<Date>get("fromDate"))).where(new Predicate[]{
            cb.equal(pf2.get("employee").get("id"), e2.get("id")),
            cb.lessThanOrEqualTo(pf2.<Date>get("fromDate"), Calendar.getInstance().getTime())
        });

        Subquery<UUID> sq3 = sq1.subquery(UUID.class);
        Root<Position> p = sq2.from(Position.class);
        Join<Position, PositionWorkUnit> pwu = p.join("workUnits");
        sq3.select(p.get("id")).where(cb.equal(pwu.get("workUnit").get("name"), filters.get("WorkUnit")));

        sq1.select(e2.get("id")).where(new Predicate[]{
            cb.equal(pf1.get("fromDate"), sq2),
            pf1.get("position").get("id").in(sq3)
        });

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(e1.get("id").in(sq1));
        for (String filterName : filters.keySet()) {
            switch (filterName) {
                case "employee.person.name":
                    Expression<String> literal = cb.upper(cb.literal("%" + filters.get(filterName) + "%"));
                    predicates.add(cb.or(cb.like(cb.upper(e1.get("person").<String>get("firstName")), literal),
                            cb.like(cb.upper(e1.get("person").<String>get("lastName")), literal)));
                    break;
                default:
                    break;
            }
        }

        cq.select(cb.count(e1)).where(e1.get("id").in(sq1));

        return cq;
    }

    @Override
    public void remove(UserMap entity) {

    }

    @Override
    public void edit(UserMap entity) {
        machineUserMapFacade.edit(entity.getUserMap());
    }

    @Override
    public void create(UserMap entity) throws EJBException {
        Employee employee = entity.getEmployee();
        FingerprintMachineUserMap machineUserMap = entity.getUserMap();        
        machineUserMap.setEmployee(employee);
        employee.setMachineUserMap(machineUserMap);
        employeeFacade.edit(employee);
    }
    
    public void unlink(UserMap userMap, FingerprintMachineUser machineUser) {
        machineUser.setMachineUserMap(null);
        FingerprintMachineUserMap machineUserMap = userMap.getUserMap();
        machineUserMap.getMachineUsers().remove(machineUser);
        machineUserMapFacade.edit(machineUserMap);
        machineUserFacade.edit(machineUser);
    }

}
