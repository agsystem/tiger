/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.ext.hr.entity;

import java.io.Serializable;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;

/**
 *
 * @author mbahbejo
 */
public class UserMap implements Serializable {

    private Employee employee;
    
    private PositionFulfillment positionFulfillment;

    private FingerprintMachineUserMap userMap;

    public UserMap(Employee employee, PositionFulfillment positionFulfillment, FingerprintMachineUserMap userMap) {
        this.employee = employee;
        this.userMap = userMap;
        this.positionFulfillment = positionFulfillment;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public PositionFulfillment getPositionFulfillment() {
        return positionFulfillment;
    }

    public void setPositionFulfillment(PositionFulfillment positionFulfillment) {
        this.positionFulfillment = positionFulfillment;
    }

    public FingerprintMachineUserMap getUserMap() {
        return userMap;
    }

    public void setUserMap(FingerprintMachineUserMap userMap) {
        this.userMap = userMap;
    }

}
