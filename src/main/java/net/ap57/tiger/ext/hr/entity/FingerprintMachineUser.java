/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(FingerprintMachineUserId.class)
public class FingerprintMachineUser extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int pin2;

    @Id
    @ManyToOne
    private FingerprintMachineGroup machineGroup;

    private String name;

    private String password;

    @Column(name = "attgroup")
    private int group;

    private int privilege;

    private int card;

    private int tz1;

    private int tz2;

    private int tz3;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Fingerprint> fingerprints;

    @ManyToOne
    private FingerprintMachineUserMap machineUserMap;

//  @OneToOne(mappedBy = "fingerprintMachineUser", cascade=CascadeType.REMOVE)
//  private FingerprintMachineUserMap fingerprintMachineUserMap;
    @OneToMany(mappedBy = "machineUser", cascade = CascadeType.REMOVE)
    private List<AttendanceLog> attendanceLogs;

    public FingerprintMachineGroup getMachineGroup() {
        return machineGroup;
    }

    public void setMachineGroup(FingerprintMachineGroup fingerprintMachineGroup) {
        this.machineGroup = fingerprintMachineGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public int getPin2() {
        return pin2;
    }

    public void setPin2(int pin2) {
        this.pin2 = pin2;
    }

    public int getTz1() {
        return tz1;
    }

    public void setTz1(int tz1) {
        this.tz1 = tz1;
    }

    public int getTz2() {
        return tz2;
    }

    public void setTz2(int tz2) {
        this.tz2 = tz2;
    }

    public int getTz3() {
        return tz3;
    }

    public void setTz3(int tz3) {
        this.tz3 = tz3;
    }

    public List<AttendanceLog> getAttendanceLogs() {
        return attendanceLogs;
    }

    public void setAttendanceLogs(List<AttendanceLog> attendanceLogs) {
        this.attendanceLogs = attendanceLogs;
    }

    public List<Fingerprint> getFingerprints() {
        return fingerprints;
    }

    public void setFingerprints(List<Fingerprint> fingerprints) {
        this.fingerprints = fingerprints;
    }

    public FingerprintMachineUserMap getMachineUserMap() {
        return machineUserMap;
    }

    public void setMachineUserMap(FingerprintMachineUserMap machineUserMap) {
        this.machineUserMap = machineUserMap;
    }

//  public FingerprintMachineUserMap getFingerprintMachineUserMap() {
//    return fingerprintMachineUserMap;
//  }
//
//  public void setFingerprintMachineUserMap(FingerprintMachineUserMap fingerprintMachineUserMap) {
//    this.fingerprintMachineUserMap = fingerprintMachineUserMap;
//  }
    @Override
    public String toString() {
        return pin2 + ":" + machineGroup.getName();
    }

}
