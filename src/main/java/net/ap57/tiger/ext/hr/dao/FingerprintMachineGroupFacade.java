/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.dao;

import java.util.ArrayList;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachine;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroup;
import net.ap57.tiger.ext.hr.service.FingerprintMachineService;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class FingerprintMachineGroupFacade extends AbstractFacade<FingerprintMachineGroup> {

    @Inject
    private EntityManager em;

    @Inject
    private FingerprintMachineService machineService;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FingerprintMachineGroupFacade() {
        super(FingerprintMachineGroup.class);
    }

    @Override
    public FingerprintMachineGroup createTransient(Map<String, Object> params) {
        FingerprintMachineGroup group = new FingerprintMachineGroup();
        group.setMachines(new ArrayList<>());

        return group;
    }

    @Override
    public Object getId(FingerprintMachineGroup entity) {
        return entity.getId();
    }

    public FingerprintMachine getDefaultMachineOfGroup(FingerprintMachineGroup machineGroup) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<FingerprintMachine> cq = cb.createQuery(FingerprintMachine.class);
        Root<FingerprintMachine> root = cq.from(FingerprintMachine.class);
        cq.select(root).where(new Predicate[]{cb.equal(root.get("machineGroup"), machineGroup), cb.equal(root.get("masterInGroup"), true)});

        TypedQuery<FingerprintMachine> q = getEntityManager().createQuery(cq);

        try {
            return q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }

    }

    public void downloadUsers(FingerprintMachineGroup machineGroup) {
//        FingerprintMachine defaultMachine = getDefaultMachineOfGroup(machineGroup);

        for (FingerprintMachine machine : machineGroup.getMachines()) {
            if (machine.isEnabled()) {
                machineService.downloadUserInfo(machine, null);
            }
        }
        
    }

    public void downloadLog(FingerprintMachineGroup machineGroup) {

        for (FingerprintMachine machine : machineGroup.getMachines()) {
            if (machine.isEnabled()) {
                machineService.processAttLog(machine, null);
            }
        }
    }

}
