/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class FingerprintMachineGroupMapId implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private Long fingerprintMachineGroup;
  
  private String workUnit;

  public Long getFingerprintMachineGroup() {
    return fingerprintMachineGroup;
  }

  public void setFingerprintMachineGroup(Long fingerprintMachineGroup) {
    this.fingerprintMachineGroup = fingerprintMachineGroup;
  }

  public String getWorkUnit() {
    return workUnit;
  }

  public void setWorkUnit(String workUnit) {
    this.workUnit = workUnit;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 29 * hash + Objects.hashCode(this.fingerprintMachineGroup);
    hash = 29 * hash + Objects.hashCode(this.workUnit);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final FingerprintMachineGroupMapId other = (FingerprintMachineGroupMapId) obj;
    if (!Objects.equals(this.fingerprintMachineGroup, other.fingerprintMachineGroup)) {
      return false;
    }
    if (!Objects.equals(this.workUnit, other.workUnit)) {
      return false;
    }
    return true;
  }
  
}
