/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
public class FingerprintMachine extends StringKeyedEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @TableGenerator(name = "FingerprintMachine", table = "KEYGEN")
  @GeneratedValue(generator = "FingerprintMachine", strategy = GenerationType.TABLE)
  private Long id;
  
  @Column(unique = true)
  private String ipAddr;
  
  @Column(unique=true)
  private String name;
  
  private boolean enabled;
  
  private String remarks;
  
  @ManyToOne
  private FingerprintMachineGroup machineGroup;
  
  private boolean masterInGroup;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIpAddr() {
    return ipAddr;
  }

  public void setIpAddr(String ipAddr) {
    this.ipAddr = ipAddr;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public FingerprintMachineGroup getMachineGroup() {
    return machineGroup;
  }

  public void setMachineGroup(FingerprintMachineGroup machineGroup) {
    this.machineGroup = machineGroup;
  }

  public boolean isMasterInGroup() {
    return masterInGroup;
  }

  public void setMasterInGroup(boolean masterInGroup) {
    this.masterInGroup = masterInGroup;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 23 * hash + Objects.hashCode(this.ipAddr);
    hash = 23 * hash + Objects.hashCode(this.name);
    hash = 23 * hash + (this.enabled ? 1 : 0);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final FingerprintMachine other = (FingerprintMachine) obj;
    if (!Objects.equals(this.ipAddr, other.ipAddr)) {
      return false;
    }
    if (!Objects.equals(this.name, other.name)) {
      return false;
    }
    if (this.enabled != other.enabled) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return name;
  }
  
}
