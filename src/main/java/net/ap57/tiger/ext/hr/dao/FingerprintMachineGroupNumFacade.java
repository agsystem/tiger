/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.dao;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroup;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroupNum;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
public class FingerprintMachineGroupNumFacade extends AbstractFacade<FingerprintMachineGroupNum> {

  @PersistenceContext(unitName = "Tiger-PU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public FingerprintMachineGroupNumFacade() {
    super(FingerprintMachineGroupNum.class);
  }

  @Override
  public FingerprintMachineGroupNum createTransient(Map<String, Object> params) {
    FingerprintMachineGroupNum groupNum = new FingerprintMachineGroupNum();
    groupNum.setId((FingerprintMachineGroup) params.get("group"));
    groupNum.setSeed(0);
    return groupNum;
  }

  @Override
  public Object getId(FingerprintMachineGroupNum entity) {
    return entity.getId();
  }

  @Lock(LockType.WRITE)
  public int getNum(FingerprintMachineGroup group) {

    TypedQuery<FingerprintMachineGroupNum> query = getEntityManager()
        .createNamedQuery("FingerprintMachineGroupNum.getLatest", FingerprintMachineGroupNum.class)
        .setParameter("group", group);

    FingerprintMachineGroupNum num = null;
    try {
      num = query.getSingleResult();
    } catch (NoResultException ex) {
      num = new FingerprintMachineGroupNum();
      num.setId(group);
      num.setSeed(1);
      try {
        create(num);
        return 1;
      } catch (Exception ex1) {
        Logger.getLogger(FingerprintMachineGroupNumFacade.class.getName()).log(Level.SEVERE, null, ex1);
      }
    }
    
    num.setSeed(num.getSeed() + 1);

    edit(num);

    return num.getSeed();
  }

}
