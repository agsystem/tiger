/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.ui.bean;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachine;
import net.ap57.tiger.ext.hr.service.FingerprintMachineService;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class FingerprintMachineDataTable extends PFLazyDataTable<FingerprintMachineFacade, FingerprintMachine> {

  protected abstract FingerprintMachineService getMachineService();

  public void handleEnablement(AjaxBehaviorEvent e) {
    Map<String, Object> attrs = e.getComponent().getAttributes();
    FingerprintMachine m = (FingerprintMachine) attrs.get("item");
    getOp().edit(m);
  }

  public void handleMastered(AjaxBehaviorEvent e) {
    Map<String, Object> attrs = e.getComponent().getAttributes();
    FingerprintMachine m = (FingerprintMachine) attrs.get("item");
    List<FingerprintMachine> machines = getOp().getMachineWithSameGroup(m);
    for (FingerprintMachine machine : machines) {
      if (machine.getId().equals(m.getId())) {
        machine.setMasterInGroup(true);
        getOp().edit(machine);
      } else {
        if (machine.isMasterInGroup()) {
          machine.setMasterInGroup(false);
          getOp().edit(machine);
        }
      }
    }

  }

//  public void downloadAttLog(ActionEvent evt) {
//    getMachineService().processAttLog(getSelection(), null);
//  }

  public void backupUsers(ActionEvent evt) {
    getMachineService().downloadUserInfo(getSelection(), null);
//    getMachineService().downloadFingerprintTemplate(getSelection(), null);
  }

  public void uploadUsers(ActionEvent evt) {
    getMachineService().uploadUsers(getSelection());
  }

  public void syncDate(ActionEvent evt) {
    getMachineService().setDate(getSelection(), Calendar.getInstance());
  }

  public void restart(ActionEvent evt) {
    getMachineService().restart(getSelection());
  }

  public void clearUser(ActionEvent evt) {
    getMachineService().clearTransaction(getSelection());
    getMachineService().clearFingerprint(getSelection());
    getMachineService().clearUser(getSelection());
  }

}
