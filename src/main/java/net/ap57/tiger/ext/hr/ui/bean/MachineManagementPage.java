/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.ui.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineFacade;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineGroupFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroup;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroupMap;
import net.ap57.tiger.ext.hr.service.FingerprintMachineService;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.view.WorkUnitPickList;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value="machineManagementPage")
@RequestScoped
@PageView(rel = "machineManagement")
public class MachineManagementPage extends Page {

  @Inject
  private FingerprintMachineService machineService;

  @Inject
  private FingerprintMachineGroupFacade fingerprintMachineGroupOp;

  @View
  private DataTable machineGroupTable;

  @Inject
  private FingerprintMachineFacade fingerprintMachineOp;

  @View
  private FingerprintMachineDataTable fingerprintMachineTable;

  @Inject
  private WorkUnitFacade workUnitOp;

  @View
  private WorkUnitPickList workUnitPickList;

  @Override
  protected void initComponents() {

    machineGroupTable = new FingerprintMachineGroupTable() {

      @Override
      protected FingerprintMachineGroupFacade getOp() {
        return fingerprintMachineGroupOp;
      }

    };

    fingerprintMachineTable = new FingerprintMachineDataTable() {

      @Override
      protected FingerprintMachineFacade getOp() {
        return fingerprintMachineOp;
      }

      @Override
      protected Map<String, Object> getNewEntityParams() {
        Map<String, Object> params = new HashMap<>();
        params.put("group", machineGroupTable.getSelection());
        return params;
      }

      @Override
      protected void addStaticFilter(Map<String, Object> filters) {
        FingerprintMachineGroup group = (FingerprintMachineGroup) machineGroupTable.getSelection();
        if (group != null) {
          filters.put("group", machineGroupTable.getSelection());
        }
      }

      @Override
      protected FingerprintMachineService getMachineService() {
        return machineService;
      }

    };

    workUnitPickList = new WorkUnitPickList() {

      @Override
      protected WorkUnitFacade getOp() {
        return workUnitOp;
      }

      @Override
      public List<WorkUnit> getTarget() {

        FingerprintMachineGroup pos = (FingerprintMachineGroup) machineGroupTable.getSelection();

        List<WorkUnit> coverages = new ArrayList<WorkUnit>();

        for (FingerprintMachineGroupMap map : pos.getFingerprintMachineGroupMaps()) {
          coverages.add(map.getWorkUnit());
        }

        return coverages;

      }

    };
  }

  @Override
  protected void initLayout() {
    setExplorer("Machine Group", "25%");
    setEditor("Machine", "75%");
  }

}
