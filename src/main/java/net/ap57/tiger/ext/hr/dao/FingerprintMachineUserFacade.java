/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.ext.hr.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUser;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserId;
import net.ap57.tiger.ext.hr.entity.UserMap;
import net.ap57.tiger.ext.hr.service.FingerprintMachineUserDownloadEvent;

/**
 *
 * @author mbahbejo
 */
@Stateless
public class FingerprintMachineUserFacade extends AbstractFacade<FingerprintMachineUser> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FingerprintMachineUserFacade() {
        super(FingerprintMachineUser.class);
    }

    @Override
    public FingerprintMachineUser createTransient(Map<String, Object> params) {
        FingerprintMachineUser user = new FingerprintMachineUser();
        return user;
    }

    @Override
    protected void addCriteria(Root<FingerprintMachineUser> root, CriteriaQuery<FingerprintMachineUser> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.asc(root.get("pin2")));
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<FingerprintMachineUser> root) {
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        switch (filterName) {
            case "name":
                Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                predicates.add(cb.or(cb.like(cb.upper(root.get("person").<String>get("firstName")), literal),
                        cb.like(cb.upper(root.get("person").<String>get("lastName")), literal)));
                break;
            case "UserMap":
                UserMap userMap = (UserMap) filterValue;
                if (userMap != null) {
                    predicates.add(cb.equal(root.get("machineUserMap").get("employee").get("id"), userMap.getEmployee().getId()));
                }
                break;
            case "orphan":
                Boolean orphan = (Boolean) filterValue;
                if (orphan.booleanValue() == true) {
                    predicates.add(cb.isNull(root.get("machineUserMap")));
                }
                break;
            default:
                break;
        }

        return predicates;
    }

    @Override
    public Object getId(FingerprintMachineUser entity) {
        FingerprintMachineUserId id = new FingerprintMachineUserId();
        id.setPin2(entity.getPin2());
        id.setMachineGroup(entity.getMachineGroup().getId());
        return id;
    }

    public void saveUser(@Observes @FingerprintMachineUserDownloadEvent FingerprintMachineUser machineUser) throws Exception {
        System.out.println("SAVEUSER");
        FingerprintMachineUser user = find(getId(machineUser));
        if (user == null) {
            System.out.println("SAVE NYU = " + machineUser.getName());
            create(machineUser);
        }
    }

    @Override
    public Long countAll(Map<String, Object> filters) {
        if (filters.containsKey("UserMap") && filters.get("UserMap") == null) {
            return 0L;
        } else {
            return super.countAll(filters);
        }
    }

    @Override
    public List<FingerprintMachineUser> findAll(int startPosition, int maxResult, Map<String, Object> filters) {
        if (filters.containsKey("UserMap") && filters.get("UserMap") == null) {
            return new ArrayList<>();
        }
        return super.findAll(startPosition, maxResult, filters);
    }

}
