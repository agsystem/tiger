/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.hr.entity.FingerprintMachine;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineGroup;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class FingerprintMachineFacade extends AbstractFacade<FingerprintMachine> {

  @Inject
  private EntityManager em;

  @Inject
  private FingerprintMachineGroupFacade groupFacade;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public FingerprintMachineFacade() {
    super(FingerprintMachine.class);
  }

  @Override
  public FingerprintMachine createTransient(Map<String, Object> params) {
    FingerprintMachine machine = new FingerprintMachine();
    FingerprintMachineGroup group = (FingerprintMachineGroup) params.get("group");
    machine.setMachineGroup(group);
    group.getMachines().add(machine);
    machine.setEnabled(true);
    machine.setMasterInGroup(false);

    return machine;
  }

  @Override
  public Object getId(FingerprintMachine entity) {
    return entity.getId();
  }

  @Override
  protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<FingerprintMachine> root) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

    List<Predicate> predicates = new ArrayList<>();

    switch (filterName) {
      case "group":
        FingerprintMachineGroup group = (FingerprintMachineGroup) filterValue;
        predicates.add(cb.equal(root.get("machineGroup").get("id"), group.getId()));
        break;
    }

    return predicates;
  }

  public List<FingerprintMachine> getMachineWithSameGroup(FingerprintMachine machine) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<FingerprintMachine> cq = cb.createQuery(FingerprintMachine.class);
    Root<FingerprintMachine> root = cq.from(FingerprintMachine.class);
    cq.select(root).where(cb.equal(root.get("machineGroup"), machine.getMachineGroup()));
    TypedQuery<FingerprintMachine> q = getEntityManager().createQuery(cq);
    return q.getResultList();
  }

}
