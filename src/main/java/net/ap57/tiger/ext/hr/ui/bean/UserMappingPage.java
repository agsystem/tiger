/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineUserFacade;
import net.ap57.tiger.ext.hr.dao.UserMapBean;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUser;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserMap;
import net.ap57.tiger.ext.hr.entity.UserMap;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import org.primefaces.event.SelectEvent;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "userMappingPage")
@RequestScoped
@PageView(rel = "userMapping")
public class UserMappingPage extends Page {

    @Inject
    private UserMapBean userMapOp;

    @View
    private FingerprintMachineUserMapTable userMapTable;

    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private DataTable workUnitTable;

    @Inject
    private FingerprintMachineUserFacade machineUserOp;

    @View
    private FingerprintMachineUserTable machineUserTable;

    @View
    private FingerprintMachineUserTable orphanMachineUserTable;

    @Override
    protected void initComponents() {

        workUnitTable = new PFLazyDataTable<WorkUnitFacade, WorkUnit>() {

            @Override
            protected WorkUnitFacade getOp() {
                return workUnitOp;
            }

            @Override
            public void setSelection(WorkUnit selection) {
                super.setSelection(selection);
//                employeeAttendanceTable.setSelections(new ArrayList<>());
            }

        };

        userMapTable = new FingerprintMachineUserMapTable() {

            @Override
            protected UserMapBean getOp() {
                return userMapOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = super.getLoadFilters();
                WorkUnit workUnit = (WorkUnit) workUnitTable.getSelection();
                filters.put("WorkUnit", workUnit);
                return filters;
            }

        };

        machineUserTable = new FingerprintMachineUserTable() {
            @Override
            protected FingerprintMachineUserFacade getOp() {
                return machineUserOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = super.getLoadFilters();
                UserMap userMap = (UserMap) userMapTable.getSelection();
                filters.put("UserMap", userMap);
                return filters;
            }

            @Override
            public void onUnlink(ActionEvent evt) {
                userMapOp.unlink(userMapTable.getSelection(), machineUserTable.getSelection());
            }

        };

        orphanMachineUserTable = new FingerprintMachineUserTable() {
            @Override
            protected FingerprintMachineUserFacade getOp() {
                return machineUserOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = super.getLoadFilters();
                filters.put("orphan", Boolean.TRUE);
                return filters;
            }

            @Override
            public void onUserSelected(SelectEvent evt) {
                
                FingerprintMachineUser machineUser = (FingerprintMachineUser) evt.getObject();
                UserMap userMap = userMapTable.getSelection();
                
                FingerprintMachineUserMap machineUserMap = userMap.getUserMap();
                
                if (machineUserMap == null) {
                    machineUserMap = new FingerprintMachineUserMap();
                    List<FingerprintMachineUser> machineUsers = new ArrayList<>();
                    machineUsers.add(machineUser);
                    machineUser.setMachineUserMap(machineUserMap);
                    machineUserMap.setMachineUsers(machineUsers);
                    userMap.setUserMap(machineUserMap);
                    try {
                        userMapOp.create(userMap);
                    } catch (Exception ex) {
                        Logger.getLogger(UserMappingPage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    machineUserMap.getMachineUsers().add(machineUser);
                    machineUser.setMachineUserMap(machineUserMap);
                    userMapOp.edit(userMap);
                }
            }

        };

    }

    @Override
    protected void initLayout() {
        setExplorer("Work Unit", "15%");
        setEditor("Employee", "70%");
        setProperties("Machine User", "15%");
    }

}
