/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.ext.hr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import net.ap57.tiger.std.hr.entity.Employee;

/** 
 *
 * @author mbahbejo
 */
@Entity
//@IdClass(FingerprintMachineUserMapId.class)
public class FingerprintMachineUserMap implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @OneToOne
    private Employee employee;
    
    @OneToMany(mappedBy = "machineUserMap", fetch=FetchType.EAGER, cascade=CascadeType.MERGE)
    private List<FingerprintMachineUser> machineUsers;
    
    public Employee getEmployee() {
        return employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<FingerprintMachineUser> getMachineUsers() {
        return machineUsers;
    }

    public void setMachineUsers(List<FingerprintMachineUser> machineUsers) {
        this.machineUsers = machineUsers;
    }

}
