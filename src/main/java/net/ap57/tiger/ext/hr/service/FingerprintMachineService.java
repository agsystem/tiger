/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import net.ap57.tiger.ext.hr.dao.AttendanceLogFacade;
import net.ap57.tiger.ext.hr.dao.AttendanceLogFoundEvent;
import net.ap57.tiger.ext.hr.entity.Fingerprint;
import net.ap57.tiger.ext.hr.entity.FingerprintMachine;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUser;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserUpload;
import net.ap57.tiger.ext.hr.entity.NativeAttendanceLog;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class FingerprintMachineService {

    @Inject
    @MachineCommands
    private Map<String, String> cmds;

    @Inject
    @ResultPatterns
    private Map<String, String> resultPatterns;

    @Inject
    @FingerprintMachineUserDownloadEvent
    private Event<FingerprintMachineUser> downloadUserEvent;

    @Inject
    @FingerprintMachineUserUploadStartEvent
    private Event<FingerprintMachine> uploadUsersEvent;

    @Inject
    @AttendanceLogFoundEvent
    private Event<NativeAttendanceLog> logFoundEvent;

    private String wrap(String request) {
        String envelope = "<?xml version=\"1.0\" standalone=\"no\" ?>"
                + "<SOAP-ENV:Envelope>"
                + "<SOAP-ENV:Body>%s</SOAP-ENV:Body>"
                + "</SOAP-ENV:Envelope>";
        Formatter fmt = new Formatter();
        fmt.format(envelope, request);

        return fmt.toString();
    }

    private String compileRequest(String src, Object... args) {
        Formatter fmt = new Formatter();
        fmt.format(src, args);

        return wrap(fmt.toString());
    }

    private String getMachineUri(FingerprintMachine machine) {
        return "http://" + machine.getIpAddr() + "/iWsService";
    }

    private void send(FingerprintMachine machine, String request, MachineResultProcessor processor) {
        System.out.println("DONLOT " + machine.getIpAddr());

        try {
            
            StringRequestEntity requestEntity = new StringRequestEntity(request, "text/xml", "UTF-8");

            PostMethod method = new PostMethod(getMachineUri(machine));
            method.setRequestEntity(requestEntity);

            HttpClient client = new HttpClient();
            int statusCode = client.executeMethod(method);

            if (statusCode == HttpStatus.SC_OK) {
                processor.process(method.getResponseBodyAsStream());
            }
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FingerprintMachineService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FingerprintMachineService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Inject
    private AttendanceLogFacade logFacade;

    public void processAttLog(FingerprintMachine machine, FingerprintMachineUser user) {
        String request = compileRequest(cmds.get("getAttLog"), user == null ? "ALL" : user.getPin2());

        send(machine, request, (InputStream input) -> {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                Pattern p = Pattern.compile(resultPatterns.get("getAttLog"));
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                reader.lines().forEach((line) -> {
//                    System.out.println(line);
                    System.out.println(machine.getIpAddr() + " = " + line);
                    Matcher matcher = p.matcher(line);
                    if (matcher.find()) {
                        try {
                            int pin = Integer.parseInt(matcher.group(1));
                            Date dateTime = format.parse(matcher.group(2));
                            
//                            logFoundEvent.fire(new NativeAttendanceLog(pin, machine.getMachineGroup().getId(), machine.getId(), dateTime));
                              logFacade.saveLog(new NativeAttendanceLog(pin, machine.getMachineGroup().getId(), machine.getId(), dateTime));
                            
                        } catch (ParseException | EJBException ex) {
                            Logger.getLogger(FingerprintMachineService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            } catch (IOException ex) {
                Logger.getLogger(FingerprintMachineService.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void downloadUserInfo(FingerprintMachine machine, final FingerprintMachineUser user) {

        String request = compileRequest(cmds.get("getUserInfo"), user == null ? "ALL" : user.getPin2());
        send(machine, request, (InputStream input) -> {

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                Pattern p = Pattern.compile(resultPatterns.get("getUserInfo"));
                reader.lines().forEach((line) -> {
                    Matcher matcher = p.matcher(line);
                    if (matcher.find()) {
                        System.out.println(line);

                        int pin = Integer.parseInt(matcher.group(1));
                        String name = matcher.group(2);
                        String password = matcher.group(3);
                        int group = Integer.parseInt(matcher.group(4));
                        int privilege = Integer.parseInt(matcher.group(5));
                        int card = Integer.parseInt(matcher.group(6));
                        int pin2 = Integer.parseInt(matcher.group(7));
                        int tz1 = Integer.parseInt(matcher.group(8));
                        int tz2 = Integer.parseInt(matcher.group(9));
                        int tz3 = Integer.parseInt(matcher.group(10));

                        FingerprintMachineUser foundedUser = null;

                        if (user != null) {
                            foundedUser = user;
                        } else {
                            foundedUser = new FingerprintMachineUser();
                        }

//            foundedUser.setPin(pin);
                        foundedUser.setPin2(pin2);
                        foundedUser.setMachineGroup(machine.getMachineGroup());
                        foundedUser.setName(name);
                        foundedUser.setPassword(password);
                        foundedUser.setGroup(group);
                        foundedUser.setPrivilege(privilege);
                        foundedUser.setCard(card);
                        foundedUser.setTz1(tz1);
                        foundedUser.setTz2(tz2);
                        foundedUser.setTz3(tz3);

                        downloadUserEvent.fire(foundedUser);

                    }
                });
            } catch (IOException ex) {
                Logger.getLogger(FingerprintMachineService.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
    }

    public void downloadFingerprintTemplate(FingerprintMachine machine, FingerprintMachineUser user) {
        String request = compileRequest(cmds.get("getUserTemplate"), user == null ? "ALL" : user.getPin2());
        send(machine, request, (InputStream input) -> {

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                Pattern p = Pattern.compile(resultPatterns.get("getUserTemplate"));
                reader.lines().forEach((line) -> {
                    Matcher matcher = p.matcher(line);
                    if (matcher.find()) {
//                        int pin = Integer.parseInt(matcher.group(1));
//                        int fingerId = Integer.parseInt(matcher.group(2));
//                        AttendanceMachineFingerprintId fingerid = new AttendanceMachineFingerprintId();
//                        Employee emp = user.getEmployee();
//                        PartyRoleId empId = new PartyRoleId();
//                        empId.setFromDate(emp.getFromDate());
//                        empId.setParty(emp.getParty().getId());
//                        empId.setPartyRoleType(emp.getPartyRoleType().getId());
//                        AttendanceMachineUserId userId = new AttendanceMachineUserId();
//                        userId.setEmployee(empId);
//                        fingerid.setAttendanceMachineUser(userId);
//                        fingerid.setFingerId(fingerId);
//
//                        AttendanceMachineFingerprint finger = getEntityManager().find(AttendanceMachineFingerprint.class, fingerid);
//
//                        if (finger == null) {
//                            AttendanceMachineUser muser = userFacade.find(userId);
//                            if (muser != null) {
//                                finger = new AttendanceMachineFingerprint();
//                                finger.setAttendanceMachineUser(muser);
//                                finger.setFingerId(fingerId);
//                                finger.setDataSize(Integer.parseInt(matcher.group(3)));
//                                finger.setValid(matcher.group(4).equals("1") ? true : false);
//                                finger.setTemplate(matcher.group(5));
//
//                                getEntityManager().persist(finger);
//                            }
//                        }
                    }
                });
            } catch (IOException ex) {
                Logger.getLogger(FingerprintMachineService.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void uploadUsers(FingerprintMachine machine) {
        uploadUsersEvent.fire(machine);
    }

    public void uploadUser(@Observes @FingerprintMachineUserUploadEvent FingerprintMachineUserUpload upload) {
        uploadUser(upload.getDestination(), upload.getUser());
    }

    public void uploadUser(FingerprintMachine machine, FingerprintMachineUser user) {
        String request = compileRequest(cmds.get("setUserInfo"),
                null,
                user.getName(),
                user.getPassword(),
                user.getGroup(),
                user.getPrivilege(),
                user.getCard(),
                user.getPin2(),
                user.getTz1(),
                user.getTz2(),
                user.getTz3()
        );
        send(machine, request, (InputStream input) -> {
        });
    }

    public void uploadFingerprint(FingerprintMachine machine, Fingerprint fingerprint) {
        String request = compileRequest(cmds.get("setUserTemplate")
        //        fingerprint.getAttendanceMachineUser().getPin(),
        //        fingerprint.getFingerId(),
        //        fingerprint.getDataSize(),
        //        fingerprint.isValid() == true ? 1 : 0,
        //        fingerprint.getTemplate()
        );
        send(machine, request, (InputStream input) -> {
        });
    }

    public void deleteUser(@Observes @FingerprintMachineUserDeleteEvent FingerprintMachineUserUpload payload) {

    }

    public void deleteUser(FingerprintMachine machine, FingerprintMachineUser user) {
        String request = compileRequest(cmds.get("deleteUser"), user.getPin2());
        send(machine, request, (InputStream input) -> {

        });
    }

    public void clearUser(FingerprintMachine machine) {
        String request = compileRequest(cmds.get("clearData"), 1);
        send(machine, request, (InputStream input) -> {

        });
    }

    public void clearFingerprint(FingerprintMachine machine) {
        String request = compileRequest(cmds.get("clearData"), 2);
        send(machine, request, (InputStream input) -> {

        });
    }

    public void clearTransaction(FingerprintMachine machine) {
        String request = compileRequest(cmds.get("clearData"), 3);
        send(machine, request, (InputStream input) -> {

        });
    }

    public void getDate(FingerprintMachine machine) {
        String request = compileRequest(cmds.get("getDate"));
        send(machine, request, (InputStream input) -> {

        });
    }

    public void setDate(FingerprintMachine machine, Calendar cal) {
        String request = compileRequest(cmds.get("setDate"), cal, cal);
        send(machine, request, (InputStream input) -> {

        });
    }

    public void refreshDB(FingerprintMachine machine) {
        String request = compileRequest(cmds.get("refreshDB"));
        send(machine, request, (InputStream input) -> {

        });
    }

    public void restart(FingerprintMachine machine) {
        String request = compileRequest(cmds.get("restart"));
        send(machine, request, (InputStream input) -> {

        });
    }

}
