/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.hr.entity.AttendanceLog;
import net.ap57.tiger.ext.hr.entity.AttendanceLogId;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUser;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserId;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserMap;
import net.ap57.tiger.ext.hr.entity.NativeAttendanceLog;
import net.ap57.tiger.std.hr.dao.EmployeeAttendanceFacade;
//import net.ap57.tiger.std.hr.dao.EmployeeWorkScheduleFacade;
import net.ap57.tiger.std.hr.entity.EmployeeAttendance;
import net.ap57.tiger.std.hr.entity.EmployeeAttendanceId;
//import net.ap57.tiger.std.hr.entity.EmployeeWorkSchedule;
import net.ap57.tiger.std.hr.event.FindAttendanceEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class AttendanceLogFacade extends AbstractFacade<AttendanceLog> {

    private static final String INSERT_NATIVE_ATTENDANCE_LOG
            = "INSERT INTO attendancelog(pin2, machinegroup_id, machine_id, datetime, strkey) "
            + "VALUES(?, ?, ?, ?, ?)";

    @Inject
    private EntityManager em;

    @Inject
    private FingerprintMachineUserFacade machineUserFacade;

    @Inject
    private FingerprintMachineUserMapFacade userMapFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AttendanceLogFacade() {
        super(AttendanceLog.class);
    }

    @Override
    public AttendanceLog createTransient(Map<String, Object> params) {
        AttendanceLog attLog = new AttendanceLog();

        return attLog;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<AttendanceLog> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        List<Predicate> predicates = new ArrayList<>();
        switch (filterName) {
            case "FingerprintMachineGroup":
                predicates.add(cb.equal(root.get("machine").get("machineGroup"), filterValue));
                break;
            case "machineUser.pin2":
                try {
                    Integer pin2 = Integer.parseInt((String) filterValue);
                    predicates.add(cb.equal(root.get("machineUser").get("pin2"), pin2));
                    break;
                } catch (NumberFormatException ex) {
                    break;
                }
            case "machineUser.name":
                Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                predicates.add(cb.like(cb.upper(root.get("machineUser").get("name")), literal));
                break;
            case "dateTime":
                Date fromDate = (Date) filterValue;
                Calendar cal = Calendar.getInstance();
                cal.setTime(fromDate);
                cal.add(Calendar.DATE, 1);
                predicates.add(cb.<Date>greaterThanOrEqualTo(root.get("dateTime"), fromDate));
                predicates.add(cb.<Date>lessThan(root.get("dateTime"), cal.getTime()));
                break;
            default:
                break;
        }
        return predicates;
    }

    @Override
    public Object getId(AttendanceLog entity) {
        AttendanceLogId id = new AttendanceLogId();
        id.setMachineUser((FingerprintMachineUserId) machineUserFacade.getId(entity.getMachineUser()));
        id.setMachine(entity.getMachine().getId());
        id.setDateTime(entity.getDateTime());

        return id;
    }

    public void saveLog(NativeAttendanceLog log) throws EJBException {
        try {
            getEntityManager().createNativeQuery(INSERT_NATIVE_ATTENDANCE_LOG)
                    .setParameter(1, log.getPin2())
                    .setParameter(2, log.getFingerprintmachinegroup_id())
                    .setParameter(3, log.getMachine_id())
                    .setParameter(4, log.getDatetime())
                    .setParameter(5, UUID.randomUUID().toString())
                    .executeUpdate();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    public void processLog(@Observes @AttendanceLogFoundEvent NativeAttendanceLog log) {
        try {
            saveLog(log);
        } catch (EJBException ex) {
            return;
        }

//        try {
//            getEntityManager().createNativeQuery(INSERT_NATIVE_ATTENDANCE_LOG)
//                    .setParameter(1, log.getPin2())
//                    .setParameter(2, log.getFingerprintmachinegroup_id())
//                    .setParameter(3, log.getMachine_id())
//                    .setParameter(4, log.getDatetime())
//                    .setParameter(5, UUID.randomUUID().toString())
//                    .executeUpdate();
//        } catch (RuntimeException ex) {
//            return;
//        }
//    AttendanceLog foundedLog = find(getId(log));
//    if (foundedLog == null) {
//      try {
//        create(log);
//      } catch (Exception ex) {
//        Logger.getLogger(AttendanceLogFacade.class.getName()).log(Level.SEVERE, null, ex);
//      }
//    }
    }

//    @Inject
//    private EmployeeWorkScheduleFacade scheduleFacade;

    @Inject
    private EmployeeAttendanceFacade attendanceFacade;

//    public void onFindAttendanceEvent(@Observes @FindAttendanceEvent EmployeeWorkSchedule schedule) {
//        FingerprintMachineUserMap userMap = userMapFacade.find(schedule.getEmployee().getId());
//
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery<AttendanceLog> cq = cb.createQuery(AttendanceLog.class);
//
//        Root<FingerprintMachineUserMap> map = cq.from(FingerprintMachineUserMap.class);
//        Join<FingerprintMachineUserMap, FingerprintMachineUser> user = map.join("machineUsers");
//        Join<FingerprintMachineUser, AttendanceLog> att = user.join("attendanceLogs");
//
//        Calendar calBegin = Calendar.getInstance();
//        calBegin.setTime(schedule.getFromDate());
//        calBegin.add(Calendar.HOUR_OF_DAY, -2);
//
//        Calendar calEnd = Calendar.getInstance();
//        calEnd.setTime(schedule.getThruDate());
//        calEnd.add(Calendar.HOUR_OF_DAY, 10);
//
//        cq.select(att).orderBy(cb.asc(att.get("dateTime"))).where(new Predicate[]{
//            cb.equal(map.get("employee").get("id"), schedule.getEmployee().getId()),
//            cb.greaterThanOrEqualTo(att.get("dateTime"), calBegin.getTime()),
//            cb.lessThan(att.get("dateTime"), calEnd.getTime())
//        });
//
//        List<AttendanceLog> logs = getEntityManager().createQuery(cq).getResultList();
//
//        EmployeeAttendanceId id = new EmployeeAttendanceId();
//        id.setEmployee(schedule.getEmployee().getId());
//        id.setWorkDate(schedule.getWorkDate());
//
//        EmployeeAttendance attendance = attendanceFacade.find(id);
//
//        if (attendance == null && !logs.isEmpty()) {
//
//            int szLog = logs.size();
//            try {
//                if (szLog == 1) {
//                    attendanceFacade.saveLog(schedule.getEmployee(), schedule.getWorkDate(), logs.get(0).getDateTime(), null);
//                } else if (szLog > 1) {
//                    attendanceFacade.saveLog(schedule.getEmployee(), schedule.getWorkDate(), logs.get(0).getDateTime(), logs.get(szLog - 1).getDateTime());
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(AttendanceLogFacade.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//
//    }

}
