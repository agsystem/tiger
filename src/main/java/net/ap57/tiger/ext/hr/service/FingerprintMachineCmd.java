/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.hr.service;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
public class FingerprintMachineCmd {

  private Map<String, String> cmds;
  
  private Map<String, String> resultPatterns;

  @PostConstruct
  public void init() {
    cmds = new HashMap<>();
    resultPatterns = new HashMap<>();
    
    cmds.put("getAttLog",
        "<GetAttLog>"
        + "     <ArgComKey xsi:type=xsd:integer>0</ArgComKey>"
        + "         <Arg>"
        + "             <PIN xsi:type=xsd:integer>%s</PIN>"
        + "         </Arg>"
        + "</GetAttLog>");
    resultPatterns.put("getAttLog", "<PIN>(.*?)</PIN><DateTime>(.*?)</DateTime>");
    
    cmds.put("getUserInfo",
        "<GetUserInfo>"
        + "     <ArgComKey xsi:type=xsd:integer>0</ArgComKey>"
        + "         <Arg>"
        + "             <PIN xsi:type=xsd:integer>%s</PIN>"
        + "         </Arg>"
        + "</GetUserInfo>");
    resultPatterns.put("getUserInfo", "<PIN>(.*?)</PIN><Name>(.*?)</Name><Password>(.*?)</Password><Group>(.*?)</Group><Privilege>(.*?)</Privilege><Card>(.*?)</Card><PIN2>(.*?)</PIN2><TZ1>(.*?)</TZ1><TZ2>(.*?)</TZ2><TZ3>(.*?)</TZ3>");
    
    cmds.put("getUserTemplate",
        "<GetUserTemplate>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "         <Arg>"
        + "             <PIN xsi:type=xsd:integer>%s</PIN>"
        + "         </Arg>"
        + "</GetUserTemplate>");
    resultPatterns.put("getUserTemplate", "<PIN>(.*?)</PIN><FingerID>(.*?)</FingerID><Size>(.*?)</Size><Valid>(.*?)</Valid><Template>(.*?)</Template>");
    
    cmds.put("setUserInfo",
        "<SetUserInfo>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "         <Arg>"
        + "             <PIN xsi:type=xsd:integer>%d</PIN>"
        + "             <Name xsi:type=xsd:string>%s</Name>"
        + "             <Password xsi:type=xsd:string>%s</Password>"
        + "             <Group xsi:type=xsd:integer>%d</Group>"
        + "             <Privilege xsi:type=xsd:integer>%d</Privilege>"
        + "             <Card xsi:type=xsd:integer>%d</Card>"
        + "             <PIN2 xsi:type=xsd:integer>%d</PIN2>"
        + "             <TZ1 xsi:type=xsd:integer>%d</TZ1>"
        + "             <TZ2 xsi:type=xsd:integer>%d</TZ2>"
        + "             <TZ3 xsi:type=xsd:integer>%d</TZ3>"
        + "         </Arg>"
        + "</SetUserInfo>");
    cmds.put("setUserTemplate",
        "<SetUserTemplate>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "         <Arg>"
        + "             <PIN xsi:type=xsd:integer>%d</PIN>"
        + "             <FingerID xsi:type=xsd:integer>%d</FingerID>"
        + "             <Size xsi:type=xsd:integer>%d</Size>"
        + "             <Valid xsi:type=xsd:integer>%d</Valid>"
        + "             <Template xsi:type=xsd:string>%s</Template>"
        + "         </Arg>"
        + "</SetUserTemplate>");
    cmds.put("deleteUser",
        "<DeleteUser>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "         <Arg>"
        + "             <PIN xsi:type=xsd:integer>%d</PIN>"
        + "         </Arg>"
        + "</DeleteUser>");
    cmds.put("clearData",
        "<ClearData>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "         <Arg>"
        + "             <Value xsi:type=xsd:integer>%d</Value>"
        + "         </Arg>"
        + "</ClearData>");
    cmds.put("getDate",
        "<GetDate>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "</GetDate>");
    cmds.put("setDate",
        "<SetDate>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "         <Arg>"
        + "             <Date xsi:type=xsd:date>%tF</Date>"
        + "             <Time xsi:type=xsd:time>%tR</Time>"
        + "         </Arg>"
        + "</SetDate>");
    cmds.put("refreshDB",
        "<RefreshDB>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "</RefreshDB>");
    cmds.put("restart",
        "<Restart>"
        + "     <ArgComKey xsi:type=xsd:integer>55143</ArgComKey>"
        + "</Restart>");
  }

  @Produces
  @MachineCommands
  public Map<String, String> getCmds() {
    return cmds;
  }

  @Produces
  @ResultPatterns
  public Map<String, String> getResultPatterns() {
    return resultPatterns;
  }

}
