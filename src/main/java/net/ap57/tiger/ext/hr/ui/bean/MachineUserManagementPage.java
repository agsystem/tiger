/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.ext.hr.ui.bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineGroupFacade;
import net.ap57.tiger.ext.hr.dao.FingerprintMachineUserFacade;
import net.ap57.tiger.ext.hr.service.FingerprintMachineService;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author mbahbejo
 */
@Named(value = "machineUserManagementPage")
@RequestScoped
@PageView(rel = "machineUserManagement")
public class MachineUserManagementPage extends Page {

    @Inject
    private FingerprintMachineGroupFacade fingerprintMachineGroupOp;

    @View
    private FingerprintMachineGroupTable fingerprintMachineGroupTree;

    @Inject
    private FingerprintMachineUserFacade machineUserOp;

    @View
    private FingerprintMachineUserTable machineUserTable;

    @Override
    protected void initLayout() {
        setExplorer("Machine Group", "20%");
        setEditor("Machine User", "80%");
    }

    @Override
    protected void initComponents() {

        fingerprintMachineGroupTree = new FingerprintMachineGroupTable() {

            @Override
            protected FingerprintMachineGroupFacade getOp() {
                return fingerprintMachineGroupOp;
            }

        };

        machineUserTable = new FingerprintMachineUserTable() {
            @Override
            protected FingerprintMachineUserFacade getOp() {
                return machineUserOp;
            }

        };

    }

}
