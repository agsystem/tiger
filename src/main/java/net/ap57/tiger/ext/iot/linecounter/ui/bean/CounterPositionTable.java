/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.ui.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.ext.iot.linecounter.dao.LineCounterDeviceFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButton;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterDevice;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class CounterPositionTable extends PFLazyDataTable<CounterPositionFacade, CounterPosition> {

    protected abstract LineCounterDeviceFacade getGroupOp();

    protected abstract Event<CounterPosition> getSelectionEventGenerator();

    protected abstract Event<WorkUnit> getClearSelectionEventGenerator();

    public WorkUnit getWorkUnit() {
        return (WorkUnit) getAttribute("workunit");
    }

    public void setWorkUnit(WorkUnit workunit) {
        setAttribute("workunit", workunit);
    }

    public boolean isLocking() {
        return (boolean) getAttribute("locking", false);
    }

    public void setLocking(boolean locked) {
        setAttribute("locking", locked);
    }

    public void notifyLocking(ActionEvent evt) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        boolean locked = Boolean.valueOf(params.get("locked"));
        System.out.println("LOCK = " + locked);
        setLocking(locked);
    }

    public int getColumns() {
        return (int) getAttribute("columns", 18);
    }

    public void setColumns(int columns) {
        setAttribute("columns", columns);
    }

    public List<CounterPosition> getPositions() {
        List<CounterPosition> positions = getOp().findAll(getLoadFilters());
        if (positions.isEmpty()) {
            return positions;
        } else {
            return positions.subList(1, 47);
        }
    }

    public List<CounterPosition> getQcPositions() {
        List<CounterPosition> positions = getOp().findAll(getLoadFilters());
        if (positions.isEmpty()) {
            return positions;
        } else {
            return positions.subList(0, 1);
        }
    }

    public void notifyBean(ActionEvent evt) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String command = params.get("command");
        switch (command) {
            case "pair":
                onPair(params.get("sender"), params.get("button"));
                break;
            default:
                break;
        }

    }

    public void invalidateDisplay(ActionEvent evt) {
        System.out.println("INVALIDATE DISPLAY");
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String[] arrStrPosNums = params.get("positionNumber").split(",");
        List<String> lstPosNums = new ArrayList<>();
        for (String strPosNum : arrStrPosNums) {
            int posNum = Integer.parseInt(strPosNum);
            if(posNum == 0) lstPosNums.add("qc-dt-tbl:" + posNum + ":counter");
            else lstPosNums.add("dt-tbl:" + (posNum - 1) + ":counter");
        }
        for(String update : lstPosNums) {
            System.out.println(update);
        }
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update(lstPosNums);
    }

    public void invalidateSelection(ActionEvent evt) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String strPosNum = params.get("positionNumber");

        List<String> lstPosNums = new ArrayList<>();

        int posNum = Integer.parseInt(strPosNum);
        if (posNum == 0) {
            lstPosNums.add("qc-dt-tbl:" + (posNum) + ":counter-lnk");
        } else {
            lstPosNums.add("dt-tbl:" + (posNum - 1) + ":counter-lnk");
        }

        String strOldPosNum = "";
        Integer oldPos = getOldSelection();
        if (oldPos != null) {
            if (oldPos == 0) {                
                strOldPosNum = "" + oldPos;
                lstPosNums.add("qc-dt-tbl:" + strOldPosNum + ":counter-lnk");
            } else {
                strOldPosNum = "" + (oldPos - 1);
                lstPosNums.add("dt-tbl:" + strOldPosNum + ":counter-lnk");
            }
        }
        lstPosNums.add("detail-frm");
        lstPosNums.add("detail-frm:sampler-dt-tbl");

        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update(lstPosNums);
    }

    public void notifyMessage(ActionEvent evt) {
//        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//        ByteBuffer data = params.get("data");
//        Message msg = new Message(data);
//        System.out.println(msg.getSource());
    }

    protected void onPair(String sender, String button) {
        if (isQualifiedSender(sender)) {
            CounterPosition position = getSelection();
            if (position == null) {
                return;
            }

            LineCounterButton counter = counter = new LineCounterButton();
            LineCounterDevice cg = getGroupOp().find(sender);
            counter.setHostDevice(cg);
            cg.getButtons().put(counter.getInputNumber(), counter);
//            counter.setInputNumber(button);
//            counter.setLastCount(0);
//            counter.setMode(LineCounterButton.NORMAL_MODE);
//            getOp().repair(position, counter);
        }
    }

    @Override
    public boolean isCreateContextEmpty() {
        if (getWorkUnit() == null) {
            return true;
        }
        return super.isCreateContextEmpty();
    }

    @Override
    public void create(CounterPosition e) {
        e.setWorkUnit(getWorkUnit());
        super.create(e);
    }

    @Override
    protected Map<String, Object> getLoadFilters() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("workUnit", getWorkUnit());
        return filters;
    }

    private boolean isQualifiedSender(String sender) {
        CounterPosition pos = getSelection();
        if (pos == null) {
            return false;
        }
        WorkUnit workUnit = pos.getWorkUnit();
        List<String> cids = null;
//        cids = getCoverageOp().getCounterGroupIdServices(workUnit);
        return cids.contains(sender);
    }

    public void onWorkUnitChange(SelectEvent evt) {
        setSelection(null);
        setWorkUnit((WorkUnit) evt.getObject());
        RequestContext rc = RequestContext.getCurrentInstance();
        String wuid = getWorkUnit().getId();
        rc.execute("onCounterUnitChange('" + wuid + "');"
                + "onManagerUnitChange('" + wuid + "')");
    }

    public void onEmployeeSelect(SelectEvent evt) {

    }

    public void resetAll(ActionEvent evt) {
        getOp().resetAll(getWorkUnit());
    }

    public void reset(ActionEvent evt) {
//        LineCounterButton counter = getSelection().getCounterButton();
//        if (counter != null) {
//            getOp().reset(counter);
//        }
    }

    public String onSelection(CounterPosition pos) {
        if (getSelection() == null) {
            return "";
        } else if (pos.equals(getSelection())) {
            return "-selected";
        } else {
            return "";
        }
    }

    public void generatePosition(ActionEvent evt) {
        getOp().generatePosition(getWorkUnit());
    }

    @Override
    public void setSelection(CounterPosition selection) {

        CounterPosition oldPos = getSelection();

        if (oldPos != null) {
            setOldSelection(oldPos.getPositionNumber());
        }

        if (isLocking() && selection != null) {
            getSelectionEventGenerator().fire(selection);
        } else if (isLocking() && selection == null) {
            WorkUnit unit = getWorkUnit();
            if (unit != null) {
                getClearSelectionEventGenerator().fire(unit);
            }
        }

        super.setSelection(selection);
    }

    public void setOldSelection(Integer oldSelection) {
        setAttribute("oldSelection", oldSelection);
    }

    public Integer getOldSelection() {
        return (Integer) getAttribute("oldSelection");
    }

    public long count(CounterPosition position) {
        return getOp().countData(position);
    }

}
