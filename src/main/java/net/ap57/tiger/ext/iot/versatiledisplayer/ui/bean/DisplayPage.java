/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.versatiledisplayer.ui.bean;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.ext.utility.dao.WorkUnitDisplayerFacade;
import net.ap57.tiger.ext.utility.entity.WorkUnitDisplayer;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.view.WorkUnitSelector;
import net.ap57.tiger.ext.iot.versatiledisplayer.dao.VersatileDisplayerFacade;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.ext.iot.versatiledisplayer.service.VersatileDisplayerService;
import org.primefaces.event.SelectEvent;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Named(value = "displayPage")
@RequestScoped
@PageView(rel = "DisplayPage")
public class DisplayPage extends Page {
    
    @Inject
    private VersatileDisplayerService displayerService;

    @Inject
    private VersatileDisplayerFacade displayerOp;

    @View
    private VersatileDisplayerTable displayerTable;
    
    @Inject
    private WorkUnitDisplayerFacade workUnitDisplayerOp;
    
    @View
    private DataTable workUnitDisplayerTable;

    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private WorkUnitSelector workUnitSelector;

    @Override
    protected void initLayout() {
        setEditor("Displayer", "100%");
    }

    @Override
    protected void initComponents() {

        displayerTable = new VersatileDisplayerTable() {
            @Override
            protected VersatileDisplayerFacade getOp() {
                return displayerOp;
            }

            @Override
            protected VersatileDisplayerService getDisplayerService() {
                return displayerService;
            }

        };
        
        workUnitDisplayerTable = new PFLazyDataTable<WorkUnitDisplayerFacade, WorkUnitDisplayer>() {
            @Override
            protected WorkUnitDisplayerFacade getOp() {
                return workUnitDisplayerOp;
            }

            @Override
            protected Map<String, Object> getNewEntityParams() {
                Map<String, Object> params = new HashMap<>();
                params.put("displayer", displayerTable.getSelection());
                return params;
            }

            @Override
            protected void addStaticFilter(Map<String, Object> filters) {
                VersatileDisplayer displayer = displayerTable.getSelection();
                if(displayer != null)
                filters.put("displayer", displayer);
            }
            
        };

        workUnitSelector = new WorkUnitSelector() {

            @Override
            protected WorkUnitFacade getOp() {
                return workUnitOp;
            }

            @Override
            public void onWorkUnitSelect(SelectEvent evt) {
                WorkUnitDisplayer selectedDisplayer = (WorkUnitDisplayer) workUnitDisplayerTable.getSelection();
                selectedDisplayer.setWorkUnit((WorkUnit) evt.getObject());
                workUnitDisplayerTable.create(selectedDisplayer);
            }

        };

    }

}
