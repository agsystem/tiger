/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButton;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterDevice;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class LineCounterDeviceFacade extends AbstractFacade<LineCounterDevice> {

    @Inject
    private EntityManager em;

    @Inject
    private LineCounterButtonFacade counterButtonFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LineCounterDeviceFacade() {
        super(LineCounterDevice.class);
    }

    @Override
    public LineCounterDevice createTransient(Map<String, Object> params) throws EJBException {
        LineCounterDevice counterGroup = new LineCounterDevice();
        return counterGroup;
    }

    @Override
    public Object getId(LineCounterDevice entity) {
        return entity.getId();
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<LineCounterDevice> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();
        if (filterName.equals("workUnit")) {
            predicates.add(cb.equal(root.get("workUnit"), filterValue));
        }
        return predicates;
    }

    @Override
    public void remove(LineCounterDevice entity) throws EJBException {
        for (LineCounterButton c : entity.getButtons().values()) {
            CounterPosition cp = c.getPosition();
            if (cp != null) {
//                cp.setCounterButton(null);
                getEntityManager().merge(cp);
            }
        }
        super.remove(entity);
    }

    public void register(UUID devId, String address, String hostname) {
        LineCounterDevice lineCounterDevice = find(devId);
        if (lineCounterDevice == null) {
            lineCounterDevice = createTransient(null);

            lineCounterDevice.setId(devId);

            lineCounterDevice.setAddress(address);
            lineCounterDevice.setHostname(hostname);
            lineCounterDevice.setReady(true);

            lineCounterDevice.setButtons(new HashMap<>());

            for (int i = 1; i <= 12; i++) {
                LineCounterButton counterButton = new LineCounterButton();
                counterButton.setHostDevice(lineCounterDevice);
                counterButton.setInputNumber(i);
                lineCounterDevice.getButtons().put(counterButton.getInputNumber(), counterButton);
            }

            create(lineCounterDevice);
        } else {
            lineCounterDevice.setAddress(address);
            lineCounterDevice.setHostname(hostname);
            lineCounterDevice.setReady(true);

            edit(lineCounterDevice);
        }
    }

    public void unregister(UUID devId) {
        LineCounterDevice lineCounterDevice = find(devId);
        if (lineCounterDevice != null) {
            lineCounterDevice.setReady(false);
            edit(lineCounterDevice);
        }
    }

    public CounterPosition count(LineCounterDevice device, int inputNumber, Date inputTime) {

        LineCounterButton button = device.getButtons().get(inputNumber);
        CounterPosition position = button.getPosition();

        if (position != null && position.isActive() && position.isEnabled()) {

//            synchronized (position) {
            counterButtonFacade.count(position, inputTime);
//            }
            return position;

        } else {
            return null;
        }

    }

}
