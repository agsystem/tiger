/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.service;

import java.util.Date;
import java.util.UUID;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import mdn.iotlib.comm.message.Message;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.LineCounterDeviceFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButton;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterDevice;
import net.ap57.tiger.ext.iot.linecounter.event.PairRequestEvent;
import net.ap57.tiger.ext.iot.linecounter.event.InvalidateCounterEvent;
import net.ap57.tiger.ext.utility.entity.HelpDeskCase;
import net.ap57.tiger.ext.utility.entity.HelpDeskCaseId;
import net.ap57.tiger.ext.utility.service.HelpDeskService;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
public class LineCounterService {

    @Inject
    private LineCounterDeviceFacade lineCounterDeviceFacade;

    @Inject
    private CounterPositionFacade counterPositionFacade;

    @Inject
    private WorkUnitFacade workUnitFacade;

    @Inject
    private HelpDeskService helpDeskService;
    
//    @Inject
//    private CounterPositionSamplerFacade samplerFacade;

    @Inject
    @PairRequestEvent
    private Event<LineCounterButton> pairRequestEvent;

    @Inject
    @InvalidateCounterEvent
    private Event<CounterPosition> invalidateCounterEvent;
    
//    @Inject
//    @InvalidateSamplerEvent
//    private Event<CounterPositionSamplerData> invalidateSamplerEvent;
    
//    @Inject
//    private TimerService timerService;

    @Asynchronous
    public void route(Message msg) {
        int flag = msg.getFlag();
        if (flag == 0) {
            System.out.println(msg.getString());
            String[] payload = msg.getString().split("\\|");
            String ipAddr = payload[0];
            String hostname = payload[1];
            register(msg.getSource(), ipAddr, hostname);
        } else if (flag == 1) {
            long duration = msg.getLong();
            if (duration >= 10000) {
                pair(msg.getSource(), msg.getInput());
            } else if (duration >= 3000) {
                help(msg.getSource(), msg.getInput());
            } else {
                count(msg.getSource(), msg.getInput(), new Date(msg.getTimestamp()));
            }
        }
    }

    public void register(UUID sender, String ipAddr, String hostname) {
        lineCounterDeviceFacade.register(sender, ipAddr, hostname);
    }

    public void unregister(UUID sender) {
        lineCounterDeviceFacade.unregister(sender);
    }

    private void pair(UUID sender, int inputNumber) {
        System.out.println("PAIR");
        LineCounterDevice device = lineCounterDeviceFacade.find(sender);
        LineCounterButton button = device.getButtons().get(inputNumber);
        pairRequestEvent.fire(button);
    }

    private void count(UUID sender, int inputNumber, Date inputTime) {

        System.out.println("COUNT");
        LineCounterDevice device = lineCounterDeviceFacade.find(sender);
        CounterPosition updatedPosition = lineCounterDeviceFacade.count(device, inputNumber, inputTime);
        if (updatedPosition != null) {
            invalidateCounterEvent.fire(updatedPosition);
        }

    }

    private void help(UUID sender, int inputNumber) {

        System.out.println("REPAIR");
        LineCounterDevice device = lineCounterDeviceFacade.find(sender);
        LineCounterButton button = device.getButtons().get(inputNumber);
        CounterPosition position = button.getPosition();
        if (position != null && position.isActive()) {
            if (position.isEnabled()) {
                String helpDeskContent = position.getMachine() + " (" + position.getPositionNumber() + ") rusak";
                HelpDeskCase aCase = helpDeskService.submitCase(position.getWorkUnit(), workUnitFacade.findSingleByAttribute("name", "Mekanik"), "system", helpDeskContent);
                position.setEnabled(false);
                position.setLastError(aCase.getSubmitDate());
                counterPositionFacade.edit(position);
            } else if (!position.isEnabled()) {
                HelpDeskCaseId helhDeskCaseId = new HelpDeskCaseId();
                helhDeskCaseId.setSrcWorkUnit(position.getWorkUnit().getId());
                helhDeskCaseId.setSubmitDate(position.getLastError());

                HelpDeskCase helpDeskCase = helpDeskService.closingCase(position.getWorkUnit(), position.getLastError());
                if (helpDeskCase != null) {
                    position.setEnabled(true);
                    counterPositionFacade.edit(position);
                }
            }

        }

        invalidateCounterEvent.fire(position);
    }

//    @Timeout
//    @Schedule(second = "0", minute = "*", hour = "*")
//    public void sample(Timer timer) {
//        for(CounterPositionSampler sampler : samplerFacade.findAllActiveSampler()) {
//            CounterPosition position = sampler.getCounterPosition();
//            Date fromDate = new Date(timer.getNextTimeout().getTime() - (120 * 1000));
//            Date thruDate = new Date(timer.getNextTimeout().getTime() - (60 * 1000));
//            
//            System.out.println("SAMPLER FROM  " + fromDate + " THRU " + thruDate);
//            
//            long count = counterPositionFacade.countDataRange(position, fromDate, thruDate);
//            
//            CounterPositionSamplerData samplerData = new CounterPositionSamplerData();
//            samplerData.setCounterPositionSampler(sampler);
//            sampler.getCounterPositionSamplerDatas().add(samplerData);
//            samplerData.setCount(count);
//            samplerData.setDateTaken(thruDate);
//            
//            samplerFacade.edit(sampler);
//            invalidateSamplerEvent.fire(samplerData);
//        }
//    }

}
