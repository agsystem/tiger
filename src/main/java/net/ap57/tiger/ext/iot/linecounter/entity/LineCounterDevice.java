/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.entity;

import java.io.Serializable;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.iot.entity.Thing;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
public class LineCounterDevice extends Thing implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    private PartyRole workUnit;

    @OneToMany(mappedBy = "hostDevice", cascade = CascadeType.ALL)
    @MapKey(name = "inputNumber")
    private Map<Integer, LineCounterButton> buttons;

    public WorkUnit getWorkUnit() {
        return (WorkUnit) workUnit;
    }

    public void setWorkUnit(WorkUnit workUnit) {
        this.workUnit = workUnit;
    }

    public Map<Integer, LineCounterButton> getButtons() {
        return buttons;
    }

    public void setButtons(Map<Integer, LineCounterButton> buttons) {
        this.buttons = buttons;
    }

    @Override
    public String toString() {
        return hostname;
    }

}
