/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSampler;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerData;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerDataId;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class CounterPositionSamplerDataFacade extends AbstractFacade<CounterPositionSamplerData> {

    @Inject
    private EntityManager em;
    
    @Inject
    private CounterPositionSamplerFacade samplerFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CounterPositionSamplerDataFacade() {
        super(CounterPositionSamplerData.class);
    }

    @Override
    public CounterPositionSamplerData createTransient(Map<String, Object> params) throws EJBException {
        CounterPositionSamplerData samplerData = new CounterPositionSamplerData();
        return samplerData;
    }

    @Override
    public Object getId(CounterPositionSamplerData entity) {
        CounterPositionSamplerDataId id = new CounterPositionSamplerDataId();
        id.setCounterPositionSampler((CounterPositionSamplerId) samplerFacade.getId(entity.getCounterPositionSampler()));
        id.setDateTaken(entity.getDateTaken());
        return id;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<CounterPositionSamplerData> root) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();
        
        switch(filterName) {        
            case "counterPositionSampler":
                CounterPositionSampler sampler = (CounterPositionSampler) filterValue;
                predicates.add(cb.equal(root.get("counterPositionSampler").get("counterPosition").get("workUnit").get("id"), sampler.getCounterPosition().getWorkUnit().getId()));
                predicates.add(cb.equal(root.get("counterPositionSampler").get("counterPosition").get("positionNumber"), sampler.getCounterPosition().getPositionNumber()));
                break;
            case "sampleFrom":
                Date sampleFrom = (Date) filterValue;
                predicates.add(cb.<Date>greaterThanOrEqualTo(root.get("dateTaken"), sampleFrom));
                break;
            case "sampleThru":
                Date sampleThru = (Date) filterValue;
                predicates.add(cb.<Date>lessThanOrEqualTo(root.get("dateTaken"), sampleThru));
                break;
        }
        
        return predicates;
        
    }

    @Override
    protected void addCriteria(Root<CounterPositionSamplerData> root, CriteriaQuery<CounterPositionSamplerData> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("dateTaken")));
    }

    @Override
    public void create(CounterPositionSamplerData entity) throws EJBException {
        samplerFacade.edit(entity.getCounterPositionSampler());
    }

    @Override
    public Long countAll(Map<String, Object> filters) {
        if(filters == null || (filters != null && filters.get("counterPositionSampler") == null)) return 0l;
        return super.countAll(filters);
    }

    @Override
    public List<CounterPositionSamplerData> findAll(int startPosition, int maxResult, Map<String, Object> filters) {
        if(filters == null || (filters != null && filters.get("counterPositionSampler") == null)) return new ArrayList<>();
        return super.findAll(startPosition, maxResult, filters);
    }
    
}
