/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.versatiledisplayer.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint.Async;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageDecoder;
import mdn.iotlib.comm.message.MessageEncoder;
import net.ap57.tiger.ext.iot.linecounter.service.LineCounterSocket;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.ext.iot.versatiledisplayer.event.VersatileDisplayerOnMessageEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
@ServerEndpoint(
        value = "/websocket/versatiledisplayer/{cid}",
        encoders = {MessageEncoder.class},
        decoders = {MessageDecoder.class}
)
public class VersatileDisplayerSocket {

    @Inject
    @VersatileDisplayerOnMessageEvent
    private Event<Message> onMessageEvent;

    private Map<UUID, Session> sessions;

    private Map<UUID, String> displays;

    private Map<String, List<UUID>> displayers;

    public Map<UUID, Session> getSessions() {
        if (sessions == null) {
            sessions = new ConcurrentHashMap<>();
        }
        return sessions;
    }

    public Map<UUID, String> getDisplays() {
        if (displays == null) {
            displays = new ConcurrentHashMap<>();
        }
        return displays;
    }

    public Map<String, List<UUID>> getDisplayers() {
        if (displayers == null) {
            displayers = new ConcurrentHashMap<>();
        }
        return displayers;
    }

    @OnOpen
    public void onOpen(@PathParam("cid") String cid, Session session, EndpointConfig config) {
        session.setMaxIdleTimeout(0);
        getSessions().put(UUID.fromString(cid), session);
    }

    @OnMessage
    public void onMessage(@PathParam("cid") String cid, Session session, Message message) {
        onMessageEvent.fire(message);
    }

    @OnError
    public void onError(@PathParam("cid") String cid, Session session, Throwable t) {
        Logger.getLogger(LineCounterSocket.class.getName()).log(Level.SEVERE, null, t);
    }

    @OnClose
    public void onClose(@PathParam("cid") String cid, Session session, CloseReason cr) {
        UUID id = UUID.fromString(cid);
        getSessions().remove(id);
        String display = getDisplays().get(id);
        if (display != null) {
            getDisplayers().get(display).remove(id);
        }
        getDisplays().remove(id);
    }

    @Asynchronous
    public void invalidate(VersatileDisplayer displayer, Message msg) {
        Session s = getSessions().get(displayer.getId());
        if (s != null && s.isOpen()) {
            Async sender = s.getAsyncRemote();
//            sender.setSendTimeout(1);
            sender.sendObject(msg);
        }
    }

    @Asynchronous
    public void invalidateAll(Message msg) {
        for (Session s : getSessions().values()) {
            if (s != null && s.isOpen()) {
                Async sender = s.getAsyncRemote();
//                sender.setSendTimeout(1);
                sender.sendObject(msg);
            }
        }
    }
    
    public void invalidatePair(Message msg) {
        for (Session s : getSessions().values()) {
            if (s != null && s.isOpen()) {
                try {
                    //                Async sender = s.getAsyncRemote();
//                sender.setSendTimeout(1);
                    s.getBasicRemote().sendObject(msg);
                } catch (IOException ex) {
                    Logger.getLogger(VersatileDisplayerSocket.class.getName()).log(Level.SEVERE, null, ex);
                } catch (EncodeException ex) {
                    Logger.getLogger(VersatileDisplayerSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Asynchronous
    public void invalidateAll(String display, Message msg) {
        List<UUID> targets = getDisplayers().get(display);
        if (targets == null) {
            return;
        }
        for (UUID target : targets) {
            Session s = getSessions().get(target);
            if (s != null && s.isOpen()) {
                Async sender = s.getAsyncRemote();
//                sender.setSendTimeout(1);
                sender.sendObject(msg);
            }
        }
    }

    @Asynchronous
    public void show(Message msg) {

        UUID target = msg.getTarget();
        String nextDisplay = msg.getString();

        String currentDisplay = getDisplays().get(target);

        if (currentDisplay != null) {
            if (!nextDisplay.equals(currentDisplay)) {

                List<UUID> currentDisplayers = getDisplayers().get(currentDisplay);
                currentDisplayers.remove(target);

            }
        }

        getDisplays().put(target, nextDisplay);

        List<UUID> nextDisplayers = getDisplayers().get(nextDisplay);
        if (nextDisplayers == null) {
            nextDisplayers = Collections.synchronizedList(new ArrayList<>());
            getDisplayers().put(nextDisplay, nextDisplayers);
        }
        nextDisplayers.add(target);

        Session s = getSessions().get(target);
        if (s != null && s.isOpen()) {
            Async sender = s.getAsyncRemote();
            sender.setSendTimeout(1);
            sender.sendObject(msg);
        }
    }

}
