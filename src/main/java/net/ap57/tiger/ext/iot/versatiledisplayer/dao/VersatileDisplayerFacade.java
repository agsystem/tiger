/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.versatiledisplayer.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.iot.entity.Thing;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.std.iot.event.ThingRemoveStartEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class VersatileDisplayerFacade extends AbstractFacade<VersatileDisplayer> {

    @Inject
    private EntityManager em;

    @Inject
    @ThingRemoveStartEvent
    private Event<Thing> thingRemoveStartEvent;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VersatileDisplayerFacade() {
        super(VersatileDisplayer.class);
    }

    @Override
    public VersatileDisplayer createTransient(Map<String, Object> params) throws EJBException {
        VersatileDisplayer displayer = new VersatileDisplayer();
        return displayer;
    }

    @Override
    public Object getId(VersatileDisplayer entity) {
        return entity.getId();
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<VersatileDisplayer> root) {
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        switch(filterName) {
            case "currentDisplay":
                predicates.add(cb.equal(root.get("currentDisplay"), filterValue));
                break;
        }
        
        return predicates;
    }

    @Override
    public void remove(VersatileDisplayer entity) throws EJBException {
        thingRemoveStartEvent.fire(entity);
        super.remove(entity);
    }

    public VersatileDisplayer register(UUID sender, String ipAddr, String hostname) {
        
        VersatileDisplayer vd = find(sender);
        
        if (vd == null) {
            vd = createTransient(null);
            vd.setId(sender);
            vd.setAddress(ipAddr);
            vd.setHostname(hostname);
            vd.setDefaultDisplay("home");
            vd.setReady(true);
            create(vd);

        } else {
            vd.setAddress(ipAddr);
            vd.setHostname(hostname);
            vd.setReady(true);
            edit(vd);
        }
        
        return vd;
    }

}
