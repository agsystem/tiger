/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.versatiledisplayer.service;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import mdn.iotlib.comm.message.Message;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.event.HelpRequestEvent;
import net.ap57.tiger.ext.iot.linecounter.event.InvalidateCounterEvent;
import net.ap57.tiger.ext.iot.linecounter.event.PairSuccessEvent;
import net.ap57.tiger.ext.iot.linecounter.event.UnpairSuccessEvent;
import net.ap57.tiger.ext.iot.versatiledisplayer.dao.VersatileDisplayerFacade;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.ext.iot.versatiledisplayer.event.VersatileDisplayerSyncEvent;
import net.ap57.tiger.ext.utility.dao.WorkUnitDisplayerFacade;
import net.ap57.tiger.ext.utility.entity.WorkUnitDisplayer;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.iot.util.JsonUtil;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class LineCounterDisplayerService {

    @Inject
    private VersatileDisplayerFacade displayerFacade;

    @Inject
    private VersatileDisplayerService displayerService;

    @Inject
    private WorkUnitDisplayerFacade workUnitDisplayerFacade;

    @Inject
    private CounterPositionFacade counterPositionFacade;

    @Asynchronous
    public void onSync(@Observes @VersatileDisplayerSyncEvent Message msg) {
        String display = msg.getString();
        if (display.equals("LineCounter")) {
            VersatileDisplayer displayer = displayerFacade.find(msg.getSource());
            for (WorkUnitDisplayer wud : workUnitDisplayerFacade.findUsages(displayer)) {
                WorkUnit unit = wud.getWorkUnit();
                displayerService.invalidate(displayer, encode("sync-unit", unit));
                Map<String, Object> params = new HashMap<>();
                params.put("workUnit", unit);
                for (CounterPosition cp : counterPositionFacade.findAll(params)) {
                    displayerService.invalidate(displayer, encode("sync-position", cp));
                }
            }
        }
    }

    public void onPair(@Observes @PairSuccessEvent CounterPosition counterPosition) {
        displayerService.invalidatePair("LineCounter", encode("sync-position", counterPosition));
    }

    public void onUnpair(@Observes @UnpairSuccessEvent CounterPosition counterPosition) {
        displayerService.invalidatePair("LineCounter", encode("sync-position", counterPosition));
    }

    @Asynchronous
    public void onCounting(@Observes @InvalidateCounterEvent CounterPosition position) {
        displayerService.invalidateAll("LineCounter", encode("sync-position", position));
    }

    private String encode(String typeOfEvent, WorkUnit unit) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        JsonObject jsonObject = jsonObjectBuilder
                .add("type", typeOfEvent)
                .add("id", unit.getId())
                .add("label", unit.getName())
                .build();

        return JsonUtil.jsonToString(jsonObject);
    }

    private String encode(String typeOfEvent, CounterPosition position) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        JsonObject jsonObject = jsonObjectBuilder
                .add("type", typeOfEvent)
                .add("workUnit", position.getWorkUnit().getId())
                .add("positionNumber", Integer.toString(position.getPositionNumber()))
                .add("lastCount", Long.toString(position.getLastCount()))
                .add("status", position.getStatus())
                .build();

        return JsonUtil.jsonToString(jsonObject);
    }

}
