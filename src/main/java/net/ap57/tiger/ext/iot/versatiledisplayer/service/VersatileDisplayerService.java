/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.versatiledisplayer.service;

import java.util.UUID;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import mdn.iotlib.comm.message.Message;
import net.ap57.tiger.ext.iot.versatiledisplayer.dao.VersatileDisplayerFacade;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.ext.iot.versatiledisplayer.event.VersatileDisplayerOnMessageEvent;
import net.ap57.tiger.ext.iot.versatiledisplayer.event.VersatileDisplayerSyncEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class VersatileDisplayerService {

    @Inject
    private VersatileDisplayerFacade displayerFacade;

    @Inject
    private VersatileDisplayerSocket displayerSocket;

    @Inject
    @VersatileDisplayerSyncEvent
    private Event<Message> syncEvent;

    @Asynchronous
    public void route(@Observes @VersatileDisplayerOnMessageEvent Message msg) {
        int flag = msg.getFlag();
        if (flag == -1) {
            syncEvent.fire(msg);
        } else if (flag == 0) {
            String[] payload = msg.getString().split("\\|");
            String ipAddr = payload[0];
            String hostname = payload[1];
            register(msg.getSource(), ipAddr, hostname);
        } else if (flag == 1) {

        }
    }

    private void register(UUID sender, String ipAddr, String hostname) {
        VersatileDisplayer vd = displayerFacade.register(sender, ipAddr, hostname);
        show(vd.getDefaultDisplay(), vd);
    }

    @Asynchronous
    public void show(String display, VersatileDisplayer displayer) {
        Message msg = new Message(display.getBytes().length);
        msg.setTarget(displayer.getId());
        msg.setFlag(1);
        msg.putString(display);
        displayerSocket.show(msg);

        displayer.setCurrentDisplay(display);
        displayerFacade.edit(displayer);

    }

    @Asynchronous
    public void invalidate(VersatileDisplayer displayer, String content) {
        Message msg = new Message(content.getBytes().length);
        msg.setTarget(displayer.getId());
        msg.setFlag(-1);
        msg.putString(content);

        displayerSocket.invalidate(displayer, msg);
    }

    public void invalidatePair(String display, String content) {
        Message msg = new Message(content.getBytes().length);
        msg.setFlag(-1);
        msg.putString(content);

        displayerSocket.invalidateAll(display, msg);
    }

    @Asynchronous
    public void invalidateAll(String display, String content) {
        Message msg = new Message(content.getBytes().length);
        msg.setFlag(-1);
        msg.putString(content);

        displayerSocket.invalidateAll(display, msg);
    }
}
