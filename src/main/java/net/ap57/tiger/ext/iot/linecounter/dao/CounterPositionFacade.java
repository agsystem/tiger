/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterData;
import net.ap57.tiger.ext.utility.event.LineCounterPairEvent;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButton;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButtonId;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionId;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSampler;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class CounterPositionFacade extends AbstractFacade<CounterPosition> {

    @Inject
    private EntityManager em;

    @Inject
    private CounterDataFacade counterDataFacade;

    @Inject
    private CounterPositionUpdater updater;

    @Inject
    @LineCounterPairEvent
    private Event<CounterPosition> lineCounterPairEvent;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CounterPositionFacade() {
        super(CounterPosition.class);
    }

    @Override
    public CounterPosition createTransient(Map<String, Object> params) throws EJBException {
        CounterPosition position = new CounterPosition();

        return position;
    }

    @Override
    public Object getId(CounterPosition entity) {
        CounterPositionId id = new CounterPositionId();
        WorkUnit wu = entity.getWorkUnit();
        if (wu == null) {
            return null;
        }
        id.setWorkUnit(entity.getWorkUnit().getId());
        id.setPositionNumber(entity.getPositionNumber());
        return id;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<CounterPosition> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        List<Predicate> predicates = new ArrayList<>();

        switch (filterName) {
            case "workUnit":
                WorkUnit workUnit = (WorkUnit) filterValue;
                predicates.add(cb.equal(root.get("workUnit").get("id"), workUnit.getId()));
                break;
            default:
                break;
        }

        return predicates;
    }

    @Override
    protected void addCriteria(Root<CounterPosition> root, CriteriaQuery<CounterPosition> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.asc(root.get("positionNumber")));
    }

    @Override
    public List<CounterPosition> findAll(int startPosition, int maxResult, Map<String, Object> filters) {
        if (filters.get("workUnit") == null) {
            return new ArrayList<>();
        }
        return super.findAll(startPosition, maxResult, filters);
    }

    @Override
    public Long countAll(Map<String, Object> filters) {
        if (filters.get("workUnit") == null) {
            return 0l;
        }
        return super.countAll(filters);
    }

//    public void repair(CounterPosition position, LineCounterButton newCounter) {
//        
//        LineCounterButton foundCounter = counterFacade.find(counterFacade.getId(newCounter));
//        if (foundCounter != null) {
//            CounterPosition oldPosition = foundCounter.getPosition();
//            oldPosition.setCounterButton(null);
//            foundCounter.setPosition(null);
//            edit(oldPosition);
//            getEntityManager().flush();
//            lineCounterPairEvent.fire(oldPosition);
//            position.setCounterButton(foundCounter);
//            foundCounter.setPosition(position);
////            foundCounter.setLastCount(0);
//            getEntityManager().merge(foundCounter);
//            getEntityManager().flush();
//            edit(position);
//        } else {
//            newCounter.setPosition(position);
//            getEntityManager().persist(newCounter);
//            getEntityManager().flush();
//            position.setCounterButton(newCounter);
//            edit(position);
//        }
//
////        lineCounterPairEvent.fire(position);
//    }
    public LineCounterButton findCounter(String cid, String bid) {
        LineCounterButtonId id = new LineCounterButtonId();
//        id.setHostDevice(cid);
//        id.setInputNumber(bid);
//        return counterFacade.find(id);
        return null;
    }

    public void updateCounter(LineCounterButton counter) {
//        counterFacade.edit(counter);
    }

    public LineCounterButton incCounter(String cid, String bid) {
        LineCounterButton counter = findCounter(cid, bid);
//        if (counter != null) {
//            counterFacade.inc(counter);
//        }

        return counter;
    }

    public void resetAll(WorkUnit unit) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CounterPosition> cq = cb.createQuery(entityClass);
        Root<CounterPosition> counterPosition = cq.from(entityClass);
        cq.select(counterPosition).where(cb.equal(counterPosition.get("workUnit").get("id"), unit.getId()));

        TypedQuery<CounterPosition> q = getEntityManager().createQuery(cq);

        for (CounterPosition position : q.getResultList()) {
            getEntityManager().createQuery("DELETE FROM CounterData cd "
                    + "WHERE cd.counterPosition.workUnit.id = '" + unit.getId() + "' "
                    + "AND cd.counterPosition.positionNumber = " + position.getPositionNumber())
                    .executeUpdate();

            position.setCounterDatas(new ArrayList<>());
            position.setLastCount(0);
            getEntityManager().merge(position);
        }
    }

    public void reset(LineCounterButton counter) {

    }

    public void generatePosition(WorkUnit workUnit) {
        int num = 0;
        CounterPosition qc = createTransient(null);
        qc.setWorkUnit(workUnit);
        qc.setPositionNumber(num);
        qc.setLabel("QC");

        CounterPositionSampler qcSampler = new CounterPositionSampler();
        qcSampler.setCounterPosition(qc);
        qcSampler.setActive(false);
        qcSampler.setCounterPositionSamplerDatas(new ArrayList<>());

        qc.setSampler(qcSampler);

        create(qc);
        num = num + 1;

        for (int i = 1; i <= 23; i++) {
            CounterPosition pos = createTransient(null);
            pos.setWorkUnit(workUnit);
            pos.setPositionNumber(i);
            pos.setLabel(String.valueOf(num));
            pos.setEnabled(false);
            pos.setActive(false);

            CounterPositionSampler sampler = new CounterPositionSampler();
            sampler.setCounterPosition(pos);
            sampler.setActive(false);
            sampler.setCounterPositionSamplerDatas(new ArrayList<>());

            pos.setSampler(sampler);

            create(pos);
            num = num + 2;
        }

        num = 2;

        for (int i = 24; i <= 46; i++) {
            CounterPosition pos = createTransient(null);
            pos.setWorkUnit(workUnit);
            pos.setPositionNumber(i);
            pos.setLabel(String.valueOf(num));
            pos.setEnabled(false);
            pos.setActive(false);

            CounterPositionSampler sampler = new CounterPositionSampler();
            sampler.setCounterPosition(pos);
            sampler.setActive(false);
            sampler.setCounterPositionSamplerDatas(new ArrayList<>());

            pos.setSampler(sampler);

            create(pos);
            num = num + 2;
        }
    }

    public void addData(CounterPosition position, Date inputTime) {
        counterDataFacade.create(new CounterData(position, Calendar.getInstance().getTime()));
        updater.incCount(position);
    }

    public long countData(CounterPosition position) {
        TypedQuery<Long> q = getEntityManager().createNamedQuery("CounterData.count", Long.class)
                .setParameter("workUnit", position.getWorkUnit().getId())
                .setParameter("positionNumber", position.getPositionNumber());

        return q.getSingleResult();
    }

    public long countDataRange(CounterPosition position, Date fromDate, Date thruDate) {
        TypedQuery<Long> q = getEntityManager().createNamedQuery("CounterData.countRange", Long.class)
                .setParameter("workUnit", position.getWorkUnit().getId())
                .setParameter("positionNumber", position.getPositionNumber())
                .setParameter("fromDate", fromDate)
                .setParameter("thruDate", thruDate);

        return q.getSingleResult();
    }

}
