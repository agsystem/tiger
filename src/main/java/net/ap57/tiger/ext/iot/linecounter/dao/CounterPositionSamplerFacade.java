/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJBException;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionId;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSampler;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerData;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerId;
import net.ap57.tiger.ext.iot.linecounter.event.InvalidateSamplerEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class CounterPositionSamplerFacade extends AbstractFacade<CounterPositionSampler> {

    @Inject
    private EntityManager em;

    @Inject
    private CounterPositionFacade positionFacade;

//    @Inject @LineCounterSamplerEvent
//    private Event<CounterPosition> samplerEvent;
    @Inject
    @InvalidateSamplerEvent
    private Event<CounterPositionSamplerData> invalidateSamplerEvent;

    @Resource
    private TimerService timerService;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CounterPositionSamplerFacade() {
        super(CounterPositionSampler.class);
    }

    @Override
    public CounterPositionSampler createTransient(Map<String, Object> params) throws EJBException {
        CounterPositionSampler sampler = new CounterPositionSampler();

        return sampler;
    }

    @Override
    public Object getId(CounterPositionSampler entity) {
        CounterPositionSamplerId id = new CounterPositionSamplerId();
        id.setCounterPosition((CounterPositionId) positionFacade.getId(entity.getCounterPosition()));
        return id;
    }

    public void saveSampler(CounterPositionSampler sampler) {
        edit(sampler);
        getEntityManager().flush();
//        samplerEvent.fire(sampler.getCounterPosition());
    }

    public List<CounterPositionSampler> findAllActiveSampler() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CounterPositionSampler> cq = cb.createQuery(entityClass);
        Root<CounterPositionSampler> sampler = cq.from(entityClass);
        cq.select(sampler).where(cb.equal(sampler.get("active"), true));

        TypedQuery<CounterPositionSampler> q = getEntityManager().createQuery(cq);

        return q.getResultList();
    }

    public void activateSampler(CounterPositionSampler sampler) {
        sampler.setActive(true);
        edit(sampler);
        getEntityManager().flush();

        System.out.println("SAMPLING PERIOD = " + sampler.getSamplingPeriod());
//        ScheduleExpression sampling = new ScheduleExpression().second("0").minute("*/" + Long.toString(sampler.getSamplingPeriod())).hour("*");
//        Timer timer = timerService.createCalendarTimer(sampling, new TimerConfig(sampler, true));

        Calendar beginning = Calendar.getInstance();
        beginning.set(Calendar.HOUR_OF_DAY, 0);
        beginning.set(Calendar.MINUTE, 0);
        beginning.set(Calendar.SECOND, 0);
        beginning.set(Calendar.MILLISECOND, 0);
        Timer timer = timerService.createIntervalTimer(beginning.getTime(), sampler.getSamplingPeriod() * 60 * 1000, new TimerConfig(sampler, true));

        System.out.println("NEXT TIMEOUT = " + timer.getNextTimeout());

    }

    public void deactivateSampler(CounterPositionSampler sampler) {
        sampler.setActive(false);
        edit(sampler);
        getEntityManager().flush();

        timerService.getTimers().stream().forEach(timer -> {
            CounterPositionSampler timerSampler = (CounterPositionSampler) timer.getInfo();
            if (timerSampler.equals(sampler)) {
                timer.cancel();
            }
        });
    }

    @Timeout
    @Asynchronous
    public void sample(Timer timer) {

        Calendar current = Calendar.getInstance();
        current.set(Calendar.SECOND, 0);
        current.set(Calendar.MILLISECOND, 0);

        CounterPositionSampler sampler = (CounterPositionSampler) timer.getInfo();
        sampler = find(getId(sampler));

        long period = sampler.getSamplingPeriod();
        Date nextTimeout = timer.getNextTimeout();
        Date fromDate = new Date(nextTimeout.getTime() - (period * 2 * 60 * 1000));
        Date thruDate = new Date(nextTimeout.getTime() - (period * 60 * 1000));

        if (thruDate.before(current.getTime())) {
            return;
        }

        System.out.println("SAMPLER FROM  " + fromDate + " THRU " + thruDate);

        CounterPosition position = sampler.getCounterPosition();
        long count = positionFacade.countDataRange(position, fromDate, thruDate);

        CounterPositionSamplerData samplerData = new CounterPositionSamplerData();
        samplerData.setCounterPositionSampler(sampler);
        sampler.getCounterPositionSamplerDatas().add(samplerData);
        samplerData.setCount(count);
        samplerData.setDateTaken(thruDate);

        edit(sampler);
        invalidateSamplerEvent.fire(samplerData);

    }

    public void activateAllSampler() {
        for (CounterPositionSampler sampler : findAllActiveSampler()) {
            Calendar beginning = Calendar.getInstance();
            beginning.set(Calendar.HOUR_OF_DAY, 0);
            beginning.set(Calendar.MINUTE, 0);
            beginning.set(Calendar.SECOND, 0);
            beginning.set(Calendar.MILLISECOND, 0);
            Timer timer = timerService.createIntervalTimer(beginning.getTime(), sampler.getSamplingPeriod() * 60 * 1000, new TimerConfig(sampler, true));

            System.out.println("NEXT TIMEOUT = " + timer.getNextTimeout());
        }
    }

}
