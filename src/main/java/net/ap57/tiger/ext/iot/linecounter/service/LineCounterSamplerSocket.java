/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageDecoder;
import mdn.iotlib.comm.message.MessageEncoder;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerData;
import net.ap57.tiger.ext.iot.linecounter.event.InvalidateSamplerEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
@ServerEndpoint(
        value = "/websocket/linecounter/sampler",
        encoders = {MessageEncoder.class},
        decoders = {MessageDecoder.class}
)
public class LineCounterSamplerSocket {

    private Set<Session> sessions;

    public Set<Session> getSessions() {
        if (sessions == null) {
            sessions = Collections.synchronizedSet(new HashSet<>());
        }
        return sessions;
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        getSessions().add(session);
    }

    @OnMessage
    public void onMessage(@PathParam("unit") String unit, Session session, Message message) {
    }

    @OnClose
    public void onClose(Session session, CloseReason cr) {
            getSessions().remove(session);
    }
    
    @Asynchronous
    public void onInvalidateSampler(@Observes @InvalidateSamplerEvent CounterPositionSamplerData data) {
        Iterator<Session> iSession = getSessions().iterator();
        while(iSession.hasNext()) {
            Session s = iSession.next();
            if(s != null && s.isOpen()) {
                s.getAsyncRemote().sendText("invalidate");
            }
        }
    }

}
