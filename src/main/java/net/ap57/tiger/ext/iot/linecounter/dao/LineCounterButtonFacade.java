/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.dao;

import java.util.Date;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButton;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButtonId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class LineCounterButtonFacade extends AbstractFacade<LineCounterButton> {

    @Inject
    private EntityManager em;

    @Inject
    private CounterPositionFacade counterPositionFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LineCounterButtonFacade() {
        super(LineCounterButton.class);
    }

    @Override
    public LineCounterButton createTransient(Map<String, Object> params) throws EJBException {
        LineCounterButton counter = new LineCounterButton();

        return counter;
    }

    @Override
    public Object getId(LineCounterButton entity) {
        LineCounterButtonId id = new LineCounterButtonId();
        id.setHostDevice(entity.getHostDevice().getId());
        id.setInputNumber(entity.getInputNumber());
        return id;
    }

    public void inc(LineCounterButton counter) {
//        counter.setLastCount(counter.getLastCount() + 1);
        edit(counter);
    }

    public void reset(LineCounterButton counter) {
//        counter.setLastCount(0);
        edit(counter);
        System.out.println("AFTER EDIT");
    }

    public void pair(LineCounterButton counterButton, CounterPosition counterPosition) {
        if (counterPosition != null) {
            counterButton.setPosition(counterPosition);
            edit(counterButton);
        }
    }

    public CounterPosition repair(LineCounterButton button, CounterPosition position) {

        CounterPosition oldPosition = button.getPosition();
        if (oldPosition == null) {
            button.setPosition(position);
            position.setActive(true);
            position.setEnabled(true);
            edit(button);
            counterPositionFacade.edit(position);
        } else if (position != null && (!oldPosition.equals(position))) {
            button.setPosition(position);
            position.setActive(true);
            position.setEnabled(true);
            oldPosition.setActive(false);
            oldPosition.setEnabled(false);
            counterPositionFacade.edit(position);
            counterPositionFacade.edit(oldPosition);
            edit(button);
        }
        
        return oldPosition;

    }

    public void count(CounterPosition position, Date inputTime) {
        counterPositionFacade.addData(position, inputTime);
    }

}
