/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.ui.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Named;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
//import net.ap57.tiger.ext.utility.dao.CounterGroupCoverageFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.LineCounterDeviceFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionSamplerDataFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionSamplerFacade;
//import net.ap57.tiger.ext.utility.entity.CounterGroupCoverage;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSampler;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterDevice;
import net.ap57.tiger.ext.iot.linecounter.event.PairClearSelectionEvent;
import net.ap57.tiger.ext.iot.linecounter.event.PairSelectionEvent;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import org.primefaces.event.SelectEvent;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Named(value = "lineCounterPage")
@RequestScoped
@PageView(rel = "LineCounterPage")
public class LineCounterPage extends Page implements Serializable {

    @Inject
    private CounterPositionFacade counterPositionOp;

    @View
    private CounterPositionTable counterPositionTable;

    @Inject
    private LineCounterDeviceFacade counterDeviceOp;

    @View
    private LineCounterDeviceTable counterDeviceTable;

    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private DataTable workUnitSelector;

    @View
    private DataList workUnitList;

    @View
    private DataTable usedCounterDevice;

    @Inject
    private PositionFulfillmentFacade employeeOp;

    @View
    private DataTable employeeTable;

    @Inject
    private CounterPositionSamplerDataFacade samplerDataFacade;

    @Inject
    private CounterPositionSamplerFacade samplerFacade;

    @View
    private CounterPositionSamplerDataTable samplerDataTable;

    @Inject
    @PairSelectionEvent
    private Event<CounterPosition> pairSelectionEvent;

    @Inject
    @PairClearSelectionEvent
    private Event<WorkUnit> pairClearSelectionEvent;

    @Override
    protected void initLayout() {
        setEditor("Counter", "100%");
    }

    @Override
    protected void initComponents() {
        counterPositionTable = new CounterPositionTable() {

            @Override
            protected CounterPositionFacade getOp() {
                return counterPositionOp;
            }

            @Override
            protected LineCounterDeviceFacade getGroupOp() {
                return counterDeviceOp;
            }

            @Override
            public void onEmployeeSelect(SelectEvent evt) {
                PositionFulfillment pf = (PositionFulfillment) evt.getObject();
                getSelection().setEmployee(pf.getEmployee());
            }

            @Override
            protected Event<CounterPosition> getSelectionEventGenerator() {
                return pairSelectionEvent;
            }

            @Override
            protected Event<WorkUnit> getClearSelectionEventGenerator() {
                return pairClearSelectionEvent;
            }

        };

        counterDeviceTable = new LineCounterDeviceTable() {

            @Override
            protected LineCounterDeviceFacade getOp() {
                return counterDeviceOp;
            }

            @Override
            public void onCounterDeviceSelect(SelectEvent evt) {
                LineCounterDevice counterDevice = (LineCounterDevice) evt.getObject();
                counterDevice.setWorkUnit(counterPositionTable.getWorkUnit());
                update(counterDevice);
            }

        };

        workUnitSelector = new PFLazyDataTable<WorkUnitFacade, WorkUnit>() {

            @Override
            protected WorkUnitFacade getOp() {
                return workUnitOp;
            }

        };

        workUnitList = new DataList<WorkUnitFacade, WorkUnit>() {

            @Override
            public WorkUnitFacade getOp() {
                return workUnitOp;
            }

            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("name", value);
            }

        };

        usedCounterDevice = new PFLazyDataTable<LineCounterDeviceFacade, LineCounterDevice>() {

            @Override
            protected LineCounterDeviceFacade getOp() {
                return counterDeviceOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                WorkUnit selectedWorkUnit = counterPositionTable.getWorkUnit();
                if (selectedWorkUnit != null) {
                    filters.put("workUnit", selectedWorkUnit);
                }
                return filters;
            }

        };

        employeeTable = new PFLazyDataTable<PositionFulfillmentFacade, PositionFulfillment>() {

            @Override
            protected PositionFulfillmentFacade getOp() {
                return employeeOp;
            }

        };

        samplerDataTable = new CounterPositionSamplerDataTable() {
            @Override
            protected CounterPositionSamplerDataFacade getOp() {
                return samplerDataFacade;
            }

            @Override
            protected CounterPositionSamplerFacade getSamplerOp() {
                return samplerFacade;
            }

            @Override
            public CounterPositionSampler getSampler() {
                CounterPosition counterPosition = counterPositionTable.getSelection();
                if (counterPosition != null) {
                    return counterPosition.getSampler();
                } else {
                    return null;
                }
            }

        };
    }

}
