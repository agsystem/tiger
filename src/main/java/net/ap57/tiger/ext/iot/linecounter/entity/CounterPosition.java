/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.base.entity.StringKeyedEntity;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
@IdClass(CounterPositionId.class)
public class CounterPosition extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    private PartyRole workUnit;

    @Id
    private int positionNumber;

    private String label;

    @ManyToOne
    private PartyRole employee;

    private String machine;

    private String description;

    private boolean enabled;

    private boolean active;

    private long lastCount;

    @OneToMany(mappedBy = "counterPosition", cascade = CascadeType.ALL, orphanRemoval=true)
    @OrderBy("inputTime")
    private List<CounterData> counterDatas;

    @OneToOne(mappedBy = "counterPosition", cascade = CascadeType.ALL)
    private CounterPositionSampler sampler;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastError;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastCounting;

    public WorkUnit getWorkUnit() {
        return (WorkUnit) workUnit;
    }

    public void setWorkUnit(WorkUnit workUnit) {
        this.workUnit = workUnit;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public PartyRole getEmployee() {
        return employee;
    }

    public void setEmployee(PartyRole employee) {
        this.employee = employee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CounterData> getCounterDatas() {
        return counterDatas;
    }

    public void setCounterDatas(List<CounterData> counterDatas) {
        this.counterDatas = counterDatas;
    }

    public CounterPositionSampler getSampler() {
        return sampler;
    }

    public void setSampler(CounterPositionSampler sampler) {
        this.sampler = sampler;
    }

    public Date getLastError() {
        return lastError;
    }

    public void setLastError(Date lastError) {
        this.lastError = lastError;
    }

    public Date getLastCounting() {
        return lastCounting;
    }

    public void setLastCounting(Date lastCounting) {
        this.lastCounting = lastCounting;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public long getLastCount() {
        return lastCount;
    }

    public void setLastCount(long lastCount) {
        this.lastCount = lastCount;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStatus() {
        if (!isActive() && !isEnabled()) {
            return "counter-unused";
        } else if (isActive() && !isEnabled()) {
            return "counter-error";
        } else {
            return "counter-used";
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.workUnit);
        hash = 73 * hash + this.positionNumber;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CounterPosition other = (CounterPosition) obj;
        if (this.positionNumber != other.positionNumber) {
            return false;
        }
        if (!Objects.equals(this.workUnit, other.workUnit)) {
            return false;
        }
        return true;
    }

}
