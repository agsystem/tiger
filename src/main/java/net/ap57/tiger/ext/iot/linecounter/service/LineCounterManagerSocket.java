/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.service;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageDecoder;
import mdn.iotlib.comm.message.MessageEncoder;
import net.ap57.tiger.base.Application;
import net.ap57.tiger.base.event.ShutdownEvent;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
@ServerEndpoint(
        value = "/websocket/linecounter/manager/{unit}",
        encoders = {MessageEncoder.class},
        decoders = {MessageDecoder.class}
)
public class LineCounterManagerSocket {

    private Map<String, Session> sessions;

    private boolean isTryingToLockMoreThanOne = false;

    public Map<String, Session> getSessions() {
        if (sessions == null) {
            sessions = new ConcurrentHashMap<>();
        }
        return sessions;
    }

    @OnOpen
    public void onOpen(@PathParam("unit") String unit, Session session, EndpointConfig config) {
        Session existingSession = getSessions().get(unit);
        if (existingSession == null) {
            session.setMaxIdleTimeout(0);
            getSessions().put(unit, session);
        } else {
            try {
                isTryingToLockMoreThanOne = true;
                session.close();
            } catch (IOException ex) {
                Logger.getLogger(LineCounterManagerSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @OnMessage
    public void onMessage(@PathParam("unit") String unit, Session session, Message message) {
        
    }

    @OnClose
    public void onClose(@PathParam("unit") String unit, Session session, CloseReason cr) {
        if (!isTryingToLockMoreThanOne) {
            getSessions().remove(unit);
        }
        isTryingToLockMoreThanOne = false;
    }

    @OnError
    public void onError(@PathParam("unit") String unit, Session session, Throwable t) {
        Logger.getLogger(LineCounterManagerSocket.class.getName()).log(Level.SEVERE, null, t);
    }

    public void onApplicationShutdown(@Observes @ShutdownEvent Application application) {
        Iterator<String> is = getSessions().keySet().iterator();
        while (is.hasNext()) {
            String id = is.next();
            Session s = getSessions().get(id);
            try {
                s.close();
                getSessions().remove(id);
            } catch (IOException ex) {
                Logger.getLogger(LineCounterManagerSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void invalidate(WorkUnit unit, CounterPosition newPosition, CounterPosition oldPosition) {
        Session unitSession = getSessions().get(unit.getId());
        if (unitSession != null && unitSession.isOpen()) {
            RemoteEndpoint.Async sender = unitSession.getAsyncRemote();
            sender.setSendTimeout(1);
            if (oldPosition != null) {
                sender.sendText(String.valueOf(newPosition.getPositionNumber()) + "," + String.valueOf(oldPosition.getPositionNumber()));
            } else {
                sender.sendText(String.valueOf(newPosition.getPositionNumber()));
            }
        }
    }

}
