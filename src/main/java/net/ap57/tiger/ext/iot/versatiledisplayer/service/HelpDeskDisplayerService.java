/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.versatiledisplayer.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import mdn.iotlib.comm.message.Message;
import net.ap57.tiger.ext.iot.versatiledisplayer.dao.VersatileDisplayerFacade;
import net.ap57.tiger.ext.iot.versatiledisplayer.entity.VersatileDisplayer;
import net.ap57.tiger.ext.iot.versatiledisplayer.event.VersatileDisplayerSyncEvent;
import net.ap57.tiger.ext.utility.dao.HelpDeskCaseFacade;
import net.ap57.tiger.ext.utility.entity.HelpDeskCase;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseDeleteEvent;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseNewEvent;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseSolveEvent;
import net.ap57.tiger.ext.utility.event.HelpDeskCaseUpdateEvent;
import net.ap57.tiger.std.iot.util.JsonUtil;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class HelpDeskDisplayerService {

    @Inject
    private VersatileDisplayerFacade displayerFacade;

    @Inject
    private VersatileDisplayerService displayerService;

    @Inject
    private HelpDeskCaseFacade helpDeskCaseFacade;

    @Asynchronous
    public void onSync(@Observes @VersatileDisplayerSyncEvent Message msg) {
        String display = msg.getString();
        if (display.equals("HelpDesk")) {
            VersatileDisplayer displayer = displayerFacade.find(msg.getSource());
            for (HelpDeskCase runningCase : helpDeskCaseFacade.findAll()) {
                displayerService.invalidate(displayer, encode("new", runningCase));
            }
        }
    }

    public void onHelpDeskCaseNew(@Observes @HelpDeskCaseNewEvent HelpDeskCase newCase) {
        displayerService.invalidateAll("HelpDesk", encode("new", newCase));
    }

    public void onHelpDeskCaseSolve(@Observes @HelpDeskCaseSolveEvent HelpDeskCase solvedCase) {
        displayerService.invalidateAll("HelpDesk", encode("solve", solvedCase));
    }

    public void onHelpDeskCaseUpdate(@Observes @HelpDeskCaseUpdateEvent HelpDeskCase updatedCase) {
        displayerService.invalidateAll("HelpDesk", encode("update", updatedCase));
    }

    public void onHelpDeskCaseDelete(@Observes @HelpDeskCaseDeleteEvent HelpDeskCase deletedCase) {
        displayerService.invalidateAll("HelpDesk", encode("delete", deletedCase));
    }

    private String encode(String typeOfEvent, HelpDeskCase aCase) {
//        DateFormat sdf = new SimpleDateFormat("MMMMM d, yyyy HH:mm:ss");
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        JsonObject jsonObject = jsonObjectBuilder
                .add("type", typeOfEvent)
                .add("source", aCase.getSrcWorkUnit().getShortName())
                .add("destination", aCase.getDestWorkUnit().getShortName())
                .add("date", Long.toString(aCase.getSubmitDate().getTime()))
                .add("content", aCase.getContent())
                .build();

        return JsonUtil.jsonToString(jsonObject);
    }

}
