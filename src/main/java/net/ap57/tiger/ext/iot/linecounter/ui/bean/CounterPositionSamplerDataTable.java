/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.ui.bean;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionSamplerDataFacade;
import net.ap57.tiger.ext.iot.linecounter.dao.CounterPositionSamplerFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSampler;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPositionSamplerData;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class CounterPositionSamplerDataTable extends PFLazyDataTable<CounterPositionSamplerDataFacade, CounterPositionSamplerData> {

    protected abstract CounterPositionSamplerFacade getSamplerOp();

    public abstract CounterPositionSampler getSampler();

    public Date getSampleFrom() {
        Calendar defCal = Calendar.getInstance();
        defCal.set(Calendar.HOUR_OF_DAY, 0);
        defCal.set(Calendar.MINUTE, 0);
        defCal.set(Calendar.SECOND, 0);
        return (Date) getAttribute("sampleFrom", defCal.getTime());
    }

    public void setSampleFrom(Date sampleFrom) {
        setAttribute("sampleFrom", sampleFrom);
    }

    public Date getSampleThru() {
        Calendar defCal = Calendar.getInstance();
        defCal.set(Calendar.HOUR_OF_DAY, 23);
        defCal.set(Calendar.MINUTE, 59);
        defCal.set(Calendar.SECOND, 59);
        return (Date) getAttribute("sampleThru", defCal.getTime());
    }

    public void setSampleThru(Date sampleThru) {
        setAttribute("sampleThru", sampleThru);
    }

    public void setActivated(boolean activated) {
        setAttribute("activated", activated);
    }

    public boolean getActivated() {
        return (boolean) getAttribute("activated", false);
    }

    @Override
    protected Map<String, Object> getLoadFilters() {
        Map<String, Object> filters = new HashMap<>();
        CounterPositionSampler sampler = getSampler();
        if (sampler != null) {
            filters.put("counterPositionSampler", sampler);
            filters.put("sampleFrom", getSampleFrom());
            filters.put("sampleThru", getSampleThru());
        }
        return filters;
    }

    public void onActivatedSwitching() {
        if (getSampler().isActive()) {
            getSampler().setActive(true);
            RequestContext.getCurrentInstance().execute("PF('samplingPrefsDlg').show()");
        } else {
            getSampler().setActive(false);
            deactivateSampler();
//            saveSampler(null);
        }
    }

//    public void saveSampler(ActionEvent evt) {
//        getSamplerOp().saveSampler(getSampler());
//    }
    
    public void activateSampler(ActionEvent evt) {
        getSamplerOp().activateSampler(getSampler());
    }
    
    public void deactivateSampler() {
        getSamplerOp().deactivateSampler(getSampler());
    }

}
