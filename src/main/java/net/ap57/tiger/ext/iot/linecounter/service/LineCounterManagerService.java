/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import net.ap57.tiger.ext.iot.linecounter.dao.LineCounterButtonFacade;
import net.ap57.tiger.ext.iot.linecounter.entity.CounterPosition;
import net.ap57.tiger.ext.iot.linecounter.entity.LineCounterButton;
import net.ap57.tiger.ext.iot.linecounter.event.PairClearSelectionEvent;
import net.ap57.tiger.ext.iot.linecounter.event.PairRequestEvent;
import net.ap57.tiger.ext.iot.linecounter.event.PairSelectionEvent;
import net.ap57.tiger.ext.iot.linecounter.event.PairSuccessEvent;
import net.ap57.tiger.ext.iot.linecounter.event.UnpairSuccessEvent;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
public class LineCounterManagerService {

    @Inject
    private LineCounterManagerSocket managerSocket;

    @Inject
    private LineCounterButtonFacade buttonFacade;

    @Inject
    @PairSuccessEvent
    private Event<CounterPosition> pairSuccessEvent;

    @Inject
    @UnpairSuccessEvent
    private Event<CounterPosition> unpairSuccessEvent;

    public Map<String, CounterPosition> selectedPositions;

    public Map<String, CounterPosition> getSelectedPositions() {
        if (selectedPositions == null) {
            selectedPositions = new ConcurrentHashMap<>();
        }
        return selectedPositions;
    }

    public void onPairRequest(@Observes @PairRequestEvent LineCounterButton button) throws InterruptedException {
        WorkUnit unit = button.getHostDevice().getWorkUnit();

        if (unit != null) {
            CounterPosition oldPosition = button.getPosition();
            CounterPosition newPosition = getSelectedPositions().get(unit.getId());
            if (newPosition.equals(oldPosition)) {
                return;
            } else {
                oldPosition = buttonFacade.repair(button, newPosition);
                managerSocket.invalidate(unit, newPosition, oldPosition);
                pairSuccessEvent.fire(newPosition);
                if (oldPosition != null) {
                    System.out.println("UPDATE OLD POSITION");
                    unpairSuccessEvent.fire(oldPosition);
                }
            }
        }
    }

    public void onPairSelectionEvent(@Observes @PairSelectionEvent CounterPosition position) {
        getSelectedPositions().put(position.getWorkUnit().getId(), position);
    }

    public void onPairClearSelectionEvent(@Observes @PairClearSelectionEvent WorkUnit unit) {
        getSelectedPositions().remove(unit.getId());
    }

}
