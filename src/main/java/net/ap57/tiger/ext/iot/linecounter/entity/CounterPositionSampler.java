/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.ext.iot.linecounter.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
@IdClass(CounterPositionSamplerId.class)
public class CounterPositionSampler implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @OneToOne
    private CounterPosition counterPosition;
    
    private boolean active;
    
    private long samplingPeriod;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date activeFrom;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date activeThru;

    @OneToMany(mappedBy = "counterPositionSampler", cascade=CascadeType.ALL)
    private List<CounterPositionSamplerData> counterPositionSamplerDatas;

    public CounterPosition getCounterPosition() {
        return counterPosition;
    }

    public void setCounterPosition(CounterPosition counterPosition) {
        this.counterPosition = counterPosition;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getSamplingPeriod() {
        return samplingPeriod;
    }

    public void setSamplingPeriod(long samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Date getActiveThru() {
        return activeThru;
    }

    public void setActiveThru(Date activeThru) {
        this.activeThru = activeThru;
    }

    public List<CounterPositionSamplerData> getCounterPositionSamplerDatas() {
        return counterPositionSamplerDatas;
    }

    public void setCounterPositionSamplerDatas(List<CounterPositionSamplerData> counterPositionSamplerDatas) {
        this.counterPositionSamplerDatas = counterPositionSamplerDatas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.counterPosition);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CounterPositionSampler other = (CounterPositionSampler) obj;
        if (!Objects.equals(this.counterPosition, other.counterPosition)) {
            return false;
        }
        return true;
    }
    
}
