/*
 * Copyright 2015 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.system.ui.bean;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFPickList;
import net.ap57.tiger.std.system.dao.SecurityGroupFacade;
import net.ap57.tiger.std.system.dao.SecurityUserFacade;
import net.ap57.tiger.std.system.entity.SecurityGroup;
import net.ap57.tiger.std.system.entity.SecurityMap;
import net.ap57.tiger.std.system.entity.SecurityUser;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author mbahbejo
 */
@Named(value = "userPage")
@RequestScoped
@PageView(rel = "User")
public class UserPage extends Page {

    @Inject
    private SecurityUserFacade userOp;

    @View
    private SecurityUserTable userTable;

    @Inject
    private SecurityGroupFacade groupOp;

    @View
    private PFPickList groupPickList;

    @Override
    protected void initLayout() {
        setEditor("User", "100%");
    }

    @Override
    protected void initComponents() {
        
        userTable = new SecurityUserTable() {
            @Override
            protected SecurityUserFacade getOp() {
                return userOp;
            }
        };

        groupPickList = new PFPickList<SecurityGroupFacade, SecurityGroup>() {

            @Override
            protected SecurityGroupFacade getOp() {
                return groupOp;
            }

            @Override
            protected boolean isEqual(SecurityGroup t1, SecurityGroup t2) {
                return t1.getGroupname().equals(t2.getGroupname());
            }

            @Override
            public List<SecurityGroup> getTarget() {
                List<SecurityGroup> securityGroups = new ArrayList<>();
 
                SecurityUser user = userTable.getSelection();

                if (user != null) {
                    for (SecurityMap userMap : user.getSecurityMaps()) {
                        securityGroups.add(userMap.getSecurityGroup());
                    }
                }

                return securityGroups;
            }

            @Override
            protected void addItems(List<SecurityGroup> items) {
                SecurityUser user = userTable.getSelection();
                List<SecurityMap> sMaps = user.getSecurityMaps();
                for (SecurityGroup group : items) {
                    sMaps.add(new SecurityMap(user, group));
                }  
            }

            @Override
            protected void delItems(List<SecurityGroup> items) {
                
                List<SecurityMap> remMaps = new ArrayList<>();              
                
                SecurityUser user = userTable.getSelection();
                
                List<SecurityMap> sMaps = user.getSecurityMaps();
                for(SecurityMap sMap : sMaps) {
                    if(items.contains(sMap.getSecurityGroup())) {
                        remMaps.add(sMap);
                    }
                }
                
                for(SecurityMap remMap : remMaps) {
                    sMaps.remove(remMap);
                }                
                
                
            }

            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("groupname", value);
            }

        };
    }

}
