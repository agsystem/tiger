/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.system;

import java.util.ArrayList;
import java.util.Calendar;
import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import net.ap57.tiger.base.Application;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.dao.OrganizationFacade;
import net.ap57.tiger.base.dao.PartyRoleTypeFacade;
import net.ap57.tiger.base.dao.PersonFacade;
import net.ap57.tiger.base.entity.Organization;
import net.ap57.tiger.base.entity.PartyRoleType;
import net.ap57.tiger.base.entity.Person;
import net.ap57.tiger.base.event.FrameworkReadyEvent;
import net.ap57.tiger.std.system.bean.SecurityUtil;
import net.ap57.tiger.std.system.entity.SecurityGroup;
import net.ap57.tiger.std.system.entity.SecurityMap;
import net.ap57.tiger.std.system.entity.SecurityUser;
import net.ap57.tiger.base.event.BootstrapEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
public class SystemModule extends Module {

    @Inject
    private PartyRoleTypeFacade roleTypeFacade;

    @Inject
    private OrganizationFacade orgFacade;

    @Inject
    private PersonFacade personFacade;

    @Inject
    private SecurityUtil securityUtil;

    @Inject
    @FrameworkReadyEvent
    private Event<Application> fwReadyEvent;

    public void boot(@Observes @BootstrapEvent Application application) throws Exception {

        Calendar cal = Calendar.getInstance();

        PartyRoleType securityGroupType = roleTypeFacade.findSingleByAttribute("name", "Security Group");

        if (securityGroupType == null) {

            Organization orgUser = new Organization();
            orgUser.setRoles(new ArrayList<>());

            Organization orgAdministrator = new Organization();
            orgAdministrator.setRoles(new ArrayList<>());

            securityGroupType = roleTypeFacade.createTransient(null);
            securityGroupType.setName("Security Group");
            roleTypeFacade.create(securityGroupType);

            SecurityGroup user = new SecurityGroup();
            user.setOrganization(orgUser);
            user.setPartyRoleType(securityGroupType);
            user.setGroupname("User");
            user.setFromDate(cal.getTime());
            user.setSecurityMap(new ArrayList<>());
            orgUser.getRoles().add(user);

            SecurityGroup administrator = new SecurityGroup();
            administrator.setParty(orgAdministrator);
            administrator.setPartyRoleType(securityGroupType);
            administrator.setGroupname("Administrator");
            administrator.setFromDate(cal.getTime());
            administrator.setSecurityMap(new ArrayList<>());
            orgAdministrator.getRoles().add(administrator);

            Organization orgSystem = new Organization();
            orgSystem.setRoles(new ArrayList<>());

            SecurityGroup system = new SecurityGroup();
            system.setParty(orgSystem);
            system.setPartyRoleType(securityGroupType);
            system.setGroupname("System");
            system.setFromDate(cal.getTime());
            system.setSecurityMap(new ArrayList<>());
            orgSystem.getRoles().add(system);

            PartyRoleType securityUserType = roleTypeFacade.findSingleByAttribute("name", "Security User");

            if (securityUserType == null) {

                Person personAdmin = new Person();
                personAdmin.setFirstName("admin");
                personAdmin.setRoles(new ArrayList<>());

                securityUserType = roleTypeFacade.createTransient(null);
                securityUserType.setName("Security User");
                roleTypeFacade.create(securityUserType);

                SecurityUser admin = new SecurityUser();
                admin.setParty(personAdmin);
                admin.setPartyRoleType(securityUserType);
                admin.setFromDate(cal.getTime());
                admin.setUsername("admin");
                admin.setPassword(securityUtil.generatePassword("1"));
                admin.setSecurityMaps(new ArrayList<>());
                personAdmin.getRoles().add(admin);

                SecurityMap adminUser = new SecurityMap();
                adminUser.setSecurityUser(admin);
                adminUser.setUsername(admin.getUsername());
                adminUser.setSecurityGroup(user);
                adminUser.setGroupname(user.getGroupname());
                admin.getSecurityMaps().add(adminUser);
                user.getSecurityMap().add(adminUser);

                SecurityMap adminAdministrator = new SecurityMap();
                adminAdministrator.setSecurityUser(admin);
                adminAdministrator.setUsername(admin.getUsername());
                adminAdministrator.setSecurityGroup(administrator);
                adminAdministrator.setGroupname(administrator.getGroupname());
                admin.getSecurityMaps().add(adminAdministrator);
                administrator.getSecurityMap().add(adminAdministrator);

                Person personSystem = new Person();
                personSystem.setFirstName("system");
                personSystem.setRoles(new ArrayList<>());

                SecurityUser systemUser = new SecurityUser();
                systemUser.setParty(personSystem);
                systemUser.setPartyRoleType(securityUserType);
                systemUser.setFromDate(cal.getTime());
                systemUser.setUsername("system");
                systemUser.setPassword(securityUtil.generatePassword("1"));
                systemUser.setSecurityMaps(new ArrayList<>());
                personSystem.getRoles().add(systemUser);

                SecurityMap systemSystem = new SecurityMap();
                systemSystem.setSecurityUser(systemUser);
                systemSystem.setUsername(systemUser.getUsername());
                systemSystem.setSecurityGroup(system);
                systemSystem.setGroupname(system.getGroupname());
                systemUser.getSecurityMaps().add(systemSystem);
                system.getSecurityMap().add(systemSystem);

                orgFacade.create(orgAdministrator);
                orgFacade.create(orgUser);
                orgFacade.create(orgSystem);
                personFacade.create(personAdmin);
                personFacade.create(personSystem);

            }

        }

        fwReadyEvent.fire(application);

    }

}
