/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.system.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.base.dao.PartyFacade;
import net.ap57.tiger.base.dao.PartyRoleTypeFacade;
import net.ap57.tiger.base.dao.PersonFacade;
import net.ap57.tiger.base.dao.PersonRoleFacade;
import net.ap57.tiger.base.entity.Person;
import net.ap57.tiger.std.system.bean.SecurityUtil;
import net.ap57.tiger.std.system.entity.SecurityMap;
import net.ap57.tiger.std.system.entity.SecurityUser;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 *
 */
@Stateless
public class SecurityUserFacade extends PersonRoleFacade<SecurityUser, Person> {
    
    @Inject
    private EntityManager em;
    
    @Inject
    private PersonFacade partyFacade;
    
    @Inject
    private PartyRoleTypeFacade partyRoleTypeFacade;
    
    @Inject
    private SecurityUtil securityUtil;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public SecurityUserFacade() {
        super(SecurityUser.class);
    }
    
    @Override
    public SecurityUser createTransient() {
        SecurityUser user = new SecurityUser();
        user.setSecurityMaps(new ArrayList<>());
        return user;
    }
    
    @Override
    public Object getId(SecurityUser entity) {
        return entity.getId();
    }
    
    @Override
    protected PartyRoleTypeFacade getRoleTypeFacade() {
        return partyRoleTypeFacade;
    }
    
    @Override
    protected PartyFacade getPartyFacade() {
        return partyFacade;
    }
    
    @Override
    protected String getRoleTypeName() {
        return "Security User";
    }
    
    @Override
    public void create(SecurityUser partyRole) throws EJBException {
        partyRole.setPassword(securityUtil.generatePassword(partyRole.getPassword()));
        for (SecurityMap map : partyRole.getSecurityMaps()) {
            map.setUsername(partyRole.getUsername());
        }
        super.create(partyRole);
    }
    
    public void changePassword(SecurityUser user) {
        String plainPassword = user.getPassword();
        if (plainPassword != null && !plainPassword.isEmpty()) {
            String newPassword = securityUtil.generatePassword(plainPassword);
            user.setPassword(newPassword);
            edit(user);
        }
        
    }
    
    public void editMembership(SecurityUser user) {
        List<SecurityMap> newMaps = user.getSecurityMaps();
        
        user = find(user.getId());
        List<SecurityMap> oldMaps = user.getSecurityMaps();
        
        List<SecurityMap> nextRemovedMaps = new ArrayList<>();
        
        for(SecurityMap oldMap : oldMaps) {
            if(!newMaps.contains(oldMap)) {
                nextRemovedMaps.add(oldMap);
            }
        }
        
        for(SecurityMap nextRemovedMap : nextRemovedMaps) {
            user.getSecurityMaps().remove(nextRemovedMap);
            nextRemovedMap.getSecurityGroup().getSecurityMap().remove(nextRemovedMap);
            getEntityManager().remove(getEntityManager().merge(nextRemovedMap));
        }
        
        for(SecurityMap newMap : newMaps) {
            if(!oldMaps.contains(newMap)) {
                oldMaps.add(newMap);
            }
        }
        
        edit(user);
    }
    
}
