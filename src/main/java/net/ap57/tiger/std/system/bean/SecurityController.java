/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.system.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import net.ap57.tiger.base.ui.event.PageChangeEvent;

/**
 *
 * @author mbahbejo
 */
@Named
@RequestScoped
public class SecurityController {

    @Inject
    private Event<PageChangeEvent> pageChangeEvent;

    public boolean isLogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return request.getUserPrincipal() != null ? true : false;
    }

    public String logout() {
        pageChangeEvent.fire(new PageChangeEvent());
        String result = "/login?faces-redirect=true";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.logout();
        } catch (ServletException ex) {
            Logger.getLogger(SecurityController.class.getName()).log(Level.SEVERE, null, ex);
            result = "/loginError?faces-redirect=true";
        }
        return result;
    }

}
