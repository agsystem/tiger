/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */

package net.ap57.tiger.std.accounting.dao;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.GLExpenseAccount;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class GLExpenseAccountFacade extends AbstractFacade<GLExpenseAccount> {
    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GLExpenseAccountFacade() {
        super(GLExpenseAccount.class);
    }

        public Long generateCode() {
        TypedQuery<Long> query = getEntityManager().createNamedQuery("ExpenseAccount.getMax", Long.class);
        try {
            
            Long result = query.getSingleResult();
            if(result != null)
            return result + 1;
            else return 1L;
        } catch (NoResultException ex) {
            return 1L;
        }
    }

    @Override
    public void create(GLExpenseAccount entity) {
//        entity.setCode(generateCode());
        super.create(entity);
    }

    @Override
    public GLExpenseAccount createTransient(Map<String, Object> params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getId(GLExpenseAccount entity) {
        return entity.getId();
    }
    
}
