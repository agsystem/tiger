/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.AccountingPeriod;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetail;
import net.ap57.tiger.std.accounting.entity.GLAccount;
import net.ap57.tiger.std.accounting.entity.GLExpenseAccount;
import net.ap57.tiger.std.accounting.entity.GLRevenueAccount;
import net.ap57.tiger.std.accounting.entity.IncomeStatement;
import net.ap57.tiger.std.accounting.entity.IncomeStatementExpenseDetail;
import net.ap57.tiger.std.accounting.entity.IncomeStatementId;
import net.ap57.tiger.std.accounting.entity.IncomeStatementRevenueDetail;
import net.ap57.tiger.std.accounting.event.NewAccountingPeriodEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class IncomeStatementFacade extends AbstractFacade<IncomeStatement> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IncomeStatementFacade() {
        super(IncomeStatement.class);
    }

    @Override
    public IncomeStatement createTransient(Map<String, Object> params) throws EJBException {
        IncomeStatement incomeStatement = new IncomeStatement();
        incomeStatement.setAccountingPeriod((AccountingPeriod) params.get("accountingPeriod"));
        incomeStatement.setRevenueDetails(new ArrayList<>());
        incomeStatement.setExpenseDetails(new ArrayList<>());

        return incomeStatement;
    }

    @Override
    public Object getId(IncomeStatement entity) {
        IncomeStatementId id = new IncomeStatementId();
        id.setAccountingPeriod(entity.getAccountingPeriod().getId());

        return id;
    }

    public IncomeStatement getStatement(AccountingPeriod period) {
        IncomeStatementId id = new IncomeStatementId();
        id.setAccountingPeriod(period.getId());
        IncomeStatement statement = find(id);

        if (statement == null) {
            statement = buildStatement(period);
            create(statement);
        }

        getEntityManager().flush();

        return statement;

    }

    private IncomeStatement buildStatement(AccountingPeriod period) {

        Map<String, Object> params = new HashMap<>();

        IncomeStatement statement = createTransient(params);
        statement.setAccountingPeriod(period);

        List<IncomeStatementRevenueDetail> revenueDetails = getAllRevenueTrans(period);
        for (IncomeStatementRevenueDetail revenueDetail : revenueDetails) {
            revenueDetail.setIncomeStatement(statement);
        }

        List<IncomeStatementExpenseDetail> expenseDetails = getAllExpenseTrans(period);
        for (IncomeStatementExpenseDetail expenseDetail : expenseDetails) {
            expenseDetail.setIncomeStatement(statement);
        }

        statement.setRevenueDetails(revenueDetails);
        statement.setExpenseDetails(expenseDetails);

        return statement;
    }

    private List<IncomeStatementRevenueDetail> getAllRevenueTrans(AccountingPeriod period) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<IncomeStatementRevenueDetail> cq = cb.createQuery(IncomeStatementRevenueDetail.class);
        Root<AcctgTrans> acctgTrans = cq.from(AcctgTrans.class);
        Join<AcctgTrans, AcctgTransDetail> acctgTransDetail = acctgTrans.join("acctgTransDetails");
        cq.select(cb.construct(IncomeStatementRevenueDetail.class, acctgTransDetail.get("account"), cb.toBigDecimal(cb.sum(acctgTransDetail.get("amount").get("amount")))))
                .where(new Predicate[]{
            cb.greaterThanOrEqualTo(acctgTrans.get("transactionDate"), period.getFromDate()),
            cb.lessThanOrEqualTo(acctgTrans.get("transactionDate"), period.getThruDate()),
            cb.equal(acctgTransDetail.get("account").type(), GLRevenueAccount.class)
        })
                .groupBy(acctgTransDetail.get("account")).orderBy(cb.asc(acctgTransDetail.get("account").get("code")));

        TypedQuery<IncomeStatementRevenueDetail> q = getEntityManager().createQuery(cq);

        List<IncomeStatementRevenueDetail> result = q.getResultList();
        List<GLRevenueAccount> accounts = getAllRevenueAccount();
        for (IncomeStatementRevenueDetail detail : result) {
            accounts.remove(detail.getAccount());
        }

        for (GLRevenueAccount blankAccount : accounts) {
            result.add(new IncomeStatementRevenueDetail(blankAccount, BigDecimal.ZERO));
        }

        return result;

    }

    private List<GLRevenueAccount> getAllRevenueAccount() {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<GLRevenueAccount> cq = cb.createQuery(GLRevenueAccount.class);

        Root<GLAccount> account = cq.from(GLAccount.class);
        cq.select(cb.treat(account, GLRevenueAccount.class))
                .where(cb.notEqual(account.get("placeholder"), true))
                .orderBy(cb.asc(account.get("code")));

        TypedQuery<GLRevenueAccount> q = getEntityManager().createQuery(cq);

        return q.getResultList();

    }

    private List<IncomeStatementExpenseDetail> getAllExpenseTrans(AccountingPeriod period) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<IncomeStatementExpenseDetail> cq = cb.createQuery(IncomeStatementExpenseDetail.class);

        Root<AcctgTrans> acctgTrans = cq.from(AcctgTrans.class);
        Join<AcctgTrans, AcctgTransDetail> acctgTransDetail = acctgTrans.join("acctgTransDetails");
        cq.select(cb.construct(IncomeStatementExpenseDetail.class, acctgTransDetail.get("account"), cb.toBigDecimal(cb.sum(acctgTransDetail.get("amount").get("amount")))))
                .where(new Predicate[]{
            cb.greaterThanOrEqualTo(acctgTrans.get("transactionDate"), period.getFromDate()),
            cb.lessThanOrEqualTo(acctgTrans.get("transactionDate"), period.getThruDate()),
            cb.equal(acctgTransDetail.get("account").type(), GLExpenseAccount.class)
        })
                .groupBy(acctgTransDetail.get("account")).orderBy(cb.asc(acctgTransDetail.get("account").get("code")));

        TypedQuery<IncomeStatementExpenseDetail> q = getEntityManager().createQuery(cq);

        List<IncomeStatementExpenseDetail> result = q.getResultList();
        List<GLExpenseAccount> accounts = getAllExpenseAccount();
        for (IncomeStatementExpenseDetail detail : result) {
            accounts.remove(detail.getAccount());
        }

        for (GLExpenseAccount blankAccount : accounts) {
            result.add(new IncomeStatementExpenseDetail(blankAccount, BigDecimal.ZERO));
        }

        return result;

    }

    private List<GLExpenseAccount> getAllExpenseAccount() {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<GLExpenseAccount> cq = cb.createQuery(GLExpenseAccount.class);

        Root<GLAccount> account = cq.from(GLAccount.class);
        cq.select(cb.treat(account, GLExpenseAccount.class))
                .where(cb.notEqual(account.get("placeholder"), true))
                .orderBy(cb.asc(account.get("code")));

        TypedQuery<GLExpenseAccount> q = getEntityManager().createQuery(cq);

        return q.getResultList();

    }

    public void refreshStatement(AccountingPeriod period) {

    }

    public List<AccountingPeriod> getTransactionContexts(Date trxDate) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AccountingPeriod> cq = cb.createQuery(AccountingPeriod.class);

        Root<AccountingPeriod> accountingPeriod = cq.from(AccountingPeriod.class);
        cq.select(accountingPeriod).where(new Predicate[]{
            cb.greaterThanOrEqualTo(accountingPeriod.get("thruDate"), trxDate),
            cb.lessThanOrEqualTo(accountingPeriod.get("fromDate"), trxDate)
        });

        TypedQuery<AccountingPeriod> q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public IncomeStatement getStatements(AccountingPeriod period) {
        IncomeStatementId id = new IncomeStatementId();
        id.setAccountingPeriod(period.getId());
        IncomeStatement statement = find(id);

        return statement;
    }
    
    public void onNewAccountingPeriodEvent(@Observes @NewAccountingPeriodEvent AccountingPeriod accountingPeriod) {
        buildStatement(accountingPeriod);
    }

}
