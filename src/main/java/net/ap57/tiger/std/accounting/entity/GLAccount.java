/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import net.ap57.tiger.base.api.HierarchicalEntity;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "GLACCOUNTTYPE_ID", discriminatorType = DiscriminatorType.INTEGER)
public abstract class GLAccount extends StringKeyedEntity implements Serializable, Comparable<GLAccount>, HierarchicalEntity<GLAccount> {

    private static final long serialVersionUID = 1L;

    @Id
    @TableGenerator(name = "GLAccount", table = "KEYGEN")
    @GeneratedValue(generator = "GLAccount", strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable=false, unique = true)
    private String name;

    private String description;
    
    private Long code;

    private boolean placeholder;
    
    @ManyToOne
    private Currency currency;
    
    @ManyToOne
    private GLAccount parent;

    @OneToMany(mappedBy = "parent", cascade=CascadeType.ALL)
    @OrderBy("code")
    private List<GLAccount> children;

    @OneToMany(mappedBy = "account")
    private List<AcctgTransDetail> acctgTransDetails;

    @OneToMany(mappedBy = "glAccount")
    private List<OrganizationAccount> organizationAccounts;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public boolean isPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(boolean placeholder) {
        this.placeholder = placeholder;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public GLAccount getParent() {
        return parent;
    }

    public void setParent(GLAccount parent) {
        this.parent = parent;
    }

    @Override
    public List<GLAccount> getChildren() {
        return children;
    }

    @Override
    public void setChildren(List<GLAccount> children) {
        this.children = children;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AcctgTransDetail> getAcctgTransDetails() {
        return acctgTransDetails;
    }

    public void setAcctgTransDetails(List<AcctgTransDetail> acctgTransDetails) {
        this.acctgTransDetails = acctgTransDetails;
    }

    public List<OrganizationAccount> getOrganizationAccounts() {
        return organizationAccounts;
    }

    public void setOrganizationAccounts(List<OrganizationAccount> organizationAccounts) {
        this.organizationAccounts = organizationAccounts;
    }

//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Object Methods">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GLAccount)) {
            return false;
        }
        GLAccount other = (GLAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Transients">
//    @Transient
//    private Object wrapper;
//    
//    @Override
//    public Object getWrapper() {
//        return wrapper;
//    }
//    
//    @Override
//    public void setWrapper(Object wrapper) {
//        this.wrapper = wrapper;
//    }
    
    public String getType() {
        if (this instanceof GLAssetAccount) {
            return "Asset";
        } else if (this instanceof GLLiabilityAccount) {
            return "Liability";
        } else if (this instanceof GLEquityAccount) {
            return "Equity";
        } else if (this instanceof GLRevenueAccount) {
            return "Revenue";
        } else if (this instanceof GLExpenseAccount) {
            return "Expense";
        } else return null;
    }
    
    @Transient
    private boolean used;
    
    public boolean isUsed() {
        return used;
    }
    
    public void setUsed(boolean used) {
        this.used = used;
    }
    
    @Transient
    private Date from;
    
    public Date getFrom() {
        return from;
    }
    
    public void setFrom(Date from) {
        this.from = from;
    }
    
    @Transient Date thru;
    
    public Date getThru() {
        return thru;
    }
    
    public void setThru(Date thru) {
        this.thru = thru;
    }
    
    public String getCanonicalName() {
        String cname = name;
        
        if(parent != null) {
            cname = parent.getCanonicalName() + ":" + cname;
        }
        
        return cname;
    }
//</editor-fold>

    @Override
    public int compareTo(GLAccount o) {
        return this.getCode().compareTo(o.getCode());
    }

}
