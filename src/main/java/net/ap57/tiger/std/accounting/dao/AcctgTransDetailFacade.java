/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.dao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetail;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetailId;
import net.ap57.tiger.std.accounting.entity.GLAccount;
import net.ap57.tiger.std.accounting.entity.GLBalance;
import net.ap57.tiger.std.accounting.entity.Money;
import net.ap57.tiger.std.accounting.entity.TransactionType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class AcctgTransDetailFacade extends AbstractFacade<AcctgTransDetail> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AcctgTransDetailFacade() {
        super(AcctgTransDetail.class);
    }

    @Override
    public AcctgTransDetail createTransient(Map<String, Object> params) {

        AcctgTrans trans = (AcctgTrans) params.get("transaction");
        BigDecimal unbalanced = trans.getUnbalanced();
        int condition = unbalanced.compareTo(BigDecimal.ZERO);

        AcctgTransDetail transDetail = new AcctgTransDetail();
        transDetail.setAcctgTrans(trans);
        transDetail.setAmount(new Money());

        if (condition < 0) {
            transDetail.setTransactionType(TransactionType.DEBIT);
        } else if (condition > 0) {
            transDetail.setTransactionType(TransactionType.CREDIT);
        }

        return transDetail;

    }

    @Override
    protected void addCriteria(Root<AcctgTransDetail> root, CriteriaQuery<AcctgTransDetail> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("acctgTrans").get("entryDate")));
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<AcctgTransDetail> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();        
        switch (filterName) {
            case "account":
                Expression<GLAccount> literal = cb.literal((GLAccount) filterValue);
                predicates.add(cb.equal(root.<GLAccount>get("account"), literal));
                break;
            default:
                break;
        }
        return predicates;
    }

    protected BigDecimal getDebit(GLAccount account, Date calculationDate) {
        BigDecimal debit = BigDecimal.ZERO;
        TypedQuery<BigDecimal> q = getEntityManager().createNamedQuery("AcctgTransDetail.getTotalAmountByDate", BigDecimal.class)
                .setParameter("account", account)
                .setParameter("date", calculationDate)
                .setParameter("type", TransactionType.DEBIT);
        try {
            debit = debit.add(q.getSingleResult());
        } catch (Exception ex) {
            debit = debit.add(BigDecimal.ZERO);
        } finally {
            return debit;
        }
    }

    protected BigDecimal getCredit(GLAccount account, Date calculationDate) {
        BigDecimal credit = BigDecimal.ZERO;
        TypedQuery<BigDecimal> q = getEntityManager().createNamedQuery("AcctgTransDetail.getTotalAmountByDate", BigDecimal.class)
                .setParameter("account", account)
                .setParameter("date", calculationDate)
                .setParameter("type", TransactionType.CREDIT);
        try {
            credit = credit.add(q.getSingleResult());
        } catch (Exception ex) {
            credit = credit.add(BigDecimal.ZERO);
        } finally {
            return credit;
        }
    }

    @Override
    protected List<AcctgTransDetail> postAction(List<AcctgTransDetail> result) {

        for (AcctgTransDetail detail : result) {
            BigDecimal debit = getDebit(detail.getAccount(), detail.getAcctgTrans().getEntryDate());
            BigDecimal credit = getCredit(detail.getAccount(), detail.getAcctgTrans().getEntryDate());
            detail.setBalance(new GLBalance(debit.add(credit.negate()).abs(), debit.compareTo(credit) == -1 ? TransactionType.CREDIT : TransactionType.DEBIT));

        }

        return result;
    }

    @Override
    public Object getId(AcctgTransDetail entity) {
        AcctgTransDetailId id = new AcctgTransDetailId();
        id.setAccount(entity.getAccount().getId());
        id.setAcctgTrans(entity.getAcctgTrans().getId());
        return id;
    }

}
