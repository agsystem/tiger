/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.accounting.dao.CurrencyFacade;
import net.ap57.tiger.std.accounting.dao.SecurityExchangeRateFacade;
import net.ap57.tiger.std.accounting.entity.Currency;
import net.ap57.tiger.std.accounting.entity.Security;
import net.ap57.tiger.std.accounting.entity.SecurityExchangeRate;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Named
@RequestScoped
@PageView(rel = "currency")
public class CurrencyPage extends Page implements Serializable {

    @Inject
    private CurrencyFacade currencyOp;

    @Inject
    private SecurityExchangeRateFacade exchangeRateOp;
    
    @View
    private DataTable currencyTable;
    
    @View
    private DataTable exchangeRateTable;

    @Override
    protected void initComponents() {
        
        currencyTable = new PFLazyDataTable<CurrencyFacade, Currency>() {

            @Override
            public CurrencyFacade getOp() {
                return currencyOp;
            }

        };

        exchangeRateTable = new PFLazyDataTable<SecurityExchangeRateFacade, SecurityExchangeRate>() {

            @Override
            protected SecurityExchangeRateFacade getOp() {
                return exchangeRateOp;
            }

            @Override
            protected SecurityExchangeRate newEntity() {
                Map<String, Object> params = new HashMap<>();
                params.put("reference", currencyOp.getDefault());
                Security security = (Security) currencyTable.getSelection();
                params.put("foreign", security);
                return getOp().createTransient(params);
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Security security = (Security) currencyTable.getSelection();
                Map<String, Object> params = new HashMap<>();
                if (security != null) {
                    params.put("foreignSecurity", security);
                }
                return params;
            }

        };

    }

    public void handleDefaultChange(AjaxBehaviorEvent e) {
        Map<String, Object> attrs = e.getComponent().getAttributes();
        Currency cst = (Currency) attrs.get("item");
        List<Currency> sts = currencyOp.findAll();
        for (Currency st : sts) {
            if (st.getName().equals(cst.getName())) {
                st.setDefaulted(true);
                currencyOp.edit(st);
            } else {
                if (st.getDefaulted()) {
                    st.setDefaulted(false);
                    currencyOp.edit(st);
                }
            }
        }
    }

    public Currency getDefault() {
        return currencyOp.getDefault();
    }

    @Override
    protected void initLayout() {
        setEditor("Securities", "100%");
    }

}
