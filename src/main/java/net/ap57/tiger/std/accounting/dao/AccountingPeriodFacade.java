/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */

package net.ap57.tiger.std.accounting.dao;

import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractTreeFacade;
import net.ap57.tiger.std.accounting.entity.AccountingPeriod;
import net.ap57.tiger.std.accounting.event.NewAccountingPeriodEvent;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class AccountingPeriodFacade extends AbstractTreeFacade<AccountingPeriod> {

    @Inject
    private EntityManager em;
    
    @Inject @NewAccountingPeriodEvent
    private Event<AccountingPeriod> newAccountingPeriodEvent;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountingPeriodFacade() {
        super(AccountingPeriod.class);
    }

    @Override
    public Object getId(AccountingPeriod entity) {
        return entity.getId();
    }

    @Override
    public void edit(AccountingPeriod entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public AccountingPeriod createTransientBasic(Map<String, Object> params) {
        return new AccountingPeriod();
    }

    @Override
    protected CriteriaQuery<AccountingPeriod> orderQuery(Root<AccountingPeriod> root, CriteriaQuery<AccountingPeriod> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        return cq.orderBy(cb.asc(root.get("fromDate")));
    }

    @Override
    public void create(AccountingPeriod entity) throws EJBException {
        super.create(entity);
        
        newAccountingPeriodEvent.fire(entity);
    }

}
