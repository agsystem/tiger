/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.std.accounting.dao.AcctgTransDetailFacade;
import net.ap57.tiger.std.accounting.dao.AcctgTransFacade;
import net.ap57.tiger.std.accounting.dao.CurrencyFacade;
import net.ap57.tiger.std.accounting.dao.GLAccountFacade;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import org.primefaces.context.RequestContext;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Named
@RequestScoped
@PageView(rel = "accounts")
public class AccountPage extends Page implements Serializable {

    @Inject
    private GLAccountFacade accountOp;

    @Inject
    private AcctgTransDetailFacade transactionDetailOp;

    @View
    private AccountDataTree accountTree;

    @View
    private AcctgTransDetailDataTable transactionDetailTable;

    @Inject
    private AcctgTransFacade transactionOp;

    @View
    private AcctgTransDataTable transactionTable;

    @Inject
    private CurrencyFacade currencyOp;
    
    @View
    private CurrencyDataList currencyList;

    @Override
    protected void initLayout() {
        setExplorer("Accounts", "15%");
        setEditor("Ledger", "85%");
    }

    @Override
    protected void initComponents() {

        accountTree = new AccountDataTree() {

            @Override
            public GLAccountFacade getOp() {
                return accountOp;
            }

            @Override
            public AcctgTransDetailDataTable getDetailTable() {
                return transactionDetailTable;
            }

        };

        transactionDetailTable = new AcctgTransDetailDataTable() {

            @Override
            protected AcctgTransDetailFacade getOp() {
                return transactionDetailOp;
            }

            @Override
            protected AcctgTrans getTransactionContext() {
                return getSelection().getAcctgTrans();
            }

            @Override
            protected CurrencyFacade getCurrencyOp() {
                return currencyOp;
            }

        };

        transactionTable = new AcctgTransDataTable() {

            @Override
            public AcctgTransFacade getOp() {
                return transactionOp;
            }

            private boolean isInBalance(AcctgTrans trx) {
                BigDecimal balance = getOp().calculateBalance(trx);
                return balance.compareTo(BigDecimal.ZERO) == 0;
            }

            @Override
            public AcctgTrans getSelection() {
                return transactionDetailTable.getTransactionContext();
            }

            @Override
            public void create(AcctgTrans e) {
                if (isInBalance(e)) {
                    super.create(e);
                    RequestContext rc = RequestContext.getCurrentInstance();
                    rc.execute("PF('createDlg').hide()");
                    ArrayList<String> updatees = new ArrayList<>();
                    updatees.add("dt-frm:dt-tbl");
                    updatees.add("messages");
                    rc.update(updatees);
                } else {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unbalanced Transaction", "Transaction is not balanced yet");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    RequestContext rc = RequestContext.getCurrentInstance();
                    rc.update("messages");
                }
            }

        };
        
        currencyList = new CurrencyDataList() {
            @Override
            public CurrencyFacade getOp() {
                return currencyOp;
            }
            
        };

    }

}
