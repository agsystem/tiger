/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.inject.Inject;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.std.accounting.dao.GLAccountFacade;
import net.ap57.tiger.std.accounting.dao.SecurityTypeFacade;
import net.ap57.tiger.std.accounting.entity.GLAccount;
import net.ap57.tiger.std.accounting.entity.GLAssetAccount;
import net.ap57.tiger.std.accounting.entity.GLEquityAccount;
import net.ap57.tiger.std.accounting.entity.GLExpenseAccount;
import net.ap57.tiger.std.accounting.entity.GLLiabilityAccount;
import net.ap57.tiger.std.accounting.entity.GLRevenueAccount;
import net.ap57.tiger.std.accounting.entity.SecurityType;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
public class AccountingModule extends Module {

    @Inject
    private GLAccountFacade accountFacade;

    @Inject
    private SecurityTypeFacade securityTypeFacade;

    @Override
    protected void init() {
        try {
            if (accountFacade.findSingleByAttribute("name", "Assets") == null) {
                GLAccount account = new GLAssetAccount();
                account.setName("Assets");
                account.setCode(1000l);
                account.setParent(null);
                account.setPlaceholder(true);
                account.setCurrency(null);
                account.setDescription("Container for all Asset accounts");
                accountFacade.create(account);
            }

            if (accountFacade.findSingleByAttribute("name", "Liabilities") == null) {
                GLAccount account = new GLLiabilityAccount();
                account.setName("Liabilities");
                account.setCode(2000l);
                account.setParent(null);
                account.setPlaceholder(true);
                account.setCurrency(null);
                account.setDescription("Container for all Liabilities accounts");
                accountFacade.create(account);
            }

            if (accountFacade.findSingleByAttribute("name", "Equities") == null) {
                GLAccount account = new GLEquityAccount();
                account.setName("Equities");
                account.setCode(3000l);
                account.setParent(null);
                account.setPlaceholder(true);
                account.setCurrency(null);
                account.setDescription("Container for all Equities accounts");
                accountFacade.create(account);
            }

            if (accountFacade.findSingleByAttribute("name", "Revenue") == null) {
                GLAccount account = new GLRevenueAccount();
                account.setName("Revenue");
                account.setCode(4000l);
                account.setParent(null);
                account.setPlaceholder(true);
                account.setCurrency(null);
                account.setDescription("Container for all Revenue accounts");
                accountFacade.create(account);
            }

            if (accountFacade.findSingleByAttribute("name", "Expense") == null) {
                GLAccount account = new GLExpenseAccount();
                account.setName("Expense");
                account.setCode(5000l);
                account.setParent(null);
                account.setPlaceholder(true);
                account.setCurrency(null);
                account.setDescription("Container for all Expense accounts");
                accountFacade.create(account);
            }

            if (securityTypeFacade.findSingleByAttribute("name", "currency") == null) {
                SecurityType securityType = securityTypeFacade.createTransient(null);
                securityType.setName("currency");
                securityTypeFacade.create(securityType);
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountingModule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
