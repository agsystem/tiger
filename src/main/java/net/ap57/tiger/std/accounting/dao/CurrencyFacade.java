/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */

package net.ap57.tiger.std.accounting.dao;
import java.math.BigDecimal;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.Currency;
import net.ap57.tiger.std.accounting.entity.Security;
import net.ap57.tiger.std.accounting.entity.SecurityExchangeRate;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class CurrencyFacade extends AbstractFacade<Currency> {
    @Inject
    private EntityManager em;
    
    @Inject 
    private SecurityTypeFacade typeFacade;
    
    @Inject
    private SecurityExchangeRateFacade exchangeRateFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CurrencyFacade() {
        super(Currency.class);
    }

    @Override
    protected void addCriteria(Root<Currency> root, CriteriaQuery<Currency> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.asc(root.get("name")));
    }
    
    public Currency getDefault() {
        TypedQuery<Currency> query = getEntityManager().createNamedQuery("Currency.getDefault", Currency.class);

        Currency result = null;
        try {
            result = query.getSingleResult();
        } catch (NoResultException ex) {
            result = null;
        } finally {
            return result;
        }
    }

    public BigDecimal convertTo(BigDecimal amount, Currency from, Currency to) {

//        if (from.equals(to)) {
            return amount;
//        } else {
//
//            if (from.getDefaulted()) {
////                return amount.divide(to.getBuy(), RoundingMode.CEILING);
//            } else if (to.getDefaulted()) {
////                return amount.multiply(from.getBuy());
//            } else {
//                Currency dCurrency = getDefault();
//                BigDecimal dFrom = convertTo(amount, from, dCurrency);
//                return convertTo(dFrom, dCurrency, to);
//            }
//
//        }
    }

    @Override
    public Currency createTransient(Map<String, Object> params) {
        Currency c = new Currency();
//        c.setBuy(BigDecimal.ZERO);
//        c.setSell(BigDecimal.ZERO);
        c.setDefaulted(Boolean.FALSE);
        c.setType(typeFacade.findSingleByAttribute("name", "currency"));
        return c;
    }

    @Override
    public Object getId(Currency entity) {
        return entity.getId();
    }
    
    public SecurityExchangeRate getLatestExchangeRate(Security reference, Security foreign) {
        return exchangeRateFacade.getLatestExchangeRate(reference, foreign);
    }
    
}
