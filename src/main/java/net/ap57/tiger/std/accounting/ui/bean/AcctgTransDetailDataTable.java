/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.accounting.dao.AcctgTransDetailFacade;
import net.ap57.tiger.std.accounting.dao.CurrencyFacade;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetail;
import net.ap57.tiger.std.accounting.entity.GLAccount;
import net.ap57.tiger.std.accounting.entity.SecurityExchangeRate;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
//@Named(value = "acctgTransDetailDataTable")
//@RequestScoped
public abstract class AcctgTransDetailDataTable extends PFLazyDataTable<AcctgTransDetailFacade, AcctgTransDetail> {

    protected abstract AcctgTrans getTransactionContext();

    protected abstract CurrencyFacade getCurrencyOp();

    public Map<GLAccount, DataModel<AcctgTransDetail>> getModels() {

        Map<GLAccount, DataModel<AcctgTransDetail>> result = (Map<GLAccount, DataModel<AcctgTransDetail>>) getAttribute("models");

        if (result == null) {
            result = new HashMap<>();
        }

        return result;
    }

    public void setModels(Map<GLAccount, DataModel<AcctgTransDetail>> models) {
        setAttribute("models", models);
    }

    public DataModel<AcctgTransDetail> getModel(GLAccount account) {
        System.out.println("MODEL = " + account.getName());

//        if (!getModels().containsKey(account)) {
//            System.out.println("NOT CONTAIN MODEL = " + account.getName());
//            getModels().put(account, createDataModel(account));
//        }
//
//        return getModels().get(account);

        return createDataModel(account);

    }

    protected DataModel<AcctgTransDetail> createDataModel(final GLAccount account) {
        return new LazyDataModel<AcctgTransDetail>() {
            {
                System.out.println("CONSTRUCTOR = " + account.getName());
                filter = account;
            }

            private final GLAccount filter;

            @Override
            public List<AcctgTransDetail> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                System.out.println("FILTER = " + filter.getName());
                Map<String, Object> allFilters = getLoadFilters();
                allFilters.put("account", filter);
                allFilters.putAll(filters);
                addStaticFilter(allFilters);
                List<AcctgTransDetail> result = getOp().findAll(first, pageSize, allFilters);
                setRowCount(getOp().countAll(allFilters).intValue());
                return result;
            }

            @Override
            public Object getRowKey(AcctgTransDetail object) {
                return object.getStrKey();
            }

            @Override
            public AcctgTransDetail getRowData(String rowKey) {
                return getOp().getObj(rowKey);
            }
        };
    }

    public void removeAccountModel(GLAccount account) {
        getModels().remove(account);
    }

    public void addDetail(ActionEvent evt) {
        addDetail((AcctgTrans) getTransactionContext());
    }

    public void addDetail(AcctgTrans trans) {
        Map<String, Object> params = new HashMap<>();
        params.put("transaction", trans);
        AcctgTransDetail transDetail = getOp().createTransient(params);
        transDetail.setStrKey(UUID.randomUUID().toString());
        trans.getAcctgTransDetails().add(transDetail);
    }

    public void deleteDetail(ActionEvent evt) {
        AcctgTransDetail detail = (AcctgTransDetail) getSelection();
        if (detail != null) {
            deleteDetail(detail);
            setSelection(null);
        }
    }

    public void deleteDetail(AcctgTransDetail detail) {
        getTransactionContext().getAcctgTransDetails().remove(detail);
    }

    public void onAccountChange(AjaxBehaviorEvent evt) {
        GLAccount account = (GLAccount) ((SelectOneMenu) evt.getComponent()).getValue();
        AcctgTransDetail detail = ((AcctgTransDetail) getSelection());
        detail.getAmount().setCurrency(account.getCurrency());
        SecurityExchangeRate rate = getCurrencyOp().getLatestExchangeRate(getCurrencyOp().getDefault(), account.getCurrency());
        detail.setExchangeRate(rate);
    }

    public void handleCellEdit(CellEditEvent evt) {
        
    }
    
    public void reviewJournal(ActionEvent evt) {
        
    }

}
