/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.ui.pf.PFDataTree;
import net.ap57.tiger.std.accounting.dao.GLAccountFacade;
import net.ap57.tiger.std.accounting.entity.GLAccount;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class AccountDataTree extends PFDataTree<GLAccountFacade, GLAccount> {

    public abstract AcctgTransDetailDataTable getDetailTable();

    public List<GLTab> getTabs() {

        if (getAttribute("tabs") == null) {
            List<GLTab> tabs = new ArrayList<>();
//            tabs.add(new GLTab(null, false));
            setTabs(tabs);
        }

        return (List<GLTab>) getAttribute("tabs");

    }

    public void setTabs(List<GLTab> tabs) {
        setAttribute("tabs", tabs);
    }

//    public TabView getTabView() {
//        return (TabView) getAttribute("tabView");
//    }
//
//    public void setTabView(TabView tabView) {
//        setAttribute("tabView", tabView);
//    }
    
    public int getActiveIndex() {
        return (Integer) getAttribute("activeIndex", 0);
    }
    
    public void setActiveIndex(int activeIndex) {
        setAttribute("activeIndex", activeIndex);
    }

    public void addTab(ActionEvent evt) {
//        System.out.println("Inside addTab()");
//        System.out.println(getSelection());
        if(getSelection() == null) return;
        GLTab newTab = new GLTab((GLAccount) getSelection(), true);
        if (!getTabs().contains(newTab)) {
//            System.out.println("No tab found");
            getTabs().add(newTab);
        }

        int tabIdx = getTabs().indexOf(newTab);
//        getTabView().setActiveIndex(tabIdx);
        setActiveIndex(tabIdx);
    }

    public void handleTabShow(ActionEvent evt) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int idx = Integer.parseInt(params.get("tab-idx"));
    }

    public void handleTabClose(ActionEvent evt) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int idx = Integer.parseInt(params.get("tab-idx"));
//        getDetailTable().removeAccountModel(getTabs().get(idx).getAccount());
        getTabs().remove(idx);
        int tabs = getTabs().size();
        setActiveIndex(tabs == 0 ? tabs : tabs - 1);
        if(tabs != 0)
        setSelection(getTabs().get(getActiveIndex()).getAccount());
        else setSelection(null);
    }

    public void handleTabChange(ActionEvent evt) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int idx = Integer.parseInt(params.get("tab-idx"));
        setActiveIndex(idx);
        setSelection(getTabs().get(idx).getAccount());
    }

    @Override
    protected String getNodeType(GLAccount data) {
        if (data.getParent() == null) {
            return "folder";
        } else if (data.isPlaceholder()) {
            return "folder";
        } else {
            return "document";
        }
    }

    @Override
    public boolean isCreateContextEmpty() {
        return getSelection() != null
                && !getSelection().isPlaceholder();
    }

    @Override
    public boolean isDeleteContextEmpty() {
        return super.isDeleteContextEmpty()
                || getSelection().getParent() == null;
    }

    @Override
    public boolean isUpdateContextEmpty() {
        return super.isUpdateContextEmpty() || getSelection().getParent() == null;
    }

}
