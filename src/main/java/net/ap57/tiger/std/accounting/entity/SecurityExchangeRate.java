/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
@IdClass(SecurityExchangeRateId.class)
@NamedQueries({
    @NamedQuery(name = "SecurityExchangeRate.latestRate",
            query = "SELECT rate1 "
            + "FROM SecurityExchangeRate rate1 "
            + "WHERE rate1.checkDate = (SELECT MAX(rate0.checkDate) "
            + "FROM SecurityExchangeRate rate0 "
            + "WHERE rate0.referenceSecurity = :reference "
            + "AND rate0.foreignSecurity = :foreign)")
})
public class SecurityExchangeRate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkDate;

    @Id
    @ManyToOne
    private Security referenceSecurity;

    @Id
    @ManyToOne
    private Security foreignSecurity;

    private BigDecimal price;

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public Security getReferenceSecurity() {
        return referenceSecurity;
    }

    public void setReferenceSecurity(Security referenceSecurity) {
        this.referenceSecurity = referenceSecurity;
    }

    public Security getForeignSecurity() {
        return foreignSecurity;
    }

    public void setForeignSecurity(Security foreignSecurity) {
        this.foreignSecurity = foreignSecurity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

//    @Override
//    public String toString() {
//        return price.toString();
//    }

}
