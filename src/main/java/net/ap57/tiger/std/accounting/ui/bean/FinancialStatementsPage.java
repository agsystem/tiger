/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import java.util.Locale;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.UserSession;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.accounting.dao.AccountingPeriodFacade;
import net.ap57.tiger.std.accounting.dao.AccountingPeriodTypeFacade;
import net.ap57.tiger.std.accounting.dao.IncomeStatementFacade;
import net.ap57.tiger.std.accounting.entity.AccountingPeriod;
import net.ap57.tiger.std.accounting.entity.PeriodType;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Named
@RequestScoped
@PageView(rel="financialStatements")
public class FinancialStatementsPage extends Page {

    @Inject
    private AccountingPeriodFacade accountingPeriodOp;
    
    @View
    private AccountingPeriodDataTree accountingPeriodTree;

    @Inject
    private AccountingPeriodTypeFacade accountingPeriodTypeOp;
    
    @View
    private DataTable accountingPeriodTypeTable;
    
    @View
    private AccountingPeriodTypeDataList accountingPeriodTypeList;
    
    @Inject
    private IncomeStatementFacade incomeStatementFacade;
    
    @View
    private IncomeStatementForm incomeStatementForm;
    
    @Inject
    private UserSession userSession;
    

    @Override
    protected void initLayout() {
        setExplorer("Accounting Period", "25%");
        setEditor("Financial Statements", "75%");
    }

    @Override
    protected void initComponents() {
        
        accountingPeriodTree = new AccountingPeriodDataTree() {
            
            @Override
            protected AccountingPeriodFacade getOp() {
                return accountingPeriodOp;
            }
            
        };
        
        accountingPeriodTypeTable = new PFLazyDataTable<AccountingPeriodTypeFacade, PeriodType>() {

            @Override
            public AccountingPeriodTypeFacade getOp() {
                return accountingPeriodTypeOp;
            }

        };
        
        accountingPeriodTypeList = new AccountingPeriodTypeDataList() {
            @Override
            public AccountingPeriodTypeFacade getOp() {
                return accountingPeriodTypeOp;
            }
            
        };
        
        incomeStatementForm = new IncomeStatementForm() {
            @Override
            protected IncomeStatementFacade getOp() {
                return incomeStatementFacade;
            }

            @Override
            protected AccountingPeriod getCtx() {
                return accountingPeriodTree.getSelection();
            }

            @Override
            protected Locale getLocale() {
                Locale locale = new Locale(userSession.getLocale());
                return locale;
            }
            
        };
        
    }

    
    
}
