/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author mbahbejo
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Currency.getDefault",
        query = "SELECT c "
        + "FROM Currency c "
        + "WHERE c.defaulted = true")
})
public class Currency extends Security implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Column(nullable=false)
    private Boolean defaulted;
    
    public Boolean getDefaulted() {
        return defaulted;
    }
    
    public void setDefaulted(Boolean defaulted) {
        this.defaulted = defaulted;
    }

    @Override
    public String toString() {
        return getName();
    }
    
}
