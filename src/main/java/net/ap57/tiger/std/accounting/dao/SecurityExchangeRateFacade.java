/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.dao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.Security;
import net.ap57.tiger.std.accounting.entity.SecurityExchangeRate;
import net.ap57.tiger.std.accounting.entity.SecurityExchangeRateId;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class SecurityExchangeRateFacade extends AbstractFacade<SecurityExchangeRate> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SecurityExchangeRateFacade() {
        super(SecurityExchangeRate.class);
    }

    @Override
    public SecurityExchangeRate createTransient(Map<String, Object> params) {
        Security referenceSecurity = (Security) params.get("reference");
        Security foreignSecurity = (Security) params.get("foreign");
        SecurityExchangeRate rate = new SecurityExchangeRate();
        rate.setReferenceSecurity(referenceSecurity);
        rate.setForeignSecurity(foreignSecurity);
        return rate;
    }

    @Override
    public void create(SecurityExchangeRate entity) {
        entity.setCheckDate(Calendar.getInstance().getTime());
        super.create(entity);
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<SecurityExchangeRate> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();        
        switch (filterName) {
            case "foreignSecurity":
                Expression<Security> literal = cb.literal((Security) filterValue);
                predicates.add(cb.equal(root.<String>get(filterName), literal));
                break;
            default:
                break;
        }
        return predicates;
    }

    public SecurityExchangeRate getLatestExchangeRate(Security reference, Security foreign) {

        TypedQuery<SecurityExchangeRate> query = getEntityManager().createNamedQuery("SecurityExchangeRate.latestRate", SecurityExchangeRate.class)
                .setParameter("reference", reference)
                .setParameter("foreign", foreign);

        try {
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }

    }

    @Override
    public Object getId(SecurityExchangeRate entity) {
        SecurityExchangeRateId id = new SecurityExchangeRateId();
        id.setReferenceSecurity(entity.getReferenceSecurity().getId());
        id.setReferenceSecurity(entity.getReferenceSecurity().getId());
        id.setCheckDate(entity.getCheckDate());
        return id;
    }

}
