/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.dao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetail;
import net.ap57.tiger.std.accounting.entity.Money;
import net.ap57.tiger.std.accounting.entity.TransactionType;
import net.ap57.tiger.std.accounting.event.NewTrxEvent;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class AcctgTransFacade extends AbstractFacade<AcctgTrans> {

    @Inject
    private EntityManager em;

    @Inject 
    private CurrencyFacade currencyFacade;
    
    @Inject @NewTrxEvent
    private Event<AcctgTrans> newTrxEvent;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AcctgTransFacade() {
        super(AcctgTrans.class);
    }

    @Override
    protected void addCriteria(Root<AcctgTrans> root, CriteriaQuery<AcctgTrans> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(new Order[]{cb.desc(root.get("transactionDate")), cb.desc(root.get("entryDate"))});
    }

    @Override
    public void create(AcctgTrans entity) {
        entity.setEntryDate(Calendar.getInstance().getTime());
        entity.setId(UUID.randomUUID().toString());
        super.create(entity);
        
        getEntityManager().flush();
        
        newTrxEvent.fire(entity);
    }

    public void addDetail(AcctgTransDetail detail) {
        getEntityManager().persist(detail);
    }

    public void removeDetail(AcctgTransDetail detail) {
        getEntityManager().remove(getEntityManager().merge(detail));
    }

    @Override
    public AcctgTrans createTransient(Map<String, Object> params) {
        AcctgTrans t = new AcctgTrans();
        t.setTransactionDate(Calendar.getInstance().getTime());

        List<AcctgTransDetail> details = new ArrayList<AcctgTransDetail>();

        AcctgTransDetail debit = new AcctgTransDetail();
        debit.setStrKey(UUID.randomUUID().toString());
        debit.setAcctgTrans(t);
        debit.setTransactionType(TransactionType.DEBIT);
        debit.setAmount(new Money(null, BigDecimal.ZERO));

        AcctgTransDetail credit = new AcctgTransDetail();
        credit.setStrKey(UUID.randomUUID().toString());
        credit.setAcctgTrans(t);
        credit.setTransactionType(TransactionType.CREDIT);
        credit.setAmount(new Money(null, BigDecimal.ZERO));

        details.add(debit);
        details.add(credit);

        t.setAcctgTransDetails(details);
        return t;
    }

    public BigDecimal calculateBalance(AcctgTrans trans) {
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal credit = BigDecimal.ZERO;
        for (AcctgTransDetail detail : trans.getAcctgTransDetails()) {
            TransactionType transType = detail.getTransactionType();
            Money amount = detail.getAmount();
            if (transType.equals(TransactionType.DEBIT)) {
                if (amount.getCurrency().equals(currencyFacade.getDefault())) {
                    debit = debit.add(amount.getAmount());
                } else {
                    debit = debit.add(amount.getAmount().multiply(detail.getExchangeRate().getPrice()));
                }
            } else if (transType.equals(TransactionType.CREDIT)) {
                if (amount.getCurrency().equals(currencyFacade.getDefault())) {
                    credit = credit.add(amount.getAmount());
                } else {
                    credit = credit.add(amount.getAmount().multiply(detail.getExchangeRate().getPrice()));
                }
            }
        }

        return debit.add(credit.negate());
    }

    @Override
    public Object getId(AcctgTrans entity) {
        return entity.getId();
    }

}
