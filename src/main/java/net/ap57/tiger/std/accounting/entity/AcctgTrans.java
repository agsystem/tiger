/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class AcctgTrans extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Temporal(TemporalType.DATE)
    private Date transactionDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;

    private String description;

    @ManyToOne
    private AcctgTransType type;

    @OneToMany(mappedBy = "acctgTrans", cascade = CascadeType.ALL, orphanRemoval=true)
    private List<AcctgTransDetail> acctgTransDetails;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AcctgTransType getType() {
        return type;
    }

    public void setType(AcctgTransType type) {
        this.type = type;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AcctgTransDetail> getAcctgTransDetails() {
        return acctgTransDetails;
    }

    public void setAcctgTransDetails(List<AcctgTransDetail> acctgTransDetails) {
        this.acctgTransDetails = acctgTransDetails;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Object Methods">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcctgTrans)) {
            return false;
        }
        AcctgTrans other = (AcctgTrans) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mdn.tiger.accounting.entity.AccountingTransaction[ id=" + id + " ]";
    }
//</editor-fold>

    public BigDecimal getAmount() {
        BigDecimal debit = BigDecimal.ZERO;        
        BigDecimal kredit = BigDecimal.ZERO;
        for(AcctgTransDetail detail : getAcctgTransDetails()) {
            TransactionType transType = detail.getTransactionType();
            if(transType == null) continue;
            if(transType.equals(TransactionType.DEBIT)) {
                debit = debit.add(detail.getAmount().getAmount());
            } else if(transType.equals(TransactionType.CREDIT)) {
                kredit = kredit.add(detail.getAmount().getAmount());
            }
        }
        return debit.subtract(kredit).equals(BigDecimal.ZERO) ? debit : BigDecimal.ZERO;
    }
    
    public BigDecimal getUnbalanced() {        
        BigDecimal debit = BigDecimal.ZERO;        
        BigDecimal kredit = BigDecimal.ZERO;
        for(AcctgTransDetail detail : getAcctgTransDetails()) {
            TransactionType transType = detail.getTransactionType();
            if(transType == null) continue;
            if(transType.equals(TransactionType.DEBIT)) {
                debit = debit.add(detail.getAmount().getAmount());
            } else if(transType.equals(TransactionType.CREDIT)) {
                kredit = kredit.add(detail.getAmount().getAmount());
            }
        }
        
        return debit.subtract(kredit);
    }

}
