/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractTreeFacade;
import net.ap57.tiger.std.accounting.entity.GLAccount;
import net.ap57.tiger.std.accounting.entity.GLAssetAccount;
import net.ap57.tiger.std.accounting.entity.GLEquityAccount;
import net.ap57.tiger.std.accounting.entity.GLExpenseAccount;
import net.ap57.tiger.std.accounting.entity.GLLiabilityAccount;
import net.ap57.tiger.std.accounting.entity.GLRevenueAccount;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class GLAccountFacade extends AbstractTreeFacade<GLAccount> {

    @Inject
    private EntityManager em;

    @Inject 
    private GLAssetAccountFacade assetFacade;

    @Inject 
    private GLEquityAccountFacade equityFacade;

    @Inject 
    private GLLiabilityAccountFacade liabilityFacade;

    @Inject 
    private GLRevenueAccountFacade revenueFacade;

    @Inject 
    private GLExpenseAccountFacade expenseFacade;
    
    @Inject 
    private CurrencyFacade currencyFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GLAccountFacade() {
        super(GLAccount.class);
    }

    @Override
    public Object getId(GLAccount entity) {
        return entity.getId();
    }

    @Override
    public void create(GLAccount entity) {
//        routeOp(entity);
        super.create(entity);
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<GLAccount> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();        
        switch (filterName) {
            case "placeholder":
                Expression<Boolean> literal = cb.literal((Boolean)filterValue);
                predicates.add(cb.equal(root.get("placeholder"), literal));
                break;
            default:
                break;
        }
        return predicates;
    }

    @Override
    public List<GLAccount> getAdam() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<GLAccount> cq = cb.createQuery(GLAccount.class);
        Root<GLAccount> root = cq.from(GLAccount.class);
        cq.select(root);
        cq.where(cb.isNull(root.get("parent"))).orderBy(cb.asc(root.get("id")));
        TypedQuery<GLAccount> query = getEntityManager().createQuery(cq);
        
        return query.getResultList();
    }

    @Override
    public void remove(GLAccount entity) {
        entity = getEntityManager().merge(entity);
        GLAccount parent = entity.getParent();
        if (parent != null) {
            parent.getChildren().remove(entity);
        }
            getEntityManager().remove(entity);
//        }
    }

    private void routeOp(GLAccount entity) {
        if (entity instanceof GLAssetAccount) {
            entity.setCode(assetFacade.generateCode());
        } else if (entity instanceof GLLiabilityAccount) {
            entity.setCode(liabilityFacade.generateCode());
        } else if (entity instanceof GLEquityAccount) {
            entity.setCode(equityFacade.generateCode());
        } else if (entity instanceof GLRevenueAccount) {
            entity.setCode(revenueFacade.generateCode());
        } else if (entity instanceof GLExpenseAccount) {
            entity.setCode(expenseFacade.generateCode());
        }
    }

    @Override
    public GLAccount createTransient(Map<String, Object> params) {
        Object parent = params.get("parent");
        GLAccount acc = createTransientBasic(params);
        if (parent instanceof GLAssetAccount) {
            acc = new GLAssetAccount();
        } else if (parent instanceof GLLiabilityAccount) {
            acc = new GLLiabilityAccount();
        } else if (parent instanceof GLEquityAccount) {
            acc = new GLEquityAccount();
        } else if (parent instanceof GLRevenueAccount) {
            acc = new GLRevenueAccount();
        } else if (parent instanceof GLExpenseAccount) {
            acc = new GLExpenseAccount();
        }
        acc.setCurrency(currencyFacade.getDefault());
        acc.setParent((GLAccount) parent);
        acc.setPlaceholder(false);
        return acc;
    }

    @Override
    public GLAccount createTransientBasic(Map<String, Object> params) {
        return null;
    }

    @Override
    protected CriteriaQuery<GLAccount> orderQuery(Root<GLAccount> root, CriteriaQuery<GLAccount> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        return cq.orderBy(cb.asc(root.get("code")));
    }

}
