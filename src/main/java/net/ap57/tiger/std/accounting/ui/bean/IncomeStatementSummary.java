/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import com.ibm.icu.text.NumberFormat;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import net.ap57.tiger.std.accounting.entity.IncomeStatement;
import net.ap57.tiger.std.accounting.entity.IncomeStatementDetail;
import net.ap57.tiger.std.accounting.entity.IncomeStatementRevenueDetail;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class IncomeStatementSummary {

    private Locale locale;

    private final List<Entry> entries;

    public IncomeStatementSummary(IncomeStatement statement, Locale locale) {
        this.locale = locale;
        entries = new ArrayList<>();
        entries.add(new Entry("Revenue", statement.getRevenueDetails()));
        entries.add(new Entry("Expense", statement.getExpenseDetails()));
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public BigDecimal getNetIncome() {
        BigDecimal revenue = entries.get(0).getTotal();
        BigDecimal expense = entries.get(1).getTotal();

        BigDecimal netIncome = revenue.subtract(expense);

        return netIncome.abs();
    }

    public String getNetIncomeAsString() {

        NumberFormat currency = NumberFormat.getCurrencyInstance(locale);

        return currency.format(getNetIncome()).substring(1);
    }

    public String getNetIncomeStatus() {
        BigDecimal revenue = entries.get(0).getTotal();
        BigDecimal expense = entries.get(1).getTotal();
        int compareResult = revenue.compareTo(expense);

        return compareResult < 0 ? "Loss" : "Income";
    }

    public class Entry<T extends IncomeStatementDetail> {

        private final String label;

        private final List<T> details;

        public Entry(String label, List<T> details) {
            this.label = label;
            this.details = details;
        }

        public String getLabel() {
            return label;
        }

        public List<T> getDetails() {
            return details;
        }

        public BigDecimal getTotal() {
            BigDecimal total = BigDecimal.ZERO;
            for (IncomeStatementDetail detail : details) {
                total = total.add(detail.getAmount().getAmount());
            }

            return total;
        }

        public String getTotalAsString() {

            NumberFormat currency = NumberFormat.getCurrencyInstance(locale);

            return currency.format(getTotal()).substring(1);
        }

    };

}
