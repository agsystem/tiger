/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import static java.lang.Math.E;
import java.util.List;
import java.util.Map;
import javax.faces.model.DataModel;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.accounting.dao.AcctgTransFacade;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetail;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class AcctgTransDataTable extends PFLazyDataTable<AcctgTransFacade, AcctgTrans> {

    public DataModel<AcctgTransDetail> getDetail() {
        DataModel<AcctgTransDetail> detailModel = (DataModel<AcctgTransDetail>) getAttribute("detailModel");
        if(detailModel == null) {
            
         setAttribute("detailModel", new LazyDataModel<AcctgTransDetail>() {

            @Override
            public List<AcctgTransDetail> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                Map<String, Object> allFilters = getLoadFilters();
                allFilters.putAll(filters);
                addStaticFilter(allFilters);
                List<AcctgTransDetail> content = getSelection().getAcctgTransDetails();
                setRowCount(content.size());
                return content;
            }

            @Override
            public AcctgTransDetail getRowData(String rowkey) {
                return keyToObj(rowkey);
            }

            @Override
            public Object getRowKey(AcctgTransDetail object) {
                return objToKey(object);
            }

            protected AcctgTransDetail keyToObj(String rowkey) {
                for (AcctgTransDetail detail : getSelection().getAcctgTransDetails()) {
                    if (detail.getStrKey().equals(rowkey)) {
                        return detail;
                    }
                }
                return null;
            }

            protected Object objToKey(AcctgTransDetail object) {
                return object.getStrKey();
            }

        });
         
        }
        
        return (DataModel<AcctgTransDetail>) getAttribute("detailModel");

    }

}
