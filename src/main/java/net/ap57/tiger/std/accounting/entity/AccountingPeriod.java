/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.api.HierarchicalEntity;
import net.ap57.tiger.std.hr.entity.InternalOrganization;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
public class AccountingPeriod implements Serializable, HierarchicalEntity<AccountingPeriod> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name = "AccountingPeriod", table = "KEYGEN")
    @GeneratedValue(generator = "AccountingPeriod", strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne
    private PeriodType type;

    @ManyToOne
    private InternalOrganization internalOrganization;
    
    @Temporal(TemporalType.DATE)
    private Date fromDate;

    @Temporal(TemporalType.DATE)
    private Date thruDate;

    @ManyToOne
    private AccountingPeriod parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AccountingPeriod> children;
    
    @OneToOne(mappedBy = "accountingPeriod", cascade = CascadeType.ALL)
    private IncomeStatement incomeStatement;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PeriodType getType() {
        return type;
    }

    public void setType(PeriodType type) {
        this.type = type;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getThruDate() {
        return thruDate;
    }

    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }
    
    @Override
    public List<AccountingPeriod> getChildren() {
        return children;
    }

    public void setChildren(List<AccountingPeriod> children) {
        this.children = children;
    }

    @Override
    public AccountingPeriod getParent() {
        return parent;
    }

    @Override
    public void setParent(AccountingPeriod parent) {
        this.parent = parent;
    }

    public IncomeStatement getIncomeStatement() {
        return incomeStatement;
    }

    public void setIncomeStatement(IncomeStatement incomeStatement) {
        this.incomeStatement = incomeStatement;
    }
    
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Object Methods">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountingPeriod)) {
            return false;
        }
        AccountingPeriod other = (AccountingPeriod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        Formatter fmt = new Formatter();
        fmt.format("[%s: %td/%2$tm/%2$tY - %td/%3$tm/%3$tY]", new Object[]{type, fromDate, thruDate});
        return fmt.toString();
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Transients">
//    @Transient
//    private Object wrapper;
//    
//    @Override
//    public Object getWrapper() {
//        return wrapper;
//    }
//    
//    @Override
//    public void setWrapper(Object wrapper) {
//        this.wrapper = wrapper;
//    }
//</editor-fold>
}
