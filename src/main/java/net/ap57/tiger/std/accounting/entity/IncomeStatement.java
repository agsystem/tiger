/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
@IdClass(IncomeStatementId.class)
//@NamedQueries({
//    @NamedQuery(name="IncomeStatement.expenseCollection", 
//                query="SELECT CASE WHEN amount != null THEN "
//                        + "NEW net.ap57.tiger.std.accounting.entity.IncomeStatementExpenseDetail(account, amount) "
//                        + "ELSE NEW net.ap57.tiger.std.accounting.entity.IncomeStatementExpenseDetail(account, java.math.BigDecimal.ZERO) "
//                        + "END "
//                        + "FROM "
//                        + "(SELECT account "
//                        + "FROM GLAccount account "
//                        + "WHERE TYPE(account) = GLExpenseAccount) "
//                        + "LEFT JOIN "
//                        + "(SELECT trxDetail.account, SUM(trxDetail.amount.amount)) "
//                        + "FROM AcctgTrans trx "
//                        + "JOIN trx.acctgTransDetails trxDetail "
//                        + "WHERE TYPE(trxDetail.account) = GLExpenseAccount "
//                        + "AND trx.transactionDate >= :fromDate "
//                        + "AND trx.transactionDate <= :thruDate "
//                        + "GROUP BY trxDetail.account)"
//                        + "ON trxDetail.account.id = account.id"
//    )
//})
public class IncomeStatement implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @Id
    @OneToOne
    private AccountingPeriod accountingPeriod;
    
    @OneToMany(mappedBy = "incomeStatement", cascade=CascadeType.ALL)
    private List<IncomeStatementRevenueDetail> revenueDetails;

    @OneToMany(mappedBy = "incomeStatement", cascade=CascadeType.ALL)
    @OrderBy("account")
    private List<IncomeStatementExpenseDetail> expenseDetails;    

    public AccountingPeriod getAccountingPeriod() {
        return accountingPeriod;
    }

    public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
        this.accountingPeriod = accountingPeriod;
    }

    public List<IncomeStatementRevenueDetail> getRevenueDetails() {
        return revenueDetails;
    }

    public void setRevenueDetails(List<IncomeStatementRevenueDetail> revenueDetails) {
        this.revenueDetails = revenueDetails;
    }

    public List<IncomeStatementExpenseDetail> getExpenseDetails() {
        return expenseDetails;
    }

    public void setExpenseDetails(List<IncomeStatementExpenseDetail> expenseDetails) {
        this.expenseDetails = expenseDetails;
    }
    
}
