/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mbahbejo
 */
@Embeddable
public class Money implements Serializable {
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Currency currency;
    private BigDecimal amount;

    public Money() {}

    public Money(Currency currency, BigDecimal amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
