/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.ui.bean;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import net.ap57.tiger.base.ui.PageForm;
import net.ap57.tiger.std.accounting.dao.IncomeStatementFacade;
import net.ap57.tiger.std.accounting.entity.AccountingPeriod;
import net.ap57.tiger.std.accounting.entity.IncomeStatement;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class IncomeStatementForm extends PageForm {

    protected abstract IncomeStatementFacade getOp();
    
    protected abstract Locale getLocale();

    protected boolean loadModel() {
        return true;
    }

    protected abstract AccountingPeriod getCtx();

    public IncomeStatementSummary getSummary() {
        AccountingPeriod context = getCtx();
        System.out.println();
        IncomeStatement statement = null;
        if (context != null) {
            statement = getOp().getStatement(context);
        } else {
            statement = new IncomeStatement();
            statement.setRevenueDetails(new ArrayList<>());
            statement.setExpenseDetails(new ArrayList<>());
        }

        IncomeStatementSummary summary = new IncomeStatementSummary(statement, getLocale());
        return summary;
    }

}
