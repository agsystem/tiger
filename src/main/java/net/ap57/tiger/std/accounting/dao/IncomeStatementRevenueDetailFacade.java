/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.accounting.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Asynchronous;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.accounting.entity.AccountingPeriod;
import net.ap57.tiger.std.accounting.entity.AcctgTrans;
import net.ap57.tiger.std.accounting.entity.AcctgTransDetail;
import net.ap57.tiger.std.accounting.entity.GLAccount;
import net.ap57.tiger.std.accounting.entity.GLRevenueAccount;
import net.ap57.tiger.std.accounting.entity.IncomeStatement;
import net.ap57.tiger.std.accounting.entity.IncomeStatementDetailId;
import net.ap57.tiger.std.accounting.entity.IncomeStatementId;
import net.ap57.tiger.std.accounting.entity.IncomeStatementRevenueDetail;
import net.ap57.tiger.std.accounting.event.NewTrxEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class IncomeStatementRevenueDetailFacade extends AbstractFacade<IncomeStatementRevenueDetail> {

    @Inject
    private EntityManager em;

    @Inject
    private IncomeStatementFacade statementFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IncomeStatementRevenueDetailFacade() {
        super(IncomeStatementRevenueDetail.class);
    }

    @Override
    public IncomeStatementRevenueDetail createTransient(Map<String, Object> params) throws EJBException {
        IncomeStatementRevenueDetail expenseDetail = new IncomeStatementRevenueDetail();
        return expenseDetail;
    }

    @Override
    public Object getId(IncomeStatementRevenueDetail entity) {
        IncomeStatementDetailId id = new IncomeStatementDetailId();
        id.setIncomeStatement((IncomeStatementId) statementFacade.getId(entity.getIncomeStatement()));
        id.setAccount(entity.getAccount().getId());
        return id;
    }

    private IncomeStatementRevenueDetail getRevenueDetail(IncomeStatement statement, GLRevenueAccount account) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<IncomeStatementRevenueDetail> cq = cb.createQuery(IncomeStatementRevenueDetail.class);

        Root<IncomeStatementRevenueDetail> statementDetail = cq.from(IncomeStatementRevenueDetail.class);
        cq.select(statementDetail).where(new Predicate[]{
            cb.equal(statementDetail.get("incomeStatement").get("accountingPeriod").get("id"), statement.getAccountingPeriod().getId()),
            cb.equal(statementDetail.get("account").get("id"), account.getId())
        });

        TypedQuery<IncomeStatementRevenueDetail> q = getEntityManager().createQuery(cq);

        IncomeStatementRevenueDetail result = null;

        try {
            result = q.getSingleResult();
        } catch (NoResultException | NonUniqueResultException ex1) {
            result = null;
        }

        return result;
    }
    
    @Asynchronous
    public void onNewTrxEvent(@Observes @NewTrxEvent AcctgTrans acctgTrans) {

        Date trxDate = acctgTrans.getTransactionDate();
        List<AccountingPeriod> transactionContexts = statementFacade.getTransactionContexts(trxDate);

        List<AcctgTransDetail> trxDetails = acctgTrans.getAcctgTransDetails();

        for (AcctgTransDetail trxDetail : trxDetails) {
            GLAccount account = trxDetail.getAccount();
            if (account instanceof GLRevenueAccount) {
                for (AccountingPeriod context : transactionContexts) {
                    IncomeStatement statement = statementFacade.getStatements(context);
                    IncomeStatementRevenueDetail detail = getRevenueDetail(statement, (GLRevenueAccount) account);
                    BigDecimal oldAmount = detail.getAmount().getAmount();
                    detail.getAmount().setAmount(oldAmount.add(trxDetail.getAmount().getAmount()));
                    edit(detail);
                }
            }
        }
    }

}
