/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeAttendance;
import net.ap57.tiger.std.hr.entity.EmployeeAttendanceId;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class EmployeeAttendanceFacade extends AbstractFacade<EmployeeAttendance> {

    private static final String INSERT_EMPLOYEE_ATTENDANCE_LOG
            = "INSERT INTO employeeattendance(employee_id, workdate, intime, outtime) "
            + "VALUES(?, ?, ?, ?)";

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeAttendanceFacade() {
        super(EmployeeAttendance.class);
    }

    @Override
    public EmployeeAttendance createTransient(Map<String, Object> params) {
        EmployeeAttendance attendance = new EmployeeAttendance();
        return attendance;
    }

    @Override
    public Object getId(EmployeeAttendance entity) {
        EmployeeAttendanceId id = new EmployeeAttendanceId();
        id.setEmployee(entity.getEmployee().getId());
        id.setWorkDate(entity.getWorkDate());
        return id;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<EmployeeAttendance> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        List<Predicate> predicates = new ArrayList<>();

        switch (filterName) {
//            case "workDate":
//                Date workDate = (Date) filterValue;
//                predicates.add(cb.equal(root.get("workDate"), workDate));
//                break;
            case "employee":                
                predicates.add(cb.equal(root.get("employee").get("id"), filterValue));
                break;
            case "from":
                predicates.add(cb.<Date>greaterThanOrEqualTo(root.get("workDate"), (Date) filterValue));
                break;
            case "thru":
                predicates.add(cb.<Date>lessThanOrEqualTo(root.get("workDate"), (Date) filterValue));
                break;
        }

        return predicates;
    }

    public void saveLog(Employee employee, Date workDate, Calendar inTime, Calendar outTime) throws Exception {

        EmployeeAttendance att = createTransient(null);
        att.setEmployee(employee);
        att.setWorkDate(workDate);
        att.setInTime(inTime);
        att.setOutTime(outTime);

        create(att);

//        System.out.println("WEKTU MASUK = " + inTime);
//        System.out.println("WEKTU PULANG = " + outTime);
//        try {
//            getEntityManager().createNativeQuery(INSERT_EMPLOYEE_ATTENDANCE_LOG)
//                    .setParameter(1, employee.getId())
//                    .setParameter(2, workDate)
//                    .setParameter(3, inTime)
//                    .setParameter(4, outTime)
//                    .executeUpdate();
//        } catch (Exception ex) {
//            throw new EJBException(ex);
//        }
    }

    public List<EmployeeAttendance> viewEmployeeAttendance(Employee employee, Date from, Date thru) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<EmployeeAttendance> cq = cb.createQuery(EmployeeAttendance.class);
        Root<EmployeeAttendance> attendance = cq.from(EmployeeAttendance.class);

        cq.select(attendance).where(new Predicate[]{
            cb.equal(attendance.get("employee").get("id"), employee.getId()),
            cb.greaterThanOrEqualTo(attendance.get("workDate"), from),
            cb.lessThanOrEqualTo(attendance.get("workDate"), thru)
        });

        TypedQuery<EmployeeAttendance> q = getEntityManager().createQuery(cq);

        return q.getResultList();

    }

}
