/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.PositionSchedule;
import net.ap57.tiger.std.hr.entity.PositionScheduleDayException;
import net.ap57.tiger.std.hr.entity.PositionScheduleDayExceptionId;
import net.ap57.tiger.std.hr.entity.PositionScheduleId;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class PositionScheduleDayExceptionFacade extends AbstractFacade<PositionScheduleDayException> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PositionScheduleDayExceptionFacade() {
        super(PositionScheduleDayException.class);
    }

    @Override
    public PositionScheduleDayException createTransient(Map<String, Object> params) throws EJBException {

        WorkScheduleDay scheduleDay = (WorkScheduleDay) params.get("scheduleDay");
        if (scheduleDay == null) {
            return null;
        }

        PositionSchedule positionSchedule = (PositionSchedule) params.get("positionSchedule");
        if (positionSchedule == null) {
            System.out.println("NO POSITIONSCHEDULE FOUND");
            return null;
        }

        PositionScheduleDayException scheduleDayException = new PositionScheduleDayException();
        scheduleDayException.setPositionSchedule(positionSchedule);
        scheduleDayException.setScheduleDay(scheduleDay);
        scheduleDayException.setFromDate(scheduleDay.getFromDate());
        scheduleDayException.setThruDate(scheduleDay.getThruDate());

        return scheduleDayException;
    }

    @Override
    public Object getId(PositionScheduleDayException entity) {

        WorkScheduleDay workScheduleDay = entity.getScheduleDay();
        WorkScheduleDayId workScheduleDayId = new WorkScheduleDayId();
        workScheduleDayId.setTemplate(workScheduleDay.getTemplate().getId());
        workScheduleDayId.setWorkday(workScheduleDay.getWorkday());

        PositionSchedule positionSchedule = entity.getPositionSchedule();
        PositionScheduleId positionScheduleId = new PositionScheduleId();
        positionScheduleId.setWorkSchedule(positionSchedule.getWorkSchedule().getId());
        positionScheduleId.setPosition(positionSchedule.getPosition().getId());
        positionScheduleId.setFromDate(positionSchedule.getFromDate());

        PositionScheduleDayExceptionId id = new PositionScheduleDayExceptionId();
        id.setPositionSchedule(positionScheduleId);
        id.setScheduleDay(workScheduleDayId);

        return id;
    }

    public PositionScheduleDayException getInherentException(WorkScheduleDay scheduleDay, PositionSchedule schedule) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PositionScheduleDayException> cq = cb.createQuery(PositionScheduleDayException.class);
        Root<PositionScheduleDayException> positionScheduleDayException = cq.from(PositionScheduleDayException.class);
        cq.select(positionScheduleDayException).where(new Predicate[]{
            cb.equal(positionScheduleDayException.get("positionSchedule").get("workSchedule").get("id"), schedule.getWorkSchedule().getId()),
            cb.equal(positionScheduleDayException.get("positionSchedule").get("position").get("id"), schedule.getPosition().getId()),
            cb.equal(positionScheduleDayException.get("positionSchedule").get("fromDate"), schedule.getFromDate()),
            cb.equal(positionScheduleDayException.get("scheduleDay").get("template").get("id"), scheduleDay.getTemplate().getId()),
            cb.equal(positionScheduleDayException.get("scheduleDay").get("workday"), scheduleDay.getWorkday()),
        });

        TypedQuery<PositionScheduleDayException> q = getEntityManager().createQuery(cq);

        try {
            return q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void create(PositionScheduleDayException entity) throws EJBException {
        PositionSchedule positionSchedule = entity.getPositionSchedule();
        positionSchedule.getPositionScheduleDayExceptions().add(entity);
        getEntityManager().merge(positionSchedule);
    }

}
