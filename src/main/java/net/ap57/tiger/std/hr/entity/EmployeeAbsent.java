/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(EmployeeAttendanceId.class)
public class EmployeeAbsent implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @OneToOne
  @JoinColumns({
    @JoinColumn(name="EMPLOYEEATTENDANCE_EMPLOYEE_ID", referencedColumnName="EMPLOYEE_ID"),
    @JoinColumn(name="EMPLOYEEATTENDANCE_WORKDATE", referencedColumnName="WORKDATE")
  })
  private EmployeeAttendance employeeAttendance;
  
  @ManyToOne
  private EmployeeAbsentType absentType;
  
  private String remarks;

  public EmployeeAttendance getEmployeeAttendance() {
    return employeeAttendance;
  }

  public void setEmployeeAttendance(EmployeeAttendance employeeAttendance) {
    this.employeeAttendance = employeeAttendance;
  }

  public EmployeeAbsentType getAbsentType() {
    return absentType;
  }

  public void setAbsentType(EmployeeAbsentType absentType) {
    this.absentType = absentType;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }
  
}
