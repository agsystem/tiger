/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(LeaveRequestId.class)
public class LeaveRequest extends StringKeyedEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @ManyToOne
  @JoinColumns({
    @JoinColumn(name="POSITIONFULFILLMENT_POSITION_ID", referencedColumnName="POSITION_ID"),
    @JoinColumn(name="POSITIONFULFILLMENT_EMPLOYEE_ID", referencedColumnName="EMPLOYEE_ID"),
    @JoinColumn(name="POSITIONFULFILLMENT_FROMDATE", referencedColumnName="FROMDATE")
  })
  private PositionFulfillment positionFulfillment;
  
  @Id
  @Column(nullable = false)
  @Temporal(TemporalType.DATE)
  private Date fromDate;
  
  @Column(nullable = false)
  @Temporal(TemporalType.DATE)
  private Date thruDate;
  
  @ManyToOne
  @JoinColumn(nullable = false)
  private LeaveType leaveType;
  
  @ManyToOne
  @JoinColumns({
    @JoinColumn(name="LEAVEPERIOD_LEAVEPERIODTYPE_ID", referencedColumnName="LEAVEPERIODTYPE_ID", nullable=false),
    @JoinColumn(name="LEAVEPERIOD_FROMDATE", referencedColumnName="FROMDATE", nullable=false)
  })
  private LeavePeriod leavePeriod;
  
  private boolean approved;
  
  @Temporal(TemporalType.DATE)
  private Date approvedDate;
  
  @OneToOne(mappedBy = "request", cascade=CascadeType.ALL)
  private ActualLeave actualLeave;
  
  private String remarks;

  public PositionFulfillment getPositionFulfillment() {
    return positionFulfillment;
  }

//  public Employee getEmployee() {
//    return employee;
//  }
//
//  public void setEmployee(Employee employee) {
//    this.employee = employee;
  public void setPositionFulfillment(PositionFulfillment positionFulfillment) {
    this.positionFulfillment = positionFulfillment;
  }

//  }
  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  public Date getThruDate() {
    return thruDate;
  }

  public void setThruDate(Date thruDate) {
    this.thruDate = thruDate;
  }

  public LeaveType getLeaveType() {
    return leaveType;
  }

  public void setLeaveType(LeaveType leaveType) {
    this.leaveType = leaveType;
  }

  public LeavePeriod getLeavePeriod() {
    return leavePeriod;
  }

  public void setLeavePeriod(LeavePeriod leavePeriod) {
    this.leavePeriod = leavePeriod;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public Date getApprovedDate() {
    return approvedDate;
  }

  public void setApprovedDate(Date approvedDate) {
    this.approvedDate = approvedDate;
  }

  public ActualLeave getActualLeave() {
    return actualLeave;
  }

  public void setActualLeave(ActualLeave actualLeave) {
    this.actualLeave = actualLeave;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }
  
}
