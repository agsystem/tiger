/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.EmployeeScheduleDayException;
import net.ap57.tiger.std.hr.entity.EmployeeScheduleDayExceptionId;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionFulfillmentId;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class EmployeeScheduleDayExceptionFacade extends AbstractFacade<EmployeeScheduleDayException> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeScheduleDayExceptionFacade() {
        super(EmployeeScheduleDayException.class);
    }

    @Override
    public EmployeeScheduleDayException createTransient(Map<String, Object> params) throws EJBException {
        
        WorkScheduleDay scheduleDay = (WorkScheduleDay) params.get("scheduleDay");
        if(scheduleDay == null) return null;
        
        PositionFulfillment positionFulfillment = (PositionFulfillment) params.get("positionFulfillment");
        if(positionFulfillment == null) return null;
        
        EmployeeScheduleDayException scheduleDayException = new EmployeeScheduleDayException();
        scheduleDayException.setPositionFulfillment(positionFulfillment);
        scheduleDayException.setScheduleDay(scheduleDay);
        scheduleDayException.setFromDate(scheduleDay.getFromDate());
        scheduleDayException.setThruDate(scheduleDay.getThruDate());
        
        return scheduleDayException;
    }

    @Override
    public Object getId(EmployeeScheduleDayException entity) {
        
        WorkScheduleDay workScheduleDay = entity.getScheduleDay();
        WorkScheduleDayId workScheduleDayId = new WorkScheduleDayId();
        workScheduleDayId.setTemplate(workScheduleDay.getTemplate().getId());
        workScheduleDayId.setWorkday(workScheduleDay.getWorkday());
        
        PositionFulfillment positionFulfillment = entity.getPositionFulfillment();
        PositionFulfillmentId positionFulfillmentId = new PositionFulfillmentId();
        positionFulfillmentId.setPosition(positionFulfillment.getPosition().getId());
        positionFulfillmentId.setEmployee(positionFulfillment.getEmployee().getId());
        positionFulfillmentId.setFromDate(positionFulfillment.getFromDate());
        
        EmployeeScheduleDayExceptionId id = new EmployeeScheduleDayExceptionId();
        id.setPositionFulfillment(positionFulfillmentId);
        id.setScheduleDay(workScheduleDayId);
        
        return id;
    }
    
    public EmployeeScheduleDayException getInherentException(WorkScheduleDay scheduleDay, PositionFulfillment positionFulfillment) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<EmployeeScheduleDayException> cq = cb.createQuery(EmployeeScheduleDayException.class);
        Root<EmployeeScheduleDayException> employeeScheduleDayException = cq.from(EmployeeScheduleDayException.class);
        
        cq.select(employeeScheduleDayException).where(new Predicate[] {
            cb.equal(employeeScheduleDayException.get("positionFulfillment").get("employee").get("id"), positionFulfillment.getEmployee().getId()),
            cb.equal(employeeScheduleDayException.get("positionFulfillment").get("position").get("id"), positionFulfillment.getPosition().getId()),
            cb.equal(employeeScheduleDayException.get("positionFulfillment").get("fromDate"), positionFulfillment.getFromDate()),
            cb.equal(employeeScheduleDayException.get("scheduleDay").get("template").get("id"), scheduleDay.getTemplate().getId()),
            cb.equal(employeeScheduleDayException.get("scheduleDay").get("workday"), scheduleDay.getWorkday()),
        });
        
        TypedQuery<EmployeeScheduleDayException> q = getEntityManager().createQuery(cq);
                    
        try {
            return q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void create(EmployeeScheduleDayException entity) throws EJBException {
        PositionFulfillment positionFulfillment = entity.getPositionFulfillment();
        positionFulfillment.getEmployeeScheduleDayExceptions().add(entity);
        getEntityManager().merge(positionFulfillment);
    }
    
}
