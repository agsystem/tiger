/*
 * Copyright 2017 aphasan.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright aphasan and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document aphasan and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of aphasan is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of aphasan

 */
package net.ap57.tiger.std.hr.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import net.ap57.tiger.base.ui.CaptureClient;
import net.ap57.tiger.base.ui.UploadClient;
import net.ap57.tiger.base.ui.pf.PFDashboard;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author aphasan
 */
public abstract class EmployeeDashboard extends PFDashboard implements CaptureClient, UploadClient {
    
    public byte[] getPhoto() {
        return (byte[]) getAttribute("photo");
    }

    public void setPhoto(byte[] photo) {
        setAttribute("photo", photo);
    }

    @Override
    public void processRawImage(byte[] raw) {
        setPhoto(raw);
    }

    @Override
    public void processUploadedImage(byte[] raw) {
        setPhoto(raw);
    }
    
    public StreamedContent getStreamed() {
        byte[] data = getPhoto();
        InputStream is = null;
        if (data != null) {
            is = new ByteArrayInputStream(data);
        } else {
            is = getContainer().getModule().getResourceAsStream("images/noimg.jpg");
        }
        return new DefaultStreamedContent(is);
    }
    
}
