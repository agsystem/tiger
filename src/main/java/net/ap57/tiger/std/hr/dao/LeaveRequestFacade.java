/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Date;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.LeavePeriod;
import net.ap57.tiger.std.hr.entity.LeavePeriodType;
import net.ap57.tiger.std.hr.entity.LeaveRequest;
import net.ap57.tiger.std.hr.entity.LeaveRequestId;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionFulfillmentId;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class LeaveRequestFacade extends AbstractFacade<LeaveRequest> {
  
  @Inject
  private EntityManager em;
  
  @Inject
  private PositionFulfillmentFacade positionFulfillmentFacade;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public LeaveRequestFacade() {
    super(LeaveRequest.class);
  }

  @Override
  public LeaveRequest createTransient(Map<String, Object> params) {
    LeaveRequest leaveRequest = new LeaveRequest();
    return leaveRequest;
  }

  @Override
  public Object getId(LeaveRequest entity) {
    LeaveRequestId id = new LeaveRequestId();
    PositionFulfillment e = entity.getPositionFulfillment();
    if(e == null) return null;
    id.setPositionFulfillment((PositionFulfillmentId) positionFulfillmentFacade.getId(entity.getPositionFulfillment()));
    id.setFromDate(entity.getFromDate());
    return id;
  }
  
  private LeavePeriod getLeavePeriodInContext(LeavePeriodType leavePeriodType, Date fromDate, Date thruDate) throws EJBException {
    
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<LeavePeriod> cq = cb.createQuery(LeavePeriod.class);
    
    Root<LeavePeriod> root = cq.from(LeavePeriod.class);
    cq.select(root).where(new Predicate[]{
      cb.equal(root.get("leavePeriodType"), leavePeriodType),
      cb.lessThanOrEqualTo(root.get("fromDate"), fromDate),
      cb.greaterThanOrEqualTo(root.get("thruDate"), thruDate)
    });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
    TypedQuery<LeavePeriod> q = getEntityManager().createQuery(cq);
    
    try{
      return q.getSingleResult();
    } catch (Exception ex) {
      throw new EJBException(ex);
    }
    
  }

  @Override
  public void create(LeaveRequest entity) throws EJBException {
    try {
    LeavePeriod leavePeriod = getLeavePeriodInContext(entity.getLeaveType().getRecurrenceType(), entity.getFromDate(), entity.getThruDate());
    entity.setLeavePeriod(leavePeriod);
    super.create(entity);
    } catch (Exception ex) {
      throw new EJBException(ex);
    }
  }
  
}
