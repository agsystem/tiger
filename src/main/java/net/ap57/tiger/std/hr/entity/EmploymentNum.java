/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;

/**
 *
 * @author mbahbejo
 */
@Entity
@NamedQueries({
  @NamedQuery(name = "EmploymentNum.getLatest",
      query = "SELECT en "
      + "FROM EmploymentNum en")
})
public class EmploymentNum implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @TableGenerator(name = "EmploymentNum", table = "KEYGEN")
  @GeneratedValue(generator = "EmploymentNum", strategy = GenerationType.TABLE)
  private Long id;

  private int y;

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  private int m;

  public int getM() {
    return m;
  }

  public void setM(int m) {
    this.m = m;
  }

  private int s;

  public int getS() {
    return s;
  }

  public void setS(int s) {
    this.s = s;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

}
