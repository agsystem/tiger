/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import net.ap57.tiger.base.entity.PartyRelationship;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.std.hr.dao.EmploymentTerminationType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
public class Employment extends PartyRelationship implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(unique = true)
    private String refNum;

    @ManyToOne
    private EmploymentAgreementType agreementType;

    @ManyToOne
    private InternalOrganization internalOrganization;

    @ManyToOne
    private Employee employee;

    @OneToMany(mappedBy = "employment", cascade = CascadeType.ALL)
    private List<PositionFulfillment> positionFulfillments;

    @ManyToOne
    private EmploymentTerminationType terminationType;
    
    private String terminationReason;

    public String getRefNum() {
        return refNum;
    }

    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }

    public EmploymentAgreementType getAgreementType() {
        return agreementType;
    }

    public void setAgreementType(EmploymentAgreementType agreementType) {
        this.agreementType = agreementType;
    }

    public InternalOrganization getInternalOrganization() {
        return internalOrganization;
    }

    public void setInternalOrganization(InternalOrganization internalOrganization) {
        this.internalOrganization = internalOrganization;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<PositionFulfillment> getPositionFulfillments() {
        return positionFulfillments;
    }

    public void setPositionFulfillments(List<PositionFulfillment> positionFulfillments) {
        this.positionFulfillments = positionFulfillments;
    }

    @Override
    public PartyRole getEstablisher() {
        return getInternalOrganization();
    }

    @Override
    public void setEstablisher(PartyRole establisher) {
        setInternalOrganization((InternalOrganization) establisher);
    }

    @Override
    public PartyRole getParticipant() {
        return getEmployee();
    }

    @Override
    public void setParticipant(PartyRole participant) {
        setEmployee((Employee) participant);
    }

    public EmploymentTerminationType getTerminationType() {
        return terminationType;
    }

    public void setTerminationType(EmploymentTerminationType terminationType) {
        this.terminationType = terminationType;
    }

    public String getTerminationReason() {
        return terminationReason;
    }

    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

}
