/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.ui.pf.PFSchedule;
import net.ap57.tiger.base.ui.pf.PFScheduleEvent;
import net.ap57.tiger.std.hr.dao.WorkScheduleDayFacade;
import net.ap57.tiger.std.hr.entity.ScheduleDayException;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkSchedule;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayException;
import net.ap57.tiger.std.hr.service.ScheduleService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class WorkdaySchedule<E extends ScheduleDayException> extends PFSchedule<WorkScheduleDay, WorkScheduleDayFacade> {

    protected abstract ScheduleService getExceptionOp();

    protected abstract WorkSchedule getScheduleContext();

    @Override
    protected Map<String, Object> getLoadFilters() {

        Map<String, Object> filters = new HashMap<>();
        filters.put("selectedSchedule", getScheduleContext());
        return filters;

    }

    @Override
    protected Map<String, Object> getNewEntityParams() {
        Map<String, Object> params = new HashMap<>();
        params.put("scheduleDay", getScheduleContext());
        params.put("selectedDate", getSelectedDate());
        return params;
    }

    protected Date getCurrentDate() {
        Calendar current = Calendar.getInstance();
        return (Date) getAttribute("currentDate", current.getTime());
    }

    protected void setCurrentDate(Date date) {
        setAttribute("currentDate", date);
    }

    @Override
    public void onDateSelect(SelectEvent e) {

        Date date = (Date) e.getObject();

        WorkScheduleDay scheduleDay = getOp().getSpecificSchedule(getScheduleContext(), date);
        if (scheduleDay != null && !hasException(scheduleDay)) {
            raiseExceptionDialog(scheduleDay);
        } else if (scheduleDay == null) {
            raiseCreateDialog(date);
        } else {

        }

    }

    protected abstract boolean hasException(WorkScheduleDay scheduleDay);

    protected void raiseCreateDialog(Date workday) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("schedule-template-day-create-dlg");
        requestContext.update("schedule-template-day-create-frm");
        requestContext.execute("PF('scheduleTemplateDayCreateDlg').show()");
        super.onDateSelect(workday);
    }

    protected void raiseExceptionDialog(WorkScheduleDay scheduleDay) {
        createException(scheduleDay);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("schedule-template-day-exception-create-dlg");
        requestContext.update("schedule-template-day-exception-create-frm");
        requestContext.execute("PF('scheduleTemplateDayExceptionCreateDlg').show()");
    }

    @Override
    public void onEventSelect(SelectEvent selectEvent) {
        super.onEventSelect(selectEvent);
        PFScheduleEvent obj = (PFScheduleEvent) selectEvent.getObject();
        if (obj.getEvent() instanceof WorkScheduleDay) {
            raiseEditDialog();
        } else if (getExceptionClass().isInstance(obj.getEvent())) {
            setException((E) obj.getEvent());
            raiseExceptionEditDialog();
        }
    }

    public void raiseEditDialog() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("schedule-template-day-edit-dlg");
        requestContext.update("schedule-template-day-edit-frm");
        requestContext.execute("PF('scheduleTemplateDayEditDlg').show()");
    }

    public void raiseExceptionEditDialog() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("schedule-template-day-exception-edit-dlg");
        requestContext.update("schedule-template-day-exception-edit-frm");
        requestContext.execute("PF('scheduleTemplateDayExceptionEditDlg').show()");
    }

    public void setException(E exception) {
        setAttribute("exception", exception);
    }

    public E getException() {
        return (E) getAttribute("exception");
    }

    protected abstract Class<E> getExceptionClass();

    protected Map<String, Object> getNewExceptionParams() {
        Map<String, Object> params = new HashMap<>();
        return params;
    }

    protected void createException(WorkScheduleDay scheduleDay) {
        Map<String, Object> params = getNewExceptionParams();
        params.put("scheduleDay", scheduleDay);
        E scheduleDayException = (E) getExceptionOp().createTransient(params, getExceptionClass());
        setException(scheduleDayException);
    }

    public void addException(ActionEvent evt) {
        getExceptionOp().create(getException());
    }

    @Override
    protected List<PFScheduleEvent> collectEvents(Date from, Date thru, Map<String, Object> filters) {

        List<WorkScheduleDay> rawEvents = getOp().getEvents(from, thru, filters);
        List<PFScheduleEvent> events = new ArrayList<>();

        for (WorkScheduleDay rawEvent : rawEvents) {
            PFScheduleEvent scheduleEvent = new PFScheduleEvent(rawEvent);

            WorkScheduleDayException workScheduleDayException = rawEvent.getException();
            if (workScheduleDayException != null) {
                scheduleEvent.setStyleClass("schedule-inactive");
                events.add(scheduleEvent);
                
                PFScheduleEvent scheduleExceptionEvent = new PFScheduleEvent(workScheduleDayException);
                scheduleExceptionEvent.setStyleClass("schedule-active");
                events.add(scheduleExceptionEvent);
            } else {                
                scheduleEvent.setStyleClass("schedule-active");
                events.add(scheduleEvent);
            }

        }

        return events;
    }

    public void deleteException(ActionEvent event) {
        E exception = getException();
        WorkScheduleDay scheduleDay = exception.getScheduleDay();
        scheduleDay.setException(null);
        getOp().merge(scheduleDay);
        getExceptionOp().remove(exception);
    }

}
