/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(EmployeeAttendanceId.class)
public class EmployeeAttendance extends StringKeyedEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @ManyToOne
    private PartyRole employee;
    
    @Id
    @Temporal(TemporalType.DATE)
    private Date workDate;
    
    @Column(columnDefinition="TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar inTime;
    
    @Column(columnDefinition="TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar outTime;
    
    @ElementCollection
    private List<Calendar> unspecifiedTimes;
    
//  @OneToMany(mappedBy = "employeeAttendance", cascade=CascadeType.ALL)
//  @MapKey(name = "logType")
//  private Map<EmployeeAttendanceLogType, EmployeeAttendanceLog> employeeAttendanceLogs;
//  @OneToOne(mappedBy = "employeeAttendance")
//  private EmployeeAbsent employeeAbsent;
    
    public Employee getEmployee() {
        return (Employee) employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public Date getWorkDate() {
        return workDate;
    }
    
    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }
    
    public Calendar getInTime() {
        return inTime;
    }

    public void setInTime(Calendar inTime) {
        this.inTime = inTime;
    }

    public Calendar getOutTime() {
        return outTime;
    }

    public void setOutTime(Calendar outTime) {
        this.outTime = outTime;
    }

//  public Map<EmployeeAttendanceLogType, EmployeeAttendanceLog> getEmployeeAttendanceLogs() {
//    return employeeAttendanceLogs;
//  }
//
//  public void setEmployeeAttendanceLogs(Map<EmployeeAttendanceLogType, EmployeeAttendanceLog> employeeAttendanceLogs) {
//    this.employeeAttendanceLogs = employeeAttendanceLogs;
//  }
//  public EmployeeAbsent getEmployeeAbsent() {
//    return employeeAbsent;
//  }
//
//  public void setEmployeeAbsent(EmployeeAbsent employeeAbsent) {
//    this.employeeAbsent = employeeAbsent;
//  }

    public List<Calendar> getUnspecifiedTimes() {
        return unspecifiedTimes;
    }

    public void setUnspecifiedTimes(List<Calendar> unspecifiedTimes) {
        this.unspecifiedTimes = unspecifiedTimes;
    }
}
