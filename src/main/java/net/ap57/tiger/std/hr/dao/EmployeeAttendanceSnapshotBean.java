/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeAttendanceSnapshot;
//import net.ap57.tiger.std.hr.entity.EmployeeWorkSchedule;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionWorkUnit;
import net.ap57.tiger.std.hr.entity.PresenceData;
import net.ap57.tiger.std.hr.entity.PresenceDataId;
import net.ap57.tiger.std.hr.service.PresenceService;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class EmployeeAttendanceSnapshotBean extends AbstractFacade<EmployeeAttendanceSnapshot> {

    @Inject
    private EntityManager em;

    @Inject
    private EmployeeFacade employeeFacade;

    @Inject
    private /*EmployeeAttendanceFacade*/PresenceDataFacade attendanceFacade;
    
    @Inject
    private PresenceService presenceService;

//    @Inject
//    private EmployeeWorkScheduleFacade scheduleFacade;

//    @Inject
//    @FindAttendanceEvent
//    private Event<EmployeeWorkSchedule> findAttendanceEvent;

    public EmployeeAttendanceSnapshotBean() {
        super(EmployeeAttendanceSnapshot.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeAttendanceSnapshot getObj(String strKey, Date workDate) {

        Employee emp = employeeFacade.findSingleByAttribute("strKey", strKey);

//        EmployeeAttendanceId id = new EmployeeAttendanceId();
        PresenceDataId id = new PresenceDataId();
        id.setEmployee(emp.getId());
        id.setWorkday(workDate);

        /*EmployeeAttendance*/PresenceData attendance = attendanceFacade.find(id);

        return new EmployeeAttendanceSnapshot(emp, attendance);

//        return null;

    }

    @Override
    public EmployeeAttendanceSnapshot createTransient(Map<String, Object> params) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getId(EmployeeAttendanceSnapshot entity) {
        return entity.getEmployee().getId();
    }

    @Override
    public void create(EmployeeAttendanceSnapshot es) throws EJBException {
        attendanceFacade.create(es.getAttendance());
    }

    @Override
    public void edit(EmployeeAttendanceSnapshot es) throws EJBException {
//        EmployeeAttendanceId attId = new EmployeeAttendanceId();
        PresenceDataId attId = new PresenceDataId();
        attId.setEmployee(es.getEmployee().getId());
        attId.setWorkday(es.getWorkDate());
        /*EmployeeAttendance*/PresenceData att = attendanceFacade.find(attId);
        if (att == null) {
            try {
                create(es);
            } catch (Exception ex) {
                throw new EJBException(ex);
            }
        } else {
//            attendanceFacade.edit(es.getAttendance());
        }
    }

    @Override
    public void remove(EmployeeAttendanceSnapshot es) {
//        attendanceFacade.remove(es.getAttendance());
    }

    @Override
    protected CriteriaQuery<EmployeeAttendanceSnapshot> constructFindQuery(Map<String, Object> filters) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<EmployeeAttendanceSnapshot> cq = cb.createQuery(EmployeeAttendanceSnapshot.class);

        Root<Employee> e1 = cq.from(Employee.class);
        Join<Employee, /*EmployeeAttendance*/PresenceData> att
                = e1.join(/*"employeeAttendances"*/"presences", JoinType.LEFT);
        att.on(cb.equal(att.get(/*"workDate"*/"workday"), filters.get(/*"workDate"*/"workday")));

        Subquery<UUID> sq1 = cq.subquery(UUID.class);
        Root<Employee> e2 = sq1.from(Employee.class);
        Join<Employee, PositionFulfillment> pf1 = e2.join("positionFulfillments");

        Subquery<Date> sq2 = sq1.subquery(Date.class);
        Root<PositionFulfillment> pf2 = sq2.from(PositionFulfillment.class);
        sq2.select(cb.greatest(pf2.<Date>get("fromDate"))).where(new Predicate[]{
            cb.equal(pf2.get("employee").get("id"), e2.get("id")),
            cb.lessThanOrEqualTo(pf2.<Date>get("fromDate"), (Date) filters.get(/*"workDate"*/"workday"))
        });

        Subquery<UUID> sq3 = sq1.subquery(UUID.class);
        Root<Position> p = sq2.from(Position.class);
        Join<Position, PositionWorkUnit> pwu = p.join("workUnits");
        sq3.select(p.get("id")).where(cb.equal(pwu.get("workUnit").get("name"), filters.get("WorkUnit")));

        sq1.select(e2.get("id")).where(new Predicate[]{
            cb.equal(pf1.get("fromDate"), sq2),
            pf1.get("position").get("id").in(sq3)
        });

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(e1.get("id").in(sq1));
        for (String filterName : filters.keySet()) {
            switch (filterName) {
                case "employee.person.name":
                    Expression<String> literal = cb.upper(cb.literal("%" + filters.get(filterName) + "%"));
                    predicates.add(cb.or(cb.like(cb.upper(e1.get("person").<String>get("firstName")), literal),
                            cb.like(cb.upper(e1.get("person").<String>get("lastName")), literal)));
                    break;
                default:
                    break;
            }
        }
        cq.multiselect(e1, att).where(predicates.toArray(new Predicate[predicates.size()]));

        return cq;

    }

    @Override
    protected CriteriaQuery<Long> constructCountQuery(Map<String, Object> filters) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<Employee> e1 = cq.from(Employee.class);
        Join<Employee, /*EmployeeAttendance*/PresenceData> att = e1.join(/*"employeeAttendances"*/"presences", JoinType.LEFT);
        att.on(cb.equal(att.get(/*"workDate"*/"workday"), filters.get(/*"workDate"*/"workday")));

        Subquery<UUID> sq1 = cq.subquery(UUID.class);
        Root<Employee> e2 = sq1.from(Employee.class);
        Join<Employee, PositionFulfillment> pf1 = e2.join("positionFulfillments");

        Subquery<Date> sq2 = sq1.subquery(Date.class);
        Root<PositionFulfillment> pf2 = sq2.from(PositionFulfillment.class);
        sq2.select(cb.greatest(pf2.<Date>get("fromDate"))).where(new Predicate[]{
            cb.equal(pf2.get("employee").get("id"), e2.get("id")),
            cb.lessThanOrEqualTo(pf2.<Date>get("fromDate"), (Date) filters.get(/*"workDate"*/"workday"))
        });

        Subquery<UUID> sq3 = sq1.subquery(UUID.class);
        Root<Position> p = sq2.from(Position.class);
        Join<Position, PositionWorkUnit> pwu = p.join("workUnits");
        sq3.select(p.get("id")).where(cb.equal(pwu.get("workUnit").get("name"), filters.get("WorkUnit")));

        sq1.select(e2.get("id")).where(new Predicate[]{
            cb.equal(pf1.get("fromDate"), sq2),
            pf1.get("position").get("id").in(sq3)
        });

        cq.select(cb.count(e1)).where(e1.get("id").in(sq1));

        return cq;

    }

    public void retrieveAttendance(Employee employee, Date from, Date thru) {
//        List<EmployeeWorkSchedule> schedules = scheduleFacade.retrieveSchedule(employee, from, thru);
//        schedules.stream().forEach((schedule) -> {
//            findAttendanceEvent.fire(schedule);
//        });
        presenceService.calculatePresence(employee, from, thru);
    }

    public void retrieveAllAttendance(Date from, Date thru) {
        System.out.println("EMPLOYEE ATTENDANCE SNAPSHOT BEAN");
        for(Employee employee : employeeFacade.findAll()) {
            System.out.println("PROCESSING: " + employee.getPerson().getName());
            retrieveAttendance(employee, from, thru);
        }
//        employeeFacade.findAll().stream().forEach((employee) -> {
//            retrieveAttendance(employee, from, thru);
//        });
    }
    
    public List<PresenceData> viewAttendance(Employee employee, Date from, Date thru) {
        return attendanceFacade.viewEmployeeAttendance(employee, from, thru);
    }

}
