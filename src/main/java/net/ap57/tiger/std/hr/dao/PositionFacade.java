/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved
/modres/report_template/bkg.png
 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.InternalOrganization;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionSchedule;
//import net.ap57.tiger.std.hr.entity.PositionSchedule;
//import net.ap57.tiger.std.hr.entity.PositionScheduleVarian;
import net.ap57.tiger.std.hr.entity.PositionStatus;
import net.ap57.tiger.std.hr.entity.PositionWorkUnit;
import net.ap57.tiger.std.hr.entity.WorkSchedule;
import net.ap57.tiger.std.hr.entity.WorkUnit;
//import net.ap57.tiger.std.hr.entity.WorkUnitSchedule;
//import net.ap57.tiger.std.hr.entity.WorkUnitScheduleVarian;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class PositionFacade extends AbstractFacade<Position> {

    @Inject
    private EntityManager em;

    @Inject
    private InternalOrganizationFacade internalOrganizationFacade;

//    @Inject
//    private WorkUnitScheduleFacade unitScheduleFacade;
//
//    @Inject
//    private PositionScheduleFacade posScheduleFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PositionFacade() {
        super(Position.class);
    }

    @Override
    public Position createTransient(Map<String, Object> params) {

        Position position = new Position();

        position.setHiringOrganization((InternalOrganization) params.get("hiringOrganization"));
        
        position.setFullTime(true);
        position.setExempt(false);
        position.setPermanent(true);
        position.setSalary(true);
        position.setStatus(PositionStatus.ACTIVE);

        position.setWorkUnits(new ArrayList<>());
        
        position.setPositionSchedules(new ArrayList<>());

        return position;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<Position> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        List<Predicate> predicates = new ArrayList<>();

        switch (filterName) {
            case "hiringOrganization":
                InternalOrganization org = (InternalOrganization) filterValue;
                List<Long> partyIds = new ArrayList<>();
                partyIds.add(org.getOrganization().getId());
                internalOrganizationFacade.getDescendants(org, partyIds);
                predicates.add(root.get("hiringOrganization").get("organization").get("id").in(partyIds));
                break;
            case "type":
                String type = (String) filterValue;
                if (type.equals("All")) {
                    break;
                }
                Expression<String> lType = cb.literal(type);
                predicates.add(cb.equal(root.get("type").get("title"), lType));
                break;
            case "name":
                Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                predicates.add(cb.like(cb.upper(root.<String>get("name")), literal));
                break;
            case "workUnits":
                String workUnitName = (String) filterValue;
                if (workUnitName.equals("All")) {
                    break;
                }
                Expression<String> lWorkUnitName = cb.literal(workUnitName);
                Join<Position, PositionWorkUnit> positionWorkUnit = root.join("workUnits");
                predicates.add(cb.equal(positionWorkUnit.get("workUnit").get("name"), lWorkUnitName));
                break;
            default:
                break;
        }

        return predicates;
    }

    @Override
    protected void addCriteria(Root<Position> root, CriteriaQuery<Position> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("created")));
    }

    @Override
    public Object getId(Position entity) {
        return entity.getId();
    }

    @Override
    public void create(Position entity) throws EJBException {

        for (PositionWorkUnit pwu : entity.getWorkUnits()) {
            WorkUnit wu = pwu.getWorkUnit();
//            for (WorkUnitScheduleVarian wsv : wu.getWorkUnitScheduleVarians()) {
//                PositionScheduleVarian psv = new PositionScheduleVarian();
//                psv.setPositionWorkUnit(pwu);
//                pwu.getPositionScheduleVarians().add(psv);
//                psv.setWorkUnitScheduleVarian(wsv);
//                wsv.getPositionScheduleVarians().add(psv);
//                psv.setDefaulted(false);
//                psv.setUsed(false);
//
//                List<PositionSchedule> positionSchedules = new ArrayList<>();
//
//                for (WorkUnitSchedule unitSchedule : unitScheduleFacade.getSchedule(wsv, entity.getEstimatedFromDate())) {
//                    PositionSchedule posSchedule = new PositionSchedule();
//                    posSchedule.setWorkDate(unitSchedule.getWorkDate());
//                    posSchedule.setFromDate(unitSchedule.getFromDate());
//                    posSchedule.setThruDate(unitSchedule.getThruDate());
//                    posSchedule.setParent(unitSchedule);
//                    unitSchedule.getChildren().add(posSchedule);
//                    posSchedule.setPositionScheduleVarian(psv);
//
//                    positionSchedules.add(posSchedule);
//                }

//                psv.setPositionSchedules(positionSchedules);
//            }
        }

        super.create(entity);
    }

    @Override
    public void edit(Position entity) {
        List<PositionWorkUnit> newPositionWorkUnits = entity.getWorkUnits();

        Position editedPosition = entity;

        entity = find(entity.getId());
        List<PositionWorkUnit> oldPositionWorkUnits = entity.getWorkUnits();

        List<PositionWorkUnit> tobeDeleted = new ArrayList<>();
        for (PositionWorkUnit oldPositionWorkUnit : oldPositionWorkUnits) {
            if (!newPositionWorkUnits.contains(oldPositionWorkUnit)) {
                tobeDeleted.add(oldPositionWorkUnit);
            }
        }

        for (PositionWorkUnit itemDeleted : tobeDeleted) {
            oldPositionWorkUnits.remove(itemDeleted);
            getEntityManager().remove(itemDeleted);
        }

        List<PositionWorkUnit> tobeAdded = new ArrayList<>();
        List<PositionWorkUnit> intactUnits = new ArrayList<>();
        for (PositionWorkUnit newPositionWorkUnit : newPositionWorkUnits) {
            if (!oldPositionWorkUnits.contains(newPositionWorkUnit)) {
                tobeAdded.add(newPositionWorkUnit);
            } else {
                int idx = oldPositionWorkUnits.indexOf(newPositionWorkUnit);
                intactUnits.add(oldPositionWorkUnits.get(idx));
            }
        }

//        for (PositionWorkUnit pwu : tobeAdded) {
//            List<PositionScheduleVarian> psvs = pwu.getPositionScheduleVarians();
//
//            if (psvs.isEmpty()) {
//                WorkUnit wu = pwu.getWorkUnit();
//                for (WorkUnitScheduleVarian wsv : wu.getWorkUnitScheduleVarians()) {
//
//                    PositionScheduleVarian psv = new PositionScheduleVarian();
//                    psv.setPositionWorkUnit(pwu);
//                    pwu.getPositionScheduleVarians().add(psv);
//                    psv.setWorkUnitScheduleVarian(wsv);
//                    wsv.getPositionScheduleVarians().add(psv);
//                    psv.setDefaulted(false);
//                    psv.setUsed(false);
//
//                    List<PositionSchedule> positionSchedules = new ArrayList<>();
//
//                    for (WorkUnitSchedule unitSchedule : unitScheduleFacade.getSchedule(wsv, editedPosition.getEstimatedFromDate())) {
//                        PositionSchedule posSchedule = new PositionSchedule();
//                        posSchedule.setWorkDate(unitSchedule.getWorkDate());
//                        posSchedule.setFromDate(unitSchedule.getFromDate());
//                        posSchedule.setThruDate(unitSchedule.getThruDate());
//                        posSchedule.setParent(unitSchedule);
//                        unitSchedule.getChildren().add(posSchedule);
//                        posSchedule.setPositionScheduleVarian(psv);
//                        
//                        positionSchedules.add(posSchedule);
//                    }
//
//                    psv.setPositionSchedules(positionSchedules);
//
//                }
//            }
//
//            oldPositionWorkUnits.add(pwu);
//        }

        if (!editedPosition.getEstimatedFromDate().equals(entity.getEstimatedFromDate())) {
            for (PositionWorkUnit intactUnit : intactUnits) {
//                for (PositionScheduleVarian intactVarian : intactUnit.getPositionScheduleVarians()) {
//                    if (editedPosition.getEstimatedFromDate().before(entity.getEstimatedFromDate())) {
//                        for (WorkUnitSchedule unitSchedule : unitScheduleFacade.getSchedule(intactVarian.getWorkUnitScheduleVarian(), editedPosition.getEstimatedFromDate(), entity.getEstimatedFromDate())) {
//                            PositionSchedule posSchedule = new PositionSchedule();
//                            posSchedule.setWorkDate(unitSchedule.getWorkDate());
//                            posSchedule.setFromDate(unitSchedule.getFromDate());
//                            posSchedule.setThruDate(unitSchedule.getThruDate());
//                            posSchedule.setParent(unitSchedule);
//                            unitSchedule.getChildren().add(posSchedule);
//                            posSchedule.setPositionScheduleVarian(intactVarian);
//
//                            intactVarian.getPositionSchedules().add(posSchedule);
//                        }
//                    } else {
//                        posScheduleFacade.deleteScheduleLess(intactVarian, editedPosition.getEstimatedFromDate());
//                    }
//                }
            }
        }

        entity.setName(editedPosition.getName());
        entity.setType(editedPosition.getType());
        entity.setStatus(editedPosition.getStatus());
        entity.setEstimatedFromDate(editedPosition.getEstimatedFromDate());
        entity.setEstimatedThruDate(editedPosition.getEstimatedThruDate());
        entity.setActualFromDate(editedPosition.getActualFromDate());
        entity.setActualThruDate(editedPosition.getActualThruDate());
        entity.setFullTime(editedPosition.isFullTime());
        entity.setPermanent(editedPosition.isPermanent());
        entity.setSalary(editedPosition.isSalary());
        entity.setExempt(editedPosition.isExempt());
        entity.setDescription(editedPosition.getDescription());
        entity.setRequired(editedPosition.getRequired());

        super.edit(entity);

    }
    
    public WorkSchedule getInherentWorkSchedule(Position pos, Date workday) {
    
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkSchedule> cq = cb.createQuery(WorkSchedule.class);
        Root<Position> position = cq.from(Position.class);
        Join<Position, PositionSchedule> positionSchedule = position.join("positionSchedules");
        
        Subquery<Date> sq = cq.subquery(Date.class);
        Root<PositionSchedule> sqPositionSchedule = sq.from(PositionSchedule.class);
        sq.select(cb.greatest(sqPositionSchedule.<Date>get("fromDate"))).where(new Predicate[] {
            cb.equal(position.get("id"), pos.getId()),
            cb.lessThanOrEqualTo(sqPositionSchedule.get("fromDate"), workday)
        });
        
        cq.select(positionSchedule.get("workSchedule")).where(new Predicate[]{
            cb.equal(position.get("id"), pos.getId()),
            cb.equal(positionSchedule.get("fromDate"), sq)
        });
        
        TypedQuery<WorkSchedule> q = getEntityManager().createQuery(cq);
        
        try {
            
            return q.getSingleResult();
            
        } catch (Exception ex) {
            return null;
        }
    }
    
    public PositionSchedule getPositionScheduleByDate(Position pos, Date workday) {
    
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PositionSchedule> cq = cb.createQuery(PositionSchedule.class);
        Root<Position> position = cq.from(Position.class);
        Join<Position, PositionSchedule> positionSchedule = position.join("positionSchedules");
        
        Subquery<Date> sq = cq.subquery(Date.class);
        Root<PositionSchedule> sqPositionSchedule = sq.from(PositionSchedule.class);
        sq.select(cb.greatest(sqPositionSchedule.<Date>get("fromDate"))).where(new Predicate[] {
            cb.equal(position.get("id"), pos.getId()),
            cb.lessThanOrEqualTo(sqPositionSchedule.get("fromDate"), workday)
        });
        
        cq.select(positionSchedule).where(new Predicate[]{
            cb.equal(position.get("id"), pos.getId()),
            cb.equal(positionSchedule.get("fromDate"), sq)
        });
        
        TypedQuery<PositionSchedule> q = getEntityManager().createQuery(cq);
        
        try {
            
            return q.getSingleResult();
            
        } catch (Exception ex) {
            return null;
        }
    }

}
