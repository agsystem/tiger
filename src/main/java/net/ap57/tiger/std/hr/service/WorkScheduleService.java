/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import net.ap57.tiger.base.service.ScriptService;
import net.ap57.tiger.std.hr.dao.EmployeeFacade;
import net.ap57.tiger.std.hr.dao.PositionFacade;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class WorkScheduleService {

    @Inject
    private ScriptService scriptService;

    @Inject
    private PositionFacade positionFacade;

//    @Inject
//    private WorkUnitScheduleVarianFacade workUnitScheduleVarianFacade;
//
//    @Inject
//    private WorkUnitScheduleFacade workUnitScheduleFacade;
//
//    @Inject
//    private PositionScheduleVarianFacade positionScheduleVarianFacade;
//
//    @Inject
//    private PositionScheduleFacade positionScheduleFacade;

    @Inject
    private EmployeeFacade employeeFacade;

    @Inject
    private PositionFulfillmentFacade positionFulfillmentFacade;
    
    public void test() {
        
    }
    
//    public void onWorkUnitScheduleCreateEvent(@Observes @WorkUnitScheduleCreateEvent WorkUnitSchedule workUnitSchedule) {
//        inherit(workUnitSchedule);
//    }

//    public void inherit(WorkUnitSchedule parent) {
//        WorkUnitScheduleVarian workUnitScheduleVarian = parent.getScheduleVarian();
//        List<PositionScheduleVarian> positionScheduleVarians = positionScheduleVarianFacade.getScheduleVarianOf(workUnitScheduleVarian);
//        for (PositionScheduleVarian positionScheduleVarian : positionScheduleVarians) {
//            PositionSchedule positionSchedule = new PositionSchedule();
//            positionSchedule.setPositionScheduleVarian(positionScheduleVarian);
//            positionScheduleVarian.getPositionSchedules().add(positionSchedule);
//            positionSchedule.setWorkDate(parent.getWorkDate());
//            positionSchedule.setFromDate(parent.getUsedFromDate());
//            positionSchedule.setThruDate(parent.getUsedThruDate());
//            positionSchedule.setParent(parent);
//            parent.getChildren().add(positionSchedule);
//            try {
//                positionScheduleFacade.create(positionSchedule);
//            } catch (Exception ex) {
//                Logger.getLogger(WorkScheduleService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        workUnitScheduleFacade.edit(parent);
//    }

//    public void onPositionScheduleCreateEvent(@Observes @PositionScheduleCreateEvent PositionSchedule positionSchedule) {
//        inherit(positionSchedule);
//    }

//    public void inherit(PositionSchedule parent) {
//        for (Employee employee : employeeFacade.getEmployeeInContext(parent)) {
//            EmployeeWorkSchedule newSchedule = new EmployeeWorkSchedule();
//            newSchedule.setEmployee(employee);
//            employee.getEmployeeWorkSchedules().add(newSchedule);
//            newSchedule.setWorkDate(parent.getWorkDate());
//            newSchedule.setFromDate(parent.getFromDate());
//            newSchedule.setThruDate(parent.getThruDate());
//            newSchedule.setParent(parent);
//            parent.getChildren().add(newSchedule);
//            employeeFacade.edit(employee);
//        }
//
//    }

//    public void interpolate(WorkUnitScheduleVarian scheduleVarian) {
//        scheduleVarian = workUnitScheduleVarianFacade.find(workUnitScheduleVarianFacade.getId(scheduleVarian));
//        for (WorkUnitSchedule schedule : workUnitScheduleVarianFacade.getUninterpolatedSchedule(scheduleVarian)) {
//            interpolate(schedule);
//        }
//        workUnitScheduleVarianFacade.edit(scheduleVarian);
//    }

//    public void interpolate(WorkUnitSchedule sourceSchedule) {
//        Calendar nextWorkDate = Calendar.getInstance();
//        nextWorkDate.setTime(sourceSchedule.getWorkDate());
//        Calendar interpolatedFromDate = Calendar.getInstance();
//        Calendar interpolatedThruDate = Calendar.getInstance();
//        if (sourceSchedule.isPropagate()) {
//            interpolatedFromDate.setTime(sourceSchedule.getUsedFromDate());
//            interpolatedThruDate.setTime(sourceSchedule.getUsedThruDate());
//        } else {
//            interpolatedFromDate.setTime(sourceSchedule.getInterpolatedFromDate());
//            interpolatedThruDate.setTime(sourceSchedule.getInterpolatedThruDate());
//        }
//        Map<String, Object> params = new HashMap<>();
//        params.put("workDate", nextWorkDate);
//        params.put("fromDate", interpolatedFromDate);
//        params.put("thruDate", interpolatedThruDate);
//        try {
//            scriptService.executeScript("HR.WorkUnitScheduleInterpolation", params, "Groovy");
//        } catch (EJBException ex) {
//            Logger.getLogger(WorkUnitScheduleVarianFacade.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        WorkUnitSchedule newSchedule = new WorkUnitSchedule();
//        newSchedule.setScheduleVarian(sourceSchedule.getScheduleVarian());
//        sourceSchedule.getScheduleVarian().getWorkUnitSchedules().add(newSchedule);
//        newSchedule.setWorkDate(nextWorkDate.getTime());
//        newSchedule.setInterpolatedFromDate(interpolatedFromDate.getTime());
//        newSchedule.setInterpolatedThruDate(interpolatedThruDate.getTime());
//        newSchedule.setUsedFromDate(newSchedule.getInterpolatedFromDate());
//        newSchedule.setUsedThruDate(newSchedule.getInterpolatedThruDate());
//        newSchedule.setSourceSchedule(sourceSchedule);
//        sourceSchedule.setInterpolatedSchedule(newSchedule);
//        newSchedule.setChildren(new ArrayList<>());
//
//        try {
//            workUnitScheduleFacade.create(newSchedule);
//            workUnitScheduleFacade.edit(sourceSchedule);
//        } catch (Exception ex) {
//            Logger.getLogger(WorkScheduleService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

}
