/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionWorkUnit;
import net.ap57.tiger.std.hr.entity.PositionWorkUnitId;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class PositionWorkUnitFacade extends AbstractFacade<PositionWorkUnit> {

  @Inject
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public PositionWorkUnitFacade() {
    super(PositionWorkUnit.class);
  }

  @Override
  public PositionWorkUnit createTransient(Map<String, Object> params) {
    Position position = (Position) params.get("position");
    WorkUnit workUnit = (WorkUnit) params.get("workUnit");
    PositionWorkUnit positionWorkUnit = new PositionWorkUnit();
    positionWorkUnit.setPosition(position);
    positionWorkUnit.setWorkUnit(workUnit);
    workUnit.getPositions().add(positionWorkUnit);
//    positionWorkUnit.setPositionScheduleVarians(new ArrayList<>());
    return positionWorkUnit;
  }

  @Override
  public Object getId(PositionWorkUnit entity) {
    PositionWorkUnitId id = new PositionWorkUnitId(entity.getPosition().getId(), entity.getWorkUnit().getId());
    return id;
  }

  public List<PositionWorkUnit> getPositionWorkUnitBy(WorkUnit workUnit) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<PositionWorkUnit> cq = cb.createQuery(PositionWorkUnit.class);
    Root<PositionWorkUnit> root = cq.from(PositionWorkUnit.class);
    cq.select(root).where(cb.equal(root.get("workUnit"), workUnit));

    TypedQuery<PositionWorkUnit> q = getEntityManager().createQuery(cq);

    return q.getResultList();

  }

  public PositionWorkUnit getPositionWorkUnitBy(Position position, WorkUnit workUnit) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<PositionWorkUnit> cq = cb.createQuery(PositionWorkUnit.class);
    Root<PositionWorkUnit> root = cq.from(PositionWorkUnit.class);
    cq.select(root).where(new Predicate[]{
      cb.equal(root.get("workUnit"), workUnit),
      cb.equal(root.get("position"), position)
    });

    TypedQuery<PositionWorkUnit> q = getEntityManager().createQuery(cq);
    
    try {
      return q.getSingleResult();
    } catch (Exception ex) {
      return null;
    }
  }

}
