/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.hr.dao.EmployeeAttendanceFacade;
import net.ap57.tiger.std.hr.dao.EmployeeAttendanceSnapshotBean;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.EmployeeAttendance;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.view.EmployeeAttendanceTable;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "employeeAttendancePage")
@RequestScoped
@PageView(rel = "EmployeeAttendance")
public class EmployeeAttendancePage extends Page {

    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private DataTable workUnitTable;

    @Inject
    private EmployeeAttendanceSnapshotBean employeeAttendanceOp;
    
    @View
    private EmployeeAttendanceTable employeeAttendanceTable;
    
    @Inject
    private EmployeeAttendanceFacade attendanceFacade;
    
    @View
    private DataTable attendanceTable;
    
    @Override
    protected void initComponents() {

        workUnitTable = new PFLazyDataTable<WorkUnitFacade, WorkUnit>() {

            @Override
            protected WorkUnitFacade getOp() {
                return workUnitOp;
            }

            @Override
            public void setSelection(WorkUnit selection) {
                super.setSelection(selection);
                employeeAttendanceTable.setSelections(new ArrayList<>());
            }

        };

        employeeAttendanceTable = new EmployeeAttendanceTable() {

            @Override
            protected EmployeeAttendanceSnapshotBean getOp() {
                return employeeAttendanceOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = super.getLoadFilters();
                WorkUnit workUnit = (WorkUnit) workUnitTable.getSelection();
                filters.put("WorkUnit", workUnit);
                return filters;
            }

            @Override
            protected WorkUnit getSelectedWorkUni() {
                return (WorkUnit) workUnitTable.getSelection();
            }

        };
        
        attendanceTable = new PFLazyDataTable<EmployeeAttendanceFacade, EmployeeAttendance>() {
            
            @Override
            protected EmployeeAttendanceFacade getOp() {
                return attendanceFacade;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                filters.put("employee", employeeAttendanceTable.getSelection().getEmployee().getId());
                filters.put("from", employeeAttendanceTable.getRetrievalFromDate());
                filters.put("thru", employeeAttendanceTable.getRetrievalThruDate());
                return filters;
            }
            
        };

    }

    @Override
    protected void initLayout() {
        setExplorer("Work Unit", "15%");
        setEditor("Attendance Log", "85%");
    }

}
