/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Calendar;
import java.util.Map;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.EmploymentNum;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
public class EmploymentNumFacade extends AbstractFacade<EmploymentNum> {

  @Inject
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public EmploymentNumFacade() {
    super(EmploymentNum.class);
  }

  @Override
  public EmploymentNum createTransient(Map<String, Object> params) {
    return new EmploymentNum();
  }

  @Override
  public Object getId(EmploymentNum entity) {
    return entity.getId();
  }

  @Lock(LockType.WRITE)
  public Long getNum() {

    Calendar now = Calendar.getInstance();
    int nowY = now.get(Calendar.YEAR);
    int nowM = now.get(Calendar.MONTH) + 1;

    TypedQuery<EmploymentNum> query = getEntityManager().createNamedQuery("EmploymentNum.getLatest", EmploymentNum.class);
    EmploymentNum pon = null;

    try {
      pon = query.getSingleResult();
      if (nowY > pon.getY()) {
        pon.setY(nowY);
        pon.setM(nowM);
        pon.setS(1);
        getEntityManager().merge(pon);
      } else if (nowM > pon.getM()) {
        pon.setM(nowM);
        pon.setS(pon.getS() + 1);
        getEntityManager().merge(pon);
      } else {
        pon.setS(pon.getS() + 1);
        getEntityManager().merge(pon);
      }
    } catch (NoResultException ex) {
      pon = new EmploymentNum();
      pon.setY(nowY);
      pon.setM(nowM);
      pon.setS(1);
      getEntityManager().persist(pon);
    }

    return (pon.getY() * 1000000L) + (pon.getM() * 10000L) + (pon.getS());
  }

}
