/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.ws.rs.PathParam;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeCode;
import net.ap57.tiger.std.hr.entity.EmployeeCodeType;
import net.ap57.tiger.std.hr.entity.Employment;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class EmployeeService {

    @Inject
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public Employment findActiveEmployment(Employee employee, Date currentDate) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Employment> cq = cb.createQuery(Employment.class);

        Root<Employment> root = cq.from(Employment.class);
        cq.select(root);

        Subquery<Date> sq = cq.subquery(Date.class);
        Root<Employment> sqroot = sq.from(Employment.class);
        sq.select(cb.greatest(sqroot.<Date>get("fromDate")));

        List<Predicate> sqPredicates = new ArrayList<>();
        sqPredicates.add(cb.equal(sqroot.get("participant").get("id"), employee.getId()));

        Calendar calDate = Calendar.getInstance();
        calDate.setTime(currentDate);
        calDate.set(Calendar.HOUR, 0);
        calDate.set(Calendar.MINUTE, 0);
        calDate.set(Calendar.MILLISECOND, 0);

        sqPredicates.add(cb.lessThanOrEqualTo(sqroot.<Date>get("fromDate"), calDate.getTime()));
        sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(root.get("participant").get("id"), employee.getId()));
        predicates.add(cb.equal(root.get("fromDate"), sq));
        cq.where(predicates.toArray(new Predicate[predicates.size()]));

        Query q = getEntityManager().createQuery(cq);

        try {
            return (Employment) q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }

    }

    public Long getEmployeeCodeByType(Employee employee, EmployeeCodeType codeType) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<Employee> emp = cq.from(Employee.class);
        Join<Employee, EmployeeCode> empCode = emp.join("employeeCodes");

        cq.select(empCode.get("code")).where(cb.equal(empCode.get("codeType").get("id"), codeType.getId()));

        TypedQuery<Long> q = getEntityManager().createQuery(cq);

        try {
            return q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    public Employee getEmployeeByCode(EmployeeCode code) {
        return getEmployeeByCode(code.getCodeType().getId(), code.getCode());
    }

    public Employee getEmployeeByCode(Long codeType, Long code) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);

        Root<Employee> employee = cq.from(Employee.class);
        Join<Employee, EmployeeCode> employeeCode = employee.join("employeeCodes");

        cq.select(employee).where(new Predicate[]{
            cb.equal(employeeCode.get("codeType").get("id"), codeType),
            cb.equal(employeeCode.get("code"), code)
        });

        TypedQuery<Employee> q = getEntityManager().createQuery(cq);

        try {
            return q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    public String getStrEmployeeByCode(@PathParam("codetype")Long codeType, @PathParam("code")Long code) {
        Employee employee = getEmployeeByCode(codeType, code);
        if (employee != null) {
            return employee.getId();
        } else {
            return null;
        }
    }

}
