/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(PositionFulfillmentId.class)
public class PositionFulfillment extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    private Position position;

    @Id
    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID")
    private PartyRole employee;

    @Id
    @Temporal(TemporalType.DATE)
    private Date fromDate;

    @Temporal(TemporalType.DATE)
    private Date thruDate;

    private String remarks;

    @ManyToOne
    private Employment employment;

    @OneToMany(mappedBy = "positionFulfillment", cascade=CascadeType.ALL)
    private List<EmployeeScheduleDayException> employeeScheduleDayExceptions;

//  @ManyToOne
//  @JoinColumns({
//    @JoinColumn(name="USEDSCHEDULEVARIAN_POSITIONWORKUNIT_POSITION_ID", referencedColumnName="POSITIONWORKUNIT_POSITION_ID"),
//    @JoinColumn(name="USEDSCHEDULEVARIAN_POSITIONWORKUNIT_WORKUNIT_ID", referencedColumnName="POSITIONWORKUNIT_WORKUNIT_ID"),
//    @JoinColumn(name="USEDSCHEDULEVARIAN_WORKUNITSCHEDULEVARIAN_WORKUNIT_ID", referencedColumnName="WORKUNITSCHEDULEVARIAN_WORKUNIT_ID"),
//    @JoinColumn(name="USEDSCHEDULEVARIAN_WORKUNITSCHEDULEVARIAN_NAME", referencedColumnName="WORKUNITSCHEDULEVARIAN_NAME")
//  })
//  private PositionScheduleVarian usedScheduleVarian;
    @OneToMany(mappedBy = "positionFulfillment", cascade = CascadeType.ALL)
    private List<LeaveRequest> leaveRequests;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Employee getEmployee() {
        return (Employee) employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getThruDate() {
        return thruDate;
    }

    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Employment getEmployment() {
        return employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public List<EmployeeScheduleDayException> getEmployeeScheduleDayExceptions() {
        return employeeScheduleDayExceptions;
    }

    public void setEmployeeScheduleDayExceptions(List<EmployeeScheduleDayException> employeeScheduleDayExceptions) {
        this.employeeScheduleDayExceptions = employeeScheduleDayExceptions;
    }

//  public PositionScheduleVarian getUsedScheduleVarian() {
//    return usedScheduleVarian;
//  }
//
//  public void setUsedScheduleVarian(PositionScheduleVarian usedScheduleVarian) {
//    this.usedScheduleVarian = usedScheduleVarian;
//  }
    public List<LeaveRequest> getLeaveRequests() {
        return leaveRequests;
    }

    public void setLeaveRequests(List<LeaveRequest> leaveRequests) {
        this.leaveRequests = leaveRequests;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "PositionFulfillment{" + "position=" + position + ", employee=" + employee + ", fromDate=" + fromDate + '}';
    }

    @Override
    public boolean equals(Object obj) {
        return getStrKey().equals(((PositionFulfillment) obj).getStrKey());
    }

}
