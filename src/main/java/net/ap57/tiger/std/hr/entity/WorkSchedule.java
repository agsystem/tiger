/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author mbahbejo
 */
@Entity
public class WorkSchedule extends StringKeyedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @TableGenerator(name = "WorkSchedule", table = "KEYGEN")
    @GeneratedValue(generator = "WorkSchedule", strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(unique=true)
    private String name;
    
    private int timePeriod;
    
    private int datePeriod;
    
    private int workhour;
    
    private String remarks;
    
    @OneToMany(mappedBy="template", cascade = CascadeType.ALL)
    private List<WorkScheduleDay> templates;

    @OneToMany(mappedBy = "workSchedule", cascade = CascadeType.ALL)
    private List<PositionSchedule> positionSchedules;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }

    public int getDatePeriod() {
        return datePeriod;
    }

    public void setDatePeriod(int datePeriod) {
        this.datePeriod = datePeriod;
    }

    public int getWorkhour() {
        return workhour;
    }

    public void setWorkhour(int workhour) {
        this.workhour = workhour;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<WorkScheduleDay> getTemplates() {
        return templates;
    }

    public void setTemplates(List<WorkScheduleDay> templates) {
        this.templates = templates;
    }

    public List<PositionSchedule> getPositionSchedules() {
        return positionSchedules;
    }

    public void setPositionSchedules(List<PositionSchedule> positionSchedules) {
        this.positionSchedules = positionSchedules;
    }

//    public Map<Integer, WorkScheduleDayTemplate> getTemplates() {
//        return templates;
//    }
//
//    public void setTemplates(Map<Integer, WorkScheduleDayTemplate> templates) {
//        this.templates = templates;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkSchedule)) {
            return false;
        }
        WorkSchedule other = (WorkSchedule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
