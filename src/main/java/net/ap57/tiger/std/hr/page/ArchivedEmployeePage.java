/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.page;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.entity.Gender;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.bean.GenderDataList;
import net.ap57.tiger.base.ui.pf.EnumDataList;
import net.ap57.tiger.base.ui.pf.PFDataTree;
import net.ap57.tiger.std.hr.dao.EmploymentAgreementTypeFacade;
import net.ap57.tiger.std.hr.dao.InternalOrganizationFacade;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.EmployeeStatus;
import net.ap57.tiger.std.hr.entity.EmploymentAgreementType;
import net.ap57.tiger.std.hr.entity.InternalOrganization;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.view.ArchivedEmployeeTable;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author mbahbejo
 */
@Named
@RequestScoped
@PageView(rel = "archivedEmployee")
public class ArchivedEmployeePage extends Page {
    
    @Inject
    private PositionFulfillmentFacade employeeOp;
    
    @View
    private ArchivedEmployeeTable archivedEmployeeTable;

    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private DataList workUnitList;

    @Inject
    private EmploymentAgreementTypeFacade agreementTypeOp;

    @View
    private DataList agreementTypeList;

    @View
    private EnumDataList<Gender> genderList;

    @Override
    protected void initLayout() {
        setEditor("Archived Employee", "85%");
    }

    @Override
    protected void initComponents() {
        
        archivedEmployeeTable = new ArchivedEmployeeTable() {
            
            @Override
            protected PositionFulfillmentFacade getOp() {
                return employeeOp;
            }
            
        };

        workUnitList = new DataList<WorkUnitFacade, WorkUnit>() {

            @Override
            protected Object strToObj(String value) {
                return workUnitOp.findSingleByAttribute("name", value);
            }

            @Override
            public WorkUnitFacade getOp() {
                return workUnitOp;
            }
            
        };

        genderList = new GenderDataList();

        agreementTypeList = new DataList<EmploymentAgreementTypeFacade, EmploymentAgreementType>() {
            
            @Override
            public EmploymentAgreementTypeFacade getOp() {
                return agreementTypeOp;
            }
            
            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("name", value);
            }
            
        };
    }

    
    
}
