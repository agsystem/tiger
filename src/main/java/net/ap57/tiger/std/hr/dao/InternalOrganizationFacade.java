/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractTreeFacade;
import net.ap57.tiger.base.dao.OrganizationFacade;
import net.ap57.tiger.base.dao.PartyRoleTypeFacade;
import net.ap57.tiger.base.entity.Organization;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.base.entity.PartyRoleType;
import net.ap57.tiger.std.hr.entity.InternalOrganization;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class InternalOrganizationFacade extends AbstractTreeFacade<InternalOrganization> { //OrganizationRoleTreeFacade<InternalOrganization> {

    @Inject
    private EntityManager em;

    @Inject
    private OrganizationFacade partyFacade;

    @Inject
    private PartyRoleTypeFacade partyRoleTypeFacade;

    public InternalOrganizationFacade() {
        super(InternalOrganization.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Object getId(InternalOrganization entity) {
        return entity.getId();
    }

    @Override
    public InternalOrganization createTransientBasic(Map<String, Object> params) {
        
        InternalOrganization internalOrganization = new InternalOrganization();
        
        internalOrganization.setPartyRoleType(getRoleType());
        
        Organization party = associateToOrganization(internalOrganization, getPartyFacade().createTransient(params));
        
        internalOrganization.setParty(party);
        party.getRoles().add(internalOrganization);
        internalOrganization.setRelationshipParticipations(new ArrayList<>());
        internalOrganization.setRelationshipsEstablished(new ArrayList<>());
        
        return internalOrganization;

    }

    @Override
    public void create(InternalOrganization entity) throws EJBException {
        Organization organization = entity.getOrganization();
        if (organization.getId() == null) {
            entity.setFromDate(Calendar.getInstance().getTime());
            getPartyFacade().create(entity.getOrganization());
        } else {

        }

    }

    @Override
    public void edit(InternalOrganization entity) {
        getPartyFacade().edit(entity.getOrganization());
    }

    @Override
    public void remove(InternalOrganization entity) {
        InternalOrganization parent = entity.getParent();
        if (parent != null) {
            parent.getChildren().remove(entity);
        }
        getPartyFacade().remove(entity.getOrganization());
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<InternalOrganization> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();
        switch (filterName) {
            case "party.name":
                Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                predicates.add(cb.like(cb.upper(root.get("party").<String>get("name")), literal));
                break;
            default:
                break;
        }
        return predicates;
    }

    public void getDescendants(InternalOrganization ancestor, List<Long> result) {
        for (InternalOrganization child : ancestor.getChildren()) {
            result.add(child.getParty().getId());
            getDescendants(child, result);
        }
    }

    public void addContactMechanisms(Organization party) {
        getPartyFacade().addContactMechanisms(party);
    }

    public void delContactMechanism(Organization party, PartyContactMechanism partyContactMechanism) {
        getPartyFacade().delContactMechanism(party, partyContactMechanism);
    }

    protected PartyRoleTypeFacade getRoleTypeFacade() {
        return partyRoleTypeFacade;
    }

    protected OrganizationFacade getPartyFacade() {
        return partyFacade;
    }

    protected String getRoleTypeName() {
        return "Internal Organization";
    }

    public Organization associateToOrganization(InternalOrganization role, Organization party) {
        if (party == null) {
            party = getPartyFacade().createTransient(null);
            party.setRoles(new ArrayList<>());
        }
        role.setParty(party);
        party.getRoles().add(role);
        return party;
    }

    public PartyRoleType getRoleType() {
        return getRoleTypeFacade().findSingleByAttribute("name", getRoleTypeName());
    }

}
