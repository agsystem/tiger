/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class EmployeeAttendanceSnapshot implements Serializable {

    private Employee employee;

    private /*EmployeeAttendance*/PresenceData attendance;

    public EmployeeAttendanceSnapshot(Employee employee,/*EmployeeAttendance*/PresenceData attendance) {
        this.employee = employee;
        if (attendance == null) {
            this.attendance = new /*EmployeeAttendance*/PresenceData();
            this.attendance.setEmployee(employee);
        } else {
            this.attendance = attendance;
        }
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public /*EmployeeAttendance*/PresenceData getAttendance() {
        return attendance;
    }

    public void setAttendance(/*EmployeeAttendance*/PresenceData attendance) {
        this.attendance = attendance;
    }

    public Date getWorkDate() {
        return attendance.getWorkday();//getWorkDate();
    }

    public void setWorkDate(Date workDate) {
        attendance.setWorkday(workDate);//setWorkDate(workDate);
    }

    public /*Calendar*/Date getIn() {
        return attendance.getInTime();
    }

    public void setIn(/*Calendar*/Date in) {
        attendance.setInTime(in);
    }

    public /*Calendar*/Date getOut() {
        return attendance.getOutTime();
    }

    public void setOut(/*Calendar*/Date out) {
        attendance.setOutTime(out);
    }

//    public List<Calendar> getUnspecifiedLog() {
//        return attendance.getUnspecifiedTimes();
//    }
//
//    public void setUnspecifiedLog(List<Calendar> unspecifiedLog) {
//        attendance.setUnspecifiedTimes(unspecifiedLog);
//    }

}
