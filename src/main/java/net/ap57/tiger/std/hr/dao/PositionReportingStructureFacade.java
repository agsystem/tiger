/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionReportingStructure;
import net.ap57.tiger.std.hr.entity.PositionReportingStructureId;

/**
 *
 * @author mbahbejo
 */
@Stateless
public class PositionReportingStructureFacade extends AbstractFacade<PositionReportingStructure> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PositionReportingStructureFacade() {
        super(PositionReportingStructure.class);
    }

    @Override
    public PositionReportingStructure createTransient(Map<String, Object> params) throws EJBException {
        PositionReportingStructure prs = new PositionReportingStructure();
        Position reporter = (Position) params.get("reporter");
        prs.setReporterPosition(reporter);
        prs.setFromDate(Calendar.getInstance().getTime());
        return prs;
    }

    @Override
    public Object getId(PositionReportingStructure entity) {
        PositionReportingStructureId id = new PositionReportingStructureId();
        id.setReportingToPosition(entity.getReportingToPosition().getId());
        id.setReporterPosition(entity.getReporterPosition().getId());
        id.setFromDate(entity.getFromDate());
        return id;
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<PositionReportingStructure> root) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();

        switch (filterName) {
            case "reporter":
                predicates.add(cb.equal(root.get("reporterPosition"), filterValue));
                break;
            default:
                break;
        }

        return predicates;
    }

    @Override
    protected void addCriteria(Root<PositionReportingStructure> root, CriteriaQuery<PositionReportingStructure> cq) {        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("fromDate")));
    }
    
    public Position getPrimaryManager(Position pos) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PositionReportingStructure> cq = cb.createQuery(PositionReportingStructure.class);
        
        Root<PositionReportingStructure> root = cq.from(PositionReportingStructure.class);
        cq.select(root).where(new Predicate[] {
            cb.equal(root.get("reporterPosition"), pos),
            cb.isTrue(root.get("primaryFlag"))
        });
        
        TypedQuery<PositionReportingStructure> q = getEntityManager().createQuery(cq);
        try {
            return q.getSingleResult().getReportingToPosition();
        } catch (Exception ex) {
            return null;
        }
    }

}
