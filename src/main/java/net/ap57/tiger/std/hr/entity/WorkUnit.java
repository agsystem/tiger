/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import net.ap57.tiger.base.entity.OrganizationRole;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
public class WorkUnit extends OrganizationRole implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Column(nullable=false, unique=true)
  private String name;
  
  private String shortName;
  
  private String description;
  
  @OneToMany(mappedBy = "workUnit", cascade=CascadeType.REMOVE)
  private List<PositionWorkUnit> positions;
  
//  @OneToMany(mappedBy = "workUnit", cascade=CascadeType.ALL)
//  private List<WorkUnitScheduleVarian> workUnitScheduleVarians;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
  
  public List<PositionWorkUnit> getPositions() {
    return positions;
  }

//  public List<Position> getPositions() {
//    return positions;
//  }
//  
//  public void setPositions(List<Position> positions) {
//    this.positions = positions;
  public void setPositions(List<PositionWorkUnit> positions) {
    this.positions = positions;
  }

//  }
//  public List<WorkUnitScheduleVarian> getWorkUnitScheduleVarians() {
//    return workUnitScheduleVarians;
//  }
//
//  public void setWorkUnitScheduleVarians(List<WorkUnitScheduleVarian> workUnitScheduleVarians) {
//    this.workUnitScheduleVarians = workUnitScheduleVarians;
//  }

  @Override
  public String toString() {
    return name;
  }

//  @Override
//  public boolean equals(Object obj) {
//    return name.equals(((WorkUnit)obj).getName());
//  }

  @Override
  public void prePersist() {
    getOrganization().setName(name);
    super.prePersist();
  }

  @Override
  public List getRelationshipsEstablished() {
    return new ArrayList<>();
  }

  @Override
  public void setRelationshipsEstablished(List relationshipsEstablished) {
    
  }

  @Override
  public List getRelationshipParticipations() {
    return new ArrayList<>();
  }

  @Override
  public void setRelationshipParticipations(List relationshipParticipations) {
    
  }

}
