/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.hr.view;

import net.ap57.tiger.base.ui.pf.EnumDataList;
import net.ap57.tiger.std.hr.entity.PositionStatus;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class PositionStatusDataList extends EnumDataList<PositionStatus> {

  public PositionStatusDataList() {
    super(PositionStatus.class);
  }

  @Override
  public PositionStatus[] getEnumValues() {
    return PositionStatus.values();
  }

    
}
