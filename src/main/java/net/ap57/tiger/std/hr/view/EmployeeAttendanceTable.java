/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.view;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.hr.dao.EmployeeAttendanceSnapshotBean;
import net.ap57.tiger.std.hr.entity.EmployeeAttendanceSnapshot;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class EmployeeAttendanceTable extends PFLazyDataTable<EmployeeAttendanceSnapshotBean, EmployeeAttendanceSnapshot> {

    @Override
    protected Object objToKey(EmployeeAttendanceSnapshot object) {
        return object.getEmployee().getStrKey();
    }
    
    @Override
    protected EmployeeAttendanceSnapshot keyToObj(String rowkey) {
        return getOp().getObj(rowkey, getSelectedWorkDate());
    }
    
    public Date getSelectedWorkDate() {
        Calendar cCal = Calendar.getInstance();
        cCal.set(Calendar.HOUR_OF_DAY, 0);
        cCal.set(Calendar.MINUTE, 0);
        cCal.set(Calendar.MILLISECOND, 0);
        return (Date) getAttribute("selectedWorkDate", cCal.getTime());
    }

    public void setSelectedWorkDate(Date date) {
        setAttribute("selectedWorkDate", date);
    }
    
    public Date getRetrievalFromDate() {
        Calendar cCal = Calendar.getInstance();
        cCal.set(Calendar.HOUR_OF_DAY, 0);
        cCal.set(Calendar.MINUTE, 0);
        cCal.set(Calendar.MILLISECOND, 0);
        return (Date) getAttribute("retrievalFromDate", cCal.getTime());
    }

    public void setRetrievalFromDate(Date date) {
        setAttribute("retrievalFromDate", date);
    }
    
    public Date getRetrievalThruDate() {
        Calendar cCal = Calendar.getInstance();
        cCal.set(Calendar.HOUR_OF_DAY, 0);
        cCal.set(Calendar.MINUTE, 0);
        cCal.set(Calendar.MILLISECOND, 0);
        return (Date) getAttribute("retrievalThruDate", cCal.getTime());
    }

    public void setRetrievalThruDate(Date date) {
        setAttribute("retrievalThruDate", date);
    }
    
    protected abstract WorkUnit getSelectedWorkUni();

    @Override
    protected Map<String, Object> getLoadFilters() {
        Map<String, Object> filters = new HashMap<>();
//        filters.put("workDate", getSelectedWorkDate());
        filters.put("WorkUnit", filters);
        filters.put("workday", getSelectedWorkDate());
        return filters;
    }

    public void onRowEdit(RowEditEvent evt) {
        EmployeeAttendanceSnapshot snapshot = (EmployeeAttendanceSnapshot) evt.getObject();
        snapshot.setWorkDate(getSelectedWorkDate());
//        PresenceData snapshot = (PresenceData) evt.getObject();
//        snapshot.setWorkday(getSelectedWorkDate());
        getOp().edit(snapshot);
    }
    
    public void onCellEdit(CellEditEvent evt) {
        
    }
    
    public void findAttendance(ActionEvent evt) {
        getOp().retrieveAllAttendance(getRetrievalFromDate(), getRetrievalThruDate());
    }
    
    public void viewAttendance(ActionEvent evt) {        
        EmployeeAttendanceSnapshot snapshot = getSelection();
        getOp().viewAttendance(snapshot.getEmployee(), getRetrievalFromDate(), getRetrievalThruDate());
    }
}
