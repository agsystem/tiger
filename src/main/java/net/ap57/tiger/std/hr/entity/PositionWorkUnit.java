/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(PositionWorkUnitId.class)
public class PositionWorkUnit implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @ManyToOne
  private Position position;
  
  @Id
  @ManyToOne
  private WorkUnit workUnit;
  
//  @OneToMany(mappedBy = "positionWorkUnit", cascade=CascadeType.ALL)
//  private List<PositionScheduleVarian> positionScheduleVarians;

  public PositionWorkUnit() {}

  public PositionWorkUnit(Position position, WorkUnit workUnit) {
    this.position = position;
    this.workUnit = workUnit;
  }

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  public WorkUnit getWorkUnit() {
    return workUnit;
  }

  public void setWorkUnit(WorkUnit workUnit) {
    this.workUnit = workUnit;
  }

//  public List<PositionScheduleVarian> getPositionScheduleVarians() {
//    return positionScheduleVarians;
//  }
//
//  public void setPositionScheduleVarians(List<PositionScheduleVarian> positionScheduleVarians) {
//    this.positionScheduleVarians = positionScheduleVarians;
//  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 97 * hash + Objects.hashCode(this.position);
    hash = 97 * hash + Objects.hashCode(this.workUnit);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PositionWorkUnit other = (PositionWorkUnit) obj;
    if (!Objects.equals(this.position, other.position)) {
      return false;
    }
    if (!Objects.equals(this.workUnit, other.workUnit)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return workUnit.getName();
  }
  
}
