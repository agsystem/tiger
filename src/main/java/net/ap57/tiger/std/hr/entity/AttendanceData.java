/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
@Table(name="attendancedata")
@NamedQuery(name="AttendanceLog.findLogWBound", 
        query="SELECT log FROM AttendanceData log "
                + "WHERE log.id.employee = :employee "
                + "AND log.id.datetime >= :loBound "
                + "AND log.id.datetime < :hiBound")
public class AttendanceData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    private AttendanceDataId id;

    public AttendanceDataId getId() {
        if(id == null) {
            id = new AttendanceDataId();
        }
        return id;
    }

    public void setId(AttendanceDataId id) {
        this.id = id;
    }
    
    public void setEmployee(String employee) {
        getId().setEmployee(employee);
    }
    
    public String getEmployee() {
        return getId().getEmployee();
    }
    
//    public void setPin(Long code) {
//        getId().setCode(code);        
//    }
//    
//    public Long getCode() {
//        return getId().getCode();
//    }
    
    public void setDatetime(Date datetime) {
        getId().setDatetime(datetime);
    }
    
    public Date getDatetime() {
        return getId().getDatetime();
    }
    
}
