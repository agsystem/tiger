/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.view;

import java.util.Date;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import org.primefaces.model.DefaultScheduleEvent;

/**
 *
 * @author mbahbejo
 */
public class ScheduleTemplateEvent extends DefaultScheduleEvent {
    
    private WorkScheduleDay data;

    public ScheduleTemplateEvent(String title, Date start, Date end, Object data) {
        super(title, start, end, data);
        this.data = (WorkScheduleDay) data;        
        System.out.println("CONSTRUCTOR -> TPL START = " + this.data.getFromDate() + ", TPL END = " + this.data.getThruDate());
    }
    
    public WorkScheduleDay getDayTemplate() {
        return data;
    }

    @Override
    public Date getEndDate() {
        Date fromDate = data.getThruDate();
        System.out.println("TPL END = " + fromDate);
        return fromDate;
    }

    @Override
    public Date getStartDate() {
        Date thruDate = data.getFromDate();
        System.out.println("TPL START = " + thruDate);
        return thruDate;
    }
    
}
