/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.hr.view;

import net.ap57.tiger.base.ui.EditableDataList;
import net.ap57.tiger.std.hr.dao.PositionTypeFacade;
import net.ap57.tiger.std.hr.entity.PositionType;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class PositionTypeDataList extends EditableDataList<PositionTypeFacade, PositionType> {
  
  @Override
  protected Object strToObj(String value) {
    return getOp().findSingleByAttribute("title", value);
  }
  
  @Override
  protected PositionType newPlaceholder() {
    return new PositionType(true);
  }
    
}
