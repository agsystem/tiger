/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.view;

import java.util.List;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.reporting.bean.Reporter;
import net.ap57.tiger.base.ui.ListDataTable;
import net.ap57.tiger.std.hr.entity.IdCard;
import org.primefaces.event.CloseEvent;

/**
 *
 * @author mbahbejo
 */
public abstract class IdCardBucket extends ListDataTable<IdCard> {
    
    private static final boolean OPEN = true;
    private static final boolean CLOSE = false;
    
    public void open(ActionEvent evt) {
        this.setAttribute("status", OPEN);
    }
    
    public void close(ActionEvent evt) {
        this.setAttribute("status", CLOSE);
    }
    
    public void setColorScheme(String scheme) {
        setAttribute("colorScheme", scheme);
    }
    
    public String getColorScheme() {
        return (String) this.getAttribute("colorScheme", "");
    }
    
    public boolean isOpen() {
        return (boolean) getAttribute("status", CLOSE);
    }

    @Override
    protected String objToKey(IdCard object) {
        return object.getName();
    }
    
    public void clearBucket(CloseEvent evt) {
        getData().clear();
        close(null);
    }
    
    public void clearBucket(ActionEvent evt) {
        getData().clear();
        close(null);
    }
    
//    public abstract Module getModule();
    
    public abstract Reporter getReporter();

    public void print(ActionEvent evt) {
        List<IdCard> data = getData().valueList();
        getReporter().writeNCombineReport(getContainer().getModule(), "name_tag_front" + getColorScheme(), "name_tag_bottom", data, data, null, null);
    }
    
}
