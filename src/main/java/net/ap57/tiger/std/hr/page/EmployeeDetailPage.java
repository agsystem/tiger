/*
 * Copyright 2017 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.page;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.dao.ContactMechanismTypeFacade;
import net.ap57.tiger.base.dao.IdentityTypeFacade;
import net.ap57.tiger.base.entity.Gender;
//import net.ap57.tiger.base.ui.ChildPageCtx;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.EditableDataList;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.bean.ContactMechanismTypeDataList;
import net.ap57.tiger.base.ui.bean.GenderDataList;
import net.ap57.tiger.base.ui.bean.IdTypeDataList;
import net.ap57.tiger.base.ui.pf.EnumDataList;
import net.ap57.tiger.base.ui.pf.PFCamera;
import net.ap57.tiger.base.ui.pf.PFCropper;
import net.ap57.tiger.base.ui.pf.PFDashboard;
import net.ap57.tiger.base.ui.pf.PFFileUpload;
import net.ap57.tiger.base.ui.pf.PFTemporaryFile;
import net.ap57.tiger.std.hr.dao.EmploymentAgreementTypeFacade;
import net.ap57.tiger.std.hr.dao.PositionTypeFacade;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.EmployeeStatus;
import net.ap57.tiger.std.hr.entity.EmploymentAgreementType;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.view.EmployeeDashboard;
import net.ap57.tiger.std.hr.view.EmployeeStatusDataList;
import net.ap57.tiger.std.hr.view.PositionTypeDataList;
import net.ap57.tiger.base.ui.annotation.View;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Named
@RequestScoped
public class EmployeeDetailPage extends Page {
    
    @View
    private PFDashboard employeeDashboard;
    
    @Inject
    private IdentityTypeFacade idTypeOp;
    
    @View
    private IdTypeDataList idTypeList;
    
    @Inject
    private ContactMechanismTypeFacade contactMechanismTypeOp;
    
    @View
    private ContactMechanismTypeDataList contactTypeList;
    
    @Inject
    private PositionTypeFacade positionTypeOp;
    
    @View
    private PositionTypeDataList positionTypeList;
    
    @View
    private PFCamera camera;
    
    @View
    private PFFileUpload uploader;
    
    @View
    private PFTemporaryFile temporaryFileManager;
    
    @View
    private PFCropper cropper;
    
    @Inject
    private WorkUnitFacade workUnitOp;
    
    @View
    private DataList workUnitList;

    @View
    private EnumDataList<Gender> genderList;
    
    @Inject
    private EmploymentAgreementTypeFacade agreementTypeOp;
    
    @View
    private DataList agreementTypeList;

    @View
    private EnumDataList<EmployeeStatus> employeeStatusList;

    @Override
    protected void initLayout() {
        setEditor("Employee Detail", "100%");
    }

    @Override
    protected void initComponents() {

        employeeDashboard = new EmployeeDashboard() {

            @Override
            protected String[][] getWidgets() {
                return new String[][]{{"photo"}, {"general", "address", "phone"}, {"agreement", "position"}};
            }

        };
        
        idTypeList = new IdTypeDataList() {
            @Override
            public IdentityTypeFacade getOp() {
                return idTypeOp;
            }

        };
        
        contactTypeList = new ContactMechanismTypeDataList() {

            @Override
            public ContactMechanismTypeFacade getOp() {
                return contactMechanismTypeOp;
            }

        };
        
        workUnitList = new DataList<WorkUnitFacade, WorkUnit>() {

            @Override
            protected Object strToObj(String value) {
                return workUnitOp.findSingleByAttribute("name", value);
            }

            @Override
            public WorkUnitFacade getOp() {
                return workUnitOp;
            }

        };
        
        genderList = new GenderDataList();
        
        employeeStatusList = new EmployeeStatusDataList();
        
        positionTypeList = new PositionTypeDataList() {
            
            @Override
            public PositionTypeFacade getOp() {
                return positionTypeOp;
            }

        };
        
        agreementTypeList = new EditableDataList<EmploymentAgreementTypeFacade, EmploymentAgreementType>() {

            @Override
            public EmploymentAgreementTypeFacade getOp() {
                return agreementTypeOp;
            }

            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("name", value);
            }

            @Override
            protected EmploymentAgreementType newPlaceholder() {
                return new EmploymentAgreementType(true);
            }

        };
        
        temporaryFileManager = new PFTemporaryFile();

        uploader = new PFFileUpload() {

            @Override
            protected PFTemporaryFile getTemporaryFileManager() {
                return temporaryFileManager;
            }

        };
        
        camera = new PFCamera() {

            @Override
            protected PFTemporaryFile getTemporaryFileManager() {
                return temporaryFileManager;
            }

        };
        
        cropper = new PFCropper() {

            @Override
            public int getX() {
                return (480 / 2) - (getW() / 2);
            }

            @Override
            public int getY() {
                return (360 / 2) - (getH() / 2);
            }

            @Override
            public int getW() {
                return 240;
            }

            @Override
            public int getH() {
                return 320;
            }

            @Override
            protected PFTemporaryFile getTemporaryFileManager() {
                return temporaryFileManager;
            }

        };

//        cropper.addClient(this);
        
    }

}
