/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.api.ScheduledEvent;

/**
 *
 * @author mbahbejo
 */
@Entity
@IdClass(WorkScheduleDayId.class)
public class WorkScheduleDay implements ScheduledEvent, Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @ManyToOne
    private WorkSchedule template;
    
    @Id
    @Temporal(TemporalType.DATE)
    private Date workday;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date thruDate;
    
    @OneToOne(mappedBy = "scheduleDay", cascade=CascadeType.ALL)
    private WorkScheduleDayException exception;

    @OneToMany(mappedBy = "scheduleDay", cascade=CascadeType.ALL)
    private List<PositionScheduleDayException> positionScheduleDayExceptions;

    @OneToMany(mappedBy = "scheduleDay", cascade=CascadeType.ALL)
    private List<EmployeeScheduleDayException> employeeScheduleDayExceptions;
    
    public WorkSchedule getTemplate() {
        return template;
    }
    
    public void setTemplate(WorkSchedule template) {
        this.template = template;
    }

    public Date getWorkday() {
        return workday;
    }

    public void setWorkday(Date workday) {
        this.workday = workday;
    }    
    
    @Override
    public String getName() {        
        SimpleDateFormat sdf = new SimpleDateFormat("MM/d HH:mm");
        if (thruDate == null) {
            return "";
        }
        return sdf.format(thruDate);
    }

    @Override
    public void setName(String name) {
        
    }

    @Override
    public Date getFromDate() {
        return fromDate;
    }

    @Override
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public Date getThruDate() {
        return thruDate;
    }

    @Override
    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }

    public WorkScheduleDayException getException() {
        return exception;
    }

    public void setException(WorkScheduleDayException exception) {
        this.exception = exception;
    }

    public List<PositionScheduleDayException> getPositionScheduleDayExceptions() {
        return positionScheduleDayExceptions;
    }

    public void setPositionScheduleDayExceptions(List<PositionScheduleDayException> positionScheduleDayExceptions) {
        this.positionScheduleDayExceptions = positionScheduleDayExceptions;
    }

    public List<EmployeeScheduleDayException> getEmployeeScheduleDayExceptions() {
        return employeeScheduleDayExceptions;
    }

    public void setEmployeeScheduleDayExceptions(List<EmployeeScheduleDayException> employeeScheduleDayExceptions) {
        this.employeeScheduleDayExceptions = employeeScheduleDayExceptions;
    }
    
}
