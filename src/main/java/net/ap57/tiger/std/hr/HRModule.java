/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.inject.Inject;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.dao.OrganizationFacade;
import net.ap57.tiger.base.dao.PartyRelationshipTypeFacade;
import net.ap57.tiger.base.dao.PartyRoleTypeFacade;
import net.ap57.tiger.base.entity.Organization;
import net.ap57.tiger.base.entity.PartyRelationshipType;
import net.ap57.tiger.base.entity.PartyRoleType;
import net.ap57.tiger.std.hr.dao.AttendanceDataFacade;
import net.ap57.tiger.std.system.dao.SecurityGroupFacade;
import net.ap57.tiger.std.system.entity.SecurityGroup;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Singleton
public class HRModule extends Module {

    @Inject
    private PartyRoleTypeFacade roleTypeFacade;

    @Inject
    private PartyRelationshipTypeFacade relTypeFacade;

    @Inject
    private SecurityGroupFacade groupFacade;

    @Inject
    private OrganizationFacade organizationFacade;
    
    @Inject
    private AttendanceDataFacade attFacade;

    @Override
    protected void init() throws Exception {

        PartyRoleType internalOrganizationType = roleTypeFacade.findSingleByAttribute("name", "Internal Organization");

        if (internalOrganizationType == null) {
            internalOrganizationType = roleTypeFacade.createTransient(null);
            internalOrganizationType.setName("Internal Organization");
            roleTypeFacade.create(internalOrganizationType);
        }

        PartyRoleType workUnitType = roleTypeFacade.findSingleByAttribute("name", "Work Unit");

        if (workUnitType == null) {
            workUnitType = roleTypeFacade.createTransient(null);
            workUnitType.setName("Work Unit");
            roleTypeFacade.create(workUnitType);
        }

        PartyRoleType employeeType = roleTypeFacade.findSingleByAttribute("name", "Employee");

        if (employeeType == null) {
            employeeType = roleTypeFacade.createTransient(null);
            employeeType.setName("Employee");
            roleTypeFacade.create(employeeType);
        }

        PartyRelationshipType relType = relTypeFacade.findSingleByAttribute("name", "Employment");

        if (relType == null) {
            relType = relTypeFacade.createTransient(null);
            relType.setName("Employment");
            relType.setEstablisherType(internalOrganizationType);
            relType.setParticipantType(employeeType);
            relTypeFacade.create(relType);
        }

        PartyRoleType securityGroupType = roleTypeFacade.findSingleByAttribute("name", "Security Group");

        SecurityGroup hr = groupFacade.findSingleByAttribute("groupname", "HR");
        if (hr == null) {
            Organization orgHr = new Organization();
            orgHr.setRoles(new ArrayList<>());
            hr = new SecurityGroup();
            hr.setOrganization(orgHr);
            hr.setPartyRoleType(securityGroupType);
            hr.setGroupname("HR");
            hr.setFromDate(Calendar.getInstance().getTime());
            hr.setSecurityMap(new ArrayList<>());
            orgHr.getRoles().add(hr);
            organizationFacade.create(orgHr);
        }
        
//        AttendanceData att = new AttendanceData();
//        att.setPin("0");
//        att.setDatetime(Calendar.getInstance().getTime());
//        
//        attFacade.create(att);

//          attFacade.getEntityManager();

//        test();

    }

    @Asynchronous
    private void test() {

        ExecutorService executor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });

        executor.execute(new Runnable() {

            ExecutorService processor = Executors.newFixedThreadPool(100);

            @Override
            public void run() {

                Properties props = new Properties();
                props.put("bootstrap.servers", "localhost:9091,localhost:9092,localhost:9093");
                props.put("group.id", "testgroup");
                props.put("auto.offset.reset", "earliest");
                props.put("key.deserializer", StringDeserializer.class.getName());
                props.put("value.deserializer", StringDeserializer.class.getName());

                KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
                consumer.subscribe(Arrays.asList("attendancetopic"));

                while (true) {
                    ConsumerRecords<String, String> records = consumer.poll(Long.MAX_VALUE);
                    for (ConsumerRecord<String, String> record : records) {
                        processor.submit(new Runnable() {
                            @Override
                            public void run() {
                                processValue(record.value());
                            }
                        });
                    }
                }
            }
        });

//        executor.shutdown();
    }

    private void processValue(String value) {
        try {
            System.out.println(value + " (Start)");
            TimeUnit.SECONDS.sleep(3);
            System.out.println(value + " (Finish)");
        } catch (InterruptedException ex) {
            Logger.getLogger(HRModule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
