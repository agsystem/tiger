/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import net.ap57.tiger.base.entity.PersonRole;
import net.ap57.tiger.ext.hr.entity.FingerprintMachineUserMap;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
public class Employee extends PersonRole implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String nik;
    
    @OneToOne(mappedBy = "employee", cascade={CascadeType.MERGE, CascadeType.REMOVE})
    private FingerprintMachineUserMap machineUserMap;
    
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<Employment> employments;
    
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    @OrderBy("fromDate DESC")
    private List<PositionFulfillment> positionFulfillments;
    
//    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
//    private List<EmployeeWorkSchedule> employeeWorkSchedules;
    
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<EmployeeAttendance> employeeAttendances;
    
    @Enumerated(EnumType.STRING)
    private EmployeeStatus status;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<EmployeeCode> employeeCodes;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private List<PresenceData> presences;

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public FingerprintMachineUserMap getMachineUserMap() {
        return machineUserMap;
    }

    public void setMachineUserMap(FingerprintMachineUserMap machineUserMap) {
        this.machineUserMap = machineUserMap;
    }

    public List<Employment> getEmployments() {
        return employments;
    }

    public void setEmployments(List<Employment> employments) {
        this.employments = employments;
    }

    public List<PositionFulfillment> getPositionFulfillments() {
        return positionFulfillments;
    }

    public void setPositionFulfillments(List<PositionFulfillment> positionFulfillments) {
        this.positionFulfillments = positionFulfillments;
    }

    public List<EmployeeCode> getEmployeeCodes() {
        return employeeCodes;
    }

    public void setEmployeeCodes(List<EmployeeCode> employeeCodes) {
        this.employeeCodes = employeeCodes;
    }

    public List<PresenceData> getPresences() {
        return presences;
    }

//    public List<EmployeeWorkSchedule> getEmployeeWorkSchedules() {
//        return employeeWorkSchedules;
//    }
//
//    public void setEmployeeWorkSchedules(List<EmployeeWorkSchedule> employeeWorkSchedules) {
//        this.employeeWorkSchedules = employeeWorkSchedules;
    public void setPresences(List<PresenceData> presences) {
        this.presences = presences;
    }

//    }
    public List<EmployeeAttendance> getEmployeeAttendances() {
        return employeeAttendances;
    }

    public void setEmployeeAttendances(List<EmployeeAttendance> employeeAttendances) {
        this.employeeAttendances = employeeAttendances;
    }

    public PositionFulfillment getLatestPositionFulfillment() {
        return getPositionFulfillments().get(0);
    }

    public BufferedImage getCode() {
        try {
            return ImageIO.read(QRCode.from(getParty().toString()).withSize(300, 300).to(ImageType.PNG).file());
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public BufferedImage getPhoto() {
        try {
            byte[] data = getPerson().getPhoto();
            if (data == null) {
                return null;
            }
            return ImageIO.read(new ByteArrayInputStream(data));
        } catch (IOException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List getRelationshipsEstablished() {
        return null;
    }

    @Override
    public void setRelationshipsEstablished(List relationshipsEstablished) {
    }
    
    @Override
    public List getRelationshipParticipations() {
        return getEmployments();
    }

    @Override
    public void setRelationshipParticipations(List relationshipParticipations) {
        setEmployments(relationshipParticipations);
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }
    
}
