/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import com.ibm.icu.util.Calendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractScheduleFacade;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayId;
import net.ap57.tiger.std.hr.entity.WorkSchedule;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class WorkScheduleDayFacade extends AbstractScheduleFacade<WorkScheduleDay> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WorkScheduleDayFacade() {
        super(WorkScheduleDay.class);
    }

    @Override
    public WorkScheduleDay createTransient(Map<String, Object> params) throws EJBException {

        WorkSchedule template = (WorkSchedule) params.get("selectedSchedule");

        Date date = (Date) params.get("selectedDate");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        WorkScheduleDay dayTemplate = new WorkScheduleDay();
//        dayTemplate.setDayNum(cal.get(Calendar.DAY_OF_WEEK));
        dayTemplate.setWorkday(date);
        dayTemplate.setFromDate(cal.getTime());
        dayTemplate.setThruDate(cal.getTime());
        dayTemplate.setTemplate(template);

        return dayTemplate;
    }

    @Override
    public Object getId(WorkScheduleDay entity) {
        WorkScheduleDayId id = new WorkScheduleDayId();

        WorkSchedule template = entity.getTemplate();
        if (template != null) {
            Long templateId = template.getId();
            id.setTemplate(templateId);
        }

        id.setWorkday(entity.getWorkday());

//        id.setDayNum(entity.getDayNum());
//        id.setFromDate(entity.getFromDate());
//        id.setThruDate(entity.getThruDate());
        return id;
    }

    @Override
    public void create(WorkScheduleDay entity) throws EJBException {
        WorkSchedule template = entity.getTemplate();
        template = getEntityManager().find(WorkSchedule.class, template.getId());
//        template.getTemplates().put(entity.getDayNum() - 1, entity);
        template.getTemplates().add(entity);
        getEntityManager().merge(template);
    }

    @Override
    public List<WorkScheduleDay> getEvents(Date fromDate, Date thruDate, Map<String, Object> filters) {

        WorkSchedule scheduleTemplate = (WorkSchedule) filters.get("selectedSchedule");

        if (scheduleTemplate == null) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkScheduleDay> cq = cb.createQuery(WorkScheduleDay.class);
        Root<WorkScheduleDay> scheduleDay = cq.from(WorkScheduleDay.class);
        cq.select(scheduleDay).where(new Predicate[]{
            cb.equal(scheduleDay.get("template").get("id"), scheduleTemplate.getId()),
            cb.greaterThanOrEqualTo(scheduleDay.get("fromDate"), fromDate),
            cb.lessThan(scheduleDay.get("thruDate"), thruDate)
        });

        TypedQuery<WorkScheduleDay> query = getEntityManager().createQuery(cq);

        return query.getResultList();

    }

    public List<WorkScheduleDay> getSchedule(WorkSchedule scheduleTemplate, Date fromDate, Date thruDate) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkScheduleDay> cq = cb.createQuery(WorkScheduleDay.class);
        Root<WorkScheduleDay> scheduleDayTemplate = cq.from(WorkScheduleDay.class);
        cq.select(scheduleDayTemplate).where(new Predicate[]{
            cb.equal(scheduleDayTemplate.get("template").get("id"), scheduleTemplate.getId()),
            cb.greaterThanOrEqualTo(scheduleDayTemplate.get("fromDate"), fromDate),
            cb.lessThan(scheduleDayTemplate.get("thruDate"), thruDate)
        });

        TypedQuery<WorkScheduleDay> query = getEntityManager().createQuery(cq);

        return query.getResultList();

    }

    public void generateSchedule(WorkSchedule scheduleContext, Date generateFrom, Date generateThru) {

    }

    public WorkScheduleDay getSpecificSchedule(WorkSchedule scheduleTemplate, Date workDay) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkScheduleDay> cq = cb.createQuery(WorkScheduleDay.class);
        Root<WorkScheduleDay> scheduleDayTemplate = cq.from(WorkScheduleDay.class);
        cq.select(scheduleDayTemplate).where(new Predicate[]{
            cb.equal(scheduleDayTemplate.get("template").get("id"), scheduleTemplate.getId()),
            cb.equal(scheduleDayTemplate.get("workday"), workDay)
        });

        TypedQuery<WorkScheduleDay> query = getEntityManager().createQuery(cq);

        try {
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

}
