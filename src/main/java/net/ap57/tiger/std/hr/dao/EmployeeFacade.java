/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.base.dao.PartyFacade;
import net.ap57.tiger.base.dao.PartyRoleTypeFacade;
import net.ap57.tiger.base.dao.PersonFacade;
import net.ap57.tiger.base.dao.PersonRoleFacade;
import net.ap57.tiger.base.entity.Person;
import net.ap57.tiger.std.hr.entity.Employee;
//import net.ap57.tiger.std.hr.entity.PositionSchedule;
//import net.ap57.tiger.std.hr.entity.PositionScheduleVarian;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Stateless
public class EmployeeFacade extends PersonRoleFacade<Employee, Person> {

  @Inject
  private EntityManager em;

  @Inject
  private PersonFacade personFacade;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  @Override
  protected PartyFacade getPartyFacade() {
    return personFacade;
  }

  @Inject
  private PartyRoleTypeFacade partyRoleTypeFacade;

  @Override
  protected PartyRoleTypeFacade getRoleTypeFacade() {
    return partyRoleTypeFacade;
  }

  @Override
  protected String getRoleTypeName() {
    return "Employee";
  }

  public EmployeeFacade() {
    super(Employee.class);
  }

  @Override
  public Employee createTransient() {
    
    Employee employee = new Employee();    
    employee.setPositionFulfillments(new ArrayList<>());
//    employee.setEmployeeWorkSchedules(new ArrayList<>());

    return employee;
  }
  
//  public List<Employee> getEmployeeInContext(PositionSchedule positionSchedule) {
//    
//    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//    
//    CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
//    
//    Root<Employee> employee = cq.from(Employee.class);
//    Join<Employee, PositionFulfillment> positionFulfillment = employee.join("positionFulfillments");
//
////    PositionScheduleVarian positionScheduleVarian = positionSchedule.getPositionScheduleVarian();
//        
//    cq.select(employee).distinct(true).where(new Predicate[]{
////      cb.equal(positionFulfillment.get("usedScheduleVarian").get("positionWorkUnit").get("position"), positionScheduleVarian.getPositionWorkUnit().getPosition()),
////      cb.equal(positionFulfillment.get("usedScheduleVarian").get("positionWorkUnit").get("workUnit"), positionScheduleVarian.getPositionWorkUnit().getWorkUnit()),
////      cb.equal(positionFulfillment.get("usedScheduleVarian").get("workUnitScheduleVarian").get("workUnit"), positionScheduleVarian.getWorkUnitScheduleVarian().getWorkUnit()),
////      cb.equal(positionFulfillment.get("usedScheduleVarian").get("workUnitScheduleVarian").get("name"), positionScheduleVarian.getWorkUnitScheduleVarian().getName()),
////      cb.lessThanOrEqualTo(positionFulfillment.<Date>get("fromDate"), positionSchedule.getWorkDate()),
//      cb.or(new Predicate[]{
//        cb.greaterThanOrEqualTo(positionFulfillment.<Date>get("thruDate"), positionSchedule.getWorkDate()),
//        cb.isNull(positionFulfillment.<Date>get("thruDate"))
//      })
//    });
//        
//    TypedQuery<Employee> q = getEntityManager().createQuery(cq);
//    
//    return q.getResultList();
//    
//  }

}
