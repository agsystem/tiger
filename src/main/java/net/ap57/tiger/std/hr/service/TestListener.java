/*
 * Copyright 2017 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import javax.ejb.Singleton;
import mdn.iotlib.comm.message.Message;
import net.ap57.tiger.base.api.AbstractSocketListener;
import net.ap57.tiger.std.hr.HRModule;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
public class TestListener extends AbstractSocketListener<HRModule> {

    public TestListener() {
        super(HRModule.class);
    }

    @Override
    protected String getServiceName() {
        return "test";
    }

    @Override
    protected void postLoad(Configuration appConfig, Configuration moduleConfig) {
        
    }

    @Override
    public void onMessage(Message message) {
        System.out.println("TestListener: " + message.getInt());
    }

    
    
}
