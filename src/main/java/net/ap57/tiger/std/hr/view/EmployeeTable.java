/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.view;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.entity.Party;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.base.ui.CaptureClient;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.service.WorkScheduleService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class EmployeeTable extends PFLazyDataTable<PositionFulfillmentFacade, PositionFulfillment> implements CaptureClient {
    
    public int getDataCount() {
        LazyDataModel<PositionFulfillment> model = (LazyDataModel<PositionFulfillment>) getModel();
        return model.getRowCount();
    }

    @Override
    public void processRawImage(byte[] raw) {
    }

    public void onPositionFulfillmentSelected(SelectEvent evt) {
    }

    public void addPartyContactMechanism(ActionEvent evt) {
        Party party = getSelection().getEmployee().getParty();
        getOp().addContactMechanisms(party);
    }

    public void delPartyContactMechanism(PartyContactMechanism partyContactMechanism) {
        Party party = getSelection().getEmployee().getParty();
        getOp().delContactMechanism(party, partyContactMechanism);
    }

    public void test(ActionEvent evt) {
        Employee employee = getSelection().getEmployee();
//    getService().generateEmployeeWorkSchedule(employee);
    }

    public WorkScheduleService getService() {
        return null;
    }

    public void setPrevPosition(PositionFulfillment prevPos) {
        setAttribute("prevPosition", prevPos);
    }

    public PositionFulfillment getPrevPosition() {
        return (PositionFulfillment) getAttribute("prevPosition");
    }

    public void setNewPosition(PositionFulfillment newPos) {
        setAttribute("newPosition", newPos);
    }

    public PositionFulfillment getNewPosition() {
        return (PositionFulfillment) getAttribute("newPosition");
    }

    public void prepareNewPosition(ActionEvent evt) {
        Employee employee = getSelection().getEmployee();
        Map<String, Object> params = new HashMap<>();
        params.put("selectedEmployee", employee);

        try {
            PositionFulfillment newPos = getOp().createTransient(params);

            setPrevPosition(getSelection());

            Calendar prevStop = Calendar.getInstance();
            prevStop.setTime(newPos.getFromDate());
            prevStop.add(Calendar.DATE, -1);
            getPrevPosition().setThruDate(prevStop.getTime());

            setNewPosition(newPos);
            setSelection(newPos);

            RequestContext requestContext = RequestContext.getCurrentInstance();
            requestContext.execute("PF('changePositionDlg').show()");

        } catch (EJBException ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }        
    }

    public void onEditNewPositionFromDate(SelectEvent event) {
        Date thruDate = (Date) event.getObject();
        Calendar prevStop = Calendar.getInstance();
        prevStop.setTime(thruDate);
        prevStop.add(Calendar.DATE, -1);
        getPrevPosition().setThruDate(prevStop.getTime());
        getNewPosition().setFromDate(thruDate);
    }

    public void saveNewPosition(ActionEvent evt) {
        try {
            getOp().saveNewPosition(getPrevPosition(), getNewPosition());
        } catch (EJBException ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

}
