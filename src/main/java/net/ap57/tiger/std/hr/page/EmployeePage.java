/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.page;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import net.ap57.tiger.base.UserSession;
import net.ap57.tiger.base.dao.ContactMechanismTypeFacade;
import net.ap57.tiger.base.dao.IdentityTypeFacade;
import net.ap57.tiger.base.dao.PersonFacade;
import net.ap57.tiger.base.entity.ContactMechanismType;
import net.ap57.tiger.base.entity.Gender;
import net.ap57.tiger.base.entity.IdentityType;
import net.ap57.tiger.base.reporting.bean.Reporter;
import net.ap57.tiger.base.ui.CaptureClient;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.EditableDataList;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.UploadClient;
import net.ap57.tiger.base.ui.annotation.PageView;
import net.ap57.tiger.base.ui.bean.ContactMechanismTypeDataList;
import net.ap57.tiger.base.ui.bean.GenderDataList;
import net.ap57.tiger.base.ui.bean.IdTypeDataList;
import net.ap57.tiger.base.ui.pf.EnumDataList;
import net.ap57.tiger.base.ui.pf.PFCamera;
import net.ap57.tiger.base.ui.pf.PFCropper;
import net.ap57.tiger.base.ui.pf.PFDashboard;
import net.ap57.tiger.base.ui.pf.PFDataTree;
import net.ap57.tiger.base.ui.pf.PFFileUpload;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.base.ui.pf.PFScheduleEvent;
import net.ap57.tiger.base.ui.pf.PFTemporaryFile;
import net.ap57.tiger.std.hr.dao.EmployeeCodeFacade;
import net.ap57.tiger.std.hr.dao.EmployeeCodeTypeFacade;
import net.ap57.tiger.std.hr.dao.EmploymentAgreementTypeFacade;
import net.ap57.tiger.std.hr.dao.EmploymentFacade;
import net.ap57.tiger.std.hr.dao.InternalOrganizationFacade;
import net.ap57.tiger.std.hr.dao.PositionFacade;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentHistoryFacade;
//import net.ap57.tiger.std.hr.dao.PositionScheduleVarianFacade;
import net.ap57.tiger.std.hr.dao.PositionTypeFacade;
import net.ap57.tiger.std.hr.dao.WorkScheduleDayFacade;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeCode;
import net.ap57.tiger.std.hr.entity.EmployeeCodeType;
import net.ap57.tiger.std.hr.entity.EmployeeScheduleDayException;
import net.ap57.tiger.std.hr.entity.EmployeeStatus;
import net.ap57.tiger.std.hr.entity.Employment;
import net.ap57.tiger.std.hr.entity.EmploymentAgreementType;
import net.ap57.tiger.std.hr.entity.IdCard;
import net.ap57.tiger.std.hr.entity.InternalOrganization;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionSchedule;
import net.ap57.tiger.std.hr.entity.PositionScheduleDayException;
import net.ap57.tiger.std.hr.entity.WorkSchedule;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayException;
//import net.ap57.tiger.std.hr.entity.PositionScheduleVarian;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.service.ScheduleService;
import net.ap57.tiger.std.hr.service.WorkScheduleService;
import net.ap57.tiger.std.hr.view.EmployeeCodeTypeDataList;
import net.ap57.tiger.std.hr.view.EmployeeStatusDataList;
import net.ap57.tiger.std.hr.view.EmployeeTable;
import net.ap57.tiger.std.hr.view.IdCardBucket;
import net.ap57.tiger.std.hr.view.PositionTable;
import net.ap57.tiger.std.hr.view.PositionTypeDataList;
import net.ap57.tiger.std.hr.view.WorkdaySchedule;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import net.ap57.tiger.base.ui.annotation.View;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named
@RequestScoped
@PageView(rel="")
public class EmployeePage extends Page implements CaptureClient, UploadClient {

//    @Inject
//    private Application theApp;
    
    @Inject
    private Reporter pdfReporter;

    @Inject
    private PositionFulfillmentFacade employeeOp;

    @Inject
    private InternalOrganizationFacade internalOrganizationOp;

    @Inject
    private EmploymentAgreementTypeFacade agreementTypeOp;

    @Inject
    private PersonFacade personOp;

    @View
    private PFDataTree internalOrganizationTree;

    @View
    private DataTable agreementTypeTable;

    @View
    private DataList agreementTypeList;

    @Inject
    private PositionFacade positionOp;

    @View
    private PositionTable positionTable;

    @View
    private EmployeeTable employeeTable;

    @Inject
    private ContactMechanismTypeFacade contactMechanismTypeOp;

    @View
    private DataTable contactMechanismTypeTable;

    @View
    private ContactMechanismTypeDataList contactTypeList;

    @View
    private PFCamera camera;

    @View
    private PFFileUpload uploader;

    @View
    private PFTemporaryFile temporaryFileManager;

    @View
    private PFCropper cropper;

    @View
    private PFDashboard employeeDashboard;

//    @Inject
//    private PositionScheduleVarianFacade positionScheduleVarianFacade;
//
//    @ViewComponent
//    private PositionScheduleVarianDataTable positionScheduleVarianTable;
    @Inject
    private WorkUnitFacade workUnitOp;

    @View
    private DataList workUnitList;

    @View
    private EnumDataList<Gender> genderList;

    @View
    private EnumDataList<EmployeeStatus> employeeStatusList;

    @Inject
    private WorkScheduleService service;

    @Inject
    private EmploymentFacade employmentOp;

    @View
    private DataTable employmentTable;

    @Inject
    private PositionFulfillmentHistoryFacade positionHistoryFacade;

    @View
    private DataTable positionHistoryTable;

    @View
    private IdCardBucket bucket;

    @Inject
    private PositionTypeFacade positionTypeOp;

    @View
    private PositionTypeDataList positionTypeList;

    @Inject
    private IdentityTypeFacade idTypeOp;

    @View
    private IdTypeDataList idTypeList;

    @View
    private DataTable idTypeTable;

    @Inject
    private WorkScheduleDayFacade workScheduleDayOp;

    @Inject
    private ScheduleService scheduleDayExceptionOp;

    @Inject
    private UserSession session;

    @View
    private WorkdaySchedule workSchedule;

    @Inject
    private EmployeeCodeTypeFacade employeeCodeTypeOp;

    @Inject
    private EmployeeCodeFacade employeeCodeOp;

    @View
    private EmployeeCodeTypeDataList employeeCodeTypeList;

    @View
    private DataTable aliasTypesTable;

    @View
    private PFLazyDataTable employeeCodeTable;
    
    @Override
    protected void initComponents() {

        internalOrganizationTree = new PFDataTree<InternalOrganizationFacade, InternalOrganization>() {

            @Override
            protected InternalOrganizationFacade getOp() {
                return internalOrganizationOp;
            }

        };

        agreementTypeTable = new PFLazyDataTable<EmploymentAgreementTypeFacade, EmploymentAgreementType>() {

            @Override
            protected EmploymentAgreementTypeFacade getOp() {
                return agreementTypeOp;
            }

            @Override
            public EmploymentAgreementType getSelection() {
                EmploymentAgreementType type = super.getSelection();
                if (type == null) {
                    super.prepareTransient(null);
                }
                return super.getSelection();
            }

            @Override
            public void create(EmploymentAgreementType e) {
                super.create(e);
                setSelection(null);
            }

        };

        agreementTypeList = new EditableDataList<EmploymentAgreementTypeFacade, EmploymentAgreementType>() {

            @Override
            public EmploymentAgreementTypeFacade getOp() {
                return agreementTypeOp;
            }

            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("name", value);
            }

            @Override
            protected EmploymentAgreementType newPlaceholder() {
                return new EmploymentAgreementType(true);
            }

        };

        positionTable = new PositionTable() {

            @Override
            protected Map<String, Object> getNewEntityParams() {
                Map<String, Object> filters = new HashMap<>();
                InternalOrganization org = (InternalOrganization) internalOrganizationTree.getSelection();
                if (org != null) {
                    filters.put("hiringOrganization", org);
                }
                return filters;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                InternalOrganization org = (InternalOrganization) internalOrganizationTree.getSelection();
                if (org != null && org.getId() != null) {
                    filters.put("hiringOrganization", org);
                }
                return filters;
            }

            @Override
            protected PositionFacade getOp() {
                return positionOp;
            }

            @Override
            public void onPositionSelected(SelectEvent evt) {
                Position pos = (Position) evt.getObject();
                employeeTable.getSelection().setPosition(pos);
            }

        };

        contactMechanismTypeTable = new PFLazyDataTable<ContactMechanismTypeFacade, ContactMechanismType>() {

            @Override
            protected ContactMechanismTypeFacade getOp() {
                return contactMechanismTypeOp;
            }

            @Override
            public ContactMechanismType getSelection() {
                ContactMechanismType type = super.getSelection();
                if (type == null) {
                    super.prepareTransient(null);
                }
                return super.getSelection();
            }

            @Override
            public void create(ContactMechanismType e) {
                super.create(e);
                setSelection(null);
            }
        };

        contactTypeList = new ContactMechanismTypeDataList() {

            @Override
            public ContactMechanismTypeFacade getOp() {
                return contactMechanismTypeOp;
            }

        };

        employeeTable = new EmployeeTable() {

            @Override
            public void init() {
            }

            @Override
            protected PositionFulfillmentFacade getOp() {
                return employeeOp;
            }

            @Override
            protected void addStaticFilter(Map<String, Object> filters) {
                InternalOrganization org = (InternalOrganization) internalOrganizationTree.getSelection();
                if (org != null) {
                    filters.put("internalOrganization", org);
                }
                filters.put("employee.archived", false);
            }

            @Override
            public void create(PositionFulfillment e) {
                e.getEmployee().getPerson().setPhoto(getPhoto());
                super.create(e);
            }

            @Override
            public void update(PositionFulfillment e) {
                byte[] photo = getPhoto();
                if (photo != null && photo.length > 0) {
                    e.getEmployee().getPerson().setPhoto(getPhoto());
                }
                super.update(e);
            }

            @Override
            public void prepareTransient(ActionEvent evt) {
                super.prepareTransient(evt);
                setPhoto(getSelection().getEmployee().getPerson().getPhoto());
            }

            @Override
            public void prepareEdit(ActionEvent evt) {
                super.prepareEdit(evt);
                setPhoto(getSelection().getEmployee().getPerson().getPhoto());
//                Map<String, Object> options = new HashMap<String, Object>();
//                options.put("modal", true);
//                RequestContext.getCurrentInstance().openDialog("edit-dlg", options, null);
            }

            @Override
            public WorkScheduleService getService() {
                return service;
            }

            @Override
            public void onPositionFulfillmentSelected(SelectEvent evt) {
                if (bucket.isOpen()) {
                    PositionFulfillment pos = (PositionFulfillment) evt.getObject();
                    IdCard card = new IdCard();
                    String nik = pos.getEmployee().getNik();
                    if (nik == null) {
                        nik = "";
                    }
                    card.setNik(nik);
                    card.setName(pos.getEmployee().getPerson().getName());
                    card.setPosition(pos.getPosition().getName());
                    card.setWorkUnits(pos.getPosition().getWorkUnits().toString());
                    card.setPhoto(pos.getEmployee().getPhoto());
                    card.setCode(pos.getEmployee().getCode());
                    bucket.put(card);
                }
            }

        };

        employeeDashboard = new PFDashboard() {

            @Override
            protected String[][] getWidgets() {
                return new String[][]{{"photo"}, {"general", "address", "phone"}, {"agreement", "position"}};
            }

        };

        workUnitList = new DataList<WorkUnitFacade, WorkUnit>() {

            @Override
            protected Object strToObj(String value) {
                return workUnitOp.findSingleByAttribute("name", value);
            }

            @Override
            public WorkUnitFacade getOp() {
                return workUnitOp;
            }

        };

        genderList = new GenderDataList();

        employeeStatusList = new EmployeeStatusDataList();

        positionHistoryTable = new PFLazyDataTable<PositionFulfillmentHistoryFacade, PositionFulfillment>() {

            @Override
            protected PositionFulfillmentHistoryFacade getOp() {
                return positionHistoryFacade;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                filters.put("employee", employeeTable.getSelection().getEmployee());
                return filters;
            }

        };

        employmentTable = new PFLazyDataTable<EmploymentFacade, Employment>() {
            @Override
            protected EmploymentFacade getOp() {
                return employmentOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                filters.put("employee", employeeTable.getSelection().getEmployee());
                return filters;
            }

        };

        workSchedule = new WorkdaySchedule<EmployeeScheduleDayException>() {

            @Override
            protected ScheduleService getExceptionOp() {
                return scheduleDayExceptionOp;
            }

            @Override
            protected WorkSchedule getScheduleContext() {
                PositionFulfillment positionFulfillment = employeeTable.getSelection();
                return positionOp.getInherentWorkSchedule(positionFulfillment.getPosition(), getCurrentDate());
            }

            @Override
            protected Map<String, Object> getNewExceptionParams() {
                Map<String, Object> params = super.getNewEntityParams();
                PositionFulfillment positionFulfillment = employeeTable.getSelection();
                params.put("positionFulfillment", positionFulfillment);
                return params;
            }

            @Override
            protected boolean hasException(WorkScheduleDay scheduleDay) {

                PositionFulfillment positionFulfillment = employeeTable.getSelection();

                EmployeeScheduleDayException exception = getExceptionOp().getScheduleException(scheduleDay, positionFulfillment);

                return exception != null;

            }

            @Override
            protected List<PFScheduleEvent> collectEvents(Date from, Date thru, Map<String, Object> filters) {
                List<WorkScheduleDay> rawEvents = getOp().getEvents(from, thru, filters);
                List<PFScheduleEvent> events = new ArrayList<>();

                for (WorkScheduleDay rawEvent : rawEvents) {

                    EmployeeScheduleDayException employeeScheduleDayException = getExceptionOp().getScheduleException(rawEvent, employeeTable.getSelection());

                    if (employeeScheduleDayException != null) {
                        PFScheduleEvent employeeScheduleExceptionEvent = new PFScheduleEvent(employeeScheduleDayException);
                        employeeScheduleExceptionEvent.setStyleClass("schedule-active");
                        events.add(employeeScheduleExceptionEvent);

                        PositionSchedule schedule = positionOp.getPositionScheduleByDate(employeeTable.getSelection().getPosition(), getCurrentDate());
                        PositionScheduleDayException positionScheduleDayException = getExceptionOp().getScheduleException(rawEvent, schedule);
                        if (positionScheduleDayException != null) {
                            PFScheduleEvent positionScheduleExceptionEvent = new PFScheduleEvent(positionScheduleDayException);
                            positionScheduleExceptionEvent.setStyleClass("schedule-inactive");
                            events.add(positionScheduleExceptionEvent);

                        } else {
                            WorkScheduleDayException exception = rawEvent.getException();
                            if (exception != null) {
                                PFScheduleEvent scheduleExceptionEvent = new PFScheduleEvent(exception);
                                scheduleExceptionEvent.setStyleClass("schedule-inactive");
                                events.add(scheduleExceptionEvent);
                            } else {
                                PFScheduleEvent scheduleEvent = new PFScheduleEvent(rawEvent);
                                scheduleEvent.setStyleClass("schedule-inactive");
                                events.add(scheduleEvent);
                            }
                        }
                    } else {
                        PositionSchedule schedule = positionOp.getPositionScheduleByDate(employeeTable.getSelection().getPosition(), getCurrentDate());
                        PositionScheduleDayException positionScheduleDayException = getExceptionOp().getScheduleException(rawEvent, schedule);
                        if (positionScheduleDayException != null) {
                            PFScheduleEvent positionScheduleExceptionEvent = new PFScheduleEvent(positionScheduleDayException);
                            positionScheduleExceptionEvent.setStyleClass("schedule-active");
                            events.add(positionScheduleExceptionEvent);

                        } else {
                            WorkScheduleDayException exception = rawEvent.getException();
                            if (exception != null) {
                                PFScheduleEvent scheduleExceptionEvent = new PFScheduleEvent(exception);
                                scheduleExceptionEvent.setStyleClass("schedule-active");
                                events.add(scheduleExceptionEvent);
                            } else {
                                PFScheduleEvent scheduleEvent = new PFScheduleEvent(rawEvent);
                                scheduleEvent.setStyleClass("schedule-active");
                                events.add(scheduleEvent);
                            }
                        }
                    }

                }

                return events;
            }

            @Override
            public void raiseExceptionEditDialog() {
                super.raiseExceptionEditDialog();
            }

            @Override
            public void raiseEditDialog() {

            }

            @Override
            protected void raiseCreateDialog(Date workday) {

            }

            @Override
            protected Class<EmployeeScheduleDayException> getExceptionClass() {
                return EmployeeScheduleDayException.class;
            }

            @Override
            public WorkScheduleDayFacade getOp() {
                return workScheduleDayOp;
            }

            @Override
            protected UserSession getSession() {
                return session;
            }

        };

        aliasTypesTable = new PFLazyDataTable<EmployeeCodeTypeFacade, EmployeeCodeType>() {
            @Override
            protected EmployeeCodeTypeFacade getOp() {
                return employeeCodeTypeOp;
            }

        };

        employeeCodeTable = new PFLazyDataTable<EmployeeCodeFacade, EmployeeCode>() {
            @Override
            protected Map<String, Object> getNewEntityParams() {
                Map<String, Object> params = new HashMap<>();
                Employee employee = employeeTable.getSelection().getEmployee();
                params.put("employee", employee);
                return params;
            }

            @Override
            protected EmployeeCodeFacade getOp() {
                return employeeCodeOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> params = new HashMap<>();
                Employee employee = employeeTable.getSelection().getEmployee();
                params.put("employee", employee);
                System.out.println("LOAD ALIASES  OF " + employee.getPerson().getName());
                return params;
            }

        };

        employeeCodeTypeList = new EmployeeCodeTypeDataList() {
            @Override
            public EmployeeCodeTypeFacade getOp() {
                return employeeCodeTypeOp;
            }

        };

        bucket = new IdCardBucket() {

//            @Override
//            public Module getModule() {
//                return getModule();
//            }
            @Override
            public Reporter getReporter() {
                return pdfReporter;
            }

        };

        positionTypeList = new PositionTypeDataList() {
            
            @Override
            public PositionTypeFacade getOp() {
                return positionTypeOp;
            }

        };

        idTypeList = new IdTypeDataList() {
            @Override
            public IdentityTypeFacade getOp() {
                return idTypeOp;
            }

        };

        idTypeTable = new PFLazyDataTable<IdentityTypeFacade, IdentityType>() {
            @Override
            protected IdentityTypeFacade getOp() {
                return idTypeOp;
            }

        };

        temporaryFileManager = new PFTemporaryFile();

        uploader = new PFFileUpload() {

            @Override
            protected PFTemporaryFile getTemporaryFileManager() {
                return temporaryFileManager;
            }

        };

        camera = new PFCamera() {

            @Override
            protected PFTemporaryFile getTemporaryFileManager() {
                return temporaryFileManager;
            }

        };

        cropper = new PFCropper() {

            @Override
            public int getX() {
                return (480 / 2) - (getW() / 2);
            }

            @Override
            public int getY() {
                return (360 / 2) - (getH() / 2);
            }

            @Override
            public int getW() {
                return 240;
            }

            @Override
            public int getH() {
                return 320;
            }

            @Override
            protected PFTemporaryFile getTemporaryFileManager() {
                return temporaryFileManager;
            }

        };

        cropper.addClient(this);

    }

    @Override
    protected void initLayout() {
        setExplorer("Organization", "15%");
        setEditor("Employee", "85%");
    }

    public byte[] getPhoto() {
        return (byte[]) getAttribute("photo");
    }

    public void setPhoto(byte[] photo) {
        setAttribute("photo", photo);
    }

    @Override
    public void processRawImage(byte[] raw) {
        setPhoto(raw);
    }

    @Override
    public void processUploadedImage(byte[] raw) {
        setPhoto(raw);
    }

    public StreamedContent getStreamed() {
        byte[] data = getPhoto();
        InputStream is = null;
        if (data != null) {
            is = new ByteArrayInputStream(data);
        } else {
            is = getModule().getResourceAsStream("images/noimg.jpg");
        }
        return new DefaultStreamedContent(is);
    }

}
