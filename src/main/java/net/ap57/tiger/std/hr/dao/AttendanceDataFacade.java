/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import net.ap57.tiger.std.hr.entity.AttendanceData;
import net.ap57.tiger.std.hr.entity.Employee;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class AttendanceDataFacade extends NoSqlAbstractFacade<AttendanceData> {

    private EntityManagerFactory emf;
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        if (em == null) {
            if (emf == null) {
                emf = Persistence.createEntityManagerFactory("Lion-PU");
            }
            em = emf.createEntityManager();
        }
        return em;
    }

    public AttendanceDataFacade() {
        super(AttendanceData.class);
        Integer i = 0;

    }

    public Date findLogWBound(Employee employee, Date loBound, Date hiBound) {
        TypedQuery<AttendanceData> query = getEntityManager().createNamedQuery("AttendanceLog.findLogWBound", AttendanceData.class)
                .setParameter("employee", employee.getId())
                .setParameter("loBound", loBound)
                .setParameter("hiBound", hiBound);
        
            List<AttendanceData> logs = query.getResultList();
            if(!logs.isEmpty()) {
                return logs.get(0).getDatetime();
            } else {
                return null;
            }
    }

}
