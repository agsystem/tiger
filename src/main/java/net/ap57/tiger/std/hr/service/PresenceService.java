/*
 * Copyright 2017 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import com.ibm.icu.util.Calendar;
import java.util.Date;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.std.hr.dao.AttendanceDataFacade;
import net.ap57.tiger.std.hr.dao.EmployeeFacade;
import net.ap57.tiger.std.hr.dao.PresenceDataFacade;
import net.ap57.tiger.std.hr.entity.AttendanceData;
import net.ap57.tiger.std.hr.entity.EffectiveScheduleDay;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.PresenceData;
import net.ap57.tiger.std.hr.entity.PresenceDataId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class PresenceService {

    private final long ONE_HOUR = 60 * 60 * 1000;
    private final long FULLDAY_HOUR = 24 * 60 * 60 * 1000;

    @Inject
    private AttendanceDataFacade attendanceDataFacade;

    @Inject
    private PresenceDataFacade presenceDataFacade;

    @Inject
    private ScheduleService scheduleService;

    @Inject
    private EmployeeFacade employeeFacade;

    public void calculatePresence(Employee employee, Date fromDate, Date thruDate) {

//        System.out.println("PresenceService.calculatePresence(" + employee.getPerson().getName() + ", " + fromDate + ", " + thruDate + ")");
//        List<EffectiveScheduleDay> scheduleDays = scheduleService.getEffectiveSchedule(employee, fromDate, thruDate);
        for (Date workday = fromDate; workday.getTime() <= thruDate.getTime();) {

//            System.out.println("PROCESSING: " + workday);
            EffectiveScheduleDay scheduleDay = scheduleService.getSchedule(employee, workday);

            if (scheduleDay != null) {

//                System.out.println("SCHEDULE: " + scheduleDay.getFromDate() + " - " + scheduleDay.getThruDate());
                long longFromDate = scheduleDay.getFromDate().getTime();
                Date thresIn1 = new Date(longFromDate - ONE_HOUR);
                Date thresIn2 = new Date(longFromDate + ONE_HOUR);
                Date inTime = findData(employee, thresIn1, thresIn2);

                long longThruDate = scheduleDay.getThruDate().getTime();
                Date thresOut1 = new Date(longThruDate - ONE_HOUR);
                Date thresOut2 = new Date(longThruDate + ONE_HOUR);
                Date outTime = findData(employee, thresOut1, thresOut2);

                if (inTime != null || outTime != null) {
//                    continue;

//                    System.out.println(employee.getPerson().getName() + "{in: " + inTime + ", out: " + outTime);
//            Date workday = scheduleDay.getWorkday();
                    createOrEditData(employee, workday, inTime, outTime);
                }

            }

            workday = new Date(workday.getTime() + FULLDAY_HOUR);

        }

    }

    public Date findData(Employee employee, Date thres1, Date thres2) {
        Date log = attendanceDataFacade.findLogWBound(employee, thres1, thres2);
        return log;
    }

    public void createOrEditData(Employee employee, Date workday, Date inTime, Date outTime) {

        PresenceData presenceData = findPresenceData(employee, workday);

        if (presenceData == null) {
            presenceData = new PresenceData();
            presenceData.setEmployee(employee);
            presenceData.setWorkday(workday);
            presenceData.setInTime(inTime);
            presenceData.setOutTime(outTime);

            presenceDataFacade.create(presenceData);

        } else {

            boolean dataChanged = false;

            if (inTime != null && presenceData.getInTime() == null) { // Prevent overwrite data.
                presenceData.setInTime(inTime);
                dataChanged = true;
            }

            if (outTime != null && presenceData.getOutTime() == null) { // Prevent overwrite data.
                presenceData.setOutTime(outTime);
                dataChanged = true;
            }

            if (dataChanged) {
                presenceDataFacade.edit(presenceData);
            }
        }

    }

    public PresenceData findPresenceData(Employee employee, Date workday) {

        PresenceDataId id = new PresenceDataId();
        id.setEmployee(employee.getId());
        id.setWorkday(workday);

        return presenceDataFacade.find(id);
    }
    
    @Inject
    private EntityManager em;

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void placeData(Employee employee, Date datetime) {

        // Guess workday.
        Calendar guessedWorkday = Calendar.getInstance();
        guessedWorkday.setTime(datetime);
        guessedWorkday.set(Calendar.HOUR_OF_DAY, 0);
        guessedWorkday.set(Calendar.MINUTE, 0);
        guessedWorkday.set(Calendar.SECOND, 0);
        guessedWorkday.set(Calendar.MILLISECOND, 0);

        // Try to place data        
        int trial = 1;
        Date trialWorkday = guessedWorkday.getTime();

        while (trial <= 2) {

            // Find effective schedule.
            EffectiveScheduleDay scheduleDay = scheduleService.getSchedule(employee, trialWorkday);
            if (scheduleDay != null) {
                long longFromDate = scheduleDay.getFromDate().getTime();
                Date thresIn1 = new Date(longFromDate - ONE_HOUR);
                Date thresIn2 = new Date(longFromDate + ONE_HOUR);

                long longThruDate = scheduleDay.getThruDate().getTime();
                Date thresOut1 = new Date(longThruDate - ONE_HOUR);
                Date thresOut2 = new Date(longThruDate + ONE_HOUR);

                if (thresIn1.getTime() <= datetime.getTime() && datetime.getTime() <= thresIn2.getTime()) {
                    createOrEditData(employee, trialWorkday, datetime, null);
//                    em.flush();
                    break;
                } else if (thresOut1.getTime() <= datetime.getTime() && datetime.getTime() <= thresOut2.getTime()) {
                    createOrEditData(employee, trialWorkday, null, datetime);
//                    em.flush();
                    break;
                }
            }

            // when failed, decrease workday.
            trialWorkday = new Date(trialWorkday.getTime() - FULLDAY_HOUR);
            trial++;
            // then repeat once more time.

        }
    }

}
