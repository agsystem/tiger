/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.base.dao.PersonFacade;
import net.ap57.tiger.base.entity.Gender;
import net.ap57.tiger.base.entity.Party;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeStatus;
//import net.ap57.tiger.std.hr.entity.EmployeeWorkSchedule;
import net.ap57.tiger.std.hr.entity.Employment;
import net.ap57.tiger.std.hr.entity.InternalOrganization;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionFulfillmentId;
//import net.ap57.tiger.std.hr.entity.PositionSchedule;
//import net.ap57.tiger.std.hr.entity.PositionScheduleVarian;
import net.ap57.tiger.std.hr.entity.PositionWorkUnit;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class PositionFulfillmentFacade extends AbstractFacade<PositionFulfillment> {

    @Inject
    private EntityManager em;

    @Inject
    private InternalOrganizationFacade organizationFacade;

    @Inject
    private EmployeeFacade employeeFacade;

    @Inject
    private EmploymentFacade employmentFacade;

//    @Inject
//    private PositionScheduleFacade positionScheduleFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PositionFulfillmentFacade() {
        super(PositionFulfillment.class);
    }

    @Override
    public PositionFulfillment createTransient(Map<String, Object> params) throws EJBException {

        PositionFulfillment positionFulfillment = new PositionFulfillment();

        Employee employee = null;
        Employment employment = null;

        if (params != null && params.containsKey("selectedEmployee")) {
            employee = (Employee) params.get("selectedEmployee");

            employment = employmentFacade.getLatestEmployment(employee);
            getEntityManager().detach(employment);

            Date tDef = Calendar.getInstance().getTime();

            Date tF = employment.getFromDate();
            Date tT = employment.getThruDate();

            if ((tT != null && tF.getTime() <= tDef.getTime() && tDef.getTime() <= tT.getTime()) || (tT == null && tF.getTime() <= tDef.getTime())) {
                positionFulfillment.setFromDate(tDef);
                positionFulfillment.setThruDate(employment.getThruDate());
            } else {
                throw new EJBException("No Active Employment");
            }

        } else {
            employee = employeeFacade.createTransient(params);
            employee.setStatus(EmployeeStatus.ACTIVE);
            employment = employmentFacade.createTransient(null);
            employment.setPositionFulfillments(new ArrayList<>());
            employment.setEmployee(employee);
        }

        positionFulfillment.setEmployee(employee);
        employee.getPositionFulfillments().add(positionFulfillment);

        positionFulfillment.setEmployment(employment);

        employment.getPositionFulfillments().add(positionFulfillment);
        employee.getRelationshipParticipations().add(employment);

        return positionFulfillment;

    }

    @Override
    public Object getId(PositionFulfillment entity) {

        PositionFulfillmentId id = new PositionFulfillmentId();

        Position pos = entity.getPosition();
        if (pos != null && pos.getId() != null) {
            id.setPosition(pos.getId());
        } else {
            return null;
        }

        Employee emp = entity.getEmployee();
        if (emp.getId() != null) {
            id.setEmployee(emp.getId());
        } else {
            return null;
        }

        Date fromDate = entity.getFromDate();
        if (fromDate != null) {
            id.setFromDate(fromDate);
        } else {
            return null;
        }

        return id;
    }    

    @Override
    protected CriteriaQuery constructFindQuery(Map<String, Object> filters) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PositionFulfillment> cq = cb.createQuery(entityClass);

        Root<Employee> employee = cq.from(Employee.class);
        
        Join<Employee, PositionFulfillment> positionFulfillment = employee.join("positionFulfillments");
        cq.select(positionFulfillment).distinct(true).orderBy(cb.desc(positionFulfillment.get("created")));

        Subquery<Date> sq = cq.subquery(Date.class);
        
        Root<PositionFulfillment> sqroot = sq.from(PositionFulfillment.class);
        
        sq.select(cb.greatest(sqroot.<Date>get("fromDate")));
        List<Predicate> sqPredicates = new ArrayList<>();
        sqPredicates.add(cb.equal(sqroot.get("employee").get("id"), employee.get("id")));

        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.HOUR, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.MILLISECOND, 0);
        sqPredicates.add(cb.lessThanOrEqualTo(sqroot.<Date>get("fromDate"), currentDate.getTime()));

        if (filters != null && !filters.isEmpty()) {

            List<Predicate> predicates = new ArrayList<>();

            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "employee.nik":
                        Expression<String> literalNik = cb.upper(cb.literal((String) filterValue));
                        predicates.add(cb.like(cb.upper(employee.<String>get("nik")), literalNik));
                        break;
                    case "employee.person.name":
                        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                        predicates.add(cb.or(cb.like(cb.upper(employee.get("person").<String>get("firstName")), literal),
                                cb.like(cb.upper(employee.get("person").<String>get("lastName")), literal)));
                        break;
                    case "position.name":
                        Expression<String> literalPos = cb.upper(cb.literal((String) filterValue));
                        predicates.add(cb.like(cb.upper(positionFulfillment.get("position").<String>get("name")), literalPos));
                        break;
                    case "internalOrganization":
                        InternalOrganization org = (InternalOrganization) filterValue;
                        List<Long> partyIds = new ArrayList<>();
                        partyIds.add(org.getParty().getId());
                        organizationFacade.getDescendants(org, partyIds);
                        sqPredicates.add(positionFulfillment.get("employment").get("internalOrganization").get("organization").get("id").in(partyIds));
                        break;
                    case "position.workUnits":
                        String workUnitName = (String) filterValue;
                        if (workUnitName.equals("All")) {
                            break;
                        }
                        Subquery<PositionWorkUnit> sqUnit = cq.subquery(PositionWorkUnit.class);
                        Root<PositionWorkUnit> sqUnitRoot = sqUnit.from(PositionWorkUnit.class);
                        sqUnit.select(sqUnitRoot);
                        sqUnit.where(new Predicate[]{
                            cb.equal(sqUnitRoot.get("workUnit").get("name"), workUnitName),
                            cb.equal(sqUnitRoot.get("position"), positionFulfillment.get("position"))
                        });
                        predicates.add(cb.isMember(sqUnit, positionFulfillment.get("position").<List<PositionWorkUnit>>get("workUnits")));
                        break;
                    case "WorkUnit":
                        WorkUnit workUnit = (WorkUnit) filterValue;
                        Subquery<PositionWorkUnit> sqUnitO = cq.subquery(PositionWorkUnit.class);
                        Root<PositionWorkUnit> sqUnitRootO = sqUnitO.from(PositionWorkUnit.class);
                        sqUnitO.select(sqUnitRootO);
                        sqUnitO.where(new Predicate[]{
                            cb.equal(sqUnitRootO.get("workUnit").get("name"), workUnit.getName()),
                            cb.equal(sqUnitRootO.get("position"), positionFulfillment.get("position"))
                        });
                        predicates.add(cb.isMember(sqUnitO, positionFulfillment.get("position").<List<PositionWorkUnit>>get("workUnits")));
                        break;
                    case "employment.agreementType":
                        String agreementTypeName = (String) filterValue;
                        if (!agreementTypeName.equals("All")) {
                            predicates.add(cb.equal(positionFulfillment.get("employment").get("agreementType").get("name"), agreementTypeName));
                        }
                        break;
                    case "employee.person.gender":
                        String strGender = (String) filterValue;
                        if (!strGender.equals("All")) {
                            Gender gender = Gender.valueOf(strGender);
                            predicates.add(cb.equal(employee.get("person").get("gender"), gender));
                        }
                        break;
                    case "employee.status":
                        String strStatus = (String) filterValue;
                        if (!strStatus.equals("All")) {
                            EmployeeStatus status = EmployeeStatus.valueOf(strStatus);
                            predicates.add(cb.equal(employee.get("status"), status));
                        }
                        break;
//                    case "employee.onboarding":
//                        Boolean onboarding = (Boolean) filterValue;
//                        predicates.add(cb.equal(employee.get("onboarding"), onboarding));
//                        break;
//                    case "employee.archived":
//                        Boolean archived = (Boolean) filterValue;
//                        predicates.add(cb.equal(employee.get("archived"), archived));
//                        break;
                    default:
                        break;
                }

            }

            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
            predicates.add(cb.equal(positionFulfillment.get("fromDate"), sq));
            
            cq.where(predicates.toArray(new Predicate[predicates.size()]));

        }

        return cq;

    }

    @Override
    protected CriteriaQuery constructCountQuery(Map<String, Object> filters) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<Employee> employee = cq.from(Employee.class);
        Join<Employee, PositionFulfillment> positionFulfillment = employee.join("positionFulfillments");
        cq.select(cb.count(employee)).distinct(true);

        Subquery<Date> sq = cq.subquery(Date.class);
        Root<PositionFulfillment> sqroot = sq.from(PositionFulfillment.class);
        sq.select(cb.greatest(sqroot.<Date>get("fromDate")));
        List<Predicate> sqPredicates = new ArrayList<>();
        sqPredicates.add(cb.equal(sqroot.get("employee").get("id"), employee.get("id")));

        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.HOUR, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.MILLISECOND, 0);
        sqPredicates.add(cb.lessThanOrEqualTo(sqroot.<Date>get("fromDate"), currentDate.getTime()));

        if (filters != null && !filters.isEmpty()) {
            List<Predicate> predicates = new ArrayList<>();
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "employee.nik":
                        Expression<String> literalNik = cb.upper(cb.literal((String) filterValue));
                        predicates.add(cb.like(cb.upper(employee.<String>get("nik")), literalNik));
                        break;
                    case "employee.person.name":
                        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                        predicates.add(cb.or(cb.like(cb.upper(employee.get("person").<String>get("firstName")), literal),
                                cb.like(cb.upper(employee.get("person").<String>get("lastName")), literal)));
                        break;
                    case "position.name":
                        Expression<String> literalPos = cb.upper(cb.literal((String) filterValue));
                        predicates.add(cb.like(cb.upper(positionFulfillment.get("position").<String>get("name")), literalPos));
                        break;
                    case "internalOrganization":
                        InternalOrganization org = (InternalOrganization) filterValue;
                        List<Long> partyIds = new ArrayList<>();
                        partyIds.add(org.getParty().getId());
                        organizationFacade.getDescendants(org, partyIds);
                        sqPredicates.add(positionFulfillment.get("employment").get("internalOrganization").get("organization").get("id").in(partyIds));
                        break;
                    case "position.workUnits":
                        String workUnitName = (String) filterValue;
                        if (workUnitName.equals("All")) {
                            break;
                        }
                        Subquery<PositionWorkUnit> sqUnit = cq.subquery(PositionWorkUnit.class);
                        Root<PositionWorkUnit> sqUnitRoot = sqUnit.from(PositionWorkUnit.class);
                        sqUnit.select(sqUnitRoot);
                        sqUnit.where(new Predicate[]{
                            cb.equal(sqUnitRoot.get("workUnit").get("name"), workUnitName),
                            cb.equal(sqUnitRoot.get("position"), positionFulfillment.get("position"))
                        });
                        predicates.add(cb.isMember(sqUnit, positionFulfillment.get("position").<List<PositionWorkUnit>>get("workUnits")));
                        break;
                    case "WorkUnit":
                        WorkUnit workUnit = (WorkUnit) filterValue;
                        Subquery<PositionWorkUnit> sqUnitO = cq.subquery(PositionWorkUnit.class);
                        Root<PositionWorkUnit> sqUnitRootO = sqUnitO.from(PositionWorkUnit.class);
                        sqUnitO.select(sqUnitRootO);
                        sqUnitO.where(new Predicate[]{
                            cb.equal(sqUnitRootO.get("workUnit").get("name"), workUnit.getName()),
                            cb.equal(sqUnitRootO.get("position"), positionFulfillment.get("position"))
                        });
                        predicates.add(cb.isMember(sqUnitO, positionFulfillment.get("position").<List<PositionWorkUnit>>get("workUnits")));
                        break;
                    case "employment.agreementType":
                        String agreementTypeName = (String) filterValue;
                        if (!agreementTypeName.equals("All")) {
                            predicates.add(cb.equal(positionFulfillment.get("employment").get("agreementType").get("name"), agreementTypeName));
                        }
                        break;
                    case "employee.person.gender":
                        String strGender = (String) filterValue;
                        if (!strGender.equals("All")) {
                            Gender gender = Gender.valueOf(strGender);
                            predicates.add(cb.equal(employee.get("person").get("gender"), gender));
                        }
                        break;
                    case "employee.status":
                        String strStatus = (String) filterValue;
                        if (!strStatus.equals("All")) {
                            EmployeeStatus status = EmployeeStatus.valueOf(strStatus);
                            predicates.add(cb.equal(employee.get("status"), status));
                        }
                        break;
//                    case "employee.onboarding":
//                        Boolean onboarding = (Boolean) filterValue;
//                        predicates.add(cb.equal(employee.get("onboarding"), onboarding));
//                        break;
                    default:
                        break;
                }
            }

            predicates.add(cb.equal(positionFulfillment.get("fromDate"), sq));
            cq.where(predicates.toArray(new Predicate[predicates.size()]));
            sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));
        }

        return cq;

    }

    public void generateEmployeeWorkSchedule(Employee employee) {

        PositionFulfillment pf = employee.getLatestPositionFulfillment();
//        PositionScheduleVarian psv = pf.getUsedScheduleVarian();

        Date fromDate = pf.getFromDate();

//        for (PositionSchedule psch : positionScheduleFacade.getAllScheduleFrom(fromDate, psv)) {
//
//            EmployeeWorkSchedule wsch = new EmployeeWorkSchedule();
//            wsch.setEmployee(employee);
//            wsch.setWorkDate(psch.getWorkDate());
//            wsch.setFromDate(psch.getFromDate());
//            wsch.setThruDate(psch.getThruDate());
//            wsch.setParent(psch);
//
//            employee.getEmployeeWorkSchedules().add(wsch);
//            psch.getChildren().add(wsch);
//        }

    }

    @Override
    public void create(PositionFulfillment entity) throws EJBException {

        Date cal = entity.getEmployment().getFromDate();

        entity.setFromDate(cal);
        Employee e = entity.getEmployee();
        e.setFromDate(cal);

        Position position = entity.getPosition();
        InternalOrganization org = position.getHiringOrganization();
        org.getRelationshipsEstablished().add(entity.getEmployment());
        entity.getEmployment().setInternalOrganization(org);
        entity.getEmployment().setRefNum(employmentFacade.generateRefNum());
        entity.setFromDate(cal);

//        generateEmployeeWorkSchedule(e);

        getEntityManager().persist(e.getParty());

    }

    public void saveNewPosition(PositionFulfillment prevPos, PositionFulfillment nextPos) throws EJBException {

        Employment employment = nextPos.getEmployment();

        Date tF = employment.getFromDate();
        Date tT = employment.getThruDate();
        Date tFF = nextPos.getFromDate();
        Date tFT = nextPos.getThruDate();

        if (tFF.getTime() >= tF.getTime()) {

            if ((tT != null && tFT.getTime() <= tT.getTime()) || tT == null) {
                getEntityManager().merge(prevPos);
                getEntityManager().persist(nextPos);
            } else {
                throw new EJBException("Invalid Thru Date");
            }

        } else {
            throw new EJBException("Invalid From Date");
        }
    }

    @Inject
    private PersonFacade personFacade;

    @Override
    public void edit(PositionFulfillment entity) {

        PositionFulfillment edited = entity;

//        entity = find(getId(entity));
        Employee e = entity.getEmployee();
        String nik = e.getNik();
        getEntityManager().merge(e.getParty());
        getEntityManager().flush();
        Employee ee = employeeFacade.find(employeeFacade.getId(e));
        ee.setNik(nik);
        getEntityManager().merge(e);
    }

    @Override
    public void remove(PositionFulfillment entity) {
        personFacade.remove(entity.getEmployee().getPerson());
    }

    public void addContactMechanisms(Party party) {
        employeeFacade.addContactMechanisms(party);
    }

    public void delContactMechanism(Party party, PartyContactMechanism partyContactMechanism) {
        employeeFacade.delContactMechanism(party, partyContactMechanism);
    }

    public PositionFulfillment getPositionFulfillmentByDate(Employee emp, Date date) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PositionFulfillment> cq = cb.createQuery(entityClass);
        
        Root<Employee> employee = cq.from(Employee.class);
        
        Join<Employee, PositionFulfillment> positionFulfillment = employee.join("positionFulfillments");
        cq.select(positionFulfillment).distinct(true);
        
        Subquery<Date> sq = cq.subquery(Date.class);
        Root<PositionFulfillment> sqroot = sq.from(PositionFulfillment.class);
        sq.select(cb.greatest(sqroot.<Date>get("fromDate")));
        List<Predicate> sqPredicates = new ArrayList<>();
        sqPredicates.add(cb.equal(sqroot.get("employee").get("id"), employee.get("id")));
        
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(date);
        currentDate.set(Calendar.HOUR, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.MILLISECOND, 0);
        sqPredicates.add(cb.lessThanOrEqualTo(sqroot.<Date>get("fromDate"), currentDate.getTime()));

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(positionFulfillment.get("fromDate"), sq));
        predicates.add(cb.equal(employee.get("id"), emp.getId()));
        cq.where(predicates.toArray(new Predicate[predicates.size()]));
        
        sq.where(sqPredicates.toArray(new Predicate[sqPredicates.size()]));

        TypedQuery<PositionFulfillment> query = getEntityManager().createQuery(cq);

        try {
            return query.getSingleResult();
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

//    @Override
//    public Long countAll(Map<String, Object> filters) {
//        if ((filters.get("internalOrganization") == null && filters.get("all") == null) || (filters.get("all") != null && filters.get("all") == Boolean.FALSE)) {
//            return 0L;
//        } else {
//            
//            return super.countAll(filters);
//        }
//    }
//
//    @Override
//    public List<PositionFulfillment> findAll(int startPosition, int maxResult, Map<String, Object> filters) {
//        if ((filters.get("internalOrganization") == null && filters.get("all") == null) || (filters.get("all") != null && filters.get("all") == Boolean.FALSE)) {
//            return new ArrayList<>();
//        } else {
//            return super.findAll(startPosition, maxResult, filters);
//        }
//    }

}
