/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import mdn.iotlib.comm.message.Message;
import net.ap57.tiger.base.api.AbstractSocketListener;
import net.ap57.tiger.std.hr.HRModule;
import net.ap57.tiger.std.hr.entity.Employee;
import org.apache.commons.configuration.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
public class AttendanceListener extends AbstractSocketListener<HRModule> {

    private final SimpleDateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Inject
    private PresenceService presenceService;

    @Inject
    private EmployeeService employeeService;

    public AttendanceListener() {
        super(HRModule.class);
    }

    @Override
    protected void postLoad(Configuration appConfig, Configuration moduleConfig) {
//        startClient();
    }

    private Properties getClientProperties() {

        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.1.254:9092");
        props.put("group.id", UUID.randomUUID().toString());
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        return props;

    }

    private void startClient() {

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(getClientProperties());
        consumer.subscribe(Arrays.asList("attendancetopic"));

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Long.MAX_VALUE);
            for (ConsumerRecord<String, String> record : records) {
                System.out.println((String) record.value());
//                processRecord(record);
            }
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NEVER)
    private void processRecord(ConsumerRecord record) {
        try {
            String[] data = ((String) record.value()).split(",");
//            System.out.println("EMP CODE: " + record.value());
            Employee employee = employeeService.getEmployeeByCode(1L, Long.parseLong(data[0]));
//            if (data[0].equals("23")) {
//                System.out.println("23 = " + employee.getPerson().getName());
//            }
            Date datetime = DATEFORMAT.parse(data[1]);
            if (employee != null) {
//                System.out.println("EMPLOYEE FOUND: " + employee.getPerson().getName());
                presenceService.placeData(employee, datetime);
            }
        } catch (ParseException ex) {
            Logger.getLogger(AttendanceListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected String getServiceName() {
        return "attendance";
    }

    @Override
    public void onMessage(Message message) {
        System.out.println("AttendanceListener: " + message.getInt());
    }

}
