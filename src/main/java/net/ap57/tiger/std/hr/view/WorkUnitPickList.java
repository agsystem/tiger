/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.view;

import net.ap57.tiger.base.ui.pf.PFPickList;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.WorkUnit;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class WorkUnitPickList extends PFPickList<WorkUnitFacade, WorkUnit> {

  @Override
  protected boolean isEqual(WorkUnit t1, WorkUnit t2) {
    if (t1.getParty().getId().equals(t2.getParty().getId())
        && t1.getPartyRoleType().getId().equals(t2.getPartyRoleType().getId())
        && t1.getFromDate().equals(t2.getFromDate())) {
      return true;
    } else {
      return false;
    }
  }
  
//  public WorkUnit getSelection() {
//    return (WorkUnit) getAttribute("selection");
//  }
//  
//  public void setSelection(WorkUnit selection) {
//    setAttribute("selection", selection);
//  }
  
//  protected WorkUnit newEntity() {
//    return getOp().createTransient(null);
//  }
//  
//  public void prepareTransient(ActionEvent evt) {
//    setSelection(newEntity());
//  }
  
//  public void create(ActionEvent evt) {
//    create((WorkUnit) getSelection());
//  }
//  
//  public void create(WorkUnit unit) {
//    try {
//      getOp().create(unit);
//    } catch (Exception ex) {
//      Logger.getLogger(WorkUnitPickList.class.getName()).log(Level.SEVERE, null, ex);
//    }
//  }

    @Override
    protected Object strToObj(String value) {
        return getOp().findSingleByAttribute("name", value);
    }

}
