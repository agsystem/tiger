/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.hr.page;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import net.ap57.tiger.base.UserSession;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.EditableDataList;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFDataTree;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.base.ui.pf.PFScheduleEvent;
import net.ap57.tiger.std.hr.dao.InternalOrganizationFacade;
import net.ap57.tiger.std.hr.dao.PositionFacade;
import net.ap57.tiger.std.hr.dao.PositionReportingStructureFacade;
import net.ap57.tiger.std.hr.dao.PositionTypeFacade;
import net.ap57.tiger.std.hr.dao.PositionWorkUnitFacade;
import net.ap57.tiger.std.hr.dao.WorkScheduleDayFacade;
import net.ap57.tiger.std.hr.dao.WorkScheduleFacade;
import net.ap57.tiger.std.hr.dao.WorkUnitFacade;
import net.ap57.tiger.std.hr.entity.InternalOrganization;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionSchedule;
import net.ap57.tiger.std.hr.entity.PositionScheduleDayException;
import net.ap57.tiger.std.hr.entity.PositionType;
import net.ap57.tiger.std.hr.entity.PositionWorkUnit;
import net.ap57.tiger.std.hr.entity.WorkSchedule;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayException;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import net.ap57.tiger.std.hr.service.ScheduleService;
import net.ap57.tiger.std.hr.view.PositionReportingStructureTable;
import net.ap57.tiger.std.hr.view.PositionStatusDataList;
import net.ap57.tiger.std.hr.view.PositionTable;
import net.ap57.tiger.std.hr.view.WorkScheduleChooser;
import net.ap57.tiger.std.hr.view.WorkUnitPickList;
import net.ap57.tiger.std.hr.view.WorkdaySchedule;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Named
@RequestScoped
@PageView(rel = "organization")
public class InternalOrganizationPage extends Page {

    @Inject
    private InternalOrganizationFacade op;

    @Inject
    private PositionFacade positionOp;

    @Inject
    private PositionTypeFacade positionTypeOp;

    @Inject
    private WorkUnitFacade workUnitOp;

    @Inject
    private PositionWorkUnitFacade positionWorkUnitFacade;

    @View
    private PFDataTree internalOrganizationTree;

    @View
    private PFDataTree internalOrganizationSelector;

    @View
    private PositionTable positionTable;

    @View
    private DataTable positionSelector;

    @View
    private DataTable positionTypeTable;

    @View
    private DataTable workUnitTable;

    @View
    private WorkUnitPickList workUnitPickList;

    @View
    private EditableDataList positionTypeList;

    @View
    private PositionStatusDataList positionStatusList;

    @View
    private DataList workUnitList;

    @Inject
    private PositionReportingStructureFacade reportingStructureOp;

    @View
    private PositionReportingStructureTable reportingStructureTable;

    @Inject
    private WorkScheduleFacade scheduleOp;

    @View
    private WorkScheduleChooser scheduleChooser;

    @Inject
    private WorkScheduleDayFacade workScheduleDayOp;

    @Inject
    private ScheduleService scheduleDayExceptionOp;

    @Inject
    private UserSession session;

    @View
    private WorkdaySchedule workSchedule;

    @Override
    protected void initComponents() {

        internalOrganizationTree = new PFDataTree<InternalOrganizationFacade, InternalOrganization>() {

            @Override
            protected InternalOrganizationFacade getOp() {
                return op;
            }

            @Override
            public void handleNodeSelected(NodeSelectEvent evt) {
                positionTable.getSelections().clear();
                super.handleNodeSelected(evt);
            }

        };

        positionTable = new PositionTable() {

            @Override
            protected PositionFacade getOp() {
                return positionOp;
            }

            @Override
            public boolean isCreateContextEmpty() {
                if (internalOrganizationTree.getNodeSelection() != null) {
                    InternalOrganization selectedOrg = (InternalOrganization) internalOrganizationTree.getNodeSelection().getData();
                    return selectedOrg.getId() == null;
                } else {
                    return true;
                }
            }

            @Override
            public void create(Position e) {
                PositionSchedule positionSchedule = new PositionSchedule();
                positionSchedule.setPosition(e);
                positionSchedule.setWorkSchedule(scheduleChooser.getValue());
                positionSchedule.setFromDate(e.getEstimatedFromDate());
                positionSchedule.setThruDate(e.getEstimatedThruDate());
                e.getPositionSchedules().add(positionSchedule);
                super.create(e);
            }

            @Override
            protected Map<String, Object> getNewEntityParams() {
                Map<String, Object> params = new HashMap<>();
                InternalOrganization intOrg = (InternalOrganization) internalOrganizationTree.getSelection();
                params.put("hiringOrganization", intOrg);
                return params;
            }

        };

        positionTypeList = new EditableDataList<PositionTypeFacade, PositionType>() {

            @Override
            protected Object strToObj(String value) {
                return getOp().findSingleByAttribute("title", value);
            }

            @Override
            protected PositionType newPlaceholder() {
                return new PositionType(true);
            }

            @Override
            public PositionTypeFacade getOp() {
                return positionTypeOp;
            }

        };

        positionTypeTable = new PFLazyDataTable<PositionTypeFacade, PositionType>() {

            @Override
            protected PositionTypeFacade getOp() {
                return positionTypeOp;
            }

        };

        positionStatusList = new PositionStatusDataList();

        workUnitList = new DataList<WorkUnitFacade, WorkUnit>() {

            @Override
            protected Object strToObj(String value) {
                return workUnitOp.findSingleByAttribute("name", value);
            }

            @Override
            public WorkUnitFacade getOp() {
                return workUnitOp;
            }

        };

        workUnitTable = new PFLazyDataTable<WorkUnitFacade, WorkUnit>() {

            @Override
            protected WorkUnitFacade getOp() {
                return workUnitOp;
            }

        };

        workUnitPickList = new WorkUnitPickList() {

            @Override
            protected WorkUnitFacade getOp() {
                return workUnitOp;
            }

            @Override
            public List<WorkUnit> getTarget() {

                Position pos = (Position) positionTable.getSelection();

                List<WorkUnit> workUnits = new ArrayList<>();

                if (pos != null) {
                    for (PositionWorkUnit positionWorkUnit : pos.getWorkUnits()) {
                        workUnits.add(positionWorkUnit.getWorkUnit());
                    }
                }

                return workUnits;

            }

            @Override
            protected void delItems(List<WorkUnit> items) {
                Position selectedPosition = (Position) positionTable.getSelection();
                List<PositionWorkUnit> remPositionWorkUnits = new ArrayList<>();
                List<PositionWorkUnit> existingPositionWorkUnits = selectedPosition.getWorkUnits();
                for (WorkUnit workUnit : items) {
                    for (PositionWorkUnit positionWorkUnit : existingPositionWorkUnits) {
                        if (positionWorkUnit.getWorkUnit().equals(workUnit)) {
                            remPositionWorkUnits.add(positionWorkUnit);
                        }
                    }
                }

                for (PositionWorkUnit remPositionWorkUnit : remPositionWorkUnits) {
                    existingPositionWorkUnits.remove(remPositionWorkUnit);
                }

            }

            @Override
            protected void addItems(List<WorkUnit> items) {
                Position selectedPosition = (Position) positionTable.getSelection();
                for (WorkUnit unit : items) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("position", selectedPosition);
                    params.put("workUnit", unit);
                    PositionWorkUnit positionWorkUnit = positionWorkUnitFacade.createTransient(params);
                    unit.getPositions().add(positionWorkUnit);
                    selectedPosition.getWorkUnits().add(positionWorkUnit);
                }
            }

        };

        internalOrganizationSelector = new PFDataTree<InternalOrganizationFacade, InternalOrganization>() {

            @Override
            protected InternalOrganizationFacade getOp() {
                return op;
            }

            @Override
            public void handleNodeSelected(NodeSelectEvent evt) {
                super.handleNodeSelected(evt);
                Position selectedPos = (Position) positionTable.getSelection();
                selectedPos.setHiringOrganization(getSelection());
            }

        };

        positionSelector = new PFLazyDataTable<PositionFacade, Position>() {

            @Override
            protected PositionFacade getOp() {
                return positionOp;
            }

        };

        reportingStructureTable = new PositionReportingStructureTable() {

            @Override
            protected PositionReportingStructureFacade getOp() {
                return reportingStructureOp;
            }

            @Override
            protected Position getReporter() {
                return (Position) positionTable.getSelection();
            }

        };

        scheduleChooser = new WorkScheduleChooser() {
            @Override
            protected WorkScheduleFacade getOp() {
                return scheduleOp;
            }

            @Override
            public void onChoose(SelectEvent evt) {
                WorkSchedule tpl = (WorkSchedule) evt.getObject();
                System.out.println(tpl.getName());
            }

        };

        workSchedule = new WorkdaySchedule<PositionScheduleDayException>() {

            @Override
            public WorkScheduleDayFacade getOp() {
                return workScheduleDayOp;
            }

            @Override
            protected UserSession getSession() {
                return session;
            }

            @Override
            protected WorkSchedule getScheduleContext() {
                return positionOp.getInherentWorkSchedule(positionTable.getSelection(), getCurrentDate());
            }

            @Override
            protected ScheduleService getExceptionOp() {
                return scheduleDayExceptionOp;
            }

            @Override
            protected Class<PositionScheduleDayException> getExceptionClass() {
                return PositionScheduleDayException.class;
            }

            @Override
            protected Map<String, Object> getNewExceptionParams() {
                Map<String, Object> params = super.getNewEntityParams();
                PositionSchedule schedule = positionOp.getPositionScheduleByDate(positionTable.getSelection(), getCurrentDate());
                params.put("positionSchedule", schedule);
                return params;
            }

            @Override
            protected List<PFScheduleEvent> collectEvents(Date from, Date thru, Map<String, Object> filters) {

                List<WorkScheduleDay> rawEvents = getOp().getEvents(from, thru, filters);
                List<PFScheduleEvent> events = new ArrayList<>();

                for (WorkScheduleDay rawEvent : rawEvents) {

//                    PositionSchedule schedule = positionOp.getInherentPositionSchedule(positionTable.getSelection(), getCurrentDate());
                    PositionSchedule schedule = positionOp.getPositionScheduleByDate(positionTable.getSelection(), getCurrentDate());
                    PositionScheduleDayException positionScheduleDayException = getExceptionOp().getScheduleException(rawEvent, schedule);
                    if (positionScheduleDayException != null) {
                        PFScheduleEvent positionScheduleExceptionEvent = new PFScheduleEvent(positionScheduleDayException);
                        positionScheduleExceptionEvent.setStyleClass("schedule-active");
                        events.add(positionScheduleExceptionEvent);

                        WorkScheduleDayException exception = rawEvent.getException();
                        if (exception != null) {
                            PFScheduleEvent scheduleExceptionEvent = new PFScheduleEvent(exception);
                            scheduleExceptionEvent.setStyleClass("schedule-inactive");
                            events.add(scheduleExceptionEvent);
                        } else {
                            PFScheduleEvent scheduleEvent = new PFScheduleEvent(rawEvent);
                            scheduleEvent.setStyleClass("schedule-inactive");
                            events.add(scheduleEvent);
                        }

                    } else {
                        WorkScheduleDayException exception = rawEvent.getException();
                        if (exception != null) {
                            PFScheduleEvent scheduleExceptionEvent = new PFScheduleEvent(exception);
                            scheduleExceptionEvent.setStyleClass("schedule-active");
                            events.add(scheduleExceptionEvent);
                        } else {
                            PFScheduleEvent scheduleEvent = new PFScheduleEvent(rawEvent);
                            scheduleEvent.setStyleClass("schedule-active");
                            events.add(scheduleEvent);
                        }
                    }

                }

                return events;
            }

            @Override
            public void raiseExceptionEditDialog() {
                super.raiseExceptionEditDialog();
            }

            @Override
            public void raiseEditDialog() {

            }

            @Override
            protected void raiseCreateDialog(Date workday) {

            }

            @Override
            protected boolean hasException(WorkScheduleDay scheduleDay) {
                PositionSchedule schedule = positionOp.getPositionScheduleByDate(positionTable.getSelection(), getCurrentDate());

                PositionScheduleDayException exception = getExceptionOp().getScheduleException(scheduleDay, schedule);

                return exception != null;
            }

        };

    }

    @Override
    protected void initLayout() {
        setExplorer("Organization", "15%");
        setEditor("Position", "85%");
    }

}
