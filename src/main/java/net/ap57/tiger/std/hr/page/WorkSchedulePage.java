/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.page;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.UserSession;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.std.hr.dao.WorkScheduleDayFacade;
import net.ap57.tiger.std.hr.dao.WorkScheduleFacade;
//import net.ap57.tiger.std.hr.dao.WorkUnitScheduleFacade;
import net.ap57.tiger.std.hr.entity.WorkSchedule;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayException;
import net.ap57.tiger.std.hr.service.ScheduleService;
import net.ap57.tiger.std.hr.view.WorkScheduleTemplateTable;
import net.ap57.tiger.std.hr.view.WorkdaySchedule;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;
//import net.ap57.tiger.std.hr.entity.WorkUnitSchedule;
//import net.ap57.tiger.std.hr.entity.WorkUnitScheduleVarian;
/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Named(value = "workSchedulePage")
@RequestScoped
@PageView(rel = "WorkSchedule")
public class WorkSchedulePage extends Page {

    @Inject
    private WorkScheduleFacade scheduleTemplateFacade;

    @Inject
    private WorkScheduleDayFacade scheduleDayTemplateFacade;

    @View
    private WorkScheduleTemplateTable scheduleTemplateTable;

    @Inject
    private WorkScheduleDayFacade workScheduleDayOp;

    @Inject
    private ScheduleService scheduleDayExceptionOp;

    @Inject
    private UserSession session;

    @View
    private WorkdaySchedule workSchedule;

    @Override
    protected void initComponents() {

        scheduleTemplateTable = new WorkScheduleTemplateTable() {

            @Override
            protected WorkScheduleFacade getOp() {
                return scheduleTemplateFacade;
            }

            @Override
            public WorkScheduleDayFacade getDayTemplateFacade() {
                return scheduleDayTemplateFacade;
            }

        };

        workSchedule = new WorkdaySchedule() {

            @Override
            public WorkScheduleDayFacade getOp() {
                return workScheduleDayOp;
            }

            @Override
            protected UserSession getSession() {
                return session;
            }

            @Override
            protected WorkSchedule getScheduleContext() {
                return (WorkSchedule) scheduleTemplateTable.getSelection();
            }

            @Override
            protected Class getExceptionClass() {
                return WorkScheduleDayException.class;
            }

            @Override
            protected ScheduleService getExceptionOp() {
                return scheduleDayExceptionOp;
            }

            @Override
            protected boolean hasException(WorkScheduleDay scheduleDay) {
                return scheduleDay.getException() != null;
            }

        };
    }

    @Override
    protected void initLayout() {
        setExplorer("Work Schedule", "15%");
        setEditor("Timetable", "70%");
        setProperties("Properties", "15%");
    }
}
