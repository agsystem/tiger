/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import net.ap57.tiger.base.api.HierarchicalEntity;
import net.ap57.tiger.base.entity.OrganizationRole;
import net.ap57.tiger.std.accounting.entity.AccountingPeriod;
import net.ap57.tiger.std.accounting.entity.OrganizationAccount;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
//public class InternalOrganization extends HierarchicalOrganizationRole<InternalOrganization, InternalOrganization> implements Serializable {
public class InternalOrganization extends OrganizationRole implements HierarchicalEntity<InternalOrganization>, Serializable {

    private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy = "internalOrganization")
    private List<Employment> employments;
    
    @ManyToOne
    private InternalOrganization parent;
    
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<InternalOrganization> children;
    
    @OneToMany(mappedBy = "hiringOrganization", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Position> positions;

    @OneToMany(mappedBy = "internalOrganization", cascade = CascadeType.ALL)
    private List<AccountingPeriod> accountingPeriods;

    @OneToMany(mappedBy = "internalOrganization", cascade = CascadeType.ALL)
    private List<OrganizationAccount> organizationAccounts;
    
    public List<Position> getPositions() {
        return positions;
    }
    
    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
    
    @Override
    public void setParent(InternalOrganization parent) {
        this.parent = parent;
    }

    @Override
    public InternalOrganization getParent() {
        return parent;
    }

    @Override
    public void setChildren(List<InternalOrganization> children) {
        this.children = children;
    }

    @Override
    public List<InternalOrganization> getChildren() {
        return children;
    }

    public List<Employment> getEmployments() {
        return employments;
    }

    public void setEmployments(List<Employment> employments) {
        this.employments = employments;
    }

    @Override
    public List getRelationshipsEstablished() {
        return getEmployments();
    }

    @Override
    public void setRelationshipsEstablished(List relationshipsEstablished) {
        setEmployments(relationshipsEstablished);
    }

    @Override
    public List getRelationshipParticipations() {
        return null;
    }

    @Override
    public void setRelationshipParticipations(List relationshipParticipations) {
    }

    public List<AccountingPeriod> getAccountingPeriods() {
        return accountingPeriods;
    }

    public void setAccountingPeriods(List<AccountingPeriod> accountingPeriods) {
        this.accountingPeriods = accountingPeriods;
    }

    public List<OrganizationAccount> getOrganizationAccounts() {
        return organizationAccounts;
    }

    public void setOrganizationAccounts(List<OrganizationAccount> organizationAccounts) {
        this.organizationAccounts = organizationAccounts;
    }

    public String getCanonicalName() {

        String cName = getOrganization().getName();

//      InternalOrganization parent = getParent();
        if (parent != null) {
            cName = parent.getCanonicalName() + "." + cName;
        }

        return cName;
    }

}
