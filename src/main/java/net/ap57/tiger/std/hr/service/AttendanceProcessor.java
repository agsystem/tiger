/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import net.ap57.tiger.std.hr.dao.AttendanceDataFacade;
import net.ap57.tiger.std.hr.dao.PresenceDataFacade;
import net.ap57.tiger.std.hr.entity.AttendanceData;
import net.ap57.tiger.std.hr.entity.EffectiveScheduleDay;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.PresenceData;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class AttendanceProcessor {

    @Inject
    private AttendanceDataFacade attendanceDataFacade;

    @Inject
    private ScheduleService scheduleService;

    @Inject
    private EmployeeService employeeService;
    
    @Inject
    private PresenceDataFacade presenceDataFacade;

    public void calculateAttendance(Employee employee, Date fromWorkday, Date thruWorkday, int thFromDate, int thThruDate) {

        List<EffectiveScheduleDay> scheduleDays = scheduleService.getEffectiveSchedule(employee, fromWorkday, thruWorkday);

        for (EffectiveScheduleDay scheduleDay : scheduleDays) {

            Date workday = scheduleDay.getWorkday();
            Date inTime = null;
            Date outTime = null;

            Calendar loFrom = Calendar.getInstance();
            loFrom.setTime(scheduleDay.getFromDate());
            loFrom.add(Calendar.HOUR_OF_DAY, -thFromDate);

            Calendar hiFrom = Calendar.getInstance();
            hiFrom.setTime(scheduleDay.getFromDate());
            hiFrom.add(Calendar.HOUR_OF_DAY, thFromDate);

            List<AttendanceData> fromDateData = calculateAttendance(employee, loFrom.getTime(), hiFrom.getTime());
            if (!fromDateData.isEmpty()) {
                inTime = fromDateData.get(0).getDatetime();
            }

            Calendar loThru = Calendar.getInstance();
            loThru.setTime(scheduleDay.getThruDate());
            loThru.add(Calendar.HOUR_OF_DAY, -thThruDate);

            Calendar hiThru = Calendar.getInstance();
            hiThru.setTime(scheduleDay.getThruDate());
            hiThru.add(Calendar.HOUR_OF_DAY, thThruDate);

            List<AttendanceData> thruDateData = calculateAttendance(employee, loThru.getTime(), hiThru.getTime());
            if (!thruDateData.isEmpty()) {
                outTime = thruDateData.get(0).getDatetime();
            }
            
            PresenceData presenceData = new PresenceData();
            presenceData.setWorkday(workday);
            presenceData.setInTime(inTime);
            presenceData.setOutTime(outTime);
            presenceData.setEmployee(employee);
            
            presenceDataFacade.create(presenceData);
            
//            employee.getPresences().add(presenceData);

        }
    }

    public List<AttendanceData> calculateAttendance(Employee employee, Date loBound, Date hiBound) {
//        return attendanceDataFacade.findLogWBound(employee.getId(), loBound, hiBound);
          return null;
    }

}
