/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.PresenceData;
import net.ap57.tiger.std.hr.entity.PresenceDataId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class PresenceDataFacade extends AbstractFacade<PresenceData> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PresenceDataFacade() {
        super(PresenceData.class);
    }

    @Override
    public PresenceData createTransient(Map<String, Object> params) throws EJBException {
        PresenceData presenceData = new PresenceData();
        
        return presenceData;
    }

    @Override
    public Object getId(PresenceData entity) {
        PresenceDataId id = new PresenceDataId();
        id.setEmployee(entity.getEmployee().getId());
        id.setWorkday(entity.getWorkday());
        
        return id;
    }

    public List<PresenceData> viewEmployeeAttendance(Employee employee, Date from, Date thru) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<PresenceData> cq = cb.createQuery(PresenceData.class);
        Root<PresenceData> attendance = cq.from(PresenceData.class);
        
        cq.select(attendance).where(new Predicate[]{
            cb.equal(attendance.get("employee").get("id"), employee.getId()),
            cb.greaterThanOrEqualTo(attendance.get("workday"), from),
            cb.lessThanOrEqualTo(attendance.get("workday"), thru)
        });
        
        TypedQuery<PresenceData> q = getEntityManager().createQuery(cq);
        
        return q.getResultList();

    }
    
}
