/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkSchedule;

/**
 *
 * @author mbahbejo
 */
@Stateless
public class WorkScheduleFacade extends AbstractFacade<WorkSchedule> {

    @Inject
    private EntityManager em;

    @Inject
    private WorkScheduleDayFacade workScheduleDayTemplateFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WorkScheduleFacade() {
        super(WorkSchedule.class);
    }

    @Override
    public WorkSchedule createTransient(Map<String, Object> params) throws EJBException {

        WorkSchedule template = new WorkSchedule();

        List<WorkScheduleDay> templates = new ArrayList<>();
        template.setTemplates(templates);

//        Calendar calCurr = Calendar.getInstance(); // current date, Sunday = 1.
//
//        int day = calCurr.get(Calendar.DAY_OF_WEEK) - 1; // current day of week number, Sunday = 0.
//
//        calCurr.add(Calendar.DATE, -day); // reset to the first day of week.
//
//        for (int i = 0; i < 6; i++) {
//            for (int j = 0; j < 3; j++) {
//                WorkScheduleDayTemplate dayTemplate = new WorkScheduleDayTemplate();
//                
//                dayTemplate.setFromDate(calCurr.getTime());
//                dayTemplate.setThruDate(calCurr.getTime());
//                dayTemplate.setTemplate(template);
//
//                templates.add(dayTemplate);
//
//                calCurr.add(Calendar.DATE, 1);
//            }
//        }
        return template;
    }

    @Override
    public Object getId(WorkSchedule entity) {
        return entity.getId();
    }

    private Date getLastScheduledDate(WorkSchedule workSchedule) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Date> cq = cb.createQuery(Date.class);
        Root<WorkScheduleDay> workScheduleDay = cq.from(WorkScheduleDay.class);

        cq.select(cb.greatest(workScheduleDay.<Date>get("workday"))).where(new Predicate[]{
            cb.equal(workScheduleDay.get("template").get("id"), workSchedule.getId())
        });

        TypedQuery<Date> query = getEntityManager().createQuery(cq);

        try {
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    public void generateSchedule(WorkSchedule selection, int len) {

        Date lastScheduledDate = getLastScheduledDate(selection);

        if (lastScheduledDate == null) {
            return;
        }

        int datePeriod = selection.getDatePeriod();
        int timePeriod = selection.getTimePeriod();
        int workhour = selection.getWorkhour();
        
        Calendar calNextRef = Calendar.getInstance();
        calNextRef.setTime(lastScheduledDate);
        calNextRef.add(Calendar.DATE, 1 - datePeriod);
        
        Calendar calNextSched = Calendar.getInstance();
        calNextSched.setTime(lastScheduledDate);
        calNextSched.add(Calendar.DATE, 1);

        for (int i = 0; i < len; i++) {

            // Reference schedule
            WorkScheduleDay refSched = workScheduleDayTemplateFacade.getSpecificSchedule(selection, calNextRef.getTime());

            if (refSched != null) {

                // New schedule
                WorkScheduleDay schedule = new WorkScheduleDay();
                schedule.setTemplate(selection);
                schedule.setWorkday(calNextSched.getTime());

                Calendar schedFrom = Calendar.getInstance();
                schedFrom.setTime(new Date(refSched.getFromDate().getTime() + (timePeriod * 60 * 1000)));
                Calendar refFrom = Calendar.getInstance();
                refFrom.setTime(refSched.getFromDate());
                schedFrom.set(Calendar.DATE, refFrom.get(Calendar.DATE));
                schedFrom.set(Calendar.MONTH, refFrom.get(Calendar.MONTH));
                schedFrom.set(Calendar.YEAR, refFrom.get(Calendar.YEAR));
                schedule.setFromDate(new Date(schedFrom.getTimeInMillis() + (datePeriod * 24 * 60 * 60 * 1000)));

                schedule.setThruDate(new Date(schedule.getFromDate().getTime() + (workhour * 60 * 1000)));

                selection.getTemplates().add(schedule);

                edit(selection);

                getEntityManager().flush();
                
            }

            calNextSched.add(Calendar.DATE, 1);
            calNextRef.add(Calendar.DATE, 1);

        }

    }

    public List<WorkScheduleDay> getScheduledDays(WorkSchedule schedule, Date fromDate, Date thruDate) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkScheduleDay> cq = cb.createQuery(WorkScheduleDay.class);

        Root<WorkSchedule> workSchedule = cq.from(WorkSchedule.class);
        Join<WorkSchedule, WorkScheduleDay> workScheduleDay = workSchedule.join("templates");
        cq.select(workScheduleDay).where(new Predicate[] {
            cb.equal(workSchedule.get("id"), schedule.getId()),
            cb.lessThanOrEqualTo(workScheduleDay.get("workday"), fromDate),
            cb.greaterThanOrEqualTo(workScheduleDay.get("workday"), thruDate)
        });

        TypedQuery<WorkScheduleDay> q = getEntityManager().createQuery(cq);

        return q.getResultList();

    }

}
