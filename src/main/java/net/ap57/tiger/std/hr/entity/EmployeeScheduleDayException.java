/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Entity
@IdClass(EmployeeScheduleDayExceptionId.class)
public class EmployeeScheduleDayException implements ScheduleDayException,  Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="POSITIONFULFILLMENT_EMPLOYEE_ID", referencedColumnName="EMPLOYEE_ID"),
        @JoinColumn(name="POSITIONFULFILLMENT_POSITION_ID", referencedColumnName="POSITION_ID"),
        @JoinColumn(name="POSITIONFULFILLMENT_FROMDATE", referencedColumnName="FROMDATE")
    })
    private PositionFulfillment positionFulfillment;
    
    @Id
    @JoinColumns({
        @JoinColumn(name="WORKSCHEDULEDAY_WORKSCHEDULE_ID", referencedColumnName="TEMPLATE_ID"),
        @JoinColumn(name="WORKSCHEDULEDAY_WORKDAY", referencedColumnName="WORKDAY")
    })
    @ManyToOne
    private WorkScheduleDay scheduleDay;
    
    @Temporal(TemporalType.DATE)
    private Date fromDate;
    
    @Temporal(TemporalType.DATE)
    private Date thruDate;
    
    public PositionFulfillment getPositionFulfillment() {
        return positionFulfillment;
    }

    public void setPositionFulfillment(PositionFulfillment positionFulfillment) {
        this.positionFulfillment = positionFulfillment;
    }

    @Override
    public WorkScheduleDay getScheduleDay() {
        return scheduleDay;
    }

    @Override
    public void setScheduleDay(WorkScheduleDay scheduleDay) {
        this.scheduleDay = scheduleDay;
    }

    @Override
    public Date getFromDate() {
        return fromDate;
    }

    @Override
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public Date getThruDate() {
        return thruDate;
    }

    @Override
    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }

    @Override
    public String getName() {       
        SimpleDateFormat sdf = new SimpleDateFormat("MM/d HH:mm");
        if (thruDate == null) {
            return "";
        }
        return sdf.format(thruDate);
    }

    @Override
    public void setName(String name) {
        
    }
    
}
