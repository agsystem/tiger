/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeCode;
import net.ap57.tiger.std.hr.entity.EmployeeCodeId;
import net.ap57.tiger.std.hr.entity.EmployeeCodeType;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class EmployeeCodeFacade extends AbstractFacade<EmployeeCode> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeCodeFacade() {
        super(EmployeeCode.class);
    }

    @Override
    public EmployeeCode createTransient(Map<String, Object> params) throws EJBException {
        EmployeeCode employeeCode = new EmployeeCode();
        employeeCode.setEmployee((Employee) params.get("employee"));
        return employeeCode;
    }

    @Override
    public Object getId(EmployeeCode entity) {

        EmployeeCodeId id = new EmployeeCodeId();
        EmployeeCodeType codeType = entity.getCodeType();

        if (codeType != null) {
            id.setCodeType(entity.getCodeType().getId());
            id.setEmployee(entity.getEmployee().getId());
        }

        return id;
    }

    @Override
    protected CriteriaQuery constructCountQuery(Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Employee> employee = cq.from(Employee.class);
        Join<Employee, EmployeeCode> employeeCode = employee.join("employeeCodes");

        cq.select(cb.count(employeeCode));

        if (filters != null && !filters.isEmpty()) {
            List<Predicate> predicates = new ArrayList<>();
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "employee":
                        Employee emp = (Employee) filterValue;
                        predicates.add(cb.equal(employeeCode.get("employee").get("id"), emp.getId()));
                    default:
                        break;
                }
            }

            cq.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        return cq;

    }

    @Override
    protected CriteriaQuery constructFindQuery(Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<EmployeeCode> cq = cb.createQuery(EmployeeCode.class);
        Root<Employee> employee = cq.from(Employee.class);
        Join<Employee, EmployeeCode> employeeCode = employee.join("employeeCodes");

        cq.select(employeeCode);

        if (filters != null && !filters.isEmpty()) {
            List<Predicate> predicates = new ArrayList<>();
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "employee":
                        Employee emp = (Employee) filterValue;
                        predicates.add(cb.equal(employeeCode.get("employee").get("id"), emp.getId()));
                    default:
                        break;
                }
            }

            cq.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        return cq;
    }

    @Override
    public List<EmployeeCode> findAll(int startPosition, int maxResult, Map<String, Object> filters) {
        List<EmployeeCode> result = super.findAll(startPosition, maxResult, filters);
        System.out.println("PAINALL = " + result.size());
        for (EmployeeCode code : result) {
            System.out.println("KODE = " + code);
        }
        return result;

    }

//    @Override
//    public void create(EmployeeCode entity) throws EJBException {
//        Employee emp = entity.getEmployee();
//        emp.getEmployeeCodes().add(entity);
//        getEntityManager().merge(emp);
//    }

}
