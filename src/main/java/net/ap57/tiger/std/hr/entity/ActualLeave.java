/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@IdClass(LeaveRequestId.class)
public class ActualLeave extends StringKeyedEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @OneToOne
  @JoinColumns({
    @JoinColumn(name="REQUEST_POSITIONFULFILLMENT_POSITION_ID", referencedColumnName="POSITIONFULFILLMENT_POSITION_ID"),
    @JoinColumn(name="REQUEST_POSITIONFULFILLMENT_EMPLOYEE_ID", referencedColumnName="POSITIONFULFILLMENT_EMPLOYEE_ID"),
    @JoinColumn(name="REQUEST_POSITIONFULFILLMENT_FROMDATE", referencedColumnName="POSITIONFULFILLMENT_FROMDATE"),
    @JoinColumn(name="REQUEST_FROMDATE", referencedColumnName="FROMDATE")
  })
  private LeaveRequest request;

  @Temporal(TemporalType.DATE)
  private Date actualFromDate;
  
  @Temporal(TemporalType.DATE)
  private Date actualThruDate;

  public Date getActualFromDate() {
    return actualFromDate;
  }

  public void setActualFromDate(Date actualFromDate) {
    this.actualFromDate = actualFromDate;
  }

  public LeaveRequest getRequest() {
    return request;
  }

  public void setRequest(LeaveRequest request) {
    this.request = request;
  }

  public Date getActualThruDate() {
    return actualThruDate;
  }

  public void setActualThruDate(Date thruDate) {
    this.actualThruDate = thruDate;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 83 * hash + Objects.hashCode(this.request);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ActualLeave other = (ActualLeave) obj;
    if (!Objects.equals(this.request, other.request)) {
      return false;
    }
    return true;
  }
  
}
