/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
public class Position extends StringKeyedEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    private InternalOrganization hiringOrganization;

    @OneToMany(mappedBy = "position", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PositionWorkUnit> workUnits;

    @ManyToOne
    private PositionType type;

    @Enumerated(EnumType.STRING)
    private PositionStatus status;

    @Temporal(TemporalType.DATE)
    private Date estimatedFromDate;

    @Temporal(TemporalType.DATE)
    private Date estimatedThruDate;

    @Temporal(TemporalType.DATE)
    private Date actualFromDate;

    @Temporal(TemporalType.DATE)
    private Date actualThruDate;

    private boolean fullTime;

    private boolean permanent;

    private boolean salary;

    private boolean exempt;

    private String description;

    @OneToMany(mappedBy = "position", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PositionFulfillment> positionFulfillments;

    @OneToMany(mappedBy = "reportingToPosition", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PositionReportingStructure> managerOf;

    @OneToMany(mappedBy = "reporterPosition")
    private List<PositionReportingStructure> reportTo;

    private int required;

    @Temporal(TemporalType.DATE)
    private Date created;

    @OneToMany(mappedBy = "position", cascade=CascadeType.ALL)
    private List<PositionSchedule> positionSchedules;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PositionType getType() {
        return type;
    }

    public void setType(PositionType type) {
        this.type = type;
    }

    public PositionStatus getStatus() {
        return status;
    }

    public void setStatus(PositionStatus status) {
        this.status = status;
    }

    public Date getEstimatedFromDate() {
        return estimatedFromDate;
    }

    public void setEstimatedFromDate(Date estimatedFromDate) {
        this.estimatedFromDate = estimatedFromDate;
    }

    public Date getEstimatedThruDate() {
        return estimatedThruDate;
    }

    public void setEstimatedThruDate(Date estimatedThruDate) {
        this.estimatedThruDate = estimatedThruDate;
    }

    public Date getActualFromDate() {
        return actualFromDate;
    }

    public void setActualFromDate(Date actualFromDate) {
        this.actualFromDate = actualFromDate;
    }

    public Date getActualThruDate() {
        return actualThruDate;
    }

    public void setActualThruDate(Date actualThruDate) {
        this.actualThruDate = actualThruDate;
    }

    public InternalOrganization getHiringOrganization() {
        return hiringOrganization;
    }

    public void setHiringOrganization(InternalOrganization hiringOrganization) {
        this.hiringOrganization = hiringOrganization;
    }

    public boolean isFullTime() {
        return fullTime;
    }

    public void setFullTime(boolean fullTime) {
        this.fullTime = fullTime;
    }

    public boolean isPermanent() {
        return permanent;
    }

    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    public boolean isSalary() {
        return salary;
    }

    public void setSalary(boolean salary) {
        this.salary = salary;
    }

    public boolean isExempt() {
        return exempt;
    }

    public void setExempt(boolean exempt) {
        this.exempt = exempt;
    }

    public List<PositionFulfillment> getPositionFulfillments() {
        return positionFulfillments;
    }

    public void setPositionFulfillments(List<PositionFulfillment> positionFulfillments) {
        this.positionFulfillments = positionFulfillments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PositionWorkUnit> getWorkUnits() {
        return workUnits;
    }

    public void setWorkUnits(List<PositionWorkUnit> workUnits) {
        this.workUnits = workUnits;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public List<PositionReportingStructure> getManagerOf() {
        return managerOf;
    }

    public void setManagerOf(List<PositionReportingStructure> managerOf) {
        this.managerOf = managerOf;
    }

    public List<PositionReportingStructure> getReportTo() {
        return reportTo;
    }

    public void setReportTo(List<PositionReportingStructure> reportTo) {
        this.reportTo = reportTo;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<PositionSchedule> getPositionSchedules() {
        return positionSchedules;
    }

    public void setPositionSchedules(List<PositionSchedule> positionSchedules) {
        this.positionSchedules = positionSchedules;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @PrePersist
    @Override
    public void prePersist() {
        String key = name + hiringOrganization.getId().toString();
        for (PositionWorkUnit workUnit : workUnits) {
            key += workUnit.getWorkUnit().getId();
        }
        setId(UUID.nameUUIDFromBytes(key.getBytes()).toString());
        setStrKey(id);
    }

    @Override
    public String toString() {
        return name + " " + getWorkUnits();
    }

}
