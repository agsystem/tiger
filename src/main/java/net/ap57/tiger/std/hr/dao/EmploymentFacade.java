/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import net.ap57.tiger.base.dao.PartyRelationshipFacade;
import net.ap57.tiger.base.dao.PartyRelationshipTypeFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.Employment;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class EmploymentFacade extends PartyRelationshipFacade<Employment> {

    @Inject
    private EntityManager em;

    @Inject
    private EmploymentNumFacade numFacade;

    @Inject
    private PartyRelationshipTypeFacade relTypeFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmploymentFacade() {
        super(Employment.class);
    }

    @Override
    public Object getId(Employment entity) {
        return entity.getId();
    }

    public String generateRefNum() {
        String refNum = Long.toString(numFacade.getNum());
        return refNum;
    }

    @Override
    protected PartyRelationshipTypeFacade getRelationshipTypeFacade() {
        return relTypeFacade;
    }

    @Override
    protected String getRelationshipTypeName() {
        return "Employment";
    }

    @Override
    public Employment getObj(String strKey) {
        return findSingleByAttribute("id", strKey);
    }

    @Override
    public String getString(Employment entity) {
        return entity.getId();
    }

    public Employment getLatestEmployment(Employee e) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Employment> cq = cb.createQuery(Employment.class);

        Root<Employment> employment = cq.from(Employment.class);

        Subquery<Date> sq = cq.subquery(Date.class);
        Root<Employment> sqEmployment = sq.from(Employment.class);
        
        sq.select(cb.greatest(sqEmployment.<Date>get("fromDate"))).where(cb.equal(sqEmployment.get("employee").get("id"), e.getId()));

        cq.select(employment).where(new Predicate[]{
            cb.equal(employment.get("employee").get("id"), e.getId()),
            cb.equal(employment.get("fromDate"), sq)
        });

        TypedQuery<Employment> q = getEntityManager().createQuery(cq);

        return q.getSingleResult();
    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<Employment> root) {
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        switch (filterName) {
            case "employee":
                Employee employee = (Employee) filterValue;
                predicates.add(cb.equal(root.get("employee"), employee));
                break;
            default:
                break;
        }
        return predicates;
    }

    @Override
    protected void addCriteria(Root<Employment> root, CriteriaQuery<Employment> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("fromDate")));
    }

}
