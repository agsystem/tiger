/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.std.hr.page;

import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import net.ap57.tiger.base.ui.DataList;
import net.ap57.tiger.base.ui.DataTable;
import net.ap57.tiger.base.ui.EditableDataList;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.std.hr.dao.ActualLeaveFacade;
import net.ap57.tiger.std.hr.dao.LeavePeriodFacade;
import net.ap57.tiger.std.hr.dao.LeavePeriodTypeFacade;
import net.ap57.tiger.std.hr.dao.LeaveRequestFacade;
import net.ap57.tiger.std.hr.dao.LeaveTypeFacade;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;
import net.ap57.tiger.std.hr.entity.LeavePeriod;
import net.ap57.tiger.std.hr.entity.LeavePeriodType;
import net.ap57.tiger.std.hr.entity.LeaveType;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.view.ActualLeaveTable;
import net.ap57.tiger.std.hr.view.EmployeeTable;
import net.ap57.tiger.std.hr.view.LeaveRequestTable;
import org.primefaces.event.SelectEvent;
import net.ap57.tiger.base.ui.annotation.View;
import net.ap57.tiger.base.ui.annotation.PageView;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "leaveManagementPage")
@RequestScoped
@PageView(rel="leaveManagement")
public class LeaveManagementPage extends Page {

  @Inject
  private LeaveTypeFacade leaveTypeFacade;
  
  @View
  private DataTable leaveTypeTable;
  
  @View
  private DataList leaveTypeList;
  
  @Inject
  private LeavePeriodFacade leavePeriodFacade;
  
  @View
  private DataTable leavePeriodTable;
  
  @Inject
  private LeaveRequestFacade leaveRequestFacade;
  
  @View
  private LeaveRequestTable leaveRequestTable;
  
  @Inject
  private ActualLeaveFacade actualLeaveFacade;
  
  @View
  private ActualLeaveTable actualLeaveTable;

  @Inject
  private PositionFulfillmentFacade employeeOp;

  @View
  private EmployeeTable employeeTable;
  
  @Inject
  private LeavePeriodTypeFacade leavePeriodTypeFacade;
 
  @View
  private DataTable leavePeriodTypeTable;
  
  @View
  private DataList leavePeriodTypeList;

  @Override
  protected void initLayout() {
    setEditor("Current Leave", "100%");
  }

  @Override
  protected void initComponents() {
    
    leaveTypeTable = new PFLazyDataTable<LeaveTypeFacade, LeaveType>() {

      @Override
      protected LeaveTypeFacade getOp() {
        return leaveTypeFacade;
      }

      @Override
      public void create(LeaveType e) {
        super.create(e);
        setSelection(null);
      }

      @Override
      public LeaveType getSelection() {
        LeaveType type = super.getSelection();
        if (type == null) {
          super.prepareTransient(null);
        }
        return super.getSelection();
      }
      
    };
    
    leaveTypeList = new EditableDataList<LeaveTypeFacade, LeaveType>() {


      @Override
      protected Object strToObj(String value) {
        return leaveTypeFacade.findSingleByAttribute("name", value);
      }

      @Override
      protected LeaveType newPlaceholder() {
        return new LeaveType(true);
      }

      @Override
      public LeaveTypeFacade getOp() {
        return leaveTypeFacade;
      }
      
    };
    
    leavePeriodTable = new PFLazyDataTable<LeavePeriodFacade, LeavePeriod>() {

      @Override
      protected LeavePeriodFacade getOp() {
        return leavePeriodFacade;
      }
      
    };
    
    leaveRequestTable = new LeaveRequestTable() {

      @Override
      protected LeaveRequestFacade getOp() {
        return leaveRequestFacade;
      }
      
    };
    
    actualLeaveTable = new ActualLeaveTable() {

      @Override
      protected ActualLeaveFacade getOp() {
        return actualLeaveFacade;
      }
      
    };

    employeeTable = new EmployeeTable() {

      @Override
      protected PositionFulfillmentFacade getOp() {
        return employeeOp;
      }

      @Override
      public void onPositionFulfillmentSelected(SelectEvent evt) {
        PositionFulfillment pf = (PositionFulfillment) evt.getObject();
        leaveRequestTable.getSelection().setPositionFulfillment(pf);
      }

        @Override
        protected void addStaticFilter(Map<String, Object> filters) {
            filters.put("all", true);
            filters.put("employee.onboarding", false);
            filters.put("employee.archived", false);
        }

    };
    
    leavePeriodTypeTable = new PFLazyDataTable<LeavePeriodTypeFacade, LeavePeriodType>() {

      @Override
      protected LeavePeriodTypeFacade getOp() {
        return leavePeriodTypeFacade;
      }

      @Override
      public void create(LeavePeriodType e) {
        super.create(e);
        setSelection(null);
      }

      @Override
      public LeavePeriodType getSelection() {
        LeavePeriodType type = super.getSelection();
        if (type == null) {
          super.prepareTransient(null);
        }
        return super.getSelection();
      }
    };
    
    leavePeriodTypeList = new EditableDataList<LeavePeriodTypeFacade, LeavePeriodType>() {

      @Override
      protected LeavePeriodType newPlaceholder() {
        return new LeavePeriodType(true);
      }

      @Override
      public LeavePeriodTypeFacade getOp() {
        return leavePeriodTypeFacade;
      }

      @Override
      protected Object strToObj(String value) {
        return leavePeriodTypeFacade.findSingleByAttribute("name", value);
      }
      
    };
  }
  
}
