/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.dao;

import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayException;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayExceptionId;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayId;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class WorkScheduleDayExceptionFacade extends AbstractFacade<WorkScheduleDayException> {

    @Inject
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WorkScheduleDayExceptionFacade() {
        super(WorkScheduleDayException.class);
    }

    @Override
    public WorkScheduleDayException createTransient(Map<String, Object> params) throws EJBException {
        
        WorkScheduleDay scheduleDay = (WorkScheduleDay) params.get("scheduleDay");
        if(scheduleDay == null) return null;
        
        WorkScheduleDayException scheduleDayException = new WorkScheduleDayException();
        scheduleDayException.setScheduleDay(scheduleDay);
        scheduleDayException.setFromDate(scheduleDay.getFromDate());
        scheduleDayException.setThruDate(scheduleDay.getThruDate());
        
        return scheduleDayException;
    }

    @Override
    public Object getId(WorkScheduleDayException entity) {
        
        WorkScheduleDay scheduleDay = entity.getScheduleDay();
        
        WorkScheduleDayId scheduleDayId = new WorkScheduleDayId();
        scheduleDayId.setTemplate(scheduleDay.getTemplate().getId());
        scheduleDayId.setWorkday(scheduleDay.getWorkday());
        
        WorkScheduleDayExceptionId id = new WorkScheduleDayExceptionId();
        id.setScheduleDay(scheduleDayId);
        
        return id;
    }

    @Override
    public void create(WorkScheduleDayException entity) throws EJBException {
        WorkScheduleDay scheduleDay = entity.getScheduleDay();
        scheduleDay.setException(entity);
        getEntityManager().merge(scheduleDay);
    }
    
}
