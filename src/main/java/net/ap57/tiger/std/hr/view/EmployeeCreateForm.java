/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.std.hr.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.entity.ContactMechanism;
import net.ap57.tiger.base.entity.Gender;
import net.ap57.tiger.base.entity.IdentityType;
import net.ap57.tiger.base.entity.MaritalStatus;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.base.entity.PartyPostalAddress;
import net.ap57.tiger.base.entity.PostalAddress;
import net.ap57.tiger.base.ui.CaptureClient;
import net.ap57.tiger.base.ui.PageForm;
import net.ap57.tiger.base.ui.pf.PFDataTree;
import net.ap57.tiger.std.hr.entity.EmploymentAgreementType;
import net.ap57.tiger.std.hr.entity.InternalOrganization;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.WorkUnit;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class EmployeeCreateForm extends PageForm implements CaptureClient {

  @Override
  public void init() {
    List<PartyPostalAddress> addresses = new ArrayList<>();
    addresses.add(new PartyPostalAddress());
    setPartyPostalAddresses(addresses);
    List<PartyContactMechanism> contacts = new ArrayList<>();
    contacts.add(new PartyContactMechanism());
    setPartyContactMechanisms(contacts);
    
  }
  
  private List<PartyPostalAddress> initAddress() {
    
    List<PartyPostalAddress> addresses = new ArrayList<>();
    PartyPostalAddress address = new PartyPostalAddress();
    address.setPostalAddress(new PostalAddress());
    addresses.add(address);
    return addresses;
    
  }
  
  private List<PartyContactMechanism> initContacts() {
    
    List<PartyContactMechanism> contacts = new ArrayList<>();
    PartyContactMechanism contact = new PartyContactMechanism();
    contact.setContactMechanism(new ContactMechanism());
    contacts.add(contact);
    
    return contacts;
  }

  public String getFirstName() {
    return (String) getAttribute("firstName");
  }

  public void setFirstName(String firstName) {
    setAttribute("firstName", firstName);
  }

  public String getLastName() {
    return (String) getAttribute("lastName");
  }

  public void setLastName(String lastName) {
    setAttribute("lastName", lastName);
  }

  public Gender getGender() {
    return (Gender) getAttribute("gender");
  }

  public void setGender(Gender gender) {
    setAttribute("gender", gender);
  }

  public String getBirthPlace() {
    return (String) getAttribute("birthPlace");
  }

  public void setBirthPlace(String birthPlace) {
    setAttribute("birthPlace", birthPlace);
  }

  public Date getBirthDate() {
    return (Date) getAttribute("birthDate");
  }

  public void setBirthDate(Date birthDate) {
    setAttribute("birthDate", birthDate);
  }

  public IdentityType getIdType() {
    return (IdentityType) getAttribute("idType");
  }

  public void setIdType(IdentityType idType) {
    setAttribute("idType", idType);
  }

  public String getIdNum() {
    return (String) getAttribute("idNum");
  }

  public void setIdNum(String idNum) {
    setAttribute("idNum", idNum);
  }

  public MaritalStatus getMaritalStatus() {
    return (MaritalStatus) getAttribute("maritalStatus");
  }

  public void setMaritalStatus(MaritalStatus maritalStatus) {
    setAttribute("maritalStatus", maritalStatus);
  }

  public List<PartyPostalAddress> getPartyPostalAddresses() {
    return (List<PartyPostalAddress>) get("partyPostalAddresses", initAddress());
  }

  public void setPartyPostalAddresses(List<PartyPostalAddress> partyPostalAddresses) {
    setAttribute("partyPostalAddresses", partyPostalAddresses);
  }

  public List<PartyContactMechanism> getPartyContactMechanisms() {
    return (List<PartyContactMechanism>) get("partyContactMechanisms", initContacts());
  }

  public void setPartyContactMechanisms(List<PartyContactMechanism> partyContactMechanisms) {
    setAttribute("partyContactMechanisms", partyContactMechanisms);
  }

  public EmploymentAgreementType getEmployeeAgreementType() {
    return (EmploymentAgreementType) getAttribute("EmploymentAgreementType");
  }

  public void setEmployeeAgreementType(EmploymentAgreementType employeeAgreementType) {
    setAttribute("employeeAgreementType", employeeAgreementType);
  }

  public Date getEmployeeAgreementFrom() {
    return (Date) getAttribute("employeeAgreementFrom");
  }

  public void setEmployeeAgreementFrom(Date fromDate) {
    setAttribute("employeeAgreementFrom", fromDate);
  }

  public Date getEmployeeAgreementThru() {
    return (Date) getAttribute("employeeAgreementThru");
  }

  public void setEmployeeAgreementThru(Date thruDate) {
    setAttribute("employeeAgreementThru", thruDate);
  }
  
  public String getEmployeeAgreementRemarks() {
    return (String) getAttribute("agreementRemarks");
  }
  
  public void setEmployeeAgreementRemarks(String remarks) {
    setAttribute("agreementRemarks", remarks);
  }

  public InternalOrganization getOrganization() {
    return (InternalOrganization) getAttribute("organization");
  }

  public void setOrganization(InternalOrganization organization) {
    setAttribute("organization", organization);
  }

  public Position getPosition() {
    return (Position) getAttribute("position");
  }

  public void setPosition(Position position) {
    setAttribute("position", position);
  }

  public WorkUnit getWorkUnit() {
    return (WorkUnit) getAttribute("workUnit");
  }

  public void setWorkUnit(WorkUnit workUnit) {
    setAttribute("workUnit", workUnit);
  }

//  public TimeSlot getShift() {
//    return (TimeSlot) get("shift");
//  }

//  public void setShift(TimeSlot shift) {
//    set("shift", shift);
//  }

  public byte[] getPhoto() {
    return (byte[]) getAttribute("photo");
  }

  public void setPhoto(byte[] photo) {
    setAttribute("photo", photo);
  }

  public void onTabChange(ActionEvent evt) {
    Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    Integer idx = Integer.parseInt(params.get("tab-idx"));
    setAttribute("tabIdx", idx);
  }

  public void onWorkUnitSelected(NodeSelectEvent evt) {
    setWorkUnit((WorkUnit) evt.getTreeNode().getData());
  }

  public void onOrganizationSelected(NodeSelectEvent evt) {
    InternalOrganization org = (InternalOrganization) evt.getTreeNode().getData();
    setOrganization(org);
    PFDataTree orgTree = getContainer().get("organizationTree");
    orgTree.setSelection(org);
  }

  public Integer getActiveTabIdx() {
    return (Integer) get("tabIdx", 0);
  }

  public void setActiveTabIdx(Integer idx) {
    setAttribute("tabIdx", idx);
  }

  @Override
  public void processRawImage(byte[] raw) {
    setPhoto(raw);
  }

  public StreamedContent getStreamed() {
    byte[] data = getPhoto();
    InputStream is = null;
    if (data != null) {
      is = new ByteArrayInputStream(data);
    } else {
      FacesContext fc = FacesContext.getCurrentInstance();
      is = fc.getExternalContext().getResourceAsStream("/resources/images/noimg.jpg");

    }
    return new DefaultStreamedContent(is);
  }

  public void addPostalAddress(ActionEvent evt) {
    getPartyPostalAddresses().add(new PartyPostalAddress());
  }

  public void delPostalAddress(PartyPostalAddress partyPostalAddress) {
    getPartyPostalAddresses().remove(partyPostalAddress);
  }

  public void addPartyContactMechanism(ActionEvent evt) {
    getPartyContactMechanisms().add(new PartyContactMechanism());
  }

  public void delPartyContactMechanism(PartyContactMechanism partyContactMechanism) {
    getPartyContactMechanisms().remove(partyContactMechanism);
  }

}
