/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.std.hr.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeStatus;
import net.ap57.tiger.std.hr.entity.Employment;
import net.ap57.tiger.std.hr.entity.Position;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionFulfillmentId;

/**
 *
 * @author mbahbejo
 */
@Stateless
public class PositionFulfillmentHistoryFacade extends AbstractFacade<PositionFulfillment> {

    @Inject
    private EntityManager em;

    @Inject
    private EmploymentFacade employmentFacade;

    @Inject
    private EmployeeFacade employeeFacade;

    public PositionFulfillmentHistoryFacade() {
        super(PositionFulfillment.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public PositionFulfillment createTransient(Map<String, Object> params) throws EJBException {

        PositionFulfillment positionFulfillment = new PositionFulfillment();

        Employee employee = null;
        Employment employment = null;

        if (params != null && params.containsKey("selectedEmployee")) {
            employee = (Employee) params.get("selectedEmployee");

            employment = employmentFacade.getLatestEmployment(employee);
            getEntityManager().detach(employment);

            Date tDef = Calendar.getInstance().getTime();

            Date tF = employment.getFromDate();
            Date tT = employment.getThruDate();

            if ((tT != null && tF.getTime() <= tDef.getTime() && tDef.getTime() <= tT.getTime()) || (tT == null && tF.getTime() <= tDef.getTime())) {
                positionFulfillment.setFromDate(tDef);
                positionFulfillment.setThruDate(employment.getThruDate());
            } else {
                throw new EJBException("No Active Employment");
            }

        } else {
            employee = employeeFacade.createTransient(params);
            employee.setStatus(EmployeeStatus.ACTIVE);
            employment = employmentFacade.createTransient(null);
            employment.setPositionFulfillments(new ArrayList<>());
            employment.setEmployee(employee);
        }

        positionFulfillment.setEmployee(employee);
        employee.getPositionFulfillments().add(positionFulfillment);

        positionFulfillment.setEmployment(employment);

        employment.getPositionFulfillments().add(positionFulfillment);
        employee.getRelationshipParticipations().add(employment);

        return positionFulfillment;

    }

    @Override
    public Object getId(PositionFulfillment entity) {

        PositionFulfillmentId id = new PositionFulfillmentId();

        Position pos = entity.getPosition();
        if (pos != null && pos.getId() != null) {
            id.setPosition(pos.getId());
        } else {
            return null;
        }

        Employee emp = entity.getEmployee();
        if (emp.getId() != null) {
            id.setEmployee(emp.getId());
        } else {
            return null;
        }

        Date fromDate = entity.getFromDate();
        if (fromDate != null) {
            id.setFromDate(fromDate);
        } else {
            return null;
        }

        return id;

    }

    @Override
    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<PositionFulfillment> root) {
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        switch (filterName) {
            case "employee":
                Employee employee = (Employee) filterValue;
                predicates.add(cb.equal(root.get("employee"), employee));
                break;
            default:
                break;
        }
        return predicates;
    }

    @Override
    protected void addCriteria(Root<PositionFulfillment> root, CriteriaQuery<PositionFulfillment> cq) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        cq.orderBy(cb.desc(root.get("fromDate")));
    }

}
