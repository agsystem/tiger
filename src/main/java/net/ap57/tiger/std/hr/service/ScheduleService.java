/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.std.hr.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.std.hr.dao.EmployeeScheduleDayExceptionFacade;
import net.ap57.tiger.std.hr.dao.PositionFacade;
import net.ap57.tiger.std.hr.dao.PositionFulfillmentFacade;
import net.ap57.tiger.std.hr.dao.PositionScheduleDayExceptionFacade;
import net.ap57.tiger.std.hr.dao.WorkScheduleDayExceptionFacade;
import net.ap57.tiger.std.hr.dao.WorkScheduleFacade;
import net.ap57.tiger.std.hr.entity.EffectiveScheduleDay;
import net.ap57.tiger.std.hr.entity.Employee;
import net.ap57.tiger.std.hr.entity.EmployeeScheduleDayException;
import net.ap57.tiger.std.hr.entity.PositionFulfillment;
import net.ap57.tiger.std.hr.entity.PositionSchedule;
import net.ap57.tiger.std.hr.entity.PositionScheduleDayException;
import net.ap57.tiger.std.hr.entity.ScheduleDayException;
import net.ap57.tiger.std.hr.entity.WorkSchedule;
import net.ap57.tiger.std.hr.entity.WorkScheduleDay;
import net.ap57.tiger.std.hr.entity.WorkScheduleDayException;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Stateless
public class ScheduleService {

    @Inject
    private EntityManager em;

    @Inject
    private WorkScheduleFacade workScheduleFacade;

    @Inject
    private WorkScheduleDayExceptionFacade workScheduleDayExceptionFacade;

    @Inject
    private PositionScheduleDayExceptionFacade positionScheduleDayExceptionFacade;

    @Inject
    private EmployeeScheduleDayExceptionFacade employeeScheduleDayExceptionFacade;

    @Inject
    private PositionFacade positionFacade;

    @Inject
    private PositionFulfillmentFacade positionFulfillmentFacade;

    private EntityManager getEntityManager() {
        return em;
    }

    public ScheduleDayException createTransient(Map<String, Object> params, Class<? extends ScheduleDayException> cls) {

        if (cls.getCanonicalName().equals(WorkScheduleDayException.class.getCanonicalName())) {
            return workScheduleDayExceptionFacade.createTransient(params);
        } else if (cls.getCanonicalName().equals(PositionScheduleDayException.class.getCanonicalName())) {
            return positionScheduleDayExceptionFacade.createTransient(params);
        } else if (cls.getCanonicalName().equals(EmployeeScheduleDayException.class.getCanonicalName())) {
            return employeeScheduleDayExceptionFacade.createTransient(params);
        } else {
            return null;
        }
    }

    public void create(ScheduleDayException exception) {

        if (exception instanceof WorkScheduleDayException) {
            workScheduleDayExceptionFacade.create((WorkScheduleDayException) exception);
        } else if (exception instanceof PositionScheduleDayException) {
            positionScheduleDayExceptionFacade.create((PositionScheduleDayException) exception);
        } else if (exception instanceof EmployeeScheduleDayException) {
            employeeScheduleDayExceptionFacade.create((EmployeeScheduleDayException) exception);
        }

    }

    public void remove(ScheduleDayException exception) {

        if (exception instanceof WorkScheduleDayException) {
            workScheduleDayExceptionFacade.remove((WorkScheduleDayException) exception);
        } else if (exception instanceof PositionScheduleDayException) {
            positionScheduleDayExceptionFacade.remove((PositionScheduleDayException) exception);
        } else if (exception instanceof EmployeeScheduleDayException) {
            employeeScheduleDayExceptionFacade.remove((EmployeeScheduleDayException) exception);
        }
    }

    public PositionScheduleDayException getScheduleException(WorkScheduleDay scheduleDay, PositionSchedule schedule) {
        return positionScheduleDayExceptionFacade.getInherentException(scheduleDay, schedule);
    }

    public EmployeeScheduleDayException getScheduleException(WorkScheduleDay scheduleDay, PositionFulfillment positionFulfillment) {
        return employeeScheduleDayExceptionFacade.getInherentException(scheduleDay, positionFulfillment);
    }

    public List<EffectiveScheduleDay> getEffectiveSchedule(Employee employee, Date fromDate, Date thruDate) {

        // Find employee position by date.
        PositionFulfillment positionFulfillment = positionFulfillmentFacade.getPositionFulfillmentByDate(employee, fromDate);

        // Find position schedule by date. It defines schedule for the employee at the date.
        PositionSchedule positionSchedule = positionFacade.getPositionScheduleByDate(positionFulfillment.getPosition(), fromDate);

        // Get scheduled days from fromDate thru thruDate inclusive.
        List<WorkScheduleDay> scheduleDays = workScheduleFacade.getScheduledDays(positionSchedule.getWorkSchedule(), fromDate, thruDate);

        List<EffectiveScheduleDay> effectiveSchedules = new ArrayList<>();

        for (WorkScheduleDay scheduleDay : scheduleDays) {

            // Is there any schedule exception on the employee level ?,
            // if 'yes' then override schedule day and continue.
            EmployeeScheduleDayException employeeScheduleDayException = ScheduleService.this.getScheduleException(scheduleDay, positionFulfillment);
            if (employeeScheduleDayException != null) {
                effectiveSchedules.add(writeLog(employeeScheduleDayException));
                continue;
            }

            // Is there any schedule exception on the position level ?,
            // if 'yes' then override schedule day and continue.
            PositionScheduleDayException positionScheduleDayException = getScheduleException(scheduleDay, positionSchedule);
            if (positionScheduleDayException != null) {
                effectiveSchedules.add(writeLog(positionScheduleDayException));
                continue;
            }

            // Is there any schedule exception ?,
            // if 'yes'then override schedule day and continue.
            WorkScheduleDayException workScheduleDayException = scheduleDay.getException();
            if (workScheduleDayException != null) {
                effectiveSchedules.add(writeLog(workScheduleDayException));
                continue;
            }

            // No exception at all.
            effectiveSchedules.add(writeLog(scheduleDay));

        }

        return effectiveSchedules;

    }

    public EffectiveScheduleDay getSchedule(Employee employee, Date workday) {

        // Find employee position fulfillment on workday.
        PositionFulfillment positionFulfillment = positionFulfillmentFacade.getPositionFulfillmentByDate(employee, workday);
//        System.out.println("POSITION FULFILLMENT: " + employee.getPerson().getName() + " -> " +  positionFulfillment);

        // Find employee position schedule on workday.
        PositionSchedule positionSchedule = positionFacade.getPositionScheduleByDate(positionFulfillment.getPosition(), workday);

        // Get employee scheduled day on workday.
        WorkScheduleDay scheduleDay = getScheduledDay(positionSchedule.getWorkSchedule(), workday);
        if(scheduleDay == null) return null;

        // Find exceptions, when found then override scheduled day.// Is there any schedule exception on the employee level ?,
        // if 'yes' then override schedule day and continue.
        EmployeeScheduleDayException employeeScheduleDayException = ScheduleService.this.getScheduleException(scheduleDay, positionFulfillment);
        if (employeeScheduleDayException != null) {
            return writeLog(employeeScheduleDayException);
        }

        // Is there any schedule exception on the position level ?,
        // if 'yes' then override schedule day and continue.
        PositionScheduleDayException positionScheduleDayException = getScheduleException(scheduleDay, positionSchedule);
        if (positionScheduleDayException != null) {
            return writeLog(positionScheduleDayException);
        }

        // Is there any schedule exception ?,
        // if 'yes'then override schedule day and continue.
        WorkScheduleDayException workScheduleDayException = scheduleDay.getException();
        if (workScheduleDayException != null) {
            return writeLog(workScheduleDayException);
        }

        // Return effective schedule.
        return writeLog(scheduleDay);
        
    }

    public WorkScheduleDay getScheduledDay(WorkSchedule schedule, Date workday) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<WorkScheduleDay> cq = cb.createQuery(WorkScheduleDay.class);

        Root<WorkSchedule> workSchedule = cq.from(WorkSchedule.class);
        Join<WorkSchedule, WorkScheduleDay> workScheduleDay = workSchedule.join("templates");
        cq.select(workScheduleDay).where(new Predicate[]{
            cb.equal(workSchedule.get("id"), schedule.getId()),
            cb.equal(workScheduleDay.get("workday"), workday)
        });

        TypedQuery<WorkScheduleDay> q = getEntityManager().createQuery(cq);

        try {
            return q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }

    }

    private EffectiveScheduleDay writeLog(WorkScheduleDay scheduleDay) {
        return new EffectiveScheduleDay(scheduleDay.getWorkday(), scheduleDay.getFromDate(), scheduleDay.getThruDate());
    }

    private EffectiveScheduleDay writeLog(WorkScheduleDayException scheduleDay) {
        return new EffectiveScheduleDay(scheduleDay.getScheduleDay().getWorkday(), scheduleDay.getFromDate(), scheduleDay.getThruDate());
    }

    private EffectiveScheduleDay writeLog(PositionScheduleDayException scheduleDay) {
        return new EffectiveScheduleDay(scheduleDay.getScheduleDay().getWorkday(), scheduleDay.getFromDate(), scheduleDay.getThruDate());
    }

    private EffectiveScheduleDay writeLog(EmployeeScheduleDayException scheduleDay) {
        return new EffectiveScheduleDay(scheduleDay.getScheduleDay().getWorkday(), scheduleDay.getFromDate(), scheduleDay.getThruDate());
    }

}
