/*
 * Copyright 2014 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.reporting.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.base.Application;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.reporting.ReportCache;
import net.ap57.tiger.base.reporting.ReportCompiler;
import net.ap57.tiger.base.reporting.ReportExporter;
import net.ap57.tiger.base.reporting.ReportFiller;
import net.ap57.tiger.base.reporting.ReportLoader;
import net.ap57.tiger.base.reporting.api.CompiledReport;
import net.ap57.tiger.base.reporting.api.FilledReport;
import net.ap57.tiger.base.reporting.api.ReportSource;
import net.ap57.tiger.base.reporting.entity.ReportTemplate;
import net.ap57.tiger.base.reporting.entity.ReportTemplateId;
import net.ap57.tiger.base.reporting.entity.ReportTemplatePool;
import net.ap57.tiger.base.reporting.qualifier.Exporter;
import net.ap57.tiger.base.reporting.qualifier.Pdf;
import net.ap57.tiger.base.util.DigestUtility;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
@LocalBean
public class ReportingUtil {

  @Inject
  private EntityManager em;

  @Inject
  private Application theApp;

  @Inject
  private ReportCache cache;

  @Inject
  private ReportLoader loader;

  @Inject
  private ReportCompiler compiler;

  @Inject
  private ReportFiller filler;

  @Inject
  @Exporter
  @Pdf
  private ReportExporter exporter;

  public String readSourceContent(InputStream source) {
    String content = "";
    String line;

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(source))) {
      while ((line = reader.readLine()) != null) {
        content += line + "\n\r";
      }
    } catch (IOException ex) {
      Logger.getLogger(ReportingUtil.class.getName()).log(Level.SEVERE, null, ex);
    }

    return content;
  }

  public ReportSource getSource(ReportTemplate tpl) {
    if (tpl != null) {
      ReportSource src = new ReportSource(tpl.getContent());
      return src;
    } else {
      return null;
    }
  }

  public void writeReport(Module module, String tplName, Object data, Map<String, Object> params, OutputStream out) {

    String id = String.valueOf(DigestUtility.getDigest((module.getModuleName() + "." + tplName).toCharArray(), "MD5"));
    CompiledReport compiled = cache.get(id);

    if (compiled == null) {
      ReportTemplatePool tpl = em.find(ReportTemplatePool.class, new ReportTemplateId(module.getModuleName(), tplName  + ".jrxml"));
      ReportSource tplSrc = new ReportSource(tpl.getContent());
      compiled = compiler.compile(loader.load(tplSrc, null), null);
      cache.put(id, compiled);
//            bundle.getReportMap().put(tplName, compiled);
    }

    FilledReport filled = filler.fill(compiled, data, params);

    exporter.export(filled, out);
  }

  public void writeNCombineReport(Module module, String tplName1, String tplName2, Object data1, Object data2, Map<String, Object> params1, Map<String, Object> params2, OutputStream out) {
    String id1 = String.valueOf(DigestUtility.getDigest((module.getModuleName() + "." + tplName1).toCharArray(), "MD5"));
    CompiledReport compiled1 = cache.get(id1);

    String id2 = String.valueOf(DigestUtility.getDigest((module.getModuleName() + "." + tplName2).toCharArray(), "MD5"));
    CompiledReport compiled2 = cache.get(id2);

    if (compiled1 == null || compiled2 == null) {
      if (compiled1 == null) {
        ReportTemplatePool tpl1 = em.find(ReportTemplatePool.class, new ReportTemplateId(module.getModuleName(), tplName1 + ".jrxml"));
        ReportSource tplSrc1 = new ReportSource(tpl1.getContent());
        compiled1 = compiler.compile(loader.load(tplSrc1, null), null);
        cache.put(id1, compiled1);
      }
      if (compiled2 == null) {
        ReportTemplatePool tpl2 = em.find(ReportTemplatePool.class, new ReportTemplateId(module.getModuleName(), tplName2 + ".jrxml"));
        ReportSource tplSrc2 = new ReportSource(tpl2.getContent());
        compiled2 = compiler.compile(loader.load(tplSrc2, null), null);
        cache.put(id2, compiled2);
      }

    }

    FilledReport filled1 = filler.fill(compiled1, data1, params1);
    FilledReport filled2 = filler.fill(compiled2, data2, params2);

    exporter.exportNCombine(filled1, filled2, out);
  }

}
