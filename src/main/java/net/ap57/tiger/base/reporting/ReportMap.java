/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.reporting;

import java.util.Map;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.util.DigestUtility;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class ReportMap {

  private final Module context;
  private final Map<String, String> lookup;

  public ReportMap(Module context, Map<String, String> lookup) {
    this.context = context;
    this.lookup = lookup;
  }

  public void put(String reportName) {
    String id = String.valueOf(DigestUtility.getDigest((context.getModuleName() + "." + reportName).toCharArray(), "MD5"));
    lookup.put(reportName, id);
  }

  public String get(String reportName) {
    String key = lookup.get(reportName);
    return key;
  }

}
