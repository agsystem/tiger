/*
 * Copyright 2014 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.reporting.jasperreports;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import net.ap57.tiger.base.reporting.ReportCompiler;
import net.ap57.tiger.base.reporting.api.CompiledReport;
import net.ap57.tiger.base.reporting.api.RawReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;

@Stateless
public class JRReportCompiler implements ReportCompiler {

    @Override
    public CompiledReport compile(RawReport rawReport, Map<String, Object> params) {
        JasperDesign design = (JasperDesign) rawReport.getWrappedObject();
        try {
            JasperReport compiled = JasperCompileManager.compileReport(design);
            CompiledReport wrapper = new CompiledReport(compiled);
            return wrapper;
        } catch (JRException ex) {
            Logger.getLogger(JRReportCompiler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
