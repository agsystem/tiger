/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.reporting.bean;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.ap57.tiger.base.api.Module;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "reporter")
@RequestScoped
public class Reporter {

  @Inject
  private ReportingUtil reportingUtil;

  public void writeReport(Module context, String tplName, Object data, Map<String, Object> params) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
    try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
      facesContext.responseComplete();
      reportingUtil.writeReport(context, tplName, data, params, servletOutputStream);
    } catch (Exception ex) {
      Logger.getLogger(Reporter.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public void writeNCombineReport(Module context, String tplName1, String tplName2, Object data1, Object data2, Map<String, Object> params1, Map<String, Object> params2) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
    try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
      reportingUtil.writeNCombineReport(context, tplName1, tplName2, data1, data2, params1, params2, servletOutputStream);
      facesContext.responseComplete();
    } catch (Exception ex) {
      Logger.getLogger(Reporter.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
