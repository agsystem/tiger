/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.reporting.jasperreports;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import net.ap57.tiger.base.reporting.ReportExporter;
import net.ap57.tiger.base.reporting.api.FilledReport;
import net.ap57.tiger.base.reporting.qualifier.Exporter;
import net.ap57.tiger.base.reporting.qualifier.Pdf;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Stateless
@Exporter
@Pdf
public class JRPdfReportExporter implements ReportExporter {

  @Override
  public void export(FilledReport filledReport, String filename) {
    try {
      JRPdfExporter exporter = new JRPdfExporter();
      exporter.setExporterInput(new SimpleExporterInput((JasperPrint) filledReport.getWrappedObject()));
      exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(filename));
      exporter.exportReport();
    } catch (JRException ex) {
      Logger.getLogger(JRPdfReportExporter.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Override
  public void export(FilledReport filledReport, OutputStream outputStream) {
    try {
      JRPdfExporter exporter = new JRPdfExporter();
      exporter.setExporterInput(new SimpleExporterInput((JasperPrint) filledReport.getWrappedObject()));
      exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
      exporter.exportReport();
    } catch (JRException ex) {
      Logger.getLogger(JRPdfReportExporter.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Override
  public void exportNCombine(FilledReport filledReport1, FilledReport filledReport2, OutputStream outputStream) {
    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
    export(filledReport1, baos1);
    export(filledReport2, baos2);
    Document document = new Document();
    try {
      PdfReader reader1 = new PdfReader(baos1.toByteArray());
      PdfReader reader2 = new PdfReader(baos2.toByteArray());
      int n = reader1.getNumberOfPages();
      
      PdfCopy copy = new PdfCopy(document, outputStream);
      
      document.open();
      
      for (int i = 1; i <= n; i++) {
        PdfImportedPage page1 = copy.getImportedPage(reader1, i);
        copy.addPage(page1);
        PdfImportedPage page2 = copy.getImportedPage(reader2, i);
        copy.addPage(page2);
      }
      
      document.close();
      
    } catch (DocumentException | IOException ex) {
      Logger.getLogger(JRPdfReportExporter.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
