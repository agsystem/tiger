/*
 * Copyright 2014 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.reporting.bean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.base.reporting.entity.ReportTemplate;
import net.ap57.tiger.base.reporting.entity.ReportTemplateId;
import net.ap57.tiger.base.reporting.entity.ReportTemplatePool;
import net.ap57.tiger.base.reporting.event.ReportTemplateAddedEvent;
import net.ap57.tiger.base.reporting.event.ReportTemplateFoundEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class ReportTemplatePooler extends AbstractFacade<ReportTemplatePool> {

    @Inject
    private EntityManager em;

    @Inject
    private ReportingUtil reportingUtil;

    @Inject
    @ReportTemplateAddedEvent
    private Event<ReportTemplatePool> reportTemplateAddedEvent;

    public ReportTemplatePooler() {
        super(ReportTemplatePool.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public ReportTemplatePool createTransient(Map<String, Object> params) {
        ReportTemplatePool entry = new ReportTemplatePool();
        entry.setBundle((String) params.get("bundle"));
        entry.setName((String) params.get("name"));

        String sourcepath = "/" + entry.getBundle().replaceAll("\\.", "/") + "/report/" + params.get("file");
        InputStream in = getClass().getResourceAsStream(sourcepath);

        entry.setContent(reportingUtil.readSourceContent(in));

        return entry;
    }

    public void newEntry(@Observes @ReportTemplateFoundEvent ReportTemplate tpl) {
        ReportTemplateId id = new ReportTemplateId();
        id.setBundle(tpl.getBundle());
        id.setName(tpl.getName());

        ReportTemplatePool entry = find(new ReportTemplateId(tpl.getBundle(), tpl.getName()));

        if (entry == null) {
            entry = new ReportTemplatePool();
            entry.setBundle(id.getBundle());
            entry.setName(id.getName());
            String sourcepath = "/" + entry.getBundle().replaceAll("\\.", "/") + "/report/" + tpl.getName();
            InputStream in = getClass().getResourceAsStream(sourcepath);

            entry.setContent(reportingUtil.readSourceContent(in));

            try {
                create(entry);
            } catch (Exception ex) {
                Logger.getLogger(ReportTemplatePooler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        reportTemplateAddedEvent.fire(entry);

    }

//    public void newEntry(Map<String, Object> params) {
//        ReportTemplateId id = new ReportTemplateId();
//        id.setBundle((String) params.get("bundle"));
//        id.setName((String) params.get("name"));
//
//        ReportTemplatePool entry = find(id);
//
//        if (entry == null) {
//            entry = createTransient(params);
//
//            try {
//                create(entry);
//            } catch (Exception ex) {
//                Logger.getLogger(ReportTemplatePooler.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    @Override
    public Object getId(ReportTemplatePool entity) {
        ReportTemplateId id = new ReportTemplateId();
        id.setBundle(entity.getBundle());
        id.setName(entity.getName());

        return id;
    }

}
