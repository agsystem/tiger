/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String id;

    private String name;

    private String folder;

    private String view;
    
    private String backing;

    private Integer menuSeq;

    private Menu parent;

    private List<Menu> children;

    public Menu(String name, String folder, String view, Menu parent) {
        this.name = name;
        this.folder = folder;
        this.view = view;
        this.parent = parent;
        this.children = new ArrayList<>();
    }

    public Menu() {
        this.name = "root";
        this.folder = "";
        this.view = "";
        this.children = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFolder() {
        if (parent == null) {
            return folder;
        } else if (folder == null) {
            return parent.getFolder();
        } else {
            return parent.getFolder() + "/" + folder;
        }
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getBacking() {
        return backing;
    }

    public void setBacking(String backing) {
        this.backing = backing;
    }

    public String getTarget() {
        if (parent == null) {
            return "";
        } else if (view == null) {
            return getFolder();
        } else {
            if(backing != null && !backing.isEmpty()) {
                return getFolder() + "/" + switchToChild(breakCamelCasedString(backing, "Page")) + ".xhtml";
            } else
            return getFolder() + "/" + view;
        }
    }

    public Integer getMenuSeq() {
        return menuSeq;
    }

    public void setMenuSeq(Integer menuSeq) {
        this.menuSeq = menuSeq;
    }

    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }
    
    public String[] breakCamelCasedString(String camelCasedString, String... excludedStrings) {

        Pattern pattern = Pattern.compile("[A-Z][a-z 0-9]*");
        Matcher matcher = pattern.matcher(camelCasedString);

        String strExcludedPattern = "";

        int count = excludedStrings.length;
        for (String excludedString : excludedStrings) {
            strExcludedPattern += "(" + excludedString + ")";
            if (count > 1) {
                strExcludedPattern += "|";
            }
            count--;
        }

        Pattern excludedPattern = null;
        if (!strExcludedPattern.isEmpty()) {
            excludedPattern = Pattern.compile(strExcludedPattern);
        }

        List<String> result = new ArrayList<>();

        while (matcher.find()) {
            String match = matcher.group();
            if (excludedPattern != null) {
                Matcher excludedMatcher = excludedPattern.matcher(match);
                if (!excludedMatcher.find()) {
                    result.add(match);
                }
            }
        }

        String[] arrStr =  result.toArray(new String[result.size()]);
        
        return arrStr;

    }
    
    

    public String switchToChild(String[] values) {

        String viewId = "";
        int count = values.length;
        for (String viewIdPart : values) {
            viewId += viewIdPart;
            if (count > 1) {
                viewId += "-";
            }
            count--;
        }
        return viewId.toLowerCase();
    }
    
    
}
