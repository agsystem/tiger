/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import net.ap57.tiger.base.Application;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.config.Config;
import net.ap57.tiger.base.config.ConfigFacade;
import net.ap57.tiger.base.ui.pf.PFLazyDataTable;
import net.ap57.tiger.base.ui.annotation.View;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class Page {

    @Inject
    private PageCtx context;

    @Inject
    private Application theApp;

    @Inject
    private ConfigFacade configOp;

    @View
    private DataTable configTable;

    @Inject
    private PageContextManager contextManager;

//    @PostConstruct
    public void init() {

        if (getAttribute("loaded") != null) {
            return;
        } else {
            System.out.println("LOAD PAGE");
            initPage();
        }

    }

    public PageContext init(PageContext parentContext) {

//        if (getAttribute("loaded") != null) {
//            return;
//        } else {
        System.out.println("LOAD PAGE");
        return initPage(parentContext);
//        }      
    }

    protected void initPage() {

        // Construct the UI for app config
        initConfig();

        // Create context for the view to be shown
        createContext();

        // Create view components
        createView();

    }

    protected PageContext initPage(PageContext parentContext) {

        // Construct the UI for app config
//        initConfig();
        // Create context for the view to be shown
        PageContext context = createContext(parentContext);

        // Create view components
        createView();
        
        return context;

    }

    // Construct UI for for app config
    protected void initConfig() {

        System.out.println("initConfig");

        configTable = new PFLazyDataTable<ConfigFacade, Config>() {

            @Override
            protected ConfigFacade getOp() {
                return configOp;
            }

            @Override
            protected Map<String, Object> getLoadFilters() {
                Map<String, Object> filters = new HashMap<>();
                Module module = getModule();
                if (module != null) {
                    filters.put("module", module.getModuleName());
                } else {
                    filters.put("module", "net.ap57.tiger.base");
                }
                return filters;
            }

        };

        // Save the instance of table for configuring app 
        context.setAttribute("configTable", configTable);

    }

    protected void createContext() {

        System.out.println("createContext");

        PageContext pageContext = new PageContext(getUrl());
        
        contextManager.setPage(pageContext);

    }

    protected PageContext createContext(PageContext parentContext) {

        System.out.println("CREATE CONTEXT WITH PARENT");

        PageContext pageContext = new PageContext(getUrl()); // THE PROBLEM IS HERE 
        pageContext.setParent(parentContext);
        contextManager.setPage(pageContext);
        
        return pageContext;
    }

    protected void createView() {

        System.out.println("createView");

        // Setup page layout
        context.setExplorerVisibility(false);
        context.setEditorVisibility(false);
        context.setPropertiesVisibility(false);
        initLayout();

        // Construct UI components
        initComponents();

        registerComponents();

        setAttribute("loaded", Boolean.TRUE);

    }

    protected String getUrl() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String url = request.getRequestURL().toString();
        return url;
    }

    // Construct UI components (to be overriden)
    protected abstract void initComponents();

    // Method to be called from page template to set the layout up
    public int getInitLayout() {

        // Setup page layout
        context.setExplorerVisibility(false);
        context.setEditorVisibility(false);
        context.setPropertiesVisibility(false);
        initLayout();

        return 0;

    }

    // Init the layout (to be overriden)
    protected abstract void initLayout();

    protected void registerComponents() {
        for (Field field : getClass().getDeclaredFields()) {

            View vc = field.getDeclaredAnnotation(View.class);

            if (vc == null) {
                continue;
            }

            String key = vc.name();
            if (key.isEmpty()) {
                field.setAccessible(true);
                key = field.getName();
            }

            try {
                PageComponent comp = getContext().getLookupComponents().get(key);
                if (comp == null) {
                    comp = (PageComponent) field.get(this);
                    register(key, comp);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                System.out.println("I'm here");
                PageContext ctx = contextManager.getPage();
                if (ctx == null) {
                    return;
                }
                PageComponent comp = ctx.getComponents().get(key);
                if (comp == null) {
                    comp = (PageComponent) field.get(this);
                    register0(key, comp);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    public void registerChildren() {

//        for (Field field : getClass().getDeclaredFields()) {
//
//            ChildView cv = field.getDeclaredAnnotation(ChildView.class);
//
//            if (cv == null) {
//                continue;
//            }
//
//            String key = cv.name();
//
//            if (key.isEmpty()) {
//                field.setAccessible(true);
//                key = field.getName();
//            }
//
//            try {
//                String childPageClassName = getChildren().get(key);
//                if (childPageClassName == null) {
////                    System.out.println("DECLARING CLASS " + Class.forName(field.getGenericType().getTypeName()).newInstance());
////                    childPage = (ChildPage) field.get(this);
////                    comp.setContext(new PageComponentCtx());
//                    register(key, field.getGenericType().getTypeName());
//                }
//            } catch (IllegalArgumentException ex) {
//                Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//        }
    }

    protected void register(String key, PageComponent pc) {
        pc.setContainer(this);
        pc.setId(key);
        getComponents().put(key, pc);
        pc.init();

    }

    protected void register0(String key, PageComponent pc) {
        pc.setContainer(this);
        pc.setId(key);
        contextManager.getPage().getComponents().put(key, pc); // trial
        pc.init();

    }

//    private void register(String key, String cp) {
//        getChildren().put(key, cp);
//    }
    public PageCtx getContext() {
        return context;
    }

    public Map<String, PageComponent> getComponents() {
        return context.getLookupComponents();
    }

    public <T extends PageComponent> T get(String id) {

        PageComponent comp = getComponents().get(id);
        comp.init();

        return (T) comp;

    }

//    public Map<String, String> getChildren() {
//        return context.getLookupChildren();
//    }
    public void loadChild(String id) {

//        String classname = id;
//
//        ChildPageCtx childPage = null;
//
//        try {
//            System.out.println("CAILD KLASS: " + classname);
//            childPage = (ChildPageCtx) Class.forName(classname).newInstance();
//            System.out.println("CAILD: " + childPage);
//            childPage.setParent(this);
//            childPage.initPage();
//        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
//            Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

//    public String spawnChildPage(String key) {
//        for (String str : context.getLookupChildren().keySet()) {
//            System.out.println("CHILDREN{" + str + "->" + context.getLookupChildren().get(str) + "}");
//        }
//        String child = context.getLookupChildren().get(key);
//
//        String[] viewIdParts = StringUtil.breakCamelCasedString(child, "Page");
//        String viewId = "";
//        int count = viewIdParts.length;
//        for (String viewIdPart : viewIdParts) {
//            viewId += viewIdPart;
//            if (count > 1) {
//                viewId += "-";
//            }
//            count--;
//        }
//
//        if (!viewId.isEmpty()) {
//            System.out.println("QEY: " + child);
//            for (String test : getChildren().keySet()) {
//                System.out.println("CONTENT{" + getChildren().get(test) + "}");
//            }
//            loadChild(child);
//            return viewId.toLowerCase();
//
//        } else {
//
//            return "";
//
//        }
//    }
    public Object getAttribute(String key) {
        return getContext().getAttributes().get(key);
    }

    public void setAttribute(String key, Object value) {
        getContext().getAttributes().put(key, value);
    }

    public void setExplorer(String title, String width) {
        context.setExplorerTitle(title);
        context.setExplorerWidth(width);
        context.setExplorerVisibility(true);
        contextManager.getPage().getExplorer().setTitle(title); // trial
        contextManager.getPage().getExplorer().setWidth(width); // trial
        contextManager.getPage().getExplorer().setVisibility(true); // trial
    }

    public void setEditor(String title, String width) {
        context.setEditorTitle(title);
        context.setEditorWidth(width);
        context.setEditorVisibility(true);
        contextManager.getPage().getEditor().setTitle(title); // trial
        contextManager.getPage().getEditor().setWidth(width); // trial    
        contextManager.getPage().getEditor().setVisibility(true); // trial
    }

    public void setProperties(String title, String width) {
        context.setPropertiesTitle(title);
        context.setPropertiesWidth(width);
        context.setPropertiesVisibility(true);
        contextManager.getPage().getProperties().setTitle(title); // trial
        contextManager.getPage().getProperties().setWidth(width); // trial
        contextManager.getPage().getProperties().setVisibility(true); // trial
    }

    public Module getModule() {
        String pkgName = getClass().getPackage().getName();
        int idx = pkgName.indexOf(".", 19);
        String moduleName = "";
        if (idx != -1) {
            moduleName = pkgName.substring(0, idx);
        } else {
            moduleName = pkgName;
        }
        return theApp.findModule(moduleName);
    }

    @Override
    public String toString() {
        return "";
    }

}
