/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import net.ap57.tiger.base.api.AbstractPickerFacade;

public abstract class DataPicker<F extends AbstractPickerFacade, M extends Serializable> extends DataTable<F, M> {

    public DataModel<M> setupModel(List<M> subtractor, Map<String, Object> filters) {
        return createDataModel(subtractor, filters);
    }

    protected DataModel<M> createDataModel(List<M> subtractor, Map<String, Object> filters) {
        List<M> data = getOp().findAll(subtractor, filters);
        return new ListDataModel(data);
    }

//    @Override
//    public DataModel<M> getModel() {
//        return (DataModel<M>) getModelManager().getModel(this);
//    }

    public void prepare(List<M> subtractor, Map<String, Object> filters) {
        setModel(setupModel(subtractor, filters));
    }

}
