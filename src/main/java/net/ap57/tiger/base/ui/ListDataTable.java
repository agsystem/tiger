/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.base.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import org.apache.commons.collections.map.ListOrderedMap;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author mbahbejo
 */
public abstract class ListDataTable<T> extends PageComponent {

    public ListOrderedMap getData() {
        return (ListOrderedMap) getAttribute("data", ListOrderedMap.decorate(new HashMap<String, T>()));
    }

    protected boolean loadModel() {
        return true;
    }

    public DataModel<T> getModel() {
        if (loadModel()) {
            return (DataModel<T>) get("model", createDataModel());
        } else {
            return null;
        }
    }

    public void setModel(DataModel<T> model) {
        setAttribute("model", model);
    }

    public T getSelection() {
        List selections = getSelections();
        if (selections.isEmpty()) {
            return null;
        } else {
            return (T) selections.get(0);
        }
    }

    public void setSelection(T selection) {
        getSelections().clear();
        getSelections().add(selection);
    }

    public List<T> getSelections() {
        List<T> obj = (List<T>) getAttribute("selections");

        if (obj == null) {
            obj = new ArrayList<>();
            setSelections(obj);
        }

        return obj;

    }

    public void setSelections(List<T> selections) {
        setAttribute("selections", selections);
    }

    public void delete(ActionEvent evt) {
        delete(getSelections());
        getSelections().clear();
    }

    protected void delete(Collection<T> es) {
        for(T t : es) {
            delete(t);
        }
    }

    protected void delete(T e) {
        remove(objToKey(e));
    }

    public T get(String key) {
        return (T) getData().get(key);
    }

    public void put(String key, T obj) {
        getData().put(key, obj);
    }

    public void put(T obj) {
        put(objToKey(obj), obj);
    }

    public void remove(String key) {
        getData().remove(key);
    }

    public void clear() {
        setAttribute("data", ListOrderedMap.decorate(new HashMap<String, T>()));
    }

    protected DataModel<T> createDataModel() {
        return new LazyDataModel<T>() {
            @Override
            public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                int max = getData().size();
                setRowCount(max);
                int last = first + pageSize;
                if (last > max) {
                    last = max;
                }
                return getData().valueList().subList(first, last);
            }

            @Override
            public Object getRowKey(T object) {
                return objToKey(object);
            }

            @Override
            public T getRowData(String rowKey) {
                return keyToObj(rowKey);
            }

        };
    }

    protected T keyToObj(String rowkey) {
        return (T) getData().get(rowkey);
    }

    protected abstract String objToKey(T object);

}
