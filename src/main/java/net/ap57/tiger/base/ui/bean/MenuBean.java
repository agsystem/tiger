/*
 * Copyright 2013 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Event;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import net.ap57.tiger.base.ui.AlternativeMenuGenerator;
import net.ap57.tiger.base.ui.Menu;
import net.ap57.tiger.base.ui.MenuCache;
import net.ap57.tiger.base.ui.MenuGenerator;
import net.ap57.tiger.base.ui.Page;
import net.ap57.tiger.base.ui.event.MenuEvent;
import net.ap57.tiger.base.ui.event.MenuEventListener;
import net.ap57.tiger.base.ui.event.MenuEventSource;
import net.ap57.tiger.base.ui.event.MenuEventType;
import net.ap57.tiger.base.ui.event.PageChangeEvent;
import net.ap57.tiger.base.util.PageUtil;
import org.apache.commons.collections.map.ListOrderedMap;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Named
@SessionScoped
public class MenuBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private Event<PageChangeEvent> pageChangeEvent;

    @Inject
    private MenuCache menuCache;

    @Inject
    @AlternativeMenuGenerator
    private MenuGenerator generator;

    private ServletContext ctx;

    private Object model;
    private Object contextMenu;
    private Object breadcrumb;

    private ListOrderedMap selections;

    @PostConstruct
    private void init() {
        selections = new ListOrderedMap();
        ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    }

    private final MenuEventSource mainMenuEventGenerator = new MenuEventSource() {

        private final List<MenuEventListener> listeners = new ArrayList<>();

        @Override
        public void addListener(MenuEventListener listener) {
            listeners.add(listener);
        }

        @Override
        public void notifyListener(MenuEvent evt) {
            listeners.stream().forEach((listener) -> {
                listener.menuClicked(evt);
            });
        }

        @Override
        public void cleanListener() {
            
        }

    };

    private final MenuEventSource contextMenuEventGenerator = new MenuEventSource() {

        private final List<MenuEventListener> listeners = new ArrayList<>();

        @Override
        public void addListener(MenuEventListener listener) {
            listeners.add(listener);
        }

        @Override
        public void notifyListener(MenuEvent evt) {
            listeners.stream().forEach((listener) -> {
                listener.menuClicked(evt);
            });
        }

        @Override
        public void cleanListener() {
            listeners.clear();
        }

    };

    public void dispatch() {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String key = params.get("id");

//        MenuCache menuCache = MenuCache.getInstance(ctx);
        Menu selectedMenu = menuCache.findByKey(key);

        selections.clear();
        traceUp(selectedMenu);

        pageChangeEvent.fire(new PageChangeEvent());

        redirect(selectedMenu, null);
    }

    public void redirect(Menu appMenu, Map<String, List<String>> params) {

        MenuEvent event = new MenuEvent(MenuEventType.MENUITEM_CLICKED, appMenu);
        event.setParam("selections", selections);

        if (appMenu.getParent().getName().equals("root")) {
            contextMenu = null;
            mainMenuEventGenerator.notifyListener(event);
            contextMenuEventGenerator.cleanListener();
        } else {
            contextMenuEventGenerator.notifyListener(event);
        }

        breadcrumb = null;

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        String contextPath = externalContext.getRequestContextPath();
        String servletPath = externalContext.getRequestServletPath();

        String root = contextPath + servletPath;

        String encodedTarget = root + appMenu.getTarget();

//        System.out.println("Context Path: " + contextPath + ", Servlet Path: " + servletPath + ", Target: " + appMenu.getTarget());
//        System.out.println("Context Path: " + contextPath + ", Servlet Path: " + servletPath + ", View: " + appMenu.getView());
        
        String beanPage = appMenu.getBacking();
        if (beanPage != null && !beanPage.isEmpty()) {
            PageUtil.loadPage(beanPage);
        }

        try {
            externalContext.redirect(encodedTarget);
        } catch (IOException ex) {
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Object getModel() {
        if (model == null) {
            model = generator.getMainMenu(mainMenuEventGenerator);
        }

        return model;
    }

    public Object getContextMenu() {
        if (contextMenu == null) {
            if (!selections.isEmpty()) {
                contextMenu = generator.getContextMenu(contextMenuEventGenerator, (Menu) selections.get(selections.get(0)));
            } else {
                contextMenu = generator.getContextMenu(contextMenuEventGenerator, null);
            }
        }
        return contextMenu;
    }

    public Object getBreadcrumb() {
        if (breadcrumb == null) {
            breadcrumb = generator.getBreadcrumb(selections);
        }
        return breadcrumb;
    }

    private void traceUp(Menu appMenu) {
        selections.put(appMenu.getId(), appMenu);
        Menu parent = appMenu.getParent();
        if (!parent.getName().equals("root")) {
            traceUp(parent);
        }
    }

}
