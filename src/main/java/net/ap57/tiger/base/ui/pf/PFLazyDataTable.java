/*
 * Copyright 2013 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.model.DataModel;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.base.ui.DataTable;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <F>
 * @param <E>
 */
public abstract class PFLazyDataTable<F extends AbstractFacade, E extends Serializable> extends DataTable<F, E> {

  @Override
  protected DataModel<E> createDataModel() {
    return new LazyDataModel<E>() {
      
      @Override
      public List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        Map<String, Object> allFilters = getLoadFilters();
        allFilters.putAll(filters);
        addStaticFilter(allFilters);
        List<E> result = getOp().findAll(first, pageSize, allFilters);
        setRowCount(getOp().countAll(allFilters).intValue());
        return result;
      }

      @Override
      public E getRowData(String rowkey) {
        return keyToObj(rowkey);
      }

      @Override
      public Object getRowKey(E object) {
        return objToKey(object);
      }

    };
  }
  
  protected E keyToObj(String rowkey) {
      return (E) getOp().getObj(rowkey);
  }
  
  protected Object objToKey(E object) {
      return getOp().getString(object);
  }

  @Override
  protected DataModel<E> createBlankDataModell() {
    return new LazyDataModel<E>() {
      
      @Override
      public List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<E> result = new ArrayList<>();
        setRowCount(0);
        return result;
      }

      @Override
      public E getRowData(String rowkey) {
        return (E) getOp().getObj(rowkey);
      }

      @Override
      public Object getRowKey(E object) {
        return getOp().getString(object);
      }

    };
  }

  protected void addStaticFilter(Map<String, Object> filters) {
  }

}
