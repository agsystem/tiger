/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.event;

import net.ap57.tiger.base.event.ApplicationEvent;
import net.ap57.tiger.base.ui.Menu;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class MenuEvent extends ApplicationEvent {

    public MenuEvent(MenuEventType eventType, Menu src) {
//        super(eventType.ordinal());
        this.parameters.put("source", src);
    }
    
    public Menu getSource() {
        return (Menu) this.parameters.get("source");
    }
    
}
