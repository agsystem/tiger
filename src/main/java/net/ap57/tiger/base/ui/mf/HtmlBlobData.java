/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.mf;

import java.io.IOException;
import java.util.UUID;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@FacesComponent(createTag = true, tagName = "blobData", namespace = "http://mukafaces.com/ui")
public class HtmlBlobData extends UIComponentBase {

    @Override
    public String getFamily() {
        return "components";
    }

    @Override
    public void encodeAll(FacesContext context) throws IOException {
        String id = (String) getAttributes().get("id");
        String onclick = (String) getAttributes().get("onclick");
        String key = UUID.randomUUID().toString();
        String height = (String) getAttributes().get("height");
        context.getExternalContext().getSessionMap().put(key, getAttributes().get("data"));
        ResponseWriter writer = context.getResponseWriter();
        writer.startElement("a", null);
        writer.writeAttribute("style", "cursor: pointer", null);
        if (onclick != null && !onclick.isEmpty()) {
            writer.writeAttribute("onclick", onclick, null);
        }
        writer.startElement("img", this);
        if (id != null && !id.isEmpty()) {
            writer.writeAttribute("id", id, null);
            writer.writeAttribute("id", id, null);
        }
        if (height != null && !height.isEmpty()) {
            writer.writeAttribute("height", height, null);
        }
        writer.writeAttribute("src", context.getExternalContext().getApplicationContextPath() + "/BinaryServlet?key=" + key, null);
        writer.endElement("img");
        writer.endElement("a");
    }

}
