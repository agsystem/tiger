/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
public class UploadedFile implements org.primefaces.model.UploadedFile {
    
    private final org.primefaces.model.UploadedFile uploadedFile;

    public UploadedFile(org.primefaces.model.UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public org.primefaces.model.UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    @Override
    public byte[] getContents() {
        try {
            InputStream in = getInputstream();
            byte[] buff = new byte[1024];
            int bytesRead = 0;
            ByteArrayOutputStream bao = new ByteArrayOutputStream();

            while ((bytesRead = in.read(buff)) != -1) {
                bao.write(buff, 0, bytesRead);
            }

            return bao.toByteArray();

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public String getFileName() {
        return getUploadedFile().getFileName();
    }

    @Override
    public InputStream getInputstream() throws IOException {
        return getUploadedFile().getInputstream();
    }

    @Override
    public long getSize() {
        return getUploadedFile().getSize();
    }

    @Override
    public String getContentType() {
        return getUploadedFile().getContentType();
    }

    public void write(String string) throws Exception {
        
    }

}
