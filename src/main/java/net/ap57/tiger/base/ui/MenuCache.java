/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import net.ap57.tiger.base.util.DigestUtility;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named
@ApplicationScoped
public class MenuCache implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private final ConcurrentHashMap<String, Menu> cache;

    public MenuCache() {
        cache = new ConcurrentHashMap<>();
    }
    
    @PostConstruct
    private void init() {
        Menu root = new Menu();
        addItem(root);
    }
    
    public void addItem(Menu menu) {
        String key = menu.getTarget(); // digest(menu.getTarget());
        menu.setId(key);
        cache.put(key, menu);
    }
    
    public Menu findByTarget(String target) {
        return findByKey(target); // (digest(target));
    }

    public Menu findByKey(String key) {
        return (Menu) cache.get(key);
    }

    private String digest(String target) {
        return String.valueOf(DigestUtility.getDigest(target.toCharArray(), "MD5"));
    }
}
