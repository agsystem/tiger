/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.io.File;
import java.util.UUID;
import javax.faces.context.FacesContext;
import net.ap57.tiger.base.ui.PageComponent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class PFTemporaryFile extends PageComponent {

    @Override
    public void init() {
        FacesContext fc = FacesContext.getCurrentInstance();

        String scheme = fc.getExternalContext().getRequestScheme();
        String base = fc.getExternalContext().getRequestServerName();
        int port = fc.getExternalContext().getRequestServerPort();
        String contextPath = fc.getExternalContext().getApplicationContextPath();
        setBaseUrl(scheme + "://" + base + ":" + port + contextPath);

        String tmpDir = fc.getExternalContext().getRealPath("/tmp/");
        setTmpDir(tmpDir);
    }

    public String getBaseUrl() {
        return (String) getAttribute("baseUrl");
    }

    public void setBaseUrl(String url) {
        setAttribute("baseUrl", url);
    }

    public String getTmpDir() {
        return (String) getAttribute("tmpDir");
    }

    public void setTmpDir(String dir) {
        setAttribute("tmpDir", dir);
    }

    public File createTmp() {

        String key = UUID.randomUUID().toString();
        setAttribute("tmp", key);

        File tmpfile = new File(getTmpDir(), key);

        return tmpfile;

    }

    public void delTmp() {

        String tmpid = (String) getAttribute("tmp");
        if (tmpid != null) {
            File tmpfile = new File(getTmpDir(), tmpid);
            tmpfile.delete();
        }

    }

    public String getTmpUrl() {

        String tmp = (String) getAttribute("tmp");

        if (tmp != null) {
            return getBaseUrl() + "/tmp/" + tmp;
        } else {
            return null;
        }

    }

}
