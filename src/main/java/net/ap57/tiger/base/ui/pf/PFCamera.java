/*
 * To change this license header,t-get choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.pf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ap57.tiger.base.ui.PageForm;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.CloseEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class PFCamera extends PageForm {

    protected abstract PFTemporaryFile getTemporaryFileManager();    

    public void onCapture(CaptureEvent captureEvent) {

        getTemporaryFileManager().delTmp();

        File out = getTemporaryFileManager().createTmp();

        try (FileOutputStream fos = new FileOutputStream(out)) {
            byte[] data = captureEvent.getData();
            fos.write(data);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PFCamera.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PFCamera.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void onClose(CloseEvent evt) {
        getTemporaryFileManager().delTmp();
    }

}
