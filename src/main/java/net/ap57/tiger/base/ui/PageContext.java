/*
 * Copyright 2017 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.base.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class PageContext {

    private final String url;

    private final UiChrome explorer;

    private final UiChrome editor;

    private final UiChrome properties;

    private final Map<String, PageComponent> components;

    private final Map<String, Object> attributes;

    private PageContext parent;

    private PageContext child;

    public PageContext(String url) {
        this.url = url;
        this.components = new HashMap<>();
        this.attributes = new HashMap<>();
        this.explorer = new UiChrome("Explorer");
        this.editor = new UiChrome("Editor");
        this.properties = new UiChrome("Properties");
    }

    public boolean hasParent() {
        return parent != null;
    }

    public boolean hasChild() {
        return child != null;
    }

    public String getUrl() {
        return url;
    }

    public UiChrome getExplorer() {
        return explorer;
    }

    public UiChrome getEditor() {
        return editor;
    }

    public UiChrome getProperties() {
        return properties;
    }

    public Map<String, PageComponent> getComponents() {
        return components;
    }

    public PageComponent get(String id) {
//        System.out.print("REGISTERED ON " + url + ": {");
//        for (String key : components.keySet()) {
//            System.out.print(key + ":" + components.get(key).getId() + ",");
//        }
//        System.out.println("}");
//        System.out.println("LOAD: " + id);
        PageComponent comp = getComponents().get(id);

        if (comp != null) {
            comp.init();
        }

        return comp;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public PageContext getParent() {
        return parent;
    }

    public void setParent(PageContext parent) {
        this.parent = parent;
    }

    public PageContext getChild() {
        return child;
    }

    public void setChild(PageContext child) {
        this.child = child;
    }

//    public void spawnChild(PageContext childContext) {
//        setChildContext(childContext);
//    }

    public void closeChild() {
        child.close();
        child.setParent(null);
        setChild(null);
    }

    public void close() {

        if (child != null) {
            closeChild();
        }

        getComponents().clear();
        getAttributes().clear();
    }

    public List<SessionVariable> getVars() {

        List<SessionVariable> vars = new ArrayList<>();

        if (attributes != null) {
            for (String key : attributes.keySet()) {
                Object value = attributes.get(key);
                vars.add(new SessionVariable(key, value));
            }
        }

        if (components != null) {
            for (String key : components.keySet()) {
                PageComponent pc = components.get(key);
                PageComponentCtx pcCtx = pc.getContext();
                for (String pcKey : pcCtx.getLookup().keySet()) {
                    Object value = pcCtx.getLookup().get(pcKey);
                    vars.add(new SessionVariable(pc.getId() + "." + pcKey, value));
                }
            }
        }

        return vars;

    }
    
    public void navUp(ActionEvent evt) {
//        close();
        System.out.println("URL: " + url);
    }

}
