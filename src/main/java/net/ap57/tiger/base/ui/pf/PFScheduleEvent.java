/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.util.Calendar;
import java.util.Date;
import net.ap57.tiger.base.api.ScheduledEvent;
import org.primefaces.model.DefaultScheduleEvent;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <T>
 */
public class PFScheduleEvent<T extends ScheduledEvent> extends DefaultScheduleEvent {

    private static final long ADAY = 24 * 60 * 60 * 1000;
    private static final long LIMIT = 9 * 60 * 60 * 1000;

    protected T event;

    public PFScheduleEvent(T event) {
        super();

        this.event = event;
        this.setAllDay(false);
        setTitle(event.getName());
        setStartDate(event.getFromDate());
        if (event.getThruDate() != null) {
            setEndDate(adjust(event));
        }
    }

    private Date adjust(T event) {

        Date fromDate = event.getFromDate();
        Date thruDate = event.getThruDate();

        Calendar calFromDate = Calendar.getInstance();
        calFromDate.setTime(fromDate);
        calFromDate.set(Calendar.HOUR_OF_DAY, 0);
        calFromDate.set(Calendar.MINUTE, 0);
        calFromDate.set(Calendar.SECOND, 0);
        calFromDate.set(Calendar.MILLISECOND, 0);

        long tdiff = thruDate.getTime() - calFromDate.getTimeInMillis();
        double diff = tdiff / (ADAY * 1d);
        long idiff = tdiff / ADAY;
        double fraction = (diff - idiff) * ADAY;

//        System.out.println(fromDate + "->" + thruDate + ": diff= " + diff + ", fraction= " + fraction);

        if (diff > 1d && fraction < (LIMIT * 1d)) {
            long lThruDate = thruDate.getTime() + ADAY;
            return new Date(lThruDate);
        } else {
            return thruDate;
        }

    }

    @Override
    public Object getData() {
        return event;
    }

    public T getEvent() {
        return event;
    }

}
