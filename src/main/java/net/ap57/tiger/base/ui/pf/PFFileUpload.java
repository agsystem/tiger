/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import com.mortennobel.imagescaling.ResampleFilters;
import com.mortennobel.imagescaling.ResampleOp;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import net.ap57.tiger.base.ui.PageForm;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class PFFileUpload extends PageForm {

    protected abstract PFTemporaryFile getTemporaryFileManager();

//    public void onUpload(FileUploadEvent event) {
//
//        getTemporaryFileManager().delTmp();
//
//        File out = getTemporaryFileManager().createTmp();
//
//        try (FileOutputStream fos = new FileOutputStream(out)) {
//            InputStream in = event.getFile().getInputstream();
//            byte[] data = new byte[1024];
//            int numBytesRead;
//            while ((numBytesRead = in.read(data, 0, data.length)) != -1) {
//                fos.write(data, 0, numBytesRead);
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(PFFileUpload.class.getName()).log(Level.SEVERE, null, ex);
//        }        
//        
//
//    }
    public void onUpload(FileUploadEvent event) {

        getTemporaryFileManager().delTmp();

        File out = getTemporaryFileManager().createTmp();

        try (FileOutputStream fos = new FileOutputStream(out)) {

            InputStream in = event.getFile().getInputstream();

//            byte[] imageData = getBytes(in);

            BufferedImage imageData = getBufferedImage(in);

            byte[] resizedImageData = resize(imageData, 480, 360);

            fos.write(resizedImageData);

        } catch (IOException ex) {
            Logger.getLogger(PFFileUpload.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public byte[] resize(BufferedImage imageData, int width, int height) {        

        int origWidth = imageData.getWidth(null);
        int origHeight = imageData.getHeight(null);

        float sx = 1f;
        float sy = 1f;

        if (origWidth < width || origHeight < height) { // Small photo
            return new byte[]{};
        } else if (origHeight > origWidth) {// Portrait orientation
            sy = (height * 1f) / (origHeight * 1f);
            sx = sy;
        } else if (origWidth > origHeight) { // Landscape orientation
            sx = (width * 1f) / (origWidth * 1f);
            sy = sx;
        } else {

        }
        
        int resizedWidth = Math.round(sx * origWidth);
        int resizedHeight = Math.round(sy * origHeight);
        ResampleOp resizeOp = new ResampleOp(resizedWidth, resizedHeight);
        resizeOp.setFilter(ResampleFilters.getLanczos3Filter());
        BufferedImage resizedImage = resizeOp.filter(imageData, null);

        BufferedImage canvas = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = canvas.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);
        int offsetX = (width / 2) - (resizedWidth / 2) ;
        int offsetY = (height / 2) - (resizedHeight / 2) ;
        g2d.drawImage(resizedImage, offsetX, offsetY, null);
        g2d.dispose();

        byte[] result = null;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(canvas, "JPG", baos);
            result = baos.toByteArray();
        } catch (IOException ex) {
            throw new EJBException(ex);
        }

        return result;
        
//        return null;
    }

    public byte[] resize(byte[] rawImageData, int width, int height) throws FileNotFoundException, IOException {

        ImageIcon imageIcon = new ImageIcon(rawImageData);
        Image inImage = imageIcon.getImage();
        int origWidth = inImage.getWidth(null);
        int origHeight = inImage.getHeight(null);

        double sx = 1d;
        double sy = 1d;

        if (origWidth < width || origHeight < height) { // Small photo
            return new byte[]{};
        } else if (origHeight > origWidth) {// Portrait orientation
            sy = (height * 1d) / (origHeight * 1d);
            sx = sy;
        } else if (origWidth > origHeight) { // Landscape orientation
            sx = (width * 1d) / (origWidth * 1d);
            sy = sx;
        } else {

        }

        AffineTransform tx = new AffineTransform();
        tx.scale(sx, sy);

//        ResampleOp resizeOp = new ResampleOp(1, 1);
//        resizeOp.setFilter(ResampleFilters.getLanczos3Filter());
        BufferedImage canvas = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//        BufferedImage canvas = resizeOp.filter(src, null);

        Graphics2D g2d = canvas.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.drawImage(inImage, tx, null);
        g2d.dispose();

        byte[] resizedImage = null;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(canvas, "JPG", baos);
            resizedImage = baos.toByteArray();
        } catch (IOException ex) {
            throw new EJBException(ex);
        }

        return resizedImage;

    }

    private byte[] getBytes(InputStream in) throws FileNotFoundException, IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = in.read(data, 0, data.length)) != -1) {
            baos.write(data, 0, nRead);
        }

        baos.flush();
        byte[] bytes = baos.toByteArray();

        return bytes;
    }
    
    private BufferedImage getBufferedImage(InputStream in) throws IOException {
        return ImageIO.read(in);
    }

    public void onClose(CloseEvent evt) {
        getTemporaryFileManager().delTmp();
    }

}
