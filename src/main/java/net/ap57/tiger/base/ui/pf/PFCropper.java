/*
 * Copyright 2016 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import net.ap57.tiger.base.ui.CaptureClient;
import net.ap57.tiger.base.ui.PageComponent;
import org.primefaces.model.CroppedImage;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class PFCropper extends PageComponent {

    protected abstract PFTemporaryFile getTemporaryFileManager();

    public abstract int getX();

    public abstract int getY();

    public abstract int getW();

    public abstract int getH();

    public CroppedImage getCroppedImage() {
        return (CroppedImage) get("cropped", new CroppedImage());
    }

    public void setCroppedImage(CroppedImage data) {
        setAttribute("cropped", data);
    }

    public void notifyClient() {
        List<CaptureClient> clients = (List<CaptureClient>) get("clients", new ArrayList());
        String tmpid = (String) getTemporaryFileManager().getAttribute("tmp");
        if (tmpid != null) {
            File tmpfile = new File(getTemporaryFileManager().getTmpDir(), tmpid);
            try {
                for (CaptureClient client : clients) {
                    client.processRawImage(subImage(tmpfile, getCroppedImage().getLeft(), getCroppedImage().getTop(), getCroppedImage().getWidth(), getCroppedImage().getHeight()));
                }
            } catch (Exception ex) {
                Logger.getLogger(PFCamera.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private byte[] subImage(File src, int x, int y, int w, int h) throws Exception {
        InputStream in = new FileInputStream(src);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = in.read(data, 0, data.length)) != -1) {
            baos.write(data, 0, nRead);
        }

        baos.flush();
        byte[] raw = baos.toByteArray();

        ImageIcon imageIcon = new ImageIcon(raw);
        Image inImage = imageIcon.getImage();
        int width = inImage.getWidth(null);
        int height = inImage.getHeight(null);

        BufferedImage outImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = outImage.createGraphics();
        g2d.drawImage(inImage, null, null);
        g2d.dispose();

        baos = new ByteArrayOutputStream();
        boolean b = ImageIO.write(outImage.getSubimage(x, y, w, h), "PNG", baos);
        byte[] bytes = baos.toByteArray();

        return bytes;

    }

    public String getCoords() {
        return getX() + "," + getY() + "," + getW() + "," + getH();
    }

    public void addClient(CaptureClient client) {
        List<CaptureClient> clients = (List<CaptureClient>) get("clients", new ArrayList());
        clients.add(client);
    }

    public void onCrop() {
        notifyClient();
    }

}
