/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.pf;

import java.util.HashMap;
import java.util.Map;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuElement;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class KeyedMenuModel extends DefaultMenuModel {

    private Map<String, Integer> indexKeys;

    public KeyedMenuModel() {
        indexKeys = new HashMap<>();
    }

    public void addSubMenu(KeyedSubMenu subMenu) {
        int index = getElements().indexOf(subMenu);
        indexKeys.put(subMenu.getKey(), index);
    }

    public void addMenuItem(KeyedMenuItem menuItem) {
        int index = getElements().indexOf(menuItem);
        indexKeys.put(menuItem.getKey(), index);
    }

    @Override
    public void addElement(MenuElement element) {
        super.addElement(element);
        if (element instanceof KeyedSubMenu) {
            addSubMenu((KeyedSubMenu) element);
        } else if (element instanceof KeyedMenuItem) {
            addMenuItem((KeyedMenuItem) element);
        }
    }

    public MenuElement get(String key) {
        return getElements().get(indexKeys.get(key));
    }

}
