/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.base.ui.PageComponent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 * @param <F>
 * @param <T>
 */
public abstract class PFPickList<F extends AbstractFacade, T extends Serializable> extends PageComponent implements Converter {

    protected abstract F getOp();

    protected abstract boolean isEqual(T t1, T t2);

    public abstract List<T> getTarget();

    public List<T> getSource() {
        return getOp().findAll();
    }

    protected DualListModel<T> createDataModel() {
        List<T> src = getSource();
        List<T> target = getTarget();
        for (T g1 : target) {
            for (T g2 : src) {
                if (isEqual(g2, g1)) {
                    src.remove(g2);
                    break;
                }
            }
        }

        return new DualListModel<>(src, target);
    }

    public DualListModel<T> getModel() {
        return createDataModel();
    }

    public void setModel(DualListModel<T> groupModel) {
    }

    public void onTransfer(TransferEvent evt) {
        List<T> items = (List<T>) evt.getItems();
        if (evt.isAdd()) {
            addItems(items);
        } else if (evt.isRemove()) {
            delItems(items);
        }
    }

    protected void addItems(List<T> items) {
        List<T> existingItems = getTarget();
        existingItems.addAll(items);
    }

    protected void delItems(List<T> items) {
        List<T> remItems = new ArrayList<>();
        List<T> existingItems = getTarget();
        for (T existingItem : existingItems) {
            if (items.contains(existingItem)) {
                remItems.add(existingItem);
            }
        }

        for (T remItem : remItems) {
            existingItems.remove(remItem);
        }
    }

    protected abstract Object strToObj(String value);

    protected String objToStr(Object obj) {
        return obj.toString();
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            return strToObj(value);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return objToStr(value);
    }

}
