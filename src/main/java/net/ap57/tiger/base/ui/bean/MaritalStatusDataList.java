/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import net.ap57.tiger.base.entity.MaritalStatus;
import net.ap57.tiger.base.ui.pf.EnumDataList;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "maritalStatusDataList")
@RequestScoped
public class MaritalStatusDataList extends EnumDataList<MaritalStatus> {

  public MaritalStatusDataList() {
    super(MaritalStatus.class);
  }

  @Override
  public MaritalStatus[] getEnumValues() {
    return MaritalStatus.values();
  }
    
}
