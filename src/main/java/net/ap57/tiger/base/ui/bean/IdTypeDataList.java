/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.bean;

import net.ap57.tiger.base.dao.IdentityTypeFacade;
import net.ap57.tiger.base.entity.IdentityType;
import net.ap57.tiger.base.ui.EditableDataList;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class IdTypeDataList extends EditableDataList<IdentityTypeFacade, IdentityType> {

    @Override
    protected Object strToObj(String value) {
        return getOp().findSingleByAttribute("name", value);
    }

    @Override
    protected IdentityType newPlaceholder() {
        return new IdentityType(true);
    }

}
