/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import net.ap57.tiger.base.ui.PageComponent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 * @param <E>
 */
public abstract class EnumDataList<E extends Enum<E>> extends PageComponent implements Converter {

  protected Class<E> entityClass;

  public EnumDataList(Class<E> entityClass) {
    this.entityClass = entityClass;
  }

  public abstract E[] getEnumValues();

  public List<E> getData() {
    List<E> es = Arrays.asList(getEnumValues());
    return es;
  }

  protected E strToObj(String value) {
    if(value == null || value.isEmpty()) return null;
    return E.valueOf(entityClass, value);
  }

  protected String objToStr(Object obj) {
    return obj.toString();
  }

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
      return strToObj(value);
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    return objToStr(value);
  }

  public List<SelectItem> getTypeList() {
    List<E> types = getData();
    List<SelectItem> si = new ArrayList<SelectItem>();
    si.add(new SelectItem(null, "All"));
    for (E type : types) {
      si.add(new SelectItem(type, type.toString()));
    }
    return si;
  }

}
