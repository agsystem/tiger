/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.FactoryFinder;
import javax.inject.Inject;
import javax.inject.Named;
import net.ap57.tiger.base.ui.AlternativeMenuGenerator;
import net.ap57.tiger.base.ui.Menu;
import net.ap57.tiger.base.ui.MenuCache;
import net.ap57.tiger.base.ui.MenuGenerator;
import net.ap57.tiger.base.ui.event.MenuEventSource;
import org.apache.commons.collections.map.ListOrderedMap;
import org.primefaces.model.menu.DefaultMenuColumn;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.MenuElement;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@AlternativeMenuGenerator
@Named(value = "pFAltMenuGenerator")
@RequestScoped
public class PFAltMenuGenerator implements MenuGenerator {

    @Inject
    private MenuCache cache;

//    @Override
    public MenuCache getCache() {
        return cache;
    }

    @Override
    public Object getMainMenu(MenuEventSource src) {

        KeyedMenuModel model = new KeyedMenuModel();

        List<Menu> menuList = getCache().findByTarget("").getChildren();
//        menuList.sort((Menu o1, Menu o2) -> o1.getMenuSeq().compareTo(o2.getMenuSeq()));
        menuList.stream().map((menu) -> {
            KeyedSubMenu item = new KeyedSubMenu(menu.getName());
            menu.getChildren().stream().map((child) -> createContextMenu(src, child)).forEach((menuElement) -> {
                item.addElement(menuElement);
            });
            return item;
        }).forEach((item) -> {
            model.addElement(item);
        });

        DefaultMenuItem logoutMenu = new DefaultMenuItem("Logout");
        logoutMenu.setImmediate(true);
        logoutMenu.setCommand("#{securityController.logout}");
        model.addElement(logoutMenu);

        return model;
    }

    @Override
    public Object getContextMenu(MenuEventSource src, Menu activeMenu) {
        return null;
    }

    @Override
    public Object getBreadcrumb(ListOrderedMap crumbs) {
        KeyedMenuModel bread = new KeyedMenuModel();

        int breadcrumbSize = crumbs.size();
        for (int i = breadcrumbSize - 1; i >= 0; i--) {
            Menu appMenu = (Menu) crumbs.get(crumbs.get(i));
            KeyedMenuItem item = new KeyedMenuItem(appMenu.getName());
            if (appMenu.getView() == null || i == 0) {
                item.setStyle("text-decoration:none");
            } else {
                item.setKey(appMenu.getId());
                item.setImmediate(true);
                item.setParam("id", appMenu.getId());
                item.setCommand("#{menuBean.dispatch}");
            }
            bread.addElement(item);
        }

        return bread;
    }

    private MenuElement createContextMenu(MenuEventSource evtSource, Menu appMenu) {
        if (appMenu.getChildren().isEmpty()) {
            KeyedMenuItem menuItem = new KeyedMenuItem(appMenu.getName());
            menuItem.setImmediate(true);
            menuItem.setKey(appMenu.getId());
            menuItem.setParam("id", appMenu.getId());
            menuItem.setCommand("#{menuBean.dispatch}");
//            evtSource.addListener(menuItem);
            return menuItem;
        } else {
            DefaultMenuColumn col = new DefaultMenuColumn();
            
            KeyedSubMenu subMenu = new KeyedSubMenu(appMenu.getName());
            appMenu.getChildren().stream().forEach((child) -> {
                subMenu.addElement(createContextMenu(evtSource, child));
            });
            col.addElement(subMenu);
            return col;
        }
    }

}
