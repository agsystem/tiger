/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.pf;

import org.primefaces.model.menu.DefaultSubMenu;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class KeyedSubMenu extends DefaultSubMenu {
    
    private String key;

    public KeyedSubMenu() {
    }

    public KeyedSubMenu(String label) {
        super(label);
    }

    public KeyedSubMenu(String label, String icon) {
        super(label, icon);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
}
