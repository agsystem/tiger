/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.event;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public interface MenuEventSource {
    public void addListener(MenuEventListener listener);
    public void notifyListener(MenuEvent evt);
    public void cleanListener();
}
