/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.bean;

import net.ap57.tiger.base.entity.Gender;
import net.ap57.tiger.base.ui.pf.EnumDataList;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class GenderDataList extends EnumDataList<Gender> {

  public GenderDataList() {
    super(Gender.class);
  }

  @Override
  public Gender[] getEnumValues() {
    return Gender.values();
  }
    
}
