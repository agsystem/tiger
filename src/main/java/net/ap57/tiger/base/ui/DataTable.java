/*
 * Copyright 2013 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import net.ap57.tiger.base.api.AbstractFacade;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <F>
 * @param <E>
 */
public abstract class DataTable<F extends AbstractFacade, E extends Serializable> extends PageComponent {

    protected abstract F getOp();

    protected E newEntity() {
        return (E) getOp().createTransient(getNewEntityParams());
    }

    protected boolean loadModel() {
        return true;
    }

    //<editor-fold defaultstate="collapsed" desc="Model Management">
    public DataModel<E> getModel() {
        if (loadModel()) {
            return (DataModel<E>) get("model", createDataModel());
        } else {
            return null;
        }
    }

    public void setModel(DataModel<E> model) {
        setAttribute("model", model);
    }

    protected DataModel<E> createBlankDataModell() {
        return new ListDataModel(new ArrayList<>());
    }

    protected DataModel<E> createDataModel() {
        List<E> data = getOp().findAll(getLoadFilters());
        return new ListDataModel(data);
    }

    protected Map<String, Object> getNewEntityParams() {
        return null;
    }

    protected Map<String, Object> getLoadFilters() {
        return new HashMap<>();
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Row Selection Management">
    public E getSelection() {
        List selections = getSelections();
        if (selections.isEmpty()) {
            return null;
        } else {
            return (E) selections.get(0);
        }
    }

    public void setSelection(E selection) {
        getSelections().clear();
        getSelections().add(selection);
    }

    public List<E> getSelections() {
        List<E> obj = (List<E>) getAttribute("selections");

        if (obj == null) {
            obj = new ArrayList<>();
            setSelections(obj);
        }

        return obj;

    }

    public void setSelections(List<E> selections) {
        setAttribute("selections", selections);
    }

    public void prepareTransient(ActionEvent evt) {
        setSelection(newEntity());
    }

    public void prepareEdit(ActionEvent evt) {

    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Basic Operations">
    public void create(ActionEvent evt) {
        create((E) getSelection());
    }

    public void create(E e) {
        try {
            getOp().create(e);
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void createBulk(Collection<E> es) {
        try {
            getOp().create(es);
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public boolean isCreateContextEmpty() {
        return false;
    }

    public boolean isUpdateContextEmpty() {
        return ((getSelection() == null) && (getSelections() == null || (getSelections().size() != 1))) || (getSelection() != null && getOp().getId(getSelection()) == null);
    }

    public boolean isDeleteContextEmpty() {
        return (getSelection() == null && (getSelections() == null || getSelections().isEmpty())) || (getSelection() != null && getOp().getId(getSelection()) == null);
    }

    public void update(ActionEvent evt) {
        update((E) getSelection());
    }

    public void update(E e) {
        getOp().edit(e);
    }

    public void updateBulk(Collection<E> es) {
        getOp().edit(es);
    }

    public void delete(ActionEvent evt) {
        delete(getSelections());
        getSelections().clear();
    }

    protected void delete(Collection<E> es) {
        getOp().remove(es);
    }

    protected void delete(E e) {
        getOp().remove(e);
    }
//</editor-fold>

//  public String showCreate() {
//    return "create-" + getContainer().getView() + "?faces-redirect=true";
//  }
//
//  public String showEdit() {
//    return "edit-" + getContainer().getView() + "?faces-redirect=true";
//  }
//
//  public String home() {
//    return getContainer().getView() + "?faces-redirect=true";
//  }
}
