/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.pf;

import net.ap57.tiger.base.ui.Menu;
import net.ap57.tiger.base.ui.event.MenuEvent;
import net.ap57.tiger.base.ui.event.MenuEventListener;
import org.apache.commons.collections.map.ListOrderedMap;
import org.primefaces.model.menu.DefaultMenuItem;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class KeyedMenuItem extends DefaultMenuItem implements MenuEventListener {
    private String key;

    public KeyedMenuItem() {
    }

    public KeyedMenuItem(Object value) {
        super(value);
    }

    public KeyedMenuItem(Object value, String icon) {
        super(value, icon);
    }

    public KeyedMenuItem(Object value, String icon, String url) {
        super(value, icon, url);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public void menuClicked(MenuEvent evt) {
        Menu menu = evt.getSource();
        ListOrderedMap selections = (ListOrderedMap) evt.getParam("selections");
        if(selections.get(key) != null) {
            setDisabled(true);
        } else {            
            setDisabled(false);
        }
    }

    @Override
    public Object geId() {
        return getKey();
    }
    
    
}
