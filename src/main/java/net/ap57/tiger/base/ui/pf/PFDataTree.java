/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import net.ap57.tiger.base.api.AbstractTreeFacade;
import net.ap57.tiger.base.api.HierarchicalEntity;
import net.ap57.tiger.base.ui.DataTable;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.TreeTableModel;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <F>
 * @param <E>
 */
public abstract class PFDataTree<F extends AbstractTreeFacade, E extends HierarchicalEntity> extends DataTable<F, E> {

    public TreeNode getRoot() {
        return (TreeNode) getAttribute("root");
    }

    public void setRoot(TreeNode root) {
        setAttribute("root", root);
    }

    public List<TreeNode> getNodeSelections() {
        return (List<TreeNode>) getAttribute("nodeSelections");
    }

    public void setNodeSelections(List<TreeNode> nodeSelections) {
        setAttribute("nodeSelections", nodeSelections);
    }

    public TreeNode[] getArrNodeSelections() {
        List<TreeNode> selections = getNodeSelections();
        if (selections != null) {
            return (TreeNode[]) selections.toArray();
        } else {
            return null;
        }
    }

    public void setArrNodeSelections(TreeNode[] nodeSelections) {
        if (nodeSelections != null) {
            setNodeSelections(Arrays.asList(nodeSelections));
        }
    }

    public TreeNode getNodeSelection() {
        return (TreeNode) getAttribute("nodeSelection");
    }

    public void setNodeSelection(TreeNode nodeSelection) {
        setAttribute("nodeSelection", nodeSelection);
    }

    public TreeNode getCutNode() {
        return (TreeNode) getAttribute("cutNode");
    }

    public void setCutNode(TreeNode cutNode) {
        setAttribute("cutNode", cutNode);
    }

    protected List<E> getFirstAncestors() {
        return getOp().getAdam();
    }

    protected void trace(TreeNode parent) {
        E data = (E) parent.getData();
//    System.out.println("TRACE = " + parent.getData());
        List<E> children = getOp().getChildren(data); //data.getChildren();
        if (children != null && !children.isEmpty()) {
//      System.out.println("TRACE CHILDREN = " + children.size());
            for (E child : children) {
                TreeNode node = new DefaultTreeNode(getNodeType((E) child), child, parent);
                node.setSelected(false);
//                child.setWrapper(node);
//        System.out.println("TRACE = " + node.getData());
                trace(node);
            }
        }
    }

    protected String getNodeType(E data) {
        List<HierarchicalEntity> children = data.getChildren();
        if (children != null && !children.isEmpty()) {
            return "folder";
        } else {
            return "document";
        }
    }

    @Override
    protected DataModel<E> createDataModel() {

        if (getRoot() == null) {
            setRoot(new DefaultTreeNode("root", null));

//    TreeNode root = new DefaultTreeNode("root", null);
            for (E ap : getFirstAncestors()) {
                TreeNode parent = new DefaultTreeNode(getNodeType(ap), ap, getRoot());
//      TreeNode parent = new DefaultTreeNode(getNodeType(ap), ap, root);
                parent.setExpanded(true);
//                ap.setWrapper(parent);
                trace(parent);
            }
        }

//    System.out.println("RUTE = " + getRoot().getData());
        TreeTableModel treeTableModel = new TreeTableModel(getRoot());
        return treeTableModel;

//    return new TreeTableModel(root);
    }

    @Override
    public void prepareTransient(ActionEvent evt) {
        super.prepareTransient(evt);
        if (getNodeSelection() != null) {
            E parent = (E) getNodeSelection().getData();
            ((E) getSelection()).<E>setParent(parent);
            parent.getChildren().add((E) getSelection());
        }
        clearCutNode(null);
    }

    public void clearCutNode(ActionEvent evt) {
        setCutNode(null);
    }

    @Override
    protected E newEntity() {
        Map<String, Object> params = new HashMap<>();

        E parent = (E) getSelection();
        
        if (parent != null) {
            params.put("parent", parent);
        }

        return (E) getOp().createTransient(params);
    }

    @Override
    public void create(E e) {
        super.create(e);
        createNode(e, getNodeSelection());
    }

    protected void createNode(E data, TreeNode parent) {

        if (parent == null) {
//      parent = new DefaultTreeNode("root", null);
            parent = getRoot();
//      System.out.println("RUTE CREATE = " + parent);
        }
//
//    for (TreeNode child : parent.getChildren()) {
//      System.out.println("ANAKE SEBELUM = " + child.getData());
//    }

        TreeNode node = new DefaultTreeNode(getNodeType(data), data, parent);

//        data.setWrapper(node);
        setNodeSelection(node);
        node.setSelected(true);
        node.getParent().setExpanded(true);
        node.getParent().setSelected(false);

//    for (TreeNode child : parent.getChildren()) {
//      System.out.println("ANAKE SESUDAH = " + child.getData());
//    }
    }

    @Override
    public void delete(ActionEvent evt) {
        deleteNode(getNodeSelection());
        delete((E) getSelection());
//        getContext().getSelections().clear();
    }

    @Override
    public void delete(E e) {
        super.delete(e);
    }

    public void deleteNode(TreeNode node) {
        TreeNode parent = node.getParent();
        parent.getChildren().remove(node);
        setNodeSelection(null);
        parent.setExpanded(true);
    }

    public void cutAction(ActionEvent evt) {
        setCutNode(getNodeSelection());
    }

    public void cutAction() {
        setCutNode(getNodeSelection());
    }

    protected boolean isDescendant(TreeNode node, TreeNode ancestor) {
        if (node == null) {
            return false;
        }
        TreeNode parent = node.getParent();
        if (!ancestor.equals(parent)) {
            return isDescendant(parent, ancestor);
        } else {
            return true;
        }
    }

    public void pasteAction(ActionEvent evt) {

        TreeNode cut = getCutNode();
        TreeNode pasted = getNodeSelection();

        if (!cut.equals(pasted) && !isDescendant(pasted, cut)) {

            E oldParent = (E) getCutNode().getParent().getData();
            E newParent = (E) getSelection();

            getOp().paste((E) getCutNode().getData(), oldParent, newParent);

//      TreeNode oldParentNode = (TreeNode) oldParent.getWrapper();
            TreeNode oldParentNode = getCutNode().getParent();
//            TreeNode newParentNode = (TreeNode) newParent.getWrapper();
            TreeNode newParentNode = getNodeSelection();

            oldParentNode.getChildren().remove(getCutNode());
            getCutNode().setParent(newParentNode);
            newParentNode.getChildren().add(getCutNode());
            newParentNode.setExpanded(true);
            newParentNode.setSelected(false);
            getCutNode().setSelected(true);
            setNodeSelection(getCutNode());
            setSelection((E) getCutNode().getData());

            clearCutNode(null);
        }

    }

    public void pasteAction() {
        pasteAction(null);
    }

    @Override
    public boolean isCreateContextEmpty() {
        return true;
    }

    public boolean isPasteContextEmpty() {
        return getCutNode() == null;
//        return isCreateContextEmpty() || getCutNode() == null
//                || !getCutNode().getData().getClass().equals(getSelection().getClass())
//                || getCutNode().getData().equals(getSelection()) ;
    }

//    @Override
    public void handleContextMenu(AjaxBehaviorEvent event) {
        setSelection((E) getNodeSelection().getData());
    }

    public void handleNodeSelected(NodeSelectEvent evt) {
        setNodeSelection(evt.getTreeNode());
        setSelection((E) (getNodeSelection().getData()));
    }

    public void handleNodeUnselected(NodeUnselectEvent evt) {

    }

}
