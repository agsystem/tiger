/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class PageComponent {

    private String id;

    private Page container;

    private PageComponentCtx context;

    public void init() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Page getContainer() {
        return container;
    }

    public void setContainer(Page container) {
        this.container = container;
    }

    public void setContext(PageComponentCtx context) {
        this.context = context;
    }

    public PageComponentCtx getContext() {
        if (context == null) {
            context = new PageComponentCtx();
        }
        return context;
    }

    public void clearContext() {
        getContext().getLookup().clear();
    }

    public Object get(String key, Object def) {
        PageComponentCtx ctx = getContext();
        Object attr = getContext().getLookup().putIfAbsent(key, def);
        return attr != null ? attr : def;
    }

    public Object getAttribute(String key) {
        return getContext().getLookup().get(key);
    }

    public Object getAttribute(String key, Object defaultValue) {
        Object val = getContext().getLookup().get(key);
        if (val == null) {
            setAttribute(key, defaultValue);
            return defaultValue;
        } else {
            return val;
        }
    }

    public void setAttribute(String key, Object value) {
        getContext().getLookup().put(key, value);
    }

//    public String home() {
//        return getContainer().getView() + "?faces-redirect=true";
//    }

    public void destroy() {
    }

}
