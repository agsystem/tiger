/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import javax.faces.event.ActionEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class PageForm extends PageComponent {

    public void submit(ActionEvent evt) {
    }

    public void reset(ActionEvent evt) {
        destroy();
    }

    @Override
    public void destroy() {
        clearContext();
    }

}
