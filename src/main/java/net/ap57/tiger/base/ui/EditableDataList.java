/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import net.ap57.tiger.base.api.AbstractFacade;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 * @param <F>
 * @param <E>
 */
public abstract class EditableDataList<F extends AbstractFacade, E extends Serializable> extends DataList<F, E> {

  protected abstract E newPlaceholder();

  @Override
  public List<E> getData() {
    List<E> result = getOp().findAll();
    result.add(newPlaceholder());
    return result;
  }

  @Override
  public List<SelectItem> getTypeList() {
    List<E> types = getData();
    types.remove(types.size() - 1);
    List<SelectItem> si = new ArrayList<>();
    si.add(new SelectItem(null, "All"));
    for (E type : types) {
      si.add(new SelectItem(type, type.toString()));
    }
    return si;
  }

  protected Map<String, Object> getNewEntityParams() {
    return null;
  }

  protected E newEntity() {
    return (E) getOp().createTransient(getNewEntityParams());
  }

  public E getSelection() {
    E e = (E) getAttribute("selection");
    if (e == null) {
      setAttribute("selection", newEntity());
      e = (E) getAttribute("selection");
    }
    return e;
  }

  public void create(ActionEvent evt) {
    create(getSelection());
    setAttribute("selection", null);
  }

  public void create(E entity) {
    try {
      getOp().create(entity);
    } catch (Exception ex) {
      FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage());
      FacesContext.getCurrentInstance().addMessage(null, message);
    }
  }

}
