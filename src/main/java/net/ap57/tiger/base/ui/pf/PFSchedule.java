/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import net.ap57.tiger.base.UserSession;
import net.ap57.tiger.base.api.AbstractScheduleFacade;
import net.ap57.tiger.base.api.ScheduledEvent;
import net.ap57.tiger.base.ui.PageComponent;
import org.primefaces.component.schedule.Schedule;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <D>
 * @param <F>
 */
public abstract class PFSchedule<D extends ScheduledEvent, F extends AbstractScheduleFacade>
        extends PageComponent {

    protected Schedule schedule;

    public abstract F getOp();

    protected abstract UserSession getSession();

    protected PFScheduleEvent<D> newEvent(D entity) {
        return new PFScheduleEvent(entity);
    }

    protected D newEntity() {
        return (D) getOp().createTransient(getNewEntityParams());
    }

    protected Map<String, Object> getLoadFilters() {
        return new HashMap<>();
    }

    public void onPrevWeek(ActionEvent evt) {
        long prevFrom = ((Date) getAttribute("lastFrom")).getTime();
        setAttribute("lastFrom", new Date(prevFrom - (7 * 24 * 60 * 60 * 1000)));

        long prevThru = ((Date) getAttribute("lastThru")).getTime();
        setAttribute("lastThru", new Date(prevThru - (7 * 24 * 60 * 60 * 1000)));

        schedule.setInitialDate(getAttribute("lastFrom"));
    }

    public void onNextWeek(ActionEvent evt) {
        long prevFrom = ((Date) getAttribute("lastFrom")).getTime();
        setAttribute("lastFrom", new Date(prevFrom + (7 * 24 * 60 * 60 * 1000)));

        long prevThru = ((Date) getAttribute("lastThru")).getTime();
        setAttribute("lastThru", new Date(prevThru + (7 * 24 * 60 * 60 * 1000)));

        schedule.setInitialDate(getAttribute("lastFrom"));

        schedule.setLocale(new Locale(getSession().getLocale()));
    }

    public Schedule getSchedule() {
        return schedule; //(Schedule) getAttribute("schedule");
    }

    public void setSchedule(Schedule schedule) {
        //setAttribute("schedule", schedule);
        this.schedule = schedule;
    }

    public ScheduleModel getModel() {

        if (getAttribute("model") == null) {
            setAttribute("model", new LazyScheduleModel() {

                @Override
                public void loadEvents(Date from, Date thru) {

                    if (getAttribute("loaded") == null) {
                        setAttribute("loaded", new Object());
                        setAttribute("lastFrom", from);
                        setAttribute("lastThru", thru);
                    }

                    List<PFScheduleEvent> events = collectEvents((Date) getAttribute("lastFrom"), (Date) getAttribute("lastThru"), getLoadFilters());

                    for (PFScheduleEvent evt : events) {
//                        addEvent(newEvent(evt));
                        addEvent(evt);
                    }

//                    schedule.setLocale(new Locale(getSession().getLocale()));
//                    System.out.println("LOAD EVENTS FROM: " + getAttribute("lastFrom") + ", THRU " + getAttribute("lastThru"));
                }

            });

        }

        return (ScheduleModel) getAttribute("model");

    }

    protected List<PFScheduleEvent> collectEvents(Date from, Date thru, Map<String, Object> filters) {
        List<D> rawEvents = getOp().getEvents(from, thru, filters);
        List<PFScheduleEvent> events = new ArrayList<>();

        for (D rawEvent : rawEvents) {
            events.add(newEvent(rawEvent));
        }

        return events;
    }

    public void setModel(ScheduleModel model) {
        setAttribute("model", model);
    }

    public PFScheduleEvent<D> getSelection() {
        return (PFScheduleEvent) getAttribute("selection");
    }

    public void setSelection(PFScheduleEvent<D> selection) {
        setAttribute("selection", selection);
    }

    public Date getSelectedDate() {
        return (Date) getAttribute("selectedDate");
    }

    public void setSelectedDate(Date selectedDate) {
        setAttribute("selectedDate", selectedDate);
    }

    protected Map<String, Object> getNewEntityParams() {
        return new HashMap<>();
    }

    public void prepareTransient(ActionEvent evt) {
        setSelection(newEvent(newEntity()));
    }

    public void onEventSelect(SelectEvent selectEvent) {
        setSelection((PFScheduleEvent<D>) selectEvent.getObject());
    }

    public void onDateSelect(SelectEvent e) {
        Date date = (Date) e.getObject();
        onDateSelect(date);
    }

    public void onDateSelect(Date date) {
        setSelectedDate(date);
        prepareTransient(null);
        PFScheduleEvent<D> scheduleEvent = getSelection();
        scheduleEvent.setStartDate(date);
        scheduleEvent.setEndDate(date);

    }

    public void onViewChange(SelectEvent e) {

    }

//  public void onEventMove(ScheduleEntryMoveEvent evt) {
//    setSelection((PFScheduleEvent<D>) evt.getScheduleEvent());
//    D event = (D) getSelection().getEvent();
//    Calendar from = Calendar.getInstance();
//    from.setTime(event.getFromDate());
//    event.setFromDate(from.getTime());
//    Calendar thru = Calendar.getInstance();
//    thru.setTime(event.getThruDate());
//    event.setThruDate(thru.getTime());
//    update(event);
//  }
//
//  public void onEventResize(ScheduleEntryResizeEvent evt) {
//    setSelection((PFScheduleEvent) evt.getScheduleEvent());
//    D event = (D) getSelection().getEvent();
//    Calendar thru = Calendar.getInstance();
//    thru.setTime(event.getThruDate());
//    event.setThruDate(thru.getTime());
//    update(event);
//  }
    public void create(ActionEvent evt) {
        create(getSelection().getEvent());
        getModel().addEvent(getSelection());
    }

    public void create(D event) {
        try {
            getOp().create(event);
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Saving Event", ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void update(ActionEvent evt) {
        update(getSelection().getEvent());
    }

    public void update(D event) {
        getOp().edit(event);
    }

    public void delete(ActionEvent actionEvent) {
        delete(getSelection().getEvent());
    }

    public void delete(D event) {
        getOp().remove(event);
    }

}
