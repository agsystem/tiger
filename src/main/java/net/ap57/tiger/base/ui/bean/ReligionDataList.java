/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui.bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import net.ap57.tiger.base.entity.Religion;
import net.ap57.tiger.base.ui.pf.EnumDataList;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value = "religionDataList")
@RequestScoped
public class ReligionDataList extends EnumDataList<Religion> {

  public ReligionDataList() {
    super(Religion.class);
  }

  @Override
  public Religion[] getEnumValues() {
    return Religion.values();
  }

}
