/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class PageComponentCtx implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private final Map<String, Object> lookup;
    
    public PageComponentCtx() {
        lookup = new HashMap<>();
    }
    
    public Map<String, Object> getLookup() {
        return lookup;
    }
   
}
