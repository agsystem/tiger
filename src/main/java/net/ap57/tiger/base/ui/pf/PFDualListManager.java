/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.ui.pf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.ap57.tiger.base.api.AbstractFacade;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <F>
 * @param <T>
 */
public abstract class PFDualListManager<F extends AbstractFacade, T extends Serializable> {

    protected abstract F getOp();

    protected abstract List<T> getExisting();

    protected boolean isEquals(T t1, T t2) {
        return t1.equals(t2);
    }

    protected DualListModel<T> createModel() {
        List<T> src = getOp().findAll();
        List<T> target = getExisting();
        for (T g1 : target) {
            for (T g2 : src) {
                if (isEquals(g2, g1)) {
                    src.remove(g2);
                    break;
                }
            }
        }

        return new DualListModel<>(src, target);
    }

    public DualListModel<T> getModel() {
        return createModel();
    }

    public void setModel(DualListModel<T> groupModel) {
    }

    public void onDuaListTransfer(TransferEvent evt) {
        List<T> items = (List<T>) evt.getItems();
        if (evt.isAdd()) {
            addItems(items); 
        } else if (evt.isRemove()) {
            delItems(items);  
        }
    }

    protected void addItems(List<T> orgs) {
        List<T> existingItems = getExisting();
        existingItems.addAll(orgs);
    }

    protected void delItems(List<T> groups) {
        List<T> remUserGroups = new ArrayList<>();
        List<T> existingItems = getExisting();
        for (T userGroup : existingItems) {
            if (groups.contains(userGroup)) {
                remUserGroups.add(userGroup);
            }
        }

        for (T userGroup : remUserGroups) {
            existingItems.remove(userGroup);
        }
    }

}
