/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.ui;

import net.ap57.tiger.base.ui.event.PageChangeEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.inject.Named;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named
@SessionScoped
public class PageCtx implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Map<String, PageComponent> lookupComponents;
    private final Map<String, Object> attributes;

    private String explorerTitle;
    private String explorerWidth;
    private boolean explorerVisibility;

    private String editorTitle;
    private String editorWidth;
    private boolean editorVisibility;

    private String propertiesTitle;
    private String propertiesWidth;
    private boolean propertiesVisibility;

    public PageCtx() {
        lookupComponents = new HashMap<>();
        attributes = new HashMap<>();
    }

    public Map<String, PageComponent> getLookupComponents() {
        return lookupComponents;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public <T extends Object> T getAttribute(String key) {
        return (T) attributes.get(key);
    }

    public void setAttribute(String key, Object value) {
        attributes.put(key, value);
    }

    public void clear(@Observes PageChangeEvent evt) {
        lookupComponents.clear();
        attributes.clear();
    }

    public String getExplorerTitle() {
        return explorerTitle != null ? explorerTitle : "Explorer";
    }

    public void setExplorerTitle(String explorerTitle) {
        this.explorerTitle = explorerTitle;
    }

    public String getEditorTitle() {
        return editorTitle != null ? editorTitle : "Editor";
    }

    public void setEditorTitle(String editorTitle) {
        this.editorTitle = editorTitle;
    }

    public String getPropertiesTitle() {
        return propertiesTitle != null ? propertiesTitle : "Properties";
    }

    public void setPropertiesTitle(String propertiesTitle) {
        this.propertiesTitle = propertiesTitle;
    }

    public String getExplorerWidth() {
        return explorerWidth;
    }

    public void setExplorerWidth(String explorerWidth) {
        this.explorerWidth = explorerWidth;
    }

    public String getEditorWidth() {
        return editorWidth;
    }

    public void setEditorWidth(String editorWidth) {
        this.editorWidth = editorWidth;
    }

    public String getPropertiesWidth() {
        return propertiesWidth;
    }

    public void setPropertiesWidth(String propertiesWidth) {
        this.propertiesWidth = propertiesWidth;
    }

    public boolean isExplorerVisibility() {
        return explorerVisibility;
    }

    public void setExplorerVisibility(boolean explorerVisibility) {
        this.explorerVisibility = explorerVisibility;
    }

    public boolean isEditorVisibility() {
        return editorVisibility;
    }

    public void setEditorVisibility(boolean editorVisibility) {
        this.editorVisibility = editorVisibility;
    }

    public boolean isPropertiesVisibility() {
        return propertiesVisibility;
    }

    public void setPropertiesVisibility(boolean propertiesVisibility) {
        this.propertiesVisibility = propertiesVisibility;
    }

    public DataTable getConfigTable() {
        return getAttribute("configTable");
    }

}
