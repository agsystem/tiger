/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import net.ap57.tiger.base.ui.Menu;
import net.ap57.tiger.base.ui.MenuCache;
import net.ap57.tiger.base.util.XmlElementEvent;
import net.ap57.tiger.base.util.XmlWalker;

/**
 * Web application lifecycle listener.
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class UILoader implements ServletContextListener {

  @Inject
  protected MenuCache cache;

  private Menu menubar;
  private Menu submenu;
  private Menu menuitem;

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    loadMenu(sce.getServletContext());
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {

  }

  public void loadMenu(ServletContext context) {
    InputStream input = context.getResourceAsStream("/WEB-INF/menu.xml");
    if (input != null) {
      new XmlWalker() {

        @Override
        public void processStartElement(XmlElementEvent evt) {
          startProcessElement(evt);
        }

        @Override
        public void processEndElement(XmlElementEvent evt) {
          finishProcessElement(evt);
        }

      }.walk(input);
    }
  }

  protected void startProcessElement(XmlElementEvent xmlEvent) {
    switch (xmlEvent.getValue()) {
      case "menubar":
        Menu root = cache.findByTarget("");
        Menu newMenubar = new Menu(xmlEvent.getAttribute("name"), xmlEvent.getAttribute("folder"), xmlEvent.getAttribute("view"), root);
//        newMenubar.setMenuSeq(Integer.parseInt(xmlEvent.getAttribute("pos")));
        root.getChildren().add(newMenubar);
        cache.addItem(newMenubar);
        menubar = newMenubar;
        break;
      case "submenu":
        Menu newSubmenu = new Menu(xmlEvent.getAttribute("name"), xmlEvent.getAttribute("folder"), null, null);
        if (submenu == null) {
          newSubmenu.setParent(menubar);
          menubar.getChildren().add(newSubmenu);
        } else {
          newSubmenu.setParent(submenu);
          submenu.getChildren().add(newSubmenu);
        }
        cache.addItem(newSubmenu);
        submenu = newSubmenu;
        break;
      case "menuitem":
        menuitem = new Menu(xmlEvent.getAttribute("name"), xmlEvent.getAttribute("folder"), xmlEvent.getAttribute("view"), null);
        menuitem.setParent(submenu == null ? menubar : submenu);
        if (submenu == null) {
          menuitem.setParent(menubar);
          menubar.getChildren().add(menuitem);
        } else {
          menuitem.setParent(submenu);
          submenu.getChildren().add(menuitem);
        }
        String backingBeanName = xmlEvent.getAttribute("backing");
        if(backingBeanName != null && !backingBeanName.isEmpty()) {
            menuitem.setBacking(backingBeanName);
        }
        cache.addItem(menuitem);
        break;
    }
  }

  protected void finishProcessElement(XmlElementEvent xmlEvent) {
    switch (xmlEvent.getValue()) {
      case "menubar":
        try {
          menubar = null;
          submenu = null;
          menuitem = null;
        } catch (Exception ex) {
          Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        break;
      case "submenu":
        submenu = submenu.getParent().getParent() == null ? null : submenu.getParent();
        menuitem = null;
        break;
      case "menuitem":
        menuitem = null;
        break;
    }
  }
}
