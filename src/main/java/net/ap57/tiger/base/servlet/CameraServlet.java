/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@WebServlet(name = "CameraServlet", urlPatterns = {"/CameraServlet/*"})
public class CameraServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        byte[] data = (byte[]) request.getSession().getAttribute("capturedImage");
        if (data != null) {
//            ImageIcon imageIcon = new ImageIcon(data);
//            Image inImage = imageIcon.getImage();
//            int width = inImage.getWidth(null);
//            int height = inImage.getHeight(null);
//
//            BufferedImage outImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//
//            Graphics2D g2d = outImage.createGraphics();
//            g2d.drawImage(inImage, null, null);
//            g2d.dispose();

            try (OutputStream out = response.getOutputStream()) {
//                boolean res = ImageIO.write(outImage, "JPG", out);
                out.write(data);
            } catch (IOException ex) {
                Logger.getLogger(CameraServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
