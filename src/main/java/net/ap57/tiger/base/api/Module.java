/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import net.ap57.tiger.base.Application;
import net.ap57.tiger.base.config.Config;
import net.ap57.tiger.base.config.ConfigFacade;
import net.ap57.tiger.base.dao.ScriptFacade;
import net.ap57.tiger.base.entity.Script;
import net.ap57.tiger.base.event.FrameworkReadyEvent;
import net.ap57.tiger.base.event.ModuleEvent;
import net.ap57.tiger.base.event.ModuleStatus;
import net.ap57.tiger.base.reporting.ReportMap;
import net.ap57.tiger.base.reporting.entity.ReportTemplate;
import net.ap57.tiger.base.reporting.entity.ReportTemplatePool;
import net.ap57.tiger.base.reporting.event.ReportTemplateAddedEvent;
import net.ap57.tiger.base.reporting.event.ReportTemplateFoundEvent;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class Module {

    protected ModuleStatus status;

    protected String moduleName;

    @Inject
    protected Configuration configuration;

    protected ReportMap reportMap;

    @Inject
    private Application theApp;

    @Inject
    private ConfigFacade configFacade;

    @Inject
    private ScriptFacade scriptFacade;

    @Inject
    protected Event<ModuleEvent> moduleEvent;

    @Inject
    @ReportTemplateFoundEvent
    private Event<ReportTemplate> reportTemplateFoundEvent;

    protected void boot() {

//        moduleName = getClass().getDeclaredAnnotation(AppModule.class).name();
        moduleName = getClass().getPackage().getName();
        reportMap = new ReportMap(this, new HashMap<>());

    }

    @Asynchronous
    public void load(@Observes @FrameworkReadyEvent Application application) {
        try {
            status = ModuleStatus.NOT_READY;
            boot();
            loadConfig();
            loadScript();
            loadReport();
            init();
            theApp.register(this);
            status = ModuleStatus.READY;
        } catch (Exception ex) {

        } finally {
            moduleEvent.fire(new ModuleEvent(moduleName, status, application.getConfiguration(), getConfiguration()));
        }
    }

    protected void loadConfig() {
        getClass().getResourceAsStream("/" + getClass().getPackage().getName().replaceAll("\\.", "/") + "/config/config.json");

        InputStream in = getClass().getResourceAsStream("/" + moduleName.replaceAll("\\.", "/") + "/config/config.json");
        if (in != null) {
            JsonObject jsonConfigurations;
            try (JsonReader jsonReader = Json.createReader(in)) {
                jsonConfigurations = jsonReader.readObject();
            }

            JsonArray jsonArray = jsonConfigurations.getJsonArray("configurations");

            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonConfiguration = jsonArray.getJsonObject(i);
                processConfig(jsonConfiguration);
            }

        }

    }

    protected void loadScript() {
        URL url = getClass().getResource("/" + moduleName.replaceAll("\\.", "/") + "/script");
        if (url != null) {
            File dir = new File(url.getFile());
            if (dir.isDirectory()) {
                for (File script : dir.listFiles()) {
                    processScript(script);
                }
            }
        }
    }

    protected void loadReport() {

        URL url = getClass().getResource("/" + moduleName.replaceAll("\\.", "/") + "/report");
        if (url != null) {
            File dir = new File(url.getFile());
            if (dir.isDirectory()) {
                for (File tpl : dir.listFiles()) {
                    ReportTemplate template = new ReportTemplate();
                    template.setBundle(moduleName);
                    template.setName(tpl.getName());
                    reportTemplateFoundEvent.fire(template);
                }
            }
        }
    }

    protected void processConfig(JsonObject jsonConfiguration) {

        String key = getModuleName() + "." + jsonConfiguration.getString("name");
        if (!getConfiguration().containsKey(key)) {
            Config cfg = new Config();
            cfg.setName(key);
            cfg.setType(jsonConfiguration.getString("type"));
            cfg.setValue(jsonConfiguration.getString("default"));
            cfg.setHumanName(jsonConfiguration.getString("humanName"));
            cfg.setRemarks(jsonConfiguration.getString("remarks"));
            try {
                configFacade.create(cfg);
            } catch (Exception ex) {
                Logger.getLogger(Module.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void processScript(File scriptFile) {

        String scriptName = getModuleName() + "." + scriptFile.getName();

        if (scriptFacade.find(scriptName) == null) {
            Script script = new Script();
            script.setName(scriptName);
            try (BufferedReader reader = new BufferedReader(new FileReader(scriptFile))) {
                StringWriter writer = new StringWriter();
                int i;
                while ((i = reader.read()) != -1) {
                    writer.append((char) i);
                }
                writer.flush();
                script.setContent(writer.toString());
                Calendar lastMod = Calendar.getInstance();
                lastMod.setTimeInMillis(scriptFile.lastModified());
                scriptFacade.create(script);
            } catch (IOException ex) {
                Logger.getLogger(Module.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

//  @Asynchronous
    public void mapReport(@Observes @ReportTemplateAddedEvent ReportTemplatePool reportTemplatePool) {
        getReportMap().put(reportTemplatePool.getName());
    }

    protected void init() throws Exception {
    }

    public String getModuleName() {
        return moduleName;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public ReportMap getReportMap() {
        return reportMap;
    }

    public InputStream getResourceAsStream(String resource) {
        return getClass().getResourceAsStream("/" + moduleName.replaceAll("\\.", "/") + "/" + resource);
    }

    public URL getResource(String resource) {
        return getClass().getResource("/" + moduleName.replaceAll("\\.", "/") + "/" + resource);
    }

}
