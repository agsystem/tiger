/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <T>
 */
public abstract class AbstractTreeFacade<T extends HierarchicalEntity> extends AbstractFacade<T> {

    public AbstractTreeFacade(Class<T> entityClass) {
        super(entityClass);
    }

    public abstract T createTransientBasic(Map<String, Object> params);

    @Override
    public T createTransient(Map<String, Object> params) {
        
        Object parent = params.get("parent");
        
        T entity = createTransientBasic(params);
        
        entity.setParent((T) parent);
        entity.setChildren(new ArrayList<T>());
        
        return entity;

    }

    @Override
    public void create(T entity) throws EJBException {

        super.create(entity);
        
        getEntityManager().flush();

        T parent = getFirstAncestor(entity);
        
        if (parent != null && !parent.equals(entity)) {
            parent = getEntityManager().find(entityClass, getId(parent));
            getEntityManager().refresh(parent);
        }

    }

    @Override
    public void remove(T entity) {
        entity = getEntityManager().merge(entity);
        T parent = (T) entity.getParent();
        if (parent != null) {
            parent.getChildren().remove(entity);
        }
        getEntityManager().remove(entity);
    }

    public List<T> getAdam() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> root = cq.from(entityClass);
        cq.select(root);
        cq.where(cb.isNull(root.get("parent")));
        cq = orderQuery(root, cq);
        TypedQuery<T> query = getEntityManager().createQuery(cq);

        return query.getResultList();
    }

    protected CriteriaQuery<T> orderQuery(Root<T> root, CriteriaQuery<T> noOrderQuery) {
        return noOrderQuery;
    }

    public T getFirstAncestor(T e) {
        T parent = (T) e.getParent();
        if (parent != null) {
            return getFirstAncestor(parent);
        } else {
            return e;
        }
    }

    public void trace(T data, ITraceCallback callback) {
        List<? extends HierarchicalEntity> children = data.getChildren();
        if (children != null) {
            for (HierarchicalEntity child : children) {
                callback.doAction((T) child);
                trace((T) child, callback);
            }
        }
    }

    public void paste(T child, T oldParent, T newParent) {
        oldParent = getEntityManager().merge(oldParent);
        newParent = getEntityManager().merge(newParent);
        child.setParent(newParent);
        preSavingPastedChild(child);
        newParent.getChildren().add(child);
        oldParent.getChildren().remove(child);
        edit(newParent);
        edit(oldParent);
    }

    public void preSavingPastedChild(T child) {
    }

    public List<T> getChildren(T parent) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> root = cq.from(entityClass);
        cq.select(root).where(cb.equal(root.get("parent"), parent));
        
        cq = orderQuery(root, cq);
//        cq.orderBy(cb.asc(root.get("code")));

        Query q = getEntityManager().createQuery(cq);

        getEntityManager().flush();

        return q.getResultList();

    }

}
