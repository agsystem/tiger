/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <T>
 */
public abstract class AbstractScheduleFacade<T extends ScheduledEvent> extends AbstractFacade<T> {

  public AbstractScheduleFacade(Class<T> entityClass) {
    super(entityClass);
  }

  @Override
  protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<T> root) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();
    switch (filterName) {
      case "from":
        Expression<Date> from = cb.literal((Date) filterValue);
        predicates.add(cb.greaterThanOrEqualTo(root.<Date>get("from"), from));
        break;
      case "thru":
        Expression<Date> thru = cb.literal((Date) filterValue);
        predicates.add(cb.lessThanOrEqualTo(root.<Date>get("thru"), thru));
        break;
      default:
        predicates.addAll(addScheduleFilter(filterName, filterValue, root));
        break;
    }

    return predicates;
  }

  protected List<Predicate> addScheduleFilter(String filterName, Object filterValue, Root<T> root) {
    List<Predicate> predicates = new ArrayList<>();

    return predicates;
  }

  public List<T> getEvents(Date from, Date thru, Map<String, Object> filters) {

    filters.put("fromDate", from);
    filters.put("thruDate", thru);

    return findAll(filters);
  }

  @Override
  public void edit(T entity) {
    getEntityManager().refresh(getEntityManager().merge(entity));
    Date from = entity.getFromDate();
    Date thru = entity.getThruDate();
    entity.setFromDate(from);
    entity.setThruDate(thru);
    entity = getEntityManager().merge(entity);
  }

}
