/*
 * Copyright 2013 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <T>
 */
public abstract class AbstractPickerFacade<T> extends AbstractFacade<T> {

    public AbstractPickerFacade(Class<T> entityClass) {
        super(entityClass);
    }

    protected CriteriaQuery constructFindQuery(List<T> subtractor, Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);

        Root<T> root = cq.from(entityClass);
        cq.select(root);
        addCriteria(root, cq);

        List<Predicate> predicates = new ArrayList<>();

        if (!subtractor.isEmpty()) {
            Predicate ex = root.in(subtractor).not();
            predicates.add(ex);
        }

        if (filters != null && !filters.isEmpty()) {
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "name":
                        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                        predicates.add(cb.like(cb.upper(root.<String>get(filterName)), literal));
                        break;
                    default:
                        predicates.addAll(addPreFilter(filterName, filterValue, root));
                        break;
                }
            }
        }

        cq.where(predicates.toArray(new Predicate[predicates.size()]));

        return cq;
    }

    protected CriteriaQuery constructCountQuery(List<T> subtractor, Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<T> root = cq.from(entityClass);
        cq.select(cb.count(root));

        List<Predicate> predicates = new ArrayList<>();

        if (!subtractor.isEmpty()) {
            Predicate ex = root.in(subtractor).not();
//            Predicate ex = root.get("id").in(subtractee).not();
            predicates.add(ex);
        }

        if (filters != null && !filters.isEmpty()) {
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "name":
                        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                        predicates.add(cb.like(cb.upper(root.<String>get(filterName)), literal));
                        break;
                    default:
                        predicates.addAll(addPreFilter(filterName, filterValue, root));
                        break;
                }
            }
        }

        cq.where(predicates.toArray(new Predicate[predicates.size()]));

        return cq;
    }

    public List<T> findAll(List<T> subtractor, Map<String, Object> filters) {
        CriteriaQuery cq = constructFindQuery(subtractor, filters);
        Query q = getEntityManager().createQuery(cq);
        return postAction(q.getResultList());
    }

    public List<T> findAll(List<T> subtractor, int startPosition, int maxResult, Map<String, Object> filters) {
        CriteriaQuery cq = constructFindQuery(subtractor, filters);
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(maxResult);
        q.setFirstResult(startPosition);
        List<T> r = postAction(q.getResultList());
        return r;
    }

    public Long countAll(List<T> subtractor, Map<String, Object> filters) {
        CriteriaQuery cq = constructCountQuery(subtractor, filters);
        Query q = getEntityManager().createQuery(cq);
        Long c = (Long) q.getSingleResult();
        filters.clear();
        return c;
    }

}
