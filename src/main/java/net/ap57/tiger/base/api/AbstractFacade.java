/*
 * Copyright 2013 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.entity.StringKeyedEntity;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <T>
 */
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public abstract class AbstractFacade<T> {

    protected Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    //<editor-fold defaultstate="collapsed" desc="Low Level">
    public abstract T createTransient(Map<String, Object> params) throws EJBException;

    public abstract Object getId(T entity);

    public T getObj(String strKey) {
        return findSingleByAttribute("strKey", strKey);
    }

    public String getString(T entity) {
        if(entity == null) return null;
        return ((StringKeyedEntity) entity).getStrKey();
    }

    public boolean isPersistent(T entity) {
        return getEntityManager().contains(entity);
    }

    public void detach(T entity) throws EJBException {
        try {
            getEntityManager().detach(entity);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    public void merge(T entity) throws EJBException {
        try {
            getEntityManager().merge(entity);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    public void refresh(T entity) throws EJBException {
        try {
            getEntityManager().refresh(getEntityManager().merge(entity));
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    public void create(T entity) throws EJBException {
        try {
            getEntityManager().persist(entity);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public void edit(T entity) throws EJBException {
        try {
            getEntityManager().merge(entity);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    public void remove(T entity) throws EJBException {
        try {
            getEntityManager().remove(getEntityManager().merge(entity));
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }
//</editor-fold>    

    //<editor-fold defaultstate="collapsed" desc="Derived">
    public void merge(Collection<T> entities) {
        for (T operand : entities) {
            merge(operand);
        }
    }

    public void create(Collection<T> entities) throws EJBException {
        for (T operand : entities) {
            create(operand);
        }
    }

    public void edit(Collection<T> entities) throws EJBException {
        for (T operand : entities) {
            edit(operand);
        }
    }

    public void remove(Collection<T> entities) throws EJBException {
        for (T operand : entities) {
            remove(operand);
        }
    }

    protected void addCriteria(Root<T> root, CriteriaQuery<T> cq) {

    }

    protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<T> root) {
        return new ArrayList<>();
    }

    protected List<T> postAction(List<T> result) {
        return result;
    }

    protected CriteriaQuery constructFindQuery(Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);

        Root<T> root = cq.from(entityClass);
        cq.select(root);
        addCriteria(root, cq);
        if (filters != null && !filters.isEmpty()) {
            List<Predicate> predicates = new ArrayList<>();
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "name":
                        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                        predicates.add(cb.like(cb.upper(root.<String>get(filterName)), literal));
                        break;
//                    case "party.name":
//                        literal = cb.upper(cb.literal("%" + filterValue + "%"));
//                        predicates.add(cb.like(cb.upper(root.get("party").<String>get("name")), literal));
//                        break;
                    default:
                        predicates.addAll(addPreFilter(filterName, filterValue, root));
                        break;
                }
            }

            cq.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        return cq;
    }

    protected CriteriaQuery constructCountQuery(Map<String, Object> filters) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<T> root = cq.from(entityClass);
        cq.select(cb.count(root));

        if (filters != null && !filters.isEmpty()) {
            List<Predicate> predicates = new ArrayList<>();
            for (String filterName : filters.keySet()) {
                Object filterValue = filters.get(filterName);
                switch (filterName) {
                    case "name":
                        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
                        predicates.add(cb.like(cb.upper(root.<String>get(filterName)), literal));
                        break;
                    default:
                        predicates.addAll(addPreFilter(filterName, filterValue, root));
                        break;
                }
            }

            cq.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        return cq;
    }

    public List<T> findAll() {
        return findAll(0, 0, null);
//    CriteriaQuery cq = constructFindQuery(null);
//    Query q = getEntityManager().createQuery(cq);

//    return postAction(q.getResultList());
    }

    public List<T> findAll(int startPosition, int maxResult) {

        return findAll(startPosition, maxResult, null);

//    CriteriaQuery cq = constructFindQuery(null);
//    Query q = getEntityManager().createQuery(cq);
//
//    q.setFirstResult(startPosition);
//    q.setMaxResults(maxResult);
//    return postAction(q.getResultList());
    }

    public List<T> findAll(Map<String, Object> filters) {

        return findAll(0, 0, filters);

//    CriteriaQuery<T> cq = constructFindQuery(filters);
//    Query q = getEntityManager().createQuery(cq);
//
//    return postAction(q.getResultList());
    }

    public List<T> findAll(int startPosition, int maxResult, Map<String, Object> filters) {

        if (filters == null) {
            filters = new HashMap<>();
        }
        preProcessFilter(filters);

        CriteriaQuery cq = constructFindQuery(filters);
        Query q = getEntityManager().createQuery(cq);

        if (startPosition != 0 && maxResult != 0) {
            q.setFirstResult(startPosition);
            q.setMaxResults(maxResult);
        }

        return postAction(q.getResultList());
    }

    public Long countAll() {

        return countAll(null);

//    CriteriaQuery cq = constructCountQuery(null);
//    Query q = getEntityManager().createQuery(cq);
//
//    return (Long) q.getSingleResult();
    }

    public Long countAll(Map<String, Object> filters) {
        if (filters == null) {
            filters = new HashMap<>();
        }
        preProcessFilter(filters);

        CriteriaQuery cq = constructCountQuery(filters);
        Query q = getEntityManager().createQuery(cq);

        return (Long) q.getSingleResult();
    }

    protected void preProcessFilter(Map<String, Object> filters) {
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Deprecated">
//    @Deprecated
//    public List<T> findRange(int[] range, Map<String, String> filters) {
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery<T> cq = cb.createQuery(entityClass);
//        Root<T> root = cq.from(entityClass);
//        createSelect(cb, cq, root);
//        if (filters != null) {
//            applyFilter(filters, cb, root, cq);
//        }
//        Query q = getEntityManager().createQuery(cq);
//        q.setMaxResults(range[1] - range[0]);
//        q.setFirstResult(range[0]);
//        return q.getResultList();
//    }
//
//    @Deprecated
//    public int count(Map<String, String> filters) {
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
//        Root<T> root = cq.from(entityClass);
//        cq.select(cb.count(root));
//        if (filters != null) {
//            applyFilter(filters, cb, root, cq);
//        }
//        Query q = getEntityManager().createQuery(cq);
//        return ((Long) q.getSingleResult()).intValue();
//    }
    public T findSingleByAttribute(String attribute, Object value) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(entityClass);
        Root<T> root = cq.from(entityClass);
        cq.select(root).where(cb.equal(root.get(attribute), value));
        try {
            return (T) getEntityManager().createQuery(cq).getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

//    @Deprecated
//    public T findSingleByAttribute(String[] attributes, Object value) {
//
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery cq = cb.createQuery(entityClass);
//        Root<T> root = cq.from(entityClass);
//
//        Path path = root;
//        for (String attribute : attributes) {
//            path = path.get(attribute);
//        }
//
//        cq.select(root).where(cb.equal(path, value));
//
//        try {
//            return (T) getEntityManager().createQuery(cq).getSingleResult();
//        } catch (NoResultException ex) {
//            return null;
//        }
//    }
//    @Deprecated
//    public List<T> findAllByAttribute(String attribute, Object value) {
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery cq = cb.createQuery(entityClass);
//        Root<T> root = cq.from(entityClass);
//
//        cq.select(root).where(cb.equal(root.get(attribute), value));
//
//        return getEntityManager().createQuery(cq).getResultList();
//    }
//
//    @Deprecated
//    public List<T> findAllByAttribute(String[] attributes, Object value) {
//        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        CriteriaQuery cq = cb.createQuery(entityClass);
//        Root<T> root = cq.from(entityClass);
//
//        Path path = root;
//        for (String attribute : attributes) {
//            path = path.get(attribute);
//        }
//
//        cq.select(root).where(cb.equal(path, value));
//
//        return getEntityManager().createQuery(cq).getResultList();
//    }
//    @Deprecated
//    private T validate(T t) throws Exception {
//
//        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
//        Set violations = validator.validate(t);
//        if (violations.size() > 0) {
//            throw new ApplicationException(violations);
//        }
//
//        return t;
//    }
//    @Deprecated
//    protected void applyFilter(Map<String, String> filters, CriteriaBuilder cb, Root<T> root, CriteriaQuery cq) {
//        List<Predicate> criteria = new ArrayList<>();
//        for (String filter : filters.keySet()) {
//            String filterValue = filters.get(filter);
//            Predicate p = createPredicate(cb, root, filter, filterValue);
//            if (p != null) {
//                criteria.add(p);
//            }
//        }
//        if (!criteria.isEmpty()) {
//            cq.where(cb.and(criteria.toArray(new Predicate[criteria.size()])));
//        }
//    }
//
//    @Deprecated
//    protected Predicate createPredicate(CriteriaBuilder cb, Root<T> root, String filter, String filterValue) {
//        if (filter.equals("name")) {
//            Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
//            return cb.like(cb.upper(root.<String>get(filter)), literal);
//        } else {
//            if (!filterValue.equals("All")) {
//                return cb.equal(root.get(filter).get("name"), filterValue);
//            } else {
//                return null;
//            }
//        }
//    }
//
//    @Deprecated
//    protected void createSelect(CriteriaBuilder cb, CriteriaQuery cq, Root<T> root) {
//        cq.select(root).orderBy(cb.asc(root.get("id")));
//    }
//</editor-fold>
}
