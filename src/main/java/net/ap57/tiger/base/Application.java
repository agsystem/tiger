/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.Map;
import java.util.logging.Level;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.faces.FacesException;
import javax.faces.FactoryFinder;
import javax.faces.application.ApplicationFactory;
import javax.faces.application.NavigationHandler;
import javax.faces.application.StateManager;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.el.MethodBinding;
import javax.faces.el.PropertyResolver;
import javax.faces.el.ReferenceSyntaxException;
import javax.faces.el.ValueBinding;
import javax.faces.el.VariableResolver;
import javax.faces.event.ActionListener;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.faces.validator.Validator;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import net.ap57.tiger.base.api.Module;
import net.ap57.tiger.base.config.Config;
import net.ap57.tiger.base.config.ConfigFacade;
import org.apache.commons.configuration.Configuration;
import net.ap57.tiger.base.event.BootstrapEvent;
import net.ap57.tiger.base.event.ShutdownEvent;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Named(value="theApp")
@ApplicationScoped
public class Application extends javax.faces.application.Application implements Serializable {

    private javax.faces.application.Application theApp;

    private Map<String, Module> registry;
    
    private Map<String, Object> attributes;

    @Inject
    @BootstrapEvent
    private Event<Application> bootstrapEvent;
    
    @Inject
    @ShutdownEvent
    private Event<Application> shutdownEvent;

    @Inject
    private ConfigFacade configFacade;

    @Inject
    protected Configuration configuration;

    public void init() {

        ApplicationFactory applicationFactory = (ApplicationFactory) FactoryFinder.getFactory(FactoryFinder.APPLICATION_FACTORY);
        theApp = applicationFactory.getApplication();
        registry = new HashMap<>();
        bootstrapEvent.fire(this);
        loadConfig();
        
    }
    
    public void shutdown() {
        shutdownEvent.fire(this);
    }

    public Configuration getConfiguration() {
        return configuration;
    }
    
    public String getTitle() {
        return getConfiguration().getString("net.ap57.tiger.base.application_title");
    }

    protected void processConfig(JsonObject jsonConfiguration) {

        String key = getClass().getPackage().getName() + "." + jsonConfiguration.getString("name");
        if (!getConfiguration().containsKey(key)) {
            Config cfg = new Config();
            cfg.setName(key);
            cfg.setType(jsonConfiguration.getString("type"));
            cfg.setValue(jsonConfiguration.getString("default"));
            cfg.setHumanName(jsonConfiguration.getString("humanName"));
            cfg.setRemarks(jsonConfiguration.getString("remarks"));
            try {
                configFacade.create(cfg);
            } catch (Exception ex) {
                Logger.getLogger(Module.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void loadConfig() {

        InputStream in = getClass().getResourceAsStream("/" + getClass().getPackage().getName().replaceAll("\\.", "/") + "/config/config.json");
        if (in != null) {
            JsonObject jsonConfigurations;
            try (JsonReader jsonReader = Json.createReader(in)) {
                jsonConfigurations = jsonReader.readObject();
            }

            JsonArray jsonArray = jsonConfigurations.getJsonArray("configurations");

            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonConfiguration = jsonArray.getJsonObject(i);
                processConfig(jsonConfiguration);
            }

        }

//        CacheManager manager = new CacheManager(getClass().getResourceAsStream("/ehcache.xml"));
//        CacheManager manager = CacheManager.create(getClass().getResourceAsStream("/ehcache.xml"));
//        System.out.println("MENEJERE = " + manager);
//        Cache cache = manager.getCache("objectCache");
//        CacheManager menejer = CacheManager.create(getClass().getResourceAsStream("/ehcache.xml"));
//        System.out.println("MENEJERE = " + menejer);
//        cache.put(new Element(null, null));
        
    }

    public void register(Module module) {
        registry.put(module.getModuleName(), module);
    }

    public Module findModule(String key) {
        return registry.get(key);
    }

    @Override
    public ActionListener getActionListener() {
        return theApp.getActionListener();
    }

    @Override
    public void setActionListener(ActionListener listener) {
        theApp.setActionListener(listener);
    }

    @Override
    public Locale getDefaultLocale() {
        return theApp.getDefaultLocale();
    }

    @Override
    public void setDefaultLocale(Locale locale) {
        theApp.setDefaultLocale(locale);
    }

    @Override
    public String getDefaultRenderKitId() {
        return theApp.getDefaultRenderKitId();
    }

    @Override
    public void setDefaultRenderKitId(String renderKitId) {
        theApp.setDefaultRenderKitId(renderKitId);
    }

    @Override
    public String getMessageBundle() {
        return theApp.getMessageBundle();
    }

    @Override
    public void setMessageBundle(String bundle) {
        theApp.setMessageBundle(bundle);
    }

    @Override
    public NavigationHandler getNavigationHandler() {
        return theApp.getNavigationHandler();
    }

    @Override
    public void setNavigationHandler(NavigationHandler handler) {
        theApp.setNavigationHandler(handler);
    }

    @Override
    public PropertyResolver getPropertyResolver() {
        return theApp.getPropertyResolver();
    }

    @Override
    public void setPropertyResolver(PropertyResolver resolver) {
        theApp.setPropertyResolver(resolver);
    }

    @Override
    public VariableResolver getVariableResolver() {
        return theApp.getVariableResolver();
    }

    @Override
    public void setVariableResolver(VariableResolver resolver) {
        theApp.setVariableResolver(resolver);
    }

    @Override
    public ViewHandler getViewHandler() {
        return theApp.getViewHandler();
    }

    @Override
    public void setViewHandler(ViewHandler handler) {
        theApp.setViewHandler(handler);
    }

    @Override
    public StateManager getStateManager() {
        return theApp.getStateManager();
    }

    @Override
    public void setStateManager(StateManager manager) {
        theApp.setStateManager(manager);
    }

    @Override
    public void addComponent(String componentType, String componentClass) {
        theApp.addComponent(componentType, componentClass);
    }

    @Override
    public UIComponent createComponent(String componentType) throws FacesException {
        return theApp.createComponent(componentType);
    }

    @Override
    public UIComponent createComponent(ValueBinding componentBinding, FacesContext context, String componentType) throws FacesException {
        return theApp.createComponent(componentBinding, context, componentType);
    }

    @Override
    public Iterator<String> getComponentTypes() {
        return theApp.getComponentTypes();
    }

    @Override
    public void addConverter(String converterId, String converterClass) {
        theApp.addConverter(converterId, converterClass);
    }

    @Override
    public void addConverter(Class<?> targetClass, String converterClass) {
        theApp.addConverter(targetClass, converterClass);
    }

    @Override
    public Converter createConverter(String converterId) {
        return theApp.createConverter(converterId);
    }

    @Override
    public Converter createConverter(Class<?> targetClass) {
        return theApp.createConverter(targetClass);
    }

    @Override
    public Iterator<String> getConverterIds() {
        return theApp.getConverterIds();
    }

    @Override
    public Iterator<Class<?>> getConverterTypes() {
        return theApp.getConverterTypes();
    }

    @Override
    public MethodBinding createMethodBinding(String ref, Class<?>[] params) throws ReferenceSyntaxException {
        return theApp.createMethodBinding(ref, params);
    }

    @Override
    public Iterator<Locale> getSupportedLocales() {
        return theApp.getSupportedLocales();
    }

    @Override
    public void setSupportedLocales(Collection<Locale> locales) {
        theApp.setSupportedLocales(locales);
    }

    @Override
    public void addValidator(String validatorId, String validatorClass) {
        theApp.addValidator(validatorId, validatorClass);
    }

    @Override
    public Validator createValidator(String validatorId) throws FacesException {
        return theApp.createValidator(validatorId);
    }

    @Override
    public Iterator<String> getValidatorIds() {
        return theApp.getValidatorIds();
    }

    @Override
    public ValueBinding createValueBinding(String ref) throws ReferenceSyntaxException {
        return theApp.createValueBinding(ref);
    }
    
    public Object getAttribute(String name) {
        return attributes.get(name);
    }
    
    public void setAttribute(String name, Object val) {
        attributes.put(name, val);
    }
    
    public void removeAttribute(String name) {
        attributes.remove(name);
    }

}
