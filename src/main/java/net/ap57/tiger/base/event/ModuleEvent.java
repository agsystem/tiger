/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.event;

import org.apache.commons.configuration.Configuration;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public class ModuleEvent {

    private String name;
    private ModuleStatus status;
    private Configuration appConfig;
    private Configuration moduleConfig;

    public ModuleEvent(String name, ModuleStatus status, Configuration appConfig, Configuration moduleConfig) {
        this.name = name;
        this.status = status;
        this.appConfig = appConfig;
        this.moduleConfig = moduleConfig;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModuleStatus getStatus() {
        return status;
    }

    public void setStatus(ModuleStatus status) {
        this.status = status;
    }

    public Configuration getAppConfig() {
        return appConfig;
    }

    public void setAppConfig(Configuration appConfig) {
        this.appConfig = appConfig;
    }

    public Configuration getModuleConfig() {
        return moduleConfig;
    }

    public void setModuleConfig(Configuration moduleConfig) {
        this.moduleConfig = moduleConfig;
    }

}
