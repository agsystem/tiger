/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.event;

import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class ApplicationEvent {
    
    protected ConcurrentHashMap<String, Object> parameters;
    
    public ApplicationEvent() {
        parameters = new ConcurrentHashMap<>();
    }
    
    public void setParam(String name, Object value) {
        parameters.put(name, value);
    }
    
    public Object getParam(String name) {
        return parameters.get(name);
    }
}
