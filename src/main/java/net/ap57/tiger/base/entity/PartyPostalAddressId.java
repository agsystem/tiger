/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
public class PartyPostalAddressId implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long party;

  private Long postalAddress;

  private Date fromDate;

  public Long getParty() {
    return party;
  }

  public void setParty(Long party) {
    this.party = party;
  }

  public Long getPostalAddress() {
    return postalAddress;
  }

  public void setPostalAddress(Long postalAddress) {
    this.postalAddress = postalAddress;
  }

  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 37 * hash + Objects.hashCode(this.party);
    hash = 37 * hash + Objects.hashCode(this.postalAddress);
    hash = 37 * hash + Objects.hashCode(this.fromDate);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PartyPostalAddressId other = (PartyPostalAddressId) obj;
    if (!Objects.equals(this.party, other.party)) {
      return false;
    }
    if (!Objects.equals(this.postalAddress, other.postalAddress)) {
      return false;
    }
    if (!Objects.equals(this.fromDate, other.fromDate)) {
      return false;
    }
    return true;
  }
  
}
