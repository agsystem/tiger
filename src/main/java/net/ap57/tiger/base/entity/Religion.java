/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.entity;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public enum Religion {
    ISLAM,
    KRISTEN,
    KATOLIK,
    HINDU,
    BUDHA,
    KONGHUCU
}
