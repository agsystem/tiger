/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Party extends StringKeyedEntity implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  @TableGenerator(name = "Party", table = "KEYGEN")
  @GeneratedValue(generator = "Party", strategy = GenerationType.TABLE)
  private Long id;

//  @OneToMany(mappedBy = "party", cascade = CascadeType.ALL, orphanRemoval = true)
//  protected List<PartyRole> roles;

  @OneToMany(mappedBy = "party", cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<PartyClassification> classifications;

  @OneToMany(mappedBy = "party", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
  protected List<PartyPostalAddress> postalAddresses;

  @OneToMany(mappedBy = "party", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
  protected List<PartyContactMechanism> contactMechanisms;
  
//  @OneToMany(mappedBy = "party", cascade = CascadeType.REMOVE)
//  private List<EntityAlias> partyAliases;

  private String description;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

//  public List<PartyRole> getRoles() {
//    return roles;
//  }
//
//  public void setRoles(List<PartyRole> roles) {
//    this.roles = roles;
//  }

  public List<PartyClassification> getClassifications() {
    return classifications;
  }

  public void setClassifications(List<PartyClassification> classifications) {
    this.classifications = classifications;
  }

  public List<PartyPostalAddress> getPostalAddresses() {
    return postalAddresses;
  }

  public void setPostalAddresses(List<PartyPostalAddress> postalAddresses) {
    this.postalAddresses = postalAddresses;
  }

  public List<PartyContactMechanism> getContactMechanisms() {
    return contactMechanisms;
  }

  public void setContactMechanisms(List<PartyContactMechanism> contactMechanisms) {
    this.contactMechanisms = contactMechanisms;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
  
  public abstract List getRoles();
  public abstract void setRoles(List roles);

//  public List<EntityAlias> getPartyAliases() {
//    return partyAliases;
//  }
//
//  public void setPartyAliases(List<EntityAlias> partyAliases) {
//    this.partyAliases = partyAliases;
//  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Party)) {
      return false;
    }
    Party other = (Party) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return Long.toString(id);
  }

}
