/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PartyRelationship implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    protected String id;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date fromDate;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date thruDate;

    @ManyToOne
    protected PartyRelationshipType relationshipType;

    @Enumerated(EnumType.STRING)
    protected PartyRelationshipStatusType relationshipStatus;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract PartyRole getEstablisher();

    public abstract void setEstablisher(PartyRole establisher);

    public abstract PartyRole getParticipant();

    public abstract void setParticipant(PartyRole participant);

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getThruDate() {
        return thruDate;
    }

    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }

    public PartyRelationshipType getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(PartyRelationshipType partyRelationshipType) {
        this.relationshipType = partyRelationshipType;
    }

    public PartyRelationshipStatusType getRelationshipStatus() {
        return relationshipStatus;
    }

    public void setRelationshipStatus(PartyRelationshipStatusType relationshipStatus) {
        this.relationshipStatus = relationshipStatus;
    }

//</editor-fold>
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(getEstablisher());
        hash = 53 * hash + Objects.hashCode(getParticipant());
        hash = 53 * hash + Objects.hashCode(this.fromDate);
        hash = 53 * hash + Objects.hashCode(this.relationshipType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PartyRelationship other = (PartyRelationship) obj;
        if (!Objects.equals(getEstablisher(), other.getEstablisher())) {
            return false;
        }
        if (!Objects.equals(this.getParticipant(), other.getParticipant())) {
            return false;
        }
        if (!Objects.equals(this.fromDate, other.fromDate)) {
            return false;
        }
        if (!Objects.equals(this.relationshipType, other.relationshipType)) {
            return false;
        }
        return true;
    }

    @PrePersist
    public void prePersist() {
        String establisherId = getEstablisher().getId().toString();
        String participantId = getParticipant().getId().toString();
        String strFromDate = fromDate.toString();
        String relType = relationshipType.getName();
        setId(UUID.nameUUIDFromBytes((establisherId + participantId + strFromDate + relType).getBytes()).toString());
    }

}
