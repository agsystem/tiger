/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Embeddable
public class IdentityInfo implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private IdentityType idType;

  private String idNumber;

  public IdentityType getIdType() {
    return idType;
  }

  public void setIdType(IdentityType idType) {
    this.idType = idType;
  }

  public String getIdNumber() {
    return idNumber;
  }

  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }
  
}
