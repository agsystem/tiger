/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
public class Person extends Party implements Serializable {

  private static final long serialVersionUID = 1L;

  @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
  private List<PersonRole> roles;

  @Column(nullable = false)
  private String firstName;

  private String lastName;

  private Gender gender;

  @Embedded
  private BirthInfo birthInfo;

  @Embedded
  private IdentityInfo identityInfo;

  private String motherName;

  private MaritalStatus maritalStatus;

  private Religion religion;

  private String comment;

  @Basic(fetch = FetchType.EAGER)
  @Lob
  private byte[] photo;

  //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public BirthInfo getBirthInfo() {
      if(birthInfo == null) { 
          birthInfo = new BirthInfo();
          birthInfo.setBirthDate(Calendar.getInstance().getTime());
      }
    return birthInfo;
  }

  public void setBirthInfo(BirthInfo birthInfo) {
    this.birthInfo = birthInfo;
  }

  public IdentityInfo getIdentityInfo() {
      if(identityInfo == null) {
          identityInfo = new IdentityInfo();
      }
    return identityInfo;
  }

  public void setIdentityInfo(IdentityInfo identityInfo) {
    this.identityInfo = identityInfo;
  }

  public String getMotherName() {
    return motherName;
  }

  public void setMotherName(String motherName) {
    this.motherName = motherName;
  }

  public MaritalStatus getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(MaritalStatus maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public Religion getReligion() {
    return religion;
  }

  public void setReligion(Religion religion) {
    this.religion = religion;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public byte[] getPhoto() {
    return photo;
  }

  public void setPhoto(byte[] photo) {
    this.photo = photo;
  }

  @Override
  public List getRoles() {
    return roles;
  }

  @Override
  public void setRoles(List roles) {
    this.roles = roles;
  }

//</editor-fold>
  public String getName() {
    return firstName + (lastName == null ? "" : " " + (lastName));
  }

  public Integer getAge() {
    if (birthInfo == null) {
      return 0;
    }

    Calendar current = Calendar.getInstance();
    Calendar calDob = Calendar.getInstance();
    Date birthDate = birthInfo.getBirthDate();
    if(birthDate == null) return 0;
    calDob.setTime(birthInfo.getBirthDate());

    int cYear = current.get(Calendar.YEAR);
    int cMonth = current.get(Calendar.MONTH);
    int cDate = current.get(Calendar.DATE);

    int year = calDob.get(Calendar.YEAR);
    int month = calDob.get(Calendar.MONTH);
    int date = calDob.get(Calendar.DATE);

    int dYear = cYear - year;
    int dMonth = cMonth - month;
    int dDate = cDate - date;

    int age = 0;

    if (dMonth == 0 && dDate == 0) {
      return Double.valueOf(dYear).intValue();
    } else if (dMonth < 0 || (dMonth == 0 && dDate < 0)) {
      return Double.valueOf(dYear - 1 + ((11 - month + cMonth + 1) / 12d)).intValue();
    } else if (dMonth > 0 || (dMonth == 0 && dDate > 0)) {
      return Double.valueOf(dYear + (cMonth / 12d)).intValue();
    } else {
      return 0;
    }

  }

}
