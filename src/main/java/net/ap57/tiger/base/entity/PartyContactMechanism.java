/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
@IdClass(PartyContactMechanismId.class)
public class PartyContactMechanism implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @ManyToOne
  private Party party;

  @Id
  @ManyToOne(cascade = CascadeType.ALL)
  private ContactMechanism contactMechanism;

  @Id
  @Temporal(TemporalType.DATE)
  private Date fromDate;

  @Temporal(TemporalType.DATE)
  private Date thruDate;

  public Party getParty() {
    return party;
  }

  public void setParty(Party party) {
    this.party = party;
  }

  public ContactMechanism getContactMechanism() {
    return contactMechanism;
  }

  public void setContactMechanism(ContactMechanism contactMechanism) {
    this.contactMechanism = contactMechanism;
  }

  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  public Date getThruDate() {
    return thruDate;
  }

  public void setThruDate(Date thruDate) {
    this.thruDate = thruDate;
  }

  @PrePersist
  private void prePersist() {
    fromDate = Calendar.getInstance().getTime();
  }

}
