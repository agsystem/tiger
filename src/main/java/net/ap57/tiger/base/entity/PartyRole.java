/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PartyRole extends StringKeyedEntity implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Id
  protected String id;

//  @Id
//  @ManyToOne
//  protected Party party;

//  @Id
  @ManyToOne//(targetEntity = PartyRoleType.class)
  protected PartyRoleType partyRoleType;

//  @Id
  @Temporal(TemporalType.TIMESTAMP)
  protected Date fromDate;

  @Temporal(TemporalType.TIMESTAMP)
  protected Date thruDate;

//  @OneToMany(mappedBy = "establisher", cascade = CascadeType.ALL, orphanRemoval = true)
//  protected List<PartyRelationship> relationshipsEstablished;
//
//  @OneToMany(mappedBy = "participant", cascade = CascadeType.ALL, orphanRemoval = true)
//  protected List<PartyRelationship> relationshipParticipations;

  //<editor-fold defaultstate="collapsed" desc="Getters & Setters">

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  } 
  
  public abstract Party getParty();

  public abstract void setParty(Party party);

  public PartyRoleType getPartyRoleType() {
    return (PartyRoleType) partyRoleType;
  }

  public void setPartyRoleType(PartyRoleType partyRoleType) {
    this.partyRoleType = partyRoleType;
  }

  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  public Date getThruDate() {
    return thruDate;
  }

  public void setThruDate(Date thruDate) {
    this.thruDate = thruDate;
  }

  public abstract List getRelationshipsEstablished();

  public abstract void setRelationshipsEstablished(List relationshipsEstablished);

  public abstract List getRelationshipParticipations();

  public abstract void setRelationshipParticipations(List relationshipParticipations);

//</editor-fold>

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 41 * hash + Objects.hashCode(getParty());
    hash = 41 * hash + Objects.hashCode(this.partyRoleType);
    hash = 41 * hash + Objects.hashCode(this.fromDate);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PartyRole other = (PartyRole) obj;
    if (!Objects.equals(this.getParty(), other.getParty())) {
      return false;
    }
    if (!Objects.equals(this.partyRoleType, other.partyRoleType)) {
      return false;
    }
    if (!Objects.equals(this.fromDate, other.fromDate)) {
      return false;
    }
    return true;
  }
  
  @PrePersist
  @Override
  public void prePersist() {
    String partyId = getParty().getId().toString();
    String partyRoleTypeId = partyRoleType.getId().toString();
    fromDate = Calendar.getInstance().getTime();
    String strFromDate = fromDate.toString();
    setId(UUID.nameUUIDFromBytes((partyId + partyRoleTypeId + strFromDate).getBytes()).toString());
    setStrKey(id);
  }
  
}
