/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ap57.tiger.base;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Locale;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mbahbejo
 */
@Named(value = "userSession")
@SessionScoped
public class UserSession implements Serializable {
    
    private String locale = "en";
    
    private String uuid = UUID.randomUUID().toString();
    
    public UserSession() {
    }
    
    public String getLocale() {
        return locale;
    }
    
    public void setLocale(String locale) {
        this.locale = locale;
    }
    
    public String switchLocale() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        String root = ((HttpServletRequest) externalContext.getRequest()).getPathInfo();
        return root + "?faces-redirect=true";
    }
    
    public String getLang() {
        Locale loc = new Locale(locale);
        String lang = loc.getLanguage();
        return lang;
    }

    public String getUuid() {
        return uuid;
    }
    
}
