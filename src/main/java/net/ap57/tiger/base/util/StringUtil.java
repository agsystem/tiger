/*
 * Copyright 2017 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.base.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class StringUtil {

    public static String[] breakCamelCasedString(String camelCasedString, String... excludedStrings) {

        Pattern pattern = Pattern.compile("[A-Z][a-z 0-9]*");
        Matcher matcher = pattern.matcher(camelCasedString);

        String strExcludedPattern = "";

        int count = excludedStrings.length;
        for (String excludedString : excludedStrings) {
            strExcludedPattern += "(" + excludedString + ")";
            if (count > 1) {
                strExcludedPattern += "|";
            }
            count--;
        }

        Pattern excludedPattern = null;
        if (!strExcludedPattern.isEmpty()) {
            excludedPattern = Pattern.compile(strExcludedPattern);
        }

        List<String> result = new ArrayList<>();

        while (matcher.find()) {
            String match = matcher.group();
            if (excludedPattern != null) {
                Matcher excludedMatcher = excludedPattern.matcher(match);
                if (!excludedMatcher.find()) {
                    result.add(match);
                }
            }
        }

        return result.toArray(new String[result.size()]);

    }

}
