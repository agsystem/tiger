/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */

package net.ap57.tiger.base.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.ejb.EJBException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
public class GraphicUtil {

    public static byte[] createThumbnail(byte[] raw, int maxDim) {

        ImageIcon imageIcon = new ImageIcon(raw);
        Image inImage = imageIcon.getImage();
        int width = inImage.getWidth(null);
        int height = inImage.getHeight(null);
        double scale = 1;
        if (height <= width) { // Portrait orientation
            scale = (double) maxDim / (double) width;
        } else if (height > width) { // Landscape orientation
            scale = (double) maxDim / (double) height;
        }

        int scaledW = (int) (scale * width);
        int scaledH = (int) (scale * height);

        BufferedImage outImage = new BufferedImage(scaledW, scaledH, BufferedImage.TYPE_INT_RGB);
        AffineTransform tx = new AffineTransform();

        if (scale < 1.0d) {
            tx.scale(scale, scale);
        }

        Graphics2D g2d = outImage.createGraphics();
        g2d.drawImage(inImage, tx, null);
        g2d.dispose();

        byte[] bytesOut = null;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(outImage, "JPG", baos);
            bytesOut = baos.toByteArray();
        } catch (IOException ex) {
            throw new EJBException(ex);
        }

        return bytesOut;
    }
}
