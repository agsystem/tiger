/*
 * Copyright 2016 mbahbejo.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright mbahbejo and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document mbahbejo and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of mbahbejo is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of mbahbejo

 */
package net.ap57.tiger.base.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import javax.ejb.Singleton;
import javax.inject.Inject;
import net.ap57.tiger.base.config.Config;
import net.ap57.tiger.base.config.ConfigFacade;
import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author mbahbejo
 */
@Singleton
public class AppConfiguration implements Configuration {

    private final Configuration config;

    @Inject
    private ConfigFacade configFacade;

    public AppConfiguration() {
        config = new AbstractConfiguration() {

            @Override
            protected void addPropertyDirect(String string, Object o) {
                Config cfg = configFacade.find(string);
                if (cfg != null) {
                    cfg.setValue(o.toString());
                    configFacade.edit(cfg);
                }
            }

            @Override
            public boolean isEmpty() {
                return configFacade.countAll() == 0;
            }

            @Override
            public boolean containsKey(String string) {
                return configFacade.find(string) != null;
            }

            @Override
            public Object getProperty(String string) {
                Config cfg = configFacade.find(string);
                if (cfg != null) {
                    return cfg.getValue();
                } else {
                    return null;
                }
            }

            @Override
            public Iterator<String> getKeys() {
                return configFacade.getAllKeys().iterator();
            }

        };
    }

    @Override
    public Configuration subset(String prefix) {
        return config.subset(prefix);
    }

    @Override
    public boolean isEmpty() {
        return config.isEmpty();
    }

    @Override
    public boolean containsKey(String key) {
        return config.containsKey(key);
    }

    @Override
    public void addProperty(String key, Object value) {
        config.addProperty(key, value);
    }

    @Override
    public void setProperty(String key, Object value) {
        config.setProperty(key, value);
    }

    @Override
    public void clearProperty(String key) {
        config.clearProperty(key);
    }

    @Override
    public void clear() {
        config.clear();
    }

    @Override
    public Object getProperty(String key) {
        return config.getProperty(key);
    }

    @Override
    public Iterator<String> getKeys(String prefix) {
        return config.getKeys(prefix);
    }

    @Override
    public Iterator<String> getKeys() {
        return config.getKeys();
    }

    @Override
    public Properties getProperties(String key) {
        return config.getProperties(key);
    }

    @Override
    public boolean getBoolean(String key) {
        return config.getBoolean(key);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        return config.getBoolean(key, defaultValue);
    }

    @Override
    public Boolean getBoolean(String key, Boolean defaultValue) {
        return config.getBoolean(key, defaultValue);
    }

    @Override
    public byte getByte(String key) {
        return config.getByte(key);
    }

    @Override
    public byte getByte(String key, byte defaultValue) {
        return config.getByte(key, defaultValue);
    }

    @Override
    public Byte getByte(String key, Byte defaultValue) {
        return config.getByte(key, defaultValue);
    }

    @Override
    public double getDouble(String key) {
        return config.getDouble(key);
    }

    @Override
    public double getDouble(String key, double defaultValue) {
        return config.getDouble(key, defaultValue);
    }

    @Override
    public Double getDouble(String key, Double defaultValue) {
        return config.getDouble(key, defaultValue);
    }

    @Override
    public float getFloat(String key) {
        return config.getFloat(key);
    }

    @Override
    public float getFloat(String key, float defaultValue) {
        return config.getFloat(key, defaultValue);
    }

    @Override
    public Float getFloat(String key, Float defaultValue) {
        return config.getFloat(key, defaultValue);
    }

    @Override
    public int getInt(String key) {
        return config.getInt(key);
    }

    @Override
    public int getInt(String key, int defaultValue) {
        return config.getInt(key, defaultValue);
    }

    @Override
    public Integer getInteger(String key, Integer defaultValue) {
        return config.getInteger(key, defaultValue);
    }

    @Override
    public long getLong(String key) {
        return config.getLong(key);
    }

    @Override
    public long getLong(String key, long defaultValue) {
        return config.getLong(key, defaultValue);
    }

    @Override
    public Long getLong(String key, Long defaultValue) {
        return config.getLong(key, defaultValue);
    }

    @Override
    public short getShort(String key) {
        return config.getShort(key);
    }

    @Override
    public short getShort(String key, short defaultValue) {
        return config.getShort(key, defaultValue);
    }

    @Override
    public Short getShort(String key, Short defaultValue) {
        return config.getShort(key, defaultValue);
    }

    @Override
    public BigDecimal getBigDecimal(String key) {
        return config.getBigDecimal(key);
    }

    @Override
    public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
        return config.getBigDecimal(key, defaultValue);
    }

    @Override
    public BigInteger getBigInteger(String key) {
        return config.getBigInteger(key);
    }

    @Override
    public BigInteger getBigInteger(String key, BigInteger defaultValue) {
        return config.getBigInteger(key, defaultValue);
    }

    @Override
    public String getString(String key) {
        return config.getString(key);
    }

    @Override
    public String getString(String key, String defaultValue) {
        return config.getString(key, defaultValue);
    }

    @Override
    public String[] getStringArray(String key) {
        return config.getStringArray(key);
    }

    @Override
    public List<Object> getList(String key) {
        return config.getList(key);
    }

    @Override
    public List<Object> getList(String key, List<?> defaultValue) {
        return config.getList(key, defaultValue);
    }
}
