/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */

package net.ap57.tiger.base.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 */
public abstract class XmlWalker {
    
    public abstract void processStartElement(XmlElementEvent evt);
    public abstract void processEndElement(XmlElementEvent evt);

    public void walk(InputStream xmlStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(xmlStream));
        XMLInputFactory inputFactory = XMLInputFactory.newFactory();
        try {

            XMLStreamReader xmlReader = inputFactory.createXMLStreamReader(reader);

            while (xmlReader.hasNext()) {

                int event = xmlReader.next();
                switch (event) {
                    case XMLStreamConstants.START_DOCUMENT:
                        break;
                    case XMLStreamConstants.PROCESSING_INSTRUCTION:
                        break;
                    case XMLStreamConstants.DTD:
                        break;
                    case XMLStreamConstants.NAMESPACE:
                        break;
                    case XMLStreamConstants.START_ELEMENT:
                        XmlElementEvent evt = new XmlElementEvent(xmlReader.getLocalName());
                        for(int i = xmlReader.getAttributeCount() - 1; i >= 0; i--) {
                            evt.putAttribute(xmlReader.getAttributeLocalName(i), xmlReader.getAttributeValue(i));
                        }
                        processStartElement(evt);
                        break;
                    case XMLStreamConstants.ATTRIBUTE:
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        break;
                    case XMLStreamConstants.CDATA:
                        break;
                    case XMLStreamConstants.SPACE:
                        break;
                    case XMLStreamConstants.ENTITY_REFERENCE:
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        processEndElement(new XmlElementEvent(xmlReader.getLocalName()));
                        break;
                    case XMLStreamConstants.END_DOCUMENT:
                        break;
                    default:
                        break;
                }
                
            }

        } catch (XMLStreamException ex) {
            Logger.getLogger(XmlWalker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
