/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.base.entity.Party;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.base.entity.PartyRole;
import net.ap57.tiger.base.entity.PartyRoleType;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <PR>
 * @param <P>
 */
public abstract class PartyRoleFacade<PR extends PartyRole, P extends Party> extends AbstractFacade<PR> {

  public PartyRoleFacade(Class<PR> entityClass) {
    super(entityClass);
  }

  protected abstract PartyRoleTypeFacade getRoleTypeFacade();

  protected abstract PartyFacade getPartyFacade();

  public abstract PR createTransient();

  protected abstract String getRoleTypeName();

  public PartyRoleType getRoleType() {
    return getRoleTypeFacade().findSingleByAttribute("name", getRoleTypeName());
  }

  @Override
  public void create(PR partyRole) throws EJBException {
    if (partyRole.getParty().getId() == null) {
      getEntityManager().persist(partyRole.getParty());
    }
    super.create(partyRole);
  }

  @Override
  public PR createTransient(Map<String, Object> params) {
    PR partyRole = createTransient();
    
    partyRole.setPartyRoleType(getRoleType());
    P party = associateToParty(partyRole, (P) getPartyFacade().createTransient(params));
    partyRole.setParty(party);
    party.getRoles().add(partyRole);
    partyRole.setRelationshipParticipations(new ArrayList<>());
    partyRole.setRelationshipsEstablished(new ArrayList<>());
    return partyRole;
  }

  @Override
  public void remove(PR partyRole) {
    Party party = partyRole.getParty();
    List<PartyRole> partyRoles = party.getRoles();
    if (partyRoles.size() == 1) {
      partyRoles.remove(partyRole);
      super.remove(partyRole);
      getEntityManager().remove(getEntityManager().merge(party));
    } else {
      partyRoles.remove(partyRole);
      super.remove(partyRole);
    }
  }

  public P associateToParty(PR role, P party) {
    if (party == null) {
      party = (P) getPartyFacade().createTransient(null);
      party.setRoles(new ArrayList<>());
    }
    role.setParty(party);
    party.getRoles().add(role);
    return party;
  }

  @Override
  public Object getId(PR entity) {
    return entity.getId();
  }
  
  @Override
  protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<PR> root) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();
    switch (filterName) {
      case "party.name":
        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
        predicates.add(cb.like(cb.upper(root.get("party").<String>get("name")), literal));
        break;
      default:
        break;
    }
    return predicates;
  }

  public void addContactMechanisms(Party party) {
    getPartyFacade().addContactMechanisms(party);
  }

  public void delContactMechanism(Party party, PartyContactMechanism partyContactMechanism) {
    getPartyFacade().delContactMechanism(party, partyContactMechanism);
  }

}
