/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.ejb.EJBException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractTreeFacade;
import net.ap57.tiger.base.api.HierarchicalEntity;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.base.entity.PartyRoleType;
import net.ap57.tiger.base.entity.Person;
import net.ap57.tiger.base.entity.PersonRole;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
public abstract class PersonRoleTreeFacade<PR extends PersonRole & HierarchicalEntity<PR>> extends AbstractTreeFacade<PR> {
  
public PersonRoleTreeFacade(Class<PR> entityClass) {
    super(entityClass);
  }

  public PartyRoleType getRoleType() {
    return getRoleTypeFacade().findSingleByAttribute("name", getRoleTypeName());
  }

  public Person associateToPerson(PR role, Person party) {
    if (party == null) {
      party = getPartyFacade().createTransient(null);
      party.setRoles(new ArrayList<>());
    }
    role.setParty(party);
    party.getRoles().add(role);
    return party;
  }

  @Override
  public PR createTransientBasic(Map<String, Object> params) {
    PR personRole = createTransient();
    personRole.setPartyRoleType(getRoleType());
    Person party = associateToPerson(personRole, getPartyFacade().createTransient(params));
    personRole.setParty(party);
    party.getRoles().add(personRole);
    personRole.setRelationshipParticipations(new ArrayList<>());
    personRole.setRelationshipsEstablished(new ArrayList<>());
    return personRole;
  }

  @Override
  public Object getId(PR entity) {
    return entity.getId();
  }
  
  @Override
  protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<PR> root) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();
    switch (filterName) {
      case "party.name":
        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
        predicates.add(cb.like(cb.upper(root.get("party").<String>get("name")), literal));
        break;
      default:
        break;
    }
    return predicates;
  }

  public void addContactMechanisms(Person party) {
    getPartyFacade().addContactMechanisms(party);
  }

  public void delContactMechanism(Person party, PartyContactMechanism partyContactMechanism) {
    getPartyFacade().delContactMechanism(party, partyContactMechanism);
  }

  public void getDescendants(PR ancestor, List<Long> result) {
    for (PR child : ancestor.<PR>getChildren()) {
      result.add(child.getParty().getId());
      getDescendants(child, result);
    }
  }

  @Override
  public void create(PR entity) throws EJBException {
    Long partyId = entity.getParty().getId();
    if (partyId == null) {
      entity.setFromDate(Calendar.getInstance().getTime());
      getPartyFacade().create(entity.getPerson());
    }

  }

  @Override
  public void remove(PR entity) {
    PR parent = (PR)entity.getParent();
    if (parent != null) {
      parent.getChildren().remove(entity);
    }
    getPartyFacade().remove(entity.getPerson());
  }

  @Override
  public void edit(PR entity) {
    getPartyFacade().edit(entity.getPerson());
  }

  protected abstract PartyRoleTypeFacade getRoleTypeFacade();

  protected abstract PersonFacade getPartyFacade();

  public abstract PR createTransient();

  protected abstract String getRoleTypeName();
    
}
