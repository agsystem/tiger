/*
 * Copyright 2014 Medinacom <hq.medinacom at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Medinacom <hq.medinacom at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Medinacom <hq.medinacom at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Medinacom <hq.medinacom at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Medinacom <hq.medinacom at gmail.com>

 */
package net.ap57.tiger.base.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.ap57.tiger.base.api.AbstractPickerFacade;
import net.ap57.tiger.base.entity.ContactMechanism;
import net.ap57.tiger.base.entity.Party;
import net.ap57.tiger.base.entity.PartyContactMechanism;
import net.ap57.tiger.base.entity.PartyPostalAddress;
import net.ap57.tiger.base.entity.PostalAddress;

/**
 *
 * @author Medinacom <hq.medinacom at gmail.com>
 * @param <T>
 */
public abstract class PartyFacade<T extends Party> extends AbstractPickerFacade<T> {

  public PartyFacade(Class<T> entityClass) {
    super(entityClass);
  }

  @Override
  protected List<Predicate> addPreFilter(String filterName, Object filterValue, Root<T> root) {
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();
    switch (filterName) {
      case "party.name":
        Expression<String> literal = cb.upper(cb.literal("%" + filterValue + "%"));
        predicates.add(cb.like(cb.upper(root.<String>get("name")), literal));
        break;
      default:
        break;
    }
    return predicates;
  }

  public void addPostalAddresses(T party) {

    List<PartyPostalAddress> partyPostalAddresses = party.getPostalAddresses();
    if (partyPostalAddresses == null) {
      partyPostalAddresses = new ArrayList<>();
      party.setPostalAddresses(partyPostalAddresses);
    }

    PartyPostalAddress partyPostalAddress = new PartyPostalAddress();
    partyPostalAddress.setParty(party);
    PostalAddress postalAddress = new PostalAddress();
    postalAddress.setAddress1("");
    partyPostalAddress.setPostalAddress(postalAddress);
    partyPostalAddresses.add(partyPostalAddress);
    
  }

  public void delPostalAddress(T party, PartyPostalAddress partyPostalAddress) {
    if (party.getPostalAddresses().size() > 1) {
      party.getPostalAddresses().remove(partyPostalAddress);
    }
  }

  public void addContactMechanisms(T party) {

    List<PartyContactMechanism> partyContactMechanisms = party.getContactMechanisms();
    if (partyContactMechanisms == null) {
      partyContactMechanisms = new ArrayList<>();
      party.setContactMechanisms(partyContactMechanisms);
    }

    PartyContactMechanism partyContactMechanism = new PartyContactMechanism();
    partyContactMechanism.setParty(party);
    partyContactMechanism.setContactMechanism(new ContactMechanism());
    partyContactMechanisms.add(partyContactMechanism);

  }

  public void delContactMechanism(T party, PartyContactMechanism partyContactMechanism) {
    if (party.getContactMechanisms().size() > 1) {
      party.getContactMechanisms().remove(partyContactMechanism);
    }
  }

  @Override
  public Object getId(T entity) {
    return entity.getId();
  }

}
