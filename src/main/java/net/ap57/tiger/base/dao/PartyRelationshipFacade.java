/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.dao;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ap57.tiger.base.api.AbstractFacade;
import net.ap57.tiger.base.entity.PartyRelationship;
import net.ap57.tiger.base.entity.PartyRelationshipStatusType;
import net.ap57.tiger.base.entity.PartyRelationshipType;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 * @param <PR>
 */
public abstract class PartyRelationshipFacade<PR extends PartyRelationship> extends AbstractFacade<PR> {

  public PartyRelationshipFacade(Class<PR> entityClass) {
    super(entityClass);
  }

  public PartyRelationshipType getRelationshipType() {
    return getRelationshipTypeFacade().findSingleByAttribute("name", getRelationshipTypeName());
  }

  @Override
  public PR createTransient(Map<String, Object> params) {
    PR rel = null;
    try {
      rel = entityClass.newInstance();
      rel.setRelationshipStatus(PartyRelationshipStatusType.ACTIVE);
      rel.setRelationshipType(getRelationshipType());
    } catch (InstantiationException | IllegalAccessException ex) {
      Logger.getLogger(PartyRelationshipFacade.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return rel;
  }

  @Override
  public Object getId(PR entity) {
    return entity.getId();
  }

  protected abstract PartyRelationshipTypeFacade getRelationshipTypeFacade();

  protected abstract String getRelationshipTypeName();

}
