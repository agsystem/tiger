/*
 * Copyright 2017 Arief Prihasanto <aphasan57 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <aphasan57 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <aphasan57 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <aphasan57 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <aphasan57 at gmail.com>

 */
package net.ap57.tiger.base.service;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import mdn.iotlib.comm.message.Message;
import mdn.iotlib.comm.message.MessageDecoder;
import mdn.iotlib.comm.message.MessageEncoder;
import net.ap57.tiger.base.api.AbstractSocketListener;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
@Singleton
@ServerEndpoint(
        value = "/socket/{module}/{service}/{cid}"
)
public class CommunicationSocket {
    
    private Map<String, AbstractSocketListener> listeners;
    private Map<String, Session> sessions;

    public Map<String, Session> getSessions() {
        if (sessions == null) {
            sessions = new ConcurrentHashMap<>();
        }
        return sessions;
    }

    public Map<String, AbstractSocketListener> getListeners() {
        if (listeners == null) {
            listeners = new ConcurrentHashMap<>();
        }
        return listeners;
    }
    
    public void registerListener(String key, AbstractSocketListener listener) {
        getListeners().put(key, listener);
    }

    @OnOpen
    public void onOpen(@PathParam("module") String module, @PathParam("service") String service, @PathParam("cid") String cid, Session session, EndpointConfig config) {
        session.setMaxIdleTimeout(0);
        getSessions().put(cid + "@" + service + "@" + module, session);
    }

    @OnMessage    
    @Asynchronous
    public void onMessage(Session session, ByteBuffer message) {
        Message msg = new Message(message);
        System.out.println("TesListener: " + msg.getInt());
//        routeMessage(service + "@" + module, message);
    }
    
    @Asynchronous
    public void routeMessage(String key, ByteBuffer message) {
        listeners.get(key).onMessage(new Message(message));
    }
    
}
