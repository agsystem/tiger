/*
 * Copyright 2015 Arief Prihasanto <ariefp5758 at gmail.com>.
 * All rights reserved

 * A lot of time, effort and money is spent designing and implementing the software.
 * All system design, text, graphics, the selection and arrangement thereof, and
 * all software compilations, underlying source code, software and all other material
 * on this software are copyright Arief Prihasanto <ariefp5758 at gmail.com> and any affiliates.
 * 
 * In simple terms, every element of this software is protected by copyright.
 * Unless you have our express written permission, you are not allowed
 * to copy partially and or completely, modify partially and or completely,
 * use partially and or completely and or reproduce any part of this  software
 * in any way, shape and or form.
 * 
 * Taking material from other source code and or document Arief Prihasanto <ariefp5758 at gmail.com> and affiliates has designed is
 * also prohibited. You can be prosecuted by the licensee as well as by us as licensor.
 * 
 * Any other use of materials of this software, including reproduction for purposes other
 * than that noted in the business agreement, modification, distribution, or republication,
 * without the prior written permission of Arief Prihasanto <ariefp5758 at gmail.com> is strictly prohibited.
 * 
 * The source code, partially and or completely, shall not be presented and or shown
 * and or performed to public and or other parties without the prior written permission
 * of Arief Prihasanto <ariefp5758 at gmail.com>

 */
package net.ap57.tiger.base.service;

import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import net.ap57.tiger.base.dao.ScriptFacade;
import net.ap57.tiger.base.entity.Script;

/**
 *
 * @author Arief Prihasanto <ariefp5758 at gmail.com>
 */
@Stateless
public class ScriptService {

  @Inject
  private ScriptEngineManager scriptEngineManager;

  @Inject
  private ScriptFacade scriptFacade;

  private ScriptEngine getGroovyEngine() {
    return scriptEngineManager.getEngineByName("Groovy");
  }

  private ScriptEngine getJavaScriptEngine() {
    return scriptEngineManager.getEngineByName("JavaScript");
  }

  public String findScript(String name) {
    Script script = scriptFacade.find(name);
    if (script != null) {
      return script.getContent();
    } else {
      return "";
    }
  }

  public void executeScript(String scriptName, Map<String, Object> params, String engineName) throws EJBException {
    try {
      ScriptEngine engine = null;
      switch (engineName) {
        case "JavaScript":
          engine = getJavaScriptEngine();
          break;
        default:
          engine = getGroovyEngine();
          break;
      }
      for(String key : params.keySet()) {
        engine.put(key, params.get(key));
      }
      engine.eval(findScript(scriptName));
    } catch (ScriptException ex) {
      throw new EJBException(ex);
    }
  }
  
  public void updateScript(Script script) {
    scriptFacade.edit(script);
  }

}
